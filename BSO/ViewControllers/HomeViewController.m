//
//  HomeViewController.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "HomeViewController.h"
#import "SettingsViewController.h"
#import "MenuViewController.h"
#import "CalendarViewController.h"
#import "ShowDetailViewController.h"
#import "PeformancesListViewController.h"
#import "PlanYourVisitViewController.h"
#import "MediaListViewController.h"
#import "WhileYouAreHereViewController.h"
#import "MoreViewController.h"
#import "SocialViewController.h"
#import "UserLoginViewController.h"
#import "SeatSelectionViewController.h"
#import "ShpingCartViewController.h"
#import "SubscribeViewController.h"
#import "WizardViewController.h"

#define TOPVALUE 88

@interface HomeViewController () <MenuViewControllerDelegate,CalendarViewControllerDelegate,PerformanceViewControllerDelegate,MoreViewControllerDelegate,UserLoginViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;

@property (nonatomic, strong) IBOutlet BSOHeaderButton *btnMenu;
@property (nonatomic, strong) IBOutlet BSOHeaderButton *btnCart;
@property (nonatomic, strong) IBOutlet BSOHeaderButton *btnTag;
@property (nonatomic, strong) IBOutlet UIImageView *imgBG;

@property (nonatomic, strong) MenuViewController *menuViewController;
@property (nonatomic, strong) CalendarViewController *calendarViewController;
@property (nonatomic, strong) PeformancesListViewController *peformancesListViewController;
@property (nonatomic, strong) PlanYourVisitViewController *planYourVisitViewController;
@property (nonatomic, strong) MediaListViewController *mediaListViewController;
@property (nonatomic, strong) ShowDetailViewController *showDetailViewController;
@property (nonatomic, strong) WhileYouAreHereViewController *whileYouAreHereViewController;
@property (nonatomic, strong) SocialViewController *socialViewController;
@property (nonatomic, strong) MoreViewController *moreViewController;
@property (nonatomic, strong) SubscribeViewController *subscribeViewController;
@property (nonatomic, assign) NSInteger selectedType;
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, strong) IBOutlet UIView *menuBtnBG;
@property (nonatomic, strong) Event *selectedEvent;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.bsoManager = [BSOManager SharedInstance];
    
    self.menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    self.menuViewController.delegate = self;
    CGRect frame = self.menuViewController.view.frame;
    
//    frame.origin.x = -frame.size.width;
    frame.origin.y = TOPVALUE;
    frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
    
    [self addChildViewController:self.menuViewController];
    [self.view addSubview:self.menuViewController.view];
    self.menuViewController.view.frame = frame;
    [self.menuViewController didMoveToParentViewController:self];
    self.menuViewController.view.alpha = 0;
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openMenuScreen:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.menuBtnBG addGestureRecognizer:gesture];
    
    gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeMenuScreen:)];
    gesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.menuBtnBG addGestureRecognizer:gesture];
    
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 90)];
    titleView.backgroundColor = [UIColor clearColor];
    self.btnLogo = [BSOButton buttonWithType:UIButtonTypeCustom];
    self.btnLogo.frame = CGRectMake(0, 0, 59, 58);
    [self.btnLogo setImage:[UIImage imageNamed:@"logo_0"] forState:UIControlStateNormal];
    [self.btnLogo addTarget:self action:@selector(btnLogoPressed) forControlEvents:UIControlEventTouchUpInside];
    [titleView addSubview:self.btnLogo];
    
    self.navigationItem.titleView = titleView;
    
    self.btnMenu = [BSOHeaderButton buttonWithType:UIButtonTypeCustom];
    self.btnMenu.frame = CGRectMake(0, -20, 27, 20);
    [self.btnMenu setImage:[UIImage imageNamed:@"btnMenu"] forState:UIControlStateNormal];
    [self.btnMenu addTarget:self action:@selector(btnMenuPressed) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnCart = [BSOHeaderButton buttonWithType:UIButtonTypeCustom];
    self.btnCart.frame = CGRectMake(0, -20, 27, 26);
    [self.btnCart setImage:[UIImage imageNamed:@"btnCart"] forState:UIControlStateNormal];
    [self.btnCart addTarget:self action:@selector(btnCartPressed) forControlEvents:UIControlEventTouchUpInside];

    self.btnTag = [BSOHeaderButton buttonWithType:UIButtonTypeCustom];
    self.btnTag.frame = CGRectMake(0, -10, 30, 30);
    [self.btnTag setTitle:@"#" forState:UIControlStateNormal];
    [self.btnTag setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnTag setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
    
    self.btnTag.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:26.0];
    [self.btnTag addTarget:self action:@selector(btnHashTagPressed) forControlEvents:UIControlEventTouchUpInside];
    self.btnTag.hidden = TRUE;
    
    self.navigationItem.leftBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:self.btnMenu];
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:self.btnCart],[[UIBarButtonItem alloc] initWithCustomView:self.btnTag]];
    
    if(GET_UD(FIRST_TIME) == nil) {
        [self performSelector:@selector(openWizardViewController) withObject:nil afterDelay:1.0];
    }
    
    [self.bsoManager retriveCrediCars];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self changeBackgroundImage];
}

#pragma mark -
#pragma mark Button Actions 

- (void)openMenuScreen:(UISwipeGestureRecognizer *)sender {
    if(sender.state == UIGestureRecognizerStateEnded) {
        if(self.btnMenu.selected) {
            return;
        }
        [self btnMenuPressed];
    }
}

- (void)closeMenuScreen:(UISwipeGestureRecognizer *)sender {
    if(sender.state == UIGestureRecognizerStateEnded) {
        if(self.btnMenu.selected == FALSE) {
            return;
        }
        [self btnMenuPressed];
    }
}

- (IBAction)btnMenuPressed {
    self.btnMenu.selected = !self.btnMenu.selected;
    CGRect frame  = self.menuViewController.menuView.frame;
    float alpha ;
    
    if(self.btnMenu.selected) {
        alpha = 0.5;
        self.menuViewController.view.alpha = 1.0;
        [self.view bringSubviewToFront:self.menuViewController.view];
        if(self.calendarViewController) {
                [self.view bringSubviewToFront:self.calendarViewController.view];
        }
//        if(self.peformancesListViewController) {
//            [self.view bringSubviewToFront:self.peformancesListViewController.view];
//        }
        if(self.moreViewController) {
            [self.view bringSubviewToFront:self.moreViewController.view];
        }
        
        frame.origin.x = 0;
    } else {
        alpha = 0.0;
         frame.origin.x = -frame.size.width;
    }
    [UIView animateWithDuration:.5 animations:^{
        self.menuViewController.bgView.alpha = alpha;
        self.menuViewController.menuView.frame = frame;
    } completion:^(BOOL finished) {
        if(self.btnMenu.selected == FALSE) {
            self.menuViewController.view.alpha = 0;
        }
    }];
}

- (IBAction)btnCartPressed {
    self.selectedEvent = nil;
    if(GET_UD(SESSION_ID)) {
        ShpingCartViewController *viewController = [[ShpingCartViewController alloc] initWithNibName:@"ShpingCartViewController" bundle:nil];
        [self.navigationController pushViewController:viewController animated:YES];

    } else {
        UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)btnHashTagPressed {
    if(self.socialViewController != nil) {
        self.btnTag.selected = !self.btnTag.selected;
        if(self.btnTag.selected)
            [self.socialViewController getHashTagsPost];
        else
            [self.socialViewController getSocialPost];
    }
}

- (IBAction)btnFilterPressed {
    if(self.subscribeViewController != nil) {
        [self.subscribeViewController openFilterView];
    }
}

- (IBAction)btnLogoPressed {
    if(self.mediaListViewController != nil)
        [self mediaListBackButtonPressed];
    if(self.whileYouAreHereViewController != nil)
       [self whileYouAreHereBackButtonPressed];
    if(self.planYourVisitViewController != nil)
        [self planYourVisitBackButtonPressed];
    if(self.peformancesListViewController != nil)
        [self performanceViewBackButtonPressed];
    if(self.calendarViewController != nil)
        [self CalendarViewBakButtonPressed];
    if(self.socialViewController != nil)
       [self socialBackButtonPressed];
    if(self.subscribeViewController != nil)
        [self subscribeBackButtonPressed];
    
    if(self.moreViewController != nil)
        [self moreBackButtonPressed];

    [self removeShowDetailsViewcontroller];
}

#pragma mark - MenuView 

- (void)MenuViewOrginizationButtonPressed:(NSInteger)organization {
    self.bsoManager.selectedType = (OrganizationType) organization - 1;
    NSString *strType = [NSString stringWithFormat:@"%u",self.bsoManager.selectedType];
    SET_UD(SELECTED_TYPE, strType);
    [self changeBackgroundImage];

    if(self.selectedType == 0) {
        [self.calendarViewController getCalendarEvents];
    }
    if(self.peformancesListViewController) {
        [self.peformancesListViewController getPefrormancesList];
    }
    if(self.mediaListViewController != nil) {
        [self.mediaListViewController getMediaAllMedia];
    }
    if(self.planYourVisitViewController != nil) {
        [self.planYourVisitViewController getPlanVisitDetails:self.bsoManager.selectedType];
    }
    if(self.whileYouAreHereViewController != nil) {
       
    }
    if(self.socialViewController != nil) {
        if(self.btnTag.selected)
            [self.socialViewController getHashTagsPost];
        else
            [self.socialViewController getSocialPost];
    }
    if(self.subscribeViewController) {
        [self.subscribeViewController getSubscribeData];
    }
    if(self.moreViewController != nil) {
        [self.moreViewController refreshContentForMoreViewScreen];
    }
}

- (void)changeBackgroundImage {
    NSString *fileName = [NSString stringWithFormat:@"bg_%u.png",self.bsoManager.selectedType];
    NSData *imgData = [NSData dataWithContentsOfFile:FILE_PATH(fileName)];
    
    if(imgData) {
        self.imgBG.image = [UIImage imageWithData:imgData];
    } else {
        if(self.bsoManager.selectedType == jBPop) {
            self.imgBG.image = [UIImage imageNamed:@"POPS_BG"];
        }
        else if(self.bsoManager.selectedType == jBSO) {
            self.imgBG.image = [UIImage imageNamed:@"BSO_BG"];
        } else {
            self.imgBG.image = [UIImage imageNamed:@"TANGLEWOOD_BG"];
        }

    }
     [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
}

- (void)MenuViewItemPressed:(NSInteger)index {
   if(self.selectedType != index) {
//       if(self.selectedType == 0 && self.calendarViewController != nil) {
//         [self calendarPressed];
//       }
        if(self.selectedType == 1 && self.peformancesListViewController != nil) {
           [self performanceViewBackButtonPressed];
        }
        if(self.selectedType == 2 && self.planYourVisitViewController != nil) {
            [self btnPlanYourVisitPressed];
        }
        if(self.selectedType == 3 && self.mediaListViewController != nil) {
            [self mediaListButtonPressed];
        }
        if(self.selectedType == 4 && self.socialViewController != nil) {
            [self socialButtonPressed];
        }
        if(self.selectedType == 5 && self.whileYouAreHereViewController != nil) {
            [self whileYouAreHereButtonPressed];
        }
        if(self.selectedType == 6 && self.moreViewController != nil) {
            [self moreButtonPressed];
        }
        if(self.selectedType == 7 && self.subscribeViewController != nil) {
            [self subscirbeButtonPressed];
        }
       
     }
    
     self.selectedType = index;
    if(index == 0) {
        [self btnLogoPressed];
    } else if(index == 1) {
        [self peformancePressed];
    }  else if(index == 2) {
        [self btnPlanYourVisitPressed];
    } else if(index == 3) {
        [self mediaListButtonPressed];
    } else if(index == 4) {
        [self socialButtonPressed];
    }
    else if(index == 5) {
        [self whileYouAreHereButtonPressed];
    }
    else if(index == 6) {
        [self moreButtonPressed];
    }
    else if(index == 7) {
        [self subscirbeButtonPressed];
    }
    else if(index == 8) {
        [self settingsPressed];
    }
    if(index != 6   && self.showDetailViewController != nil) {
        [self.showDetailViewController stopMusicPlayer];
      //  [self removeShowDetailsViewcontroller];
    }
    
    [self performSelector:@selector(checkHashTagButton) withObject:nil afterDelay:.6];

}

- (void)checkHashTagButton {
    if(self.childViewControllers.count) {
        UIViewController *controller = self.childViewControllers.lastObject;
        if([controller isKindOfClass:[SocialViewController class]]) {
            self.btnTag.hidden = FALSE;
        } else {
            self.btnTag.hidden = TRUE;
        }
        if([controller isKindOfClass:[SubscribeViewController class]]) {
           // self.btnFilter.hidden = FALSE;
        } else {
            self.btnFilter.hidden = TRUE;
        }
    }
}

- (void)MenuViewBackPressed {
    [self btnMenuPressed];
}

- (void)calendarPressed {
    if(self.calendarViewController) {
        [self CalendarViewBakButtonPressed];
    } else {
        self.calendarViewController = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
        self.calendarViewController.delegate = self;
        CGRect frame = self.calendarViewController.view.frame;
        
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.calendarViewController];
        [self.view addSubview:self.calendarViewController.view];
        self.calendarViewController.view.frame = frame;
        [self.calendarViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.calendarViewController.view.frame = frame;
        }];
    }
}

- (void)peformancePressed {
    if(self.peformancesListViewController.view.frame.origin.x == 0 && self.peformancesListViewController) {
        [self performanceViewBackButtonPressed];
    } else {
        if(self.peformancesListViewController == nil)
            self.peformancesListViewController = [[PeformancesListViewController alloc]     initWithNibName:@"PeformancesListViewController" bundle:nil];
        
        CGRect frame = self.peformancesListViewController.view.frame;
        self.peformancesListViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.peformancesListViewController];
        [self.view addSubview:self.peformancesListViewController.view];
        self.peformancesListViewController.view.frame = frame;
        [self.peformancesListViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.peformancesListViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [self btnMenuPressed];
        }];
    }
}

- (void)settingsPressed {
    SettingsViewController *viewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)CalendarViewBakButtonPressed {
    CGRect frame = self.calendarViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.calendarViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.calendarViewController willMoveToParentViewController:nil];
        [self.calendarViewController.view removeFromSuperview];
        [self.calendarViewController removeFromParentViewController];
        
        self.calendarViewController = nil;
    }];
}

- (void)CalendarViewInfoButtonPressed:(Event *)aEvent {
    Show *aShow = [[Show alloc] init];
    aShow.ID = aEvent.showID;
    [self performanceViewDetailButtonPressed:aShow];
}

- (void)CalendarViewTicketButtonPressed:(Event *)aEvent {
    self.selectedEvent = aEvent;
    if(GET_UD(SESSION_ID)) {
        [self callBestAvailabeSeatWS];
        
    } else {
        UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }

}

- (void)callBestAvailabeSeatWS {
    [Utility showLoader];
    [self.bsoManager getBestAvailableSeats:self.selectedEvent.pefrormanceID success:^(id responseObject) {
        if([responseObject isKindOfClass:[AvailalbeSeatResponse class]]) {
            SeatSelectionViewController *viewController = [[SeatSelectionViewController alloc] initWithNibName:@"SeatSelectionViewController" bundle:nil];
            viewController.aAvailalbeSeat = responseObject;
            [self.navigationController pushViewController:viewController animated:YES];
        } else {
            [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
            
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)userLoginSuccessfully:(BOOL)success {
    if(success) {
        if(self.selectedEvent) { // Clicked Buy Button
            [self callBestAvailabeSeatWS];
        } else { // Clicked Cart button
            ShpingCartViewController *viewController = [[ShpingCartViewController alloc] initWithNibName:@"ShpingCartViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];

        }
    }
}


#pragma mark - Peformance Delegate

- (void)performanceViewBackButtonPressed {
    CGRect frame = self.peformancesListViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.peformancesListViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.peformancesListViewController willMoveToParentViewController:nil];
        [self.peformancesListViewController.view removeFromSuperview];
        [self.peformancesListViewController removeFromParentViewController];
        
      //  self.peformancesListViewController = nil;
    }];
}

- (void)performanceViewDetailButtonPressed:(Show *)aShow {
    [self removeShowDetailsViewcontroller];
    self.showDetailViewController = [[ShowDetailViewController alloc] initWithNibName:@"ShowDetailViewController" bundle:nil];
    self.showDetailViewController.aShow = aShow;
    CGRect frame = self.showDetailViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    frame.size.width = self.view.bounds.size.width;
    frame.origin.y = TOPVALUE;
    frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
    
    [self addChildViewController:self.showDetailViewController];
    [self.view addSubview:self.showDetailViewController.view];
    self.showDetailViewController.view.frame = frame;
    [self.showDetailViewController didMoveToParentViewController:self];
    
    frame.origin.x =   self.view.bounds.size.width - frame.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.showDetailViewController.view.frame = frame;
        
    } completion:^(BOOL finished) {

        if(self.calendarViewController)
            [self CalendarViewBakButtonPressed];
        
        if(self.moreViewController)
            [self moreBackButtonPressed];
        if(self.peformancesListViewController)
            [self performanceViewBackButtonPressed];
        
        self.menuViewController.selectedIndex = -1;
        
        [self.menuViewController.table reloadData];
    }];
    
}

- (void)performanceViewTicketButtonPressed:(Show *)aShow {
    [Utility openURLInInterBrowser:aShow.ticketURL from:self];
}

- (void)removeShowDetailsViewcontroller {
    if(self.showDetailViewController) {
        [self.showDetailViewController willMoveToParentViewController:nil];
        [self.showDetailViewController.view removeFromSuperview];
        [self.showDetailViewController removeFromParentViewController];
        
        self.showDetailViewController = nil;
    }
}

- (void)btnPlanYourVisitPressed {
    if(self.planYourVisitViewController) {
        [self planYourVisitBackButtonPressed];
    } else {
        self.planYourVisitViewController = [[PlanYourVisitViewController alloc] initWithNibName:@"PlanYourVisitViewController" bundle:nil];
        CGRect frame = self.planYourVisitViewController.view.frame;
        //self.planYourVisitViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.size.width = self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.planYourVisitViewController];
        [self.view addSubview:self.planYourVisitViewController.view];
        self.planYourVisitViewController.view.frame = frame;
        [self.planYourVisitViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.planYourVisitViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
           [self btnMenuPressed];
        }];
    }
}

- (void)planYourVisitBackButtonPressed {
    CGRect frame = self.planYourVisitViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.planYourVisitViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.planYourVisitViewController willMoveToParentViewController:nil];
        [self.planYourVisitViewController.view removeFromSuperview];
        [self.planYourVisitViewController removeFromParentViewController];
        
        self.planYourVisitViewController = nil;
    }];
}

- (void)mediaListButtonPressed {
    if(self.mediaListViewController) {
        [self mediaListBackButtonPressed];
    } else {
        self.mediaListViewController = [[MediaListViewController alloc] initWithNibName:@"MediaListViewController" bundle:nil];
        CGRect frame = self.mediaListViewController.view.frame;
        //self.planYourVisitViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.mediaListViewController];
        [self.view addSubview:self.mediaListViewController.view];
        self.mediaListViewController.view.frame = frame;
        [self.mediaListViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.mediaListViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
            [self btnMenuPressed];
        }];
    }
}

- (void)mediaListBackButtonPressed {
    CGRect frame = self.mediaListViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.mediaListViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.mediaListViewController willMoveToParentViewController:nil];
        [self.mediaListViewController.view removeFromSuperview];
        [self.mediaListViewController removeFromParentViewController];
        
        self.mediaListViewController = nil;
    }];
}

- (void)whileYouAreHereButtonPressed {
    if(self.whileYouAreHereViewController) {
        [self whileYouAreHereBackButtonPressed];
    } else {
        self.whileYouAreHereViewController = [[WhileYouAreHereViewController alloc] initWithNibName:@"WhileYouAreHereViewController" bundle:nil];
        CGRect frame = self.whileYouAreHereViewController.view.frame;
        //self.whileYouAreHereViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.whileYouAreHereViewController];
        [self.view addSubview:self.whileYouAreHereViewController.view];
        self.whileYouAreHereViewController.view.frame = frame;
        [self.whileYouAreHereViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.whileYouAreHereViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
            [self btnMenuPressed];
        }];
    }
}

- (void)whileYouAreHereBackButtonPressed {
    CGRect frame = self.whileYouAreHereViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.whileYouAreHereViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.whileYouAreHereViewController willMoveToParentViewController:nil];
        [self.whileYouAreHereViewController.view removeFromSuperview];
        [self.whileYouAreHereViewController removeFromParentViewController];
        
        self.whileYouAreHereViewController = nil;
    }];
}

- (void)socialButtonPressed {
    if(self.socialViewController) {
        [self socialBackButtonPressed];
    } else {
        self.btnTag.hidden = FALSE;
        self.btnTag.selected = FALSE;
        
        self.socialViewController = [[SocialViewController alloc] initWithNibName:@"SocialViewController" bundle:nil];
        CGRect frame = self.socialViewController.view.frame;
        //self.whileYouAreHereViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.socialViewController];
        [self.view addSubview:self.socialViewController.view];
        self.socialViewController.view.frame = frame;
        [self.socialViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.socialViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
            [self btnMenuPressed];
        }];
    }
}

- (void)socialBackButtonPressed {
    self.btnTag.hidden = TRUE;
    CGRect frame = self.socialViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.socialViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.socialViewController willMoveToParentViewController:nil];
        [self.socialViewController.view removeFromSuperview];
        [self.socialViewController removeFromParentViewController];
        
        self.socialViewController = nil;
    }];
}

- (void)moreButtonPressed {
    if(self.moreViewController) {
        [self moreBackButtonPressed];
    } else {
        self.moreViewController = [[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
        CGRect frame = self.moreViewController.view.frame;
        self.moreViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.moreViewController];
        [self.view addSubview:self.moreViewController.view];
        self.moreViewController.view.frame = frame;
        [self.moreViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.moreViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void)moreBackButtonPressed {
    CGRect frame = self.moreViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.moreViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.moreViewController willMoveToParentViewController:nil];
        [self.moreViewController.view removeFromSuperview];
        [self.moreViewController removeFromParentViewController];
        
        self.moreViewController = nil;
    }];
    
}
- (void)MoreViewBakButtonPressed {
    [self moreBackButtonPressed];
}

- (void)subscirbeButtonPressed {
    if(self.subscribeViewController) {
        [self subscribeBackButtonPressed];
    } else {
       // self.btnFilter.hidden = FALSE;

       self.subscribeViewController = [[SubscribeViewController alloc] initWithNibName:@"SubscribeViewController" bundle:nil];
        CGRect frame = self.subscribeViewController.view.frame;
        //self.whileYouAreHereViewController.delegate = self;
        frame.origin.x =  self.view.bounds.size.width;
        frame.origin.y = TOPVALUE;
        frame.size.width = self.view.bounds.size.width;
        frame.size.height  = self.view.bounds.size.height -  frame.origin.y;
        
        [self addChildViewController:self.subscribeViewController];
        [self.view addSubview:self.subscribeViewController.view];
        self.subscribeViewController.view.frame = frame;
        [self.subscribeViewController didMoveToParentViewController:self];
        
        frame.origin.x =   self.view.bounds.size.width - frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            self.subscribeViewController.view.frame = frame;
            
        } completion:^(BOOL finished) {
            [self btnMenuPressed];
        }];
    }
}

- (void)subscribeBackButtonPressed {
    self.btnFilter.hidden = TRUE;

    CGRect frame = self.subscribeViewController.view.frame;
    frame.origin.x =  self.view.bounds.size.width;
    [UIView animateWithDuration:.5 animations:^{
        self.socialViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.subscribeViewController willMoveToParentViewController:nil];
        [self.subscribeViewController.view removeFromSuperview];
        [self.subscribeViewController removeFromParentViewController];
        
        self.subscribeViewController = nil;
    }];
}

#pragma mark - Wizard 

- (void)openWizardViewController {
    SET_UD(FIRST_TIME, @"1");
    WizardViewController *wizardViewController = [[WizardViewController alloc] initWithNibName:@"WizardViewController" bundle:nil];
    wizardViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:wizardViewController];
    navController.preferredContentSize = CGSizeMake(470.0, 350.0);
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [navController.navigationBar setTintColor:[UIColor whiteColor]];
    
    [self presentViewController:navController animated:YES completion:nil];
    
    navController.view.superview.frame = CGRectMake(0, 0, 470.0, 350.0);
    navController.view.superview.center = self.view.center;
}



@end