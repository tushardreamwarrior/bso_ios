//
//  MyPlaylistActionViewController.m
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MyPlaylistActionViewController.h"

@interface MyPlaylistActionViewController ()
@property (nonatomic, strong) IBOutlet UITableView *table;
@end

@implementation MyPlaylistActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"anyCell"];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:20.0];
        cell.textLabel.textColor = RGB(137, 137, 137);
        
        cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans" size:18];
        cell.detailTextLabel.textColor = RGB(137, 137, 137);
        
        UIButton *btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPlay setImage:[UIImage imageNamed:@"myPlaylist_Delete"] forState:UIControlStateNormal];
        btnPlay.frame = CGRectMake(0, 0, 20, 20);
        [btnPlay addTarget:self action:@selector(btnDeletePressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = btnPlay;
    }
    cell.textLabel.text = @"Arstist Name";
    cell.detailTextLabel.text = @"Track title and more information";
    UIButton *btnPlay = (UIButton *)cell.accessoryView;
    btnPlay.tag = indexPath.row;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, -15, 0, 15)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 20, 0, 30)];
    }
}

- (void)btnDeletePressed:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(MyPlaylistDeleteTrackPressed)]) {
        [self.delegate MyPlaylistDeleteTrackPressed];
    }
}

- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShufflePressed {
    if([self.delegate respondsToSelector:@selector(MyPlaylistShufflePressed)]) {
        [self.delegate MyPlaylistShufflePressed];
    }
}

- (IBAction)btnDeletePlayListPressed {
    if([self.delegate respondsToSelector:@selector(MyPlaylistDeletePressed)]) {
        [self.delegate MyPlaylistDeletePressed];
    }
}

- (IBAction)btnPlayPressed {
    if([self.delegate respondsToSelector:@selector(MyPlaylistPlayPressed)]) {
        [self.delegate MyPlaylistPlayPressed];
    }
}

- (CGSize)preferredContentSize {
    return CGSizeMake(465, 568);
}
@end
