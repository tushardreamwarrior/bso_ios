//
//  SubscribeViewController.m
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SubscribeViewController.h"
#import "HomeViewController.h"
#import "CollectionHeader.h"
#import "CollectionCell.h"
#import "SeatSelectionCYOViewController.h"
#import "UserLoginViewController.h"
#import "SeatSelectionViewController.h"

@interface SubscribeViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,UserLoginViewDelegate>

@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet UICollectionView *collection;
@property (nonatomic, strong) NSMutableArray *arrSubscribes;
@property (nonatomic, strong) NSMutableArray *arrAllSubscribes;

@property (nonatomic, strong) UIPopoverController *popOverController;
@property (nonatomic, strong) IBOutlet UIView *filterView;
@property (nonatomic, strong) IBOutlet BSOTextField *txtSearch;
@property (nonatomic, strong) IBOutlet UILabel *lblHeader;
@property (nonatomic, strong) IBOutlet UILabel *lblNoData;

@property (nonatomic, strong) IBOutlet BSOButton *btnChooseOwn;
@property (nonatomic, strong) IBOutlet BSOButton *btnFixed;
@property (nonatomic, strong) IBOutlet BSOButton *btnFlex;
@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;
@property (nonatomic, strong) IBOutlet UIImageView *imgArrow;
@property (nonatomic, strong) IBOutlet UIImageView *imgScrolArrow;
@property (nonatomic, strong)  BSOButton *btnSelected;
@property (nonatomic, strong)  BSOButton *btnSelectedDay;
@property (nonatomic, strong)  NSMutableArray *arrDaysWeek;
@property (nonatomic, strong)  NSMutableArray *arrAllDaysWeek;
@property (nonatomic, strong)  NSMutableArray *arrIds;

@property (nonatomic, strong) Subscription *aSubscription;
@end

@implementation SubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self.collection registerClass:[CollectionHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    [self.collection registerClass:[CollectionCell class] forCellWithReuseIdentifier:@"CollectionCell"];
    self.collection.backgroundColor = [UIColor clearColor];
    
    self.arrSubscribes = [NSMutableArray new];
    self.arrDaysWeek = [NSMutableArray new];
    [self getSubscribeData];
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    
    [Utility setBorderColorWithView:self.txtSearch color:[UIColor whiteColor]];
    [Utility setLeftViewWithTextField:self.txtSearch];
    
    [self.txtSearch addTarget:self action:@selector(searchTextValueChaged) forControlEvents:UIControlEventEditingChanged];
    self.txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtSearch.enablesReturnKeyAutomatically = YES;
    
    UIViewController *controller = [[UIViewController alloc] init];
    controller.view = self.filterView;
    _popOverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    _popOverController.popoverContentSize = self.filterView.frame.size;
    _popOverController.delegate = self;
    if ([self.popOverController respondsToSelector:@selector(setBackgroundColor:)]) {
        [self.popOverController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]]];
    }
    
//    self.filterView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
//    [Utility setCornerToView:self.filterView corner:12];

        self.btnSelected = self.btnFixed;
        self.filterView.hidden = TRUE;
}

- (void)getSubscribeData {
    self.lblHeader.hidden = self.lblNoData.hidden = YES;
    [Utility showLoader];
    [self.bsoManager getSubscriptionListing:self.bsoManager.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSMutableArray class]]) {
            self.arrAllSubscribes  = [[NSMutableArray alloc] initWithArray:responseObject];

            for(Subscription *aSubscription in self.arrAllSubscribes) {
                if(aSubscription.subCode == 3) {
                    aSubscription.isOpened = YES;
                }
            }
            
            NSArray *arrDays = @[@"MON",@"TUE",@"WEN",@"THU",@"FRI",@"SAT",@"SUN"];
            self.arrAllDaysWeek = [[NSMutableArray alloc] init];
            
            for(NSString *day in arrDays) { // Make Date of Week Daynamic
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY SELF.arrPerformance.weekDay == %@",day];
               NSArray *filtered = [self.arrAllSubscribes filteredArrayUsingPredicate:predicate];
                if(filtered.count) {
                    [self.arrAllDaysWeek addObject:day];
                }
            }
        
            if(self.arrAllSubscribes.count == 0) {
                self.lblNoData.hidden = NO;
                self.filterView.hidden = TRUE;
                self.collection.hidden = TRUE;
            }
            else {
                 self.collection.hidden = FALSE;
                self.lblHeader.hidden = NO;
               self.filterView.hidden = FALSE;
               [self createFilterDayButtons];
               [self btnDayOfWeekPressed:self.btnSelectedDay];
                
                if(self.btnSelected.tag == 3) { // CYO
                    self.filterView.hidden = TRUE;
                    CGRect frame = self.lblHeader.frame;
                    frame.origin.y = 110;
                    self.lblHeader.frame = frame;
                    
                    frame = self.collection.frame;
                    frame.origin.y = 135;
                    frame.size.height = self.view.frame.size.height - frame.origin.y;
                    self.collection.frame = frame;
                    
                    
                } else {
                    self.filterView.hidden = FALSE;
                
                    CGRect frame = self.lblHeader.frame;
                    frame.origin.y = 164;
                    self.lblHeader.frame = frame;
                    
                    frame = self.collection.frame;
                    frame.origin.y = 190;
                    frame.size.height = self.view.frame.size.height - frame.origin.y;
                    self.collection.frame = frame;
                }
               
           }
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)openFilterView {
    HomeViewController *parentController = (HomeViewController *)self.parentViewController;
    CGRect frame = parentController.btnFilter.frame;
    [self.popOverController presentPopoverFromRect:frame inView:self.parentViewController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

#pragma mark - CollectionView Header

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.arrSubscribes.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    Subscription *aSubscription = [self.arrSubscribes objectAtIndex:section];
    float width = ([UIScreen mainScreen].bounds.size.width / 768) * 420.0;
    float height = [Utility getTextHeightOfText:aSubscription.strPeformanceDescription font:[UIFont fontWithName:@"OpenSans" size:14] width:width];
    height = MAX(height, 40);
    if(aSubscription.isOpened) {
        return CGSizeMake(DEVICE_WIDTH,height + 160);
    }
    else {
        return CGSizeMake(DEVICE_WIDTH,height + 10);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        Subscription *aSubscription = [self.arrSubscribes objectAtIndex:indexPath.section];
       CollectionHeader  *headerView = [self.collection dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        headerView.btnClick.tag = indexPath.section;
        headerView.btnSeats.tag = indexPath.section;
        headerView.lblTitle.text = aSubscription.title;
      //  headerView.lblSubType.text = aSubscription.subType;
        headerView.lblDesc.text = aSubscription.strDescription;
        headerView.lblPeromanceDesc.text = aSubscription.strPeformanceDescription;
        headerView.lblPrice.text = aSubscription.priceRange;
        headerView.btnSeats.hidden = !aSubscription.isOnSale;
        
        float width = ([UIScreen mainScreen].bounds.size.width / 768) * 420.0;
        float height = [Utility getTextHeightOfText:aSubscription.strPeformanceDescription font:[UIFont fontWithName:@"OpenSans" size:14] width:width];
        height = MAX(height, 40);
        
        CGRect frame = headerView.lblPeromanceDesc.frame;
        frame.size.height = height;
        headerView.lblPeromanceDesc.frame = frame;
        
        frame = headerView.lblLine.frame;
        frame.origin.y = height + 9;
        headerView.lblLine.frame = frame;
        
        frame = headerView.btnClick.frame;
        frame.size.height = height + 10;
        headerView.btnClick.frame = frame;
        
        if(aSubscription.isOpened) {
            headerView.btnClick.backgroundColor = RGB(35,35,35);
            headerView.lblTitle.textColor = BSO_COLOR;
            headerView.lblPeromanceDesc.textColor = BSO_COLOR;
            headerView.innerView.hidden = FALSE;
            
            frame = headerView.innerView.frame;
            frame.origin.y = height + 10;
            headerView.innerView.frame = frame;

        } else {
            headerView.btnClick.backgroundColor = [UIColor clearColor];
            headerView.lblTitle.textColor = [UIColor whiteColor];
            headerView.lblPeromanceDesc.textColor = [UIColor whiteColor];
            headerView.innerView.hidden = TRUE;
        }
        
        if(aSubscription.subCode == 3) {
            headerView.lblPeformanceRange.text = [NSString stringWithFormat:@"Please check between %d and %d performances",aSubscription.minPerformance,aSubscription.maxPerformance];
        } else {
            headerView.lblPeformanceRange.text = @"";
        }
        [headerView.btnClick addTarget:self action:@selector(btnOpenCloseHeaderView:) forControlEvents:UIControlEventTouchUpInside];
        [headerView.btnSeats addTarget:self action:@selector(btnBestAvailableSeatsPressed:) forControlEvents:UIControlEventTouchUpInside];
        reusableview = headerView;
    }
    return reusableview;
}

#pragma mark  CollectionViewCell

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(DEVICE_WIDTH/2,100);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    Subscription *aSubscription = [self.arrSubscribes objectAtIndex:section];
    if(aSubscription.isOpened) {
        return aSubscription.arrPerformance.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    Subscription *aSubscription = [self.arrSubscribes objectAtIndex:indexPath.section];
    Performance *aPerformance = [aSubscription.arrPerformance objectAtIndex:indexPath.row];
    cell.lblTitle.text = aPerformance.title;
    cell.lblDate.text = aPerformance.date;
    if(aSubscription.subCode == 3) {
        cell.btnCheck.hidden = FALSE;
        cell.btnCheck.selected = aPerformance.isSelected;
    } else {
        cell.btnCheck.hidden = TRUE;
    }
    if([aPerformance.imageURL isKindOfClass:[NSNull class]]) {
        cell.imgView.hidden = TRUE;
    } else {
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aPerformance.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"]];
        cell.imgView.hidden = FALSE;
    }
    
    [cell.btnCheck addTarget:self action:@selector(btnCheckMarkPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)btnOpenCloseHeaderView:(BSOButton *)sender {
    Subscription *aSubscription = [self.arrSubscribes objectAtIndex:sender.tag];
    aSubscription.isOpened = !aSubscription.isOpened;
    [self.collection reloadData];
}

- (void)btnBestAvailableSeatsPressed:(BSOButton *)sender {
    Subscription *aSubscription = [self.arrSubscribes objectAtIndex:sender.tag];
    self.aSubscription = [self.arrSubscribes objectAtIndex:sender.tag];
    
    if(aSubscription.subCode == 3) {
        int selected = 0;
       
        self.arrIds = [[NSMutableArray alloc] init];
        for(Performance *aPerformance in aSubscription.arrPerformance) {
            if(aPerformance.isSelected) {
                [self.arrIds addObject:aPerformance.listID];
                selected ++;
            }
        }
        
        if(selected < aSubscription.minPerformance || selected > aSubscription.maxPerformance) {
             [Utility ShowAlertWithTitle:@"Alert" Message:[NSString stringWithFormat:@"Choose minimum of %d and a maximum of %d performances",aSubscription.minPerformance,aSubscription.maxPerformance]];
        } else {
            
            if(GET_UD(SESSION_ID)) {
                [self callBestAvailableCYO];
            } else {
                UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
                viewController.delegate = self;
                [self.navigationController pushViewController:viewController animated:YES];
            }
         
        }
    } else {
        if(GET_UD(SESSION_ID)) {
            [self callBestAvailableFixed];
        } else {
            UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (void)callBestAvailableCYO {
    
    [Utility showLoader];
    [self.bsoManager subscriptionBestAvailableCYO:self.aSubscription.ID list:self.arrIds success:^(id responseObject) {
        if([responseObject isKindOfClass:[SubscriptionSeatCYO class]]) {
            SeatSelectionCYOViewController *viewController = [[SeatSelectionCYOViewController alloc] initWithNibName:@"SeatSelectionCYOViewController" bundle:nil];
            viewController.aCYO = responseObject;
            viewController.aSubscription = self.aSubscription;
            [self.navigationController pushViewController:viewController animated:YES];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)callBestAvailableFixed {
    
    [Utility showLoader];
    [self.bsoManager subscriptionBestAvailableFixed:self.aSubscription.ID success:^(id responseObject) {
        if([responseObject isKindOfClass:[AvailalbeSeatResponse class]]) {
            SeatSelectionViewController *viewController = [[SeatSelectionViewController alloc] initWithNibName:@"SeatSelectionViewController" bundle:nil];
            viewController.aAvailalbeSeat = responseObject;
            viewController.aSubscription = self.aSubscription;
            [self.navigationController pushViewController:viewController animated:YES];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)btnCheckMarkPressed:(BSOButton *)sender {
    sender.selected = !sender.selected;
    NSIndexPath *indexPath = [self.collection indexPathForItemAtPoint:[self.collection convertPoint:sender.center fromView:sender.superview]];
    if(indexPath) {
        Subscription *aSubscription = [self.arrSubscribes objectAtIndex:indexPath.section];
        Performance *aPerformance = [aSubscription.arrPerformance objectAtIndex:indexPath.row];
        aPerformance.isSelected = !aPerformance.isSelected;
    }
}

- (void)userLoginSuccessfully:(BOOL)success {
    if(success) {
        dispatch_async(dispatch_get_main_queue(), ^(){
            if(self.aSubscription.subCode == 3) {
                [self performSelector:@selector(callBestAvailableCYO) withObject:nil afterDelay:.1];
            } else {
                [self performSelector:@selector(callBestAvailableFixed) withObject:nil afterDelay:.1];
            }
            
        });
    }
}

#pragma mark - Filter

- (void)searchTextValueChaged {
    [self filterSubScibeRecords];
}

- (IBAction)btnDayOfWeekPressed:(BSOButton *)sender {
    [self.arrDaysWeek removeAllObjects];
    [self.arrDaysWeek addObject:[sender titleForState:UIControlStateNormal]];
    self.btnSelectedDay = sender;
    CGPoint center = self.imgScrolArrow.center;
    center.x = sender.center.x;
    self.imgScrolArrow.center = center;
    
//    sender.selected = !sender.selected;
//    if(sender.selected) {
//        [self.arrDaysWeek addObject:[sender titleForState:UIControlStateNormal]];
//        [Utility setBorderColorWithView:sender color:[sender titleColorForState:UIControlStateSelected]];
//    } else {
//        [self.arrDaysWeek removeObject:[sender titleForState:UIControlStateNormal]];
//        [Utility setBorderColorWithView:sender color:[sender titleColorForState:UIControlStateNormal]];
//    }
    [self filterSubScibeRecords];
}

- (void)filterSubScibeRecords {
    [self.arrSubscribes removeAllObjects];
    NSMutableArray *arrPredicates = [[NSMutableArray alloc] init];
    
    if(self.btnSelected.tag == 1) { // if Fixes
        if(self.arrDaysWeek.count) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY SELF.arrPerformance.weekDay IN %@",self.arrDaysWeek];
            [arrPredicates addObject:predicate];
        }
    }
    
    if(self.txtSearch.text.length) {
         NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY SELF.arrPerformance.title CONTAINS[cd] %@ OR self.title CONTAINS[cd] %@ OR self.strDescription CONTAINS[cd] %@ OR self.strPeformanceDescription CONTAINS[cd] %@",self.txtSearch.text,self.txtSearch.text,self.txtSearch.text,self.txtSearch.text];
        [arrPredicates addObject:predicate];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.subCode == %d",self.btnSelected.tag];
    [arrPredicates addObject:predicate];
    
    if(arrPredicates.count) {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:arrPredicates];
        [self.arrSubscribes addObjectsFromArray:[self.arrAllSubscribes filteredArrayUsingPredicate:predicate]];
    } else {
        [self.arrSubscribes addObjectsFromArray:self.arrAllSubscribes];
    }

    [self.collection reloadData];
    
}

- (void)changeBorderColor {
    for(BSOButton *btn in self.filterView.subviews) {
        if(btn.tag) {
            if(btn.selected) {
                [Utility setBorderColorWithView:btn color:[btn titleColorForState:UIControlStateSelected]];
            } else {
                [Utility setBorderColorWithView:btn color:[btn titleColorForState:UIControlStateNormal]];
            }
        }
       
    }
}

- (void)changeTopButtonPositions {
    int width = [UIScreen mainScreen].bounds.size.width / self.arrAllDaysWeek.count;
    float x = ([UIScreen mainScreen].bounds.size.width - (width * self.arrAllDaysWeek.count)) / 2.0;
    for(BSOButton *btn in self.filterView.subviews) {
        if(btn.tag) {
            CGRect frame = btn.frame;
            frame.origin.x = x;
            frame.size.width = width;
            btn.frame = frame;
            x+=width;
        }
    }
    
    CGPoint center = self.imgScrolArrow.center;
    center.x = self.btnSelectedDay.center.x;
    self.imgScrolArrow.center = center;
}

- (void)createFilterDayButtons {
    int width = [UIScreen mainScreen].bounds.size.width / self.arrAllDaysWeek.count;
    float x = ([UIScreen mainScreen].bounds.size.width - (width * self.arrAllDaysWeek.count)) / 2.0;
    int tag = 1;
    for(NSString *day in self.arrAllDaysWeek) {
        BSOButton *btn = [BSOButton buttonWithType:UIButtonTypeCustom];
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn setTitle:day forState:UIControlStateNormal];
        btn.tag = tag;
        [btn addTarget:self action:@selector(btnDayOfWeekPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect frame = CGRectMake(x, 0, width, 50);
        btn.frame = frame;
        
        [self.filterView addSubview:btn];
        
        x+=width;
        tag++;
        
    }
    
    self.btnSelectedDay = [self.filterView viewWithTag:1];
    
    CGPoint center = self.imgScrolArrow.center;
    center.x = self.btnSelectedDay.center.x;
    self.imgScrolArrow.center = center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)topButtonPressed:(BSOButton *)sender {
    self.btnSelected.selected = FALSE;
    sender.selected = TRUE;
    self.btnSelected = sender;
    
    CGRect frame = self.imgSelectedBorder.frame;
    frame.origin.x = sender.frame.origin.x;
    self.imgSelectedBorder.frame = frame;
    
    CGPoint center = self.imgArrow.center;
    center.x = self.btnSelected.center.x;
    self.imgArrow.center = center;
    
    if(self.arrAllSubscribes.count == 0) {
        return;
    }
    
    [self filterSubScibeRecords];
    
    if(sender.tag == 3) { // CYO
        self.filterView.hidden = TRUE;
        CGRect frame = self.lblHeader.frame;
        frame.origin.y = 110;
        self.lblHeader.frame = frame;
        
        frame = self.collection.frame;
        frame.origin.y = 135;
        frame.size.height = self.view.frame.size.height - frame.origin.y;
        self.collection.frame = frame;
        
    } else {
        self.filterView.hidden = FALSE;
        
        CGRect frame = self.lblHeader.frame;
        frame.origin.y = 164;
        self.lblHeader.frame = frame;
        
        frame = self.collection.frame;
        frame.origin.y = 190;
        frame.size.height = self.view.frame.size.height - frame.origin.y;
        self.collection.frame = frame;
    }
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        [self.collection reloadData];
        self.imgSelectedBorder.frame = self.btnSelected.frame;
        CGPoint center = self.imgArrow.center;
        center.x = self.btnSelected.center.x;
        self.imgArrow.center = center;
        [self changeTopButtonPositions];
        
    } completion:^(id  _Nonnull context) {
        self.imgSelectedBorder.frame = self.btnSelected.frame;
        
        CGPoint center = self.imgArrow.center;
        center.x = self.btnSelected.center.x;
        self.imgArrow.center = center;
        
    }];
}

- (void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view {
    HomeViewController *parentController = (HomeViewController *)self.parentViewController;
    CGRect frame = parentController.btnFilter.frame;
    *rect = frame;
}
@end
