//
//  CalendarViewController.h
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarViewControllerDelegate <NSObject>
@optional
- (void)CalendarViewInfoButtonPressed:(Event *)aEvent;
- (void)CalendarViewTicketButtonPressed:(Event *)aEvent;
- (void)CalendarViewBakButtonPressed;

@end


@interface CalendarViewController : UIViewController
@property (nonatomic, assign) id<CalendarViewControllerDelegate> delegate;
- (void)getCalendarEvents;
@end
