//
//  ShowDetailViewController.h
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowDetailViewController : UIViewController
@property (nonatomic, strong) Show *aShow;
- (void)stopMusicPlayer;
@end
