//
//  WhileYouAreHereViewController.m
//  BSO
//
//  Created by MAC on 25/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "WhileYouAreHereViewController.h"
#import "MemeGeneratorViewController.h"
#import "VineGalleryViewController.h"
#import "PhotoBoothViewController.h"
//#import "CloudRecoViewController.h"

@interface WhileYouAreHereViewController ()
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation WhileYouAreHereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.srcView.contentSize = self.srcView.frame.size;
    
    for (UIView *aView in self.srcView.subviews) {
        [Utility setBorderColorWithView:aView color:RGB(26, 26, 28)];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    self.bsoManager.compactNav = FALSE;
}

#pragma mark -  Vine

- (IBAction)btnCreateVinePressed {
    self.bsoManager.compactNav = TRUE;
    VineGalleryViewController *viewController = [[VineGalleryViewController alloc] initWithNibName:@"VineGalleryViewController" bundle:nil];
    UINavigationController *navControlller = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.parentViewController presentViewController:navControlller animated:YES completion:nil];
}

- (IBAction)memeGenerateButtonPressed {
    self.bsoManager.compactNav = TRUE;
    MemeGeneratorViewController *viewController = [[MemeGeneratorViewController alloc] initWithNibName:@"MemeGeneratorViewController" bundle:nil];
    UINavigationController *navControlller = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.parentViewController presentViewController:navControlller animated:YES completion:nil];
}

- (IBAction)btnPhotoBoothPressed {
    self.bsoManager.compactNav = TRUE;
    PhotoBoothViewController *viewController = [[PhotoBoothViewController alloc] initWithNibName:@"PhotoBoothViewController" bundle:nil];
    UINavigationController *navControlller = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.parentViewController presentViewController:navControlller animated:YES completion:nil];
}

- (IBAction)beaconButtonPressed {
    [[GimbalManager SharedInstance] startBeaconMonitoring];
}

- (IBAction)ARButtonPressed {
 //   CloudRecoViewController *viewController = [[CloudRecoViewController alloc] initWithNibName:@"CloudRecoViewController" bundle:nil];
 //   [self.parentViewController.navigationController pushViewController:viewController animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[GimbalManager SharedInstance] stopBeaconMonitoring];
}

@end