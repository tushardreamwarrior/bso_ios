//
//  SubscribeViewController.h
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubscribeViewController : UIViewController
- (void)getSubscribeData;
- (void)openFilterView;
@end
