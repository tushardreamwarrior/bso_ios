//
//  EnterCardViewController.h
//  BSO
//
//  Created by MAC on 20/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EnterCardViewControllerDelegate <NSObject>
@optional
- (void)cardInfoEnteredSuccessfully;
@end

@interface EnterCardViewController : UIViewController
@property (nonatomic, assign) id <EnterCardViewControllerDelegate> delegate;
@end
