//
//  WizardViewController.m
//  BSO
//
//  Created by MAC on 20/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "WizardViewController.h"
#import "UserLoginViewController.h"
#import "CreatPinViewController.h"
#import "EnterCardViewController.h"

@interface WizardViewController () <UserLoginViewDelegate,CreatPinViewControllerDelegate,EnterCardViewControllerDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnAction;
@property (nonatomic, strong) IBOutlet BSOButton *btnSkip;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMessage;
@end

@implementation WizardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.bsoManager.wizardStep = 0;
    self.bsoManager.compactNav = TRUE;
    [Utility setBorderColorWithView:self.btnAction color:[UIColor whiteColor]];
    [Utility setBorderColorWithView:self.btnSkip color:[UIColor whiteColor]];
    [Utility setCornerToView:self.btnAction corner:5];
    [Utility setCornerToView:self.btnSkip corner:5];
    
    [self setupMessageAndAction];
    
    [self.bsoManager retriveCrediCars];
}

- (void)setupMessageAndAction {
    self.bsoManager.wizardStep  ++;
     self.title = [NSString stringWithFormat:@"Step %d", self.bsoManager.wizardStep];
    if(self.bsoManager.wizardStep == 1) {
        self.lblMessage.text = @"Do you want to Auto login into application ?";
        [self.btnAction setTitle:@"Login" forState:UIControlStateNormal];
    }
    else if(self.bsoManager.wizardStep == 2) {
        self.lblMessage.text = @"Do you want to protect your device by entering 4 digit PIN?\n This will be asked when application opens";
        [self.btnAction setTitle:@"Yes" forState:UIControlStateNormal];
    }
    else if(self.bsoManager.wizardStep == 3) {
        if(GET_UD(USER_EMAIL)) {
            self.lblMessage.text = @"Do you want to do one click purchasing?";
            [self.btnAction setTitle:@"Yes" forState:UIControlStateNormal];
        } else {
            self.bsoManager.compactNav = TRUE;
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
        
    } else {
        self.bsoManager.compactNav = TRUE;
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = FALSE;
}

- (IBAction)btnActionPressed {
    if(self.bsoManager.wizardStep == 1) {
        UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if(self.bsoManager.wizardStep == 2) {
       CreatPinViewController *viewController = [[CreatPinViewController alloc] initWithNibName:@"CreatPinViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else if(self.bsoManager.wizardStep == 3) {
        EnterCardViewController *viewController = [[EnterCardViewController alloc] initWithNibName:@"EnterCardViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)btnSkipPressed {
    [self setupMessageAndAction];
}

- (void)userLoginSuccessfully:(BOOL)success {
    if(success) {
        [self setupMessageAndAction];
    }
}

- (void)createPinViewCancelButtonPressed {
    
}

- (void)createPinViewSubmitButtonPressed:(NSString *)pin {
    SET_UD(ENABLE_4DIGITPIN, @"1");
    [self setupMessageAndAction];
}

- (void)cardInfoEnteredSuccessfully {
    [self setupMessageAndAction];
}

@end
