//
//  EnterCardViewController.m
//  BSO
//
//  Created by MAC on 20/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "EnterCardViewController.h"
#import "SelectionView.h"

@interface EnterCardViewController ()  <SelectionDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) NSMutableArray *arrMonths;
@property (nonatomic, strong) NSMutableArray *arrYear;
@property (nonatomic, strong) NSMutableArray *arrCards;

@property (nonatomic, strong) IBOutlet BSOTextField *txtCarNumber;
@property (nonatomic, strong) IBOutlet BSOTextField *txtNameonCard;
@property (nonatomic, strong) IBOutlet BSOTextField *txtCVV;
@property (nonatomic, strong) IBOutlet BSOButton *btnCardType;
@property (nonatomic, strong) IBOutlet BSOButton *btnExpireMonth;
@property (nonatomic, strong) IBOutlet BSOButton *btnExpireYear;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;

@end

@implementation EnterCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
    self.arrMonths = [[NSMutableArray alloc] init];
    [self.btnExpireMonth setTitle:@"01" forState:UIControlStateNormal];
    [self.btnExpireYear setTitle:[Utility stringFromDate:[NSDate date] format:@"yyyy"] forState:UIControlStateNormal];
    
    self.arrMonths = [[NSMutableArray alloc] init];
    self.arrYear = [[NSMutableArray alloc] init];
    self.arrCards = [[NSMutableArray alloc] init];

    for (CreditCard *aCard in self.bsoManager.arrCreditCards) {
        [self.arrCards addObject:aCard.cardName];
    }
    if(self.arrCards.count)
        [self.btnCardType setTitle:self.arrCards[0] forState:UIControlStateNormal];
    
    for(int i=1;i<=12;i++) {
        [self.arrMonths addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    int currentYear = [[Utility stringFromDate:[NSDate date] format:@"yyyy"] intValue];
    for(int i=0;i<5;i++) {
        [self.arrYear addObject:[NSString stringWithFormat:@"%d",currentYear+i]];
    }
    
    for(UIView *aView in self.srcView.subviews) {
        if([aView isKindOfClass:[BSOTextField class]]) {
            BSOTextField *txtField = (BSOTextField *)aView;
            [Utility setBorderColorWithView:txtField color:RGB(135, 135, 135)];
            [Utility setLeftViewWithTextField:txtField];
            [Utility setCornerToView:txtField corner:10];
        }
    }
    
    self.navigationController.navigationBarHidden = TRUE;
}

- (IBAction)btnCardTypePressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrCards];
    selView.delegate = self;
    selView.selectedIndex = [self.arrCards indexOfObject:[self.btnCardType titleForState:UIControlStateNormal]];
    [selView showInView:self.btnCardType];
}

- (IBAction)btnExpireMonthPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrMonths];
    selView.delegate = self;
    [selView showInView:self.btnExpireMonth];
}

- (IBAction)btnExpireYearPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrYear];
    selView.delegate = self;
    [selView showInView:self.btnExpireYear];
}

- (void)SelectionView:(SelectionView *)selectionView didSelectItem:(NSString *)item withIndex:(NSInteger)index SenderView:(UIView *)senderView {
    BSOButton *btn = (BSOButton *)senderView;
    [btn setTitle:item forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSubmitPressed {
    if(allTrim(self.txtCarNumber.text).length < 8) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Valid Card number"];
        return;
    }
    if(allTrim(self.txtNameonCard.text).length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Name of card"];
        return;
    }
    if(allTrim(self.txtCVV.text).length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter CVV"];
        return;
    }
    
    CreditCard *aCard = [[CreditCard alloc] init];
    aCard.cardNumber = self.txtCarNumber.text;
    aCard.cardName = self.txtNameonCard.text;
    aCard.cardCVV = self.txtCVV.text;
    aCard.cardExpireMonth = [self.btnExpireMonth titleForState:UIControlStateNormal];
    aCard.cardExpireYear = [self.btnExpireYear titleForState:UIControlStateNormal];
    aCard.cardType = [self.btnCardType titleForState:UIControlStateNormal];
    
    [Utility showLoader];
    BSOManager *bsoManager = [BSOManager SharedInstance];
    [bsoManager validateCardInfo:aCard success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {

            NSString *key = GET_UD(KEY);
            NSString *encyptedName = [Utility encryptString:self.txtNameonCard.text withKey:key];
            NSString *encyptedType = [Utility encryptString:[self.btnCardType titleForState:UIControlStateNormal] withKey:key];
            NSString *encyptedNumer = [Utility encryptString:[self.txtCarNumber.text substringToIndex:self.txtCarNumber.text.length - 4] withKey:key];
            NSString *encyptedCVV = [Utility encryptString:self.txtCVV.text withKey:key];
            
            
            SET_UD(PAYMENT_CARD_NAME,encyptedName);
            SET_UD(PAYMENT_CARD_TYPE,encyptedType);
            SET_UD(PAYMENT_CARD_NUMBER,encyptedNumer);
            SET_UD(PAYMENT_CARD_CVV,encyptedCVV);
            
           if([self.delegate respondsToSelector:@selector(cardInfoEnteredSuccessfully)])
                [self.delegate cardInfoEnteredSuccessfully];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
       
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

@end
