//
//  SeatSelectionViewController.m
//  BSO
//
//  Created by MAC on 13/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SeatSelectionViewController.h"
#import "ShpingCartViewController.h"

@interface SeatSelectionViewController () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;
@property (nonatomic, strong) IBOutlet BSOTextField *txtSeat;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) NSInteger selectedHeader;
@property (nonatomic, assign) int maxNumbeofSeats;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@end

@implementation SeatSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.txtSeat.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    self.txtSeat.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtSeat.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.selectedIndex = -1;
    self.selectedHeader = -1;
    
    if(self.aEvent) {
        if([[self.aAvailalbeSeat.dictOtherInfo valueForKey:@"MaximumNumberOfSeatsForThePerformance"] intValue]) {
            self.maxNumbeofSeats = [[self.aAvailalbeSeat.dictOtherInfo valueForKey:@"MaximumNumberOfSeatsForThePerformance"] intValue];
        } else {
            self.maxNumbeofSeats = 20;
        }
        
        self.lblDate.text = [NSString stringWithFormat:@"%@ at %@",self.aEvent.date,self.aEvent.time];
        self.lblTitle.text = self.aEvent.title;

    } else {
        self.lblDate.text = self.aSubscription.strPeformanceDescription;
        self.lblTitle.text = self.aSubscription.title;
        self.maxNumbeofSeats = 500000; // un limited

    }
    
    [self.bsoManager checkUserSession:GET_UD(USER_EMAIL) success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        
    }];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    self.table.tableHeaderView = self.headerView;
    self.table.tableFooterView = [[UIView alloc] init];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.aAvailalbeSeat.arrSections.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:section];
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 35)];
    UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 34, tableView.frame.size.width, 1)];
    lblLine.backgroundColor = [UIColor whiteColor];
    lblLine.text = @"";
    lblLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [aView addSubview:lblLine];

    
    aView.backgroundColor = [UIColor blackColor];
    UIButton *btnPrice = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPrice.frame = CGRectMake(20, 0, tableView.frame.size.width, 35);
    [btnPrice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPrice setTitle:[NSString stringWithFormat:@"▶︎ $%@",aSection.sectionPrice] forState:UIControlStateNormal];
    btnPrice.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
    btnPrice.tag = section;
    [btnPrice addTarget:self action:@selector(btnPriceHeaderPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnPrice.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    btnPrice.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [aView addSubview:btnPrice];
    return aView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == self.selectedHeader) {
        SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:section];
        return aSection.arrSeats.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"anyCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
        cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
    }
    
    SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:indexPath.section];
    
    Seat *aSeat = [aSection.arrSeats objectAtIndex:indexPath.row];
    cell.textLabel.text =  [NSString stringWithFormat:@"        %@",aSeat.seatDescription];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"        Available Seats: %@",aSeat.seatsAvailable];
    cell.detailTextLabel.text = @"";
    
    if(indexPath.row == self.selectedIndex && self.selectedHeader == indexPath.section) {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tickMark"]];
    } else {
        cell.accessoryView = nil;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)btnPriceHeaderPressed:(UIButton *)sender {
    if(sender.tag == self.selectedHeader) {
        return;
    }
    
    [self.table beginUpdates];
    if(self.selectedHeader != -1) {
            SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:self.selectedHeader];
        for (NSUInteger i = 0; i < aSection.arrSeats.count; i++) {
            [self.table deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:self.selectedHeader]] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }
    
    SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:sender.tag];
    for (NSUInteger i = 0; i < aSection.arrSeats.count; i++) {
        [self.table insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:sender.tag]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    self.selectedHeader = sender.tag;
    self.selectedIndex = -1;
    [self.table endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.row;
    [self.table reloadData];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)btnProceedPressed {
    if(self.txtSeat.text.length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter number of seats"];
        return;
    }
    if([self.txtSeat.text intValue] <= 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter valid number of seats"];
        return;
    }
    if(self.selectedIndex == -1) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Select seating postion"];
        return;
    }
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([self.txtSeat.text rangeOfCharacterFromSet:notDigits].location != NSNotFound) {
       [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter valid number of seats"];
        return;
    }
    if([self.txtSeat.text intValue] > self.maxNumbeofSeats) {
        [Utility ShowAlertWithTitle:@"Alert" Message:[NSString stringWithFormat:@"You can select maximum %d Seats",self.maxNumbeofSeats]];
        return;
    }
    
    SeatSection *aSection = [self.aAvailalbeSeat.arrSections objectAtIndex:self.selectedHeader];
    Seat *selectedSeat = [aSection.arrSeats objectAtIndex:self.selectedIndex];
    
    if([selectedSeat.seatsAvailable intValue] < [self.txtSeat.text intValue]) {
        [Utility ShowAlertWithTitle:@"Alert" Message:[NSString stringWithFormat:@"There are only %d seats availabe",[selectedSeat.seatsAvailable intValue]]];
        return;
    }
    
    [Utility showLoader];
    NSMutableDictionary *request = [[NSMutableDictionary alloc] initWithDictionary:self.aAvailalbeSeat.dictOtherInfo];
    
    [request setValue:self.txtSeat.text forKey:@"NumberOfSeats"];
    [request setValue:selectedSeat.seatID forKey:@"SelectedZoneId"];
    [request setValue:aSection.sectionPrice forKey:@"Prices"];
    [request setValue:selectedSeat.seatPriceID forKey:@"SelectedPriceTypes"];
    [request setValue:@"" forKey:@"SeasonId"];
    
    
    if(self.aEvent) {
        [self.bsoManager chekAvailableSeats:request success:^(id responseObject) {
            if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
                
                [self.bsoManager addToCart:request success:^(id responseObject) {
                    if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
                        [self performSelectorOnMainThread:@selector(openShppingCart) withObject:nil waitUntilDone:NO];
                    } else {
                        [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
                    }
                    [Utility hideLoader];
                } failure:^(NSError *error) {
                    [Utility hideLoader];
                }];
            } else {
                [Utility hideLoader];
                [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
            }
            
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];

    } else {
        [request setValue:@"" forKey:@"MaximumNumberOfSeatsForThePerformance"];
        [self.bsoManager SubscriptionFixedAddToCart:request success:^(id responseObject) {
            if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
                [self performSelectorOnMainThread:@selector(openShppingCart) withObject:nil waitUntilDone:NO];
            } else {
                [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
            }
            [Utility hideLoader];
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];
 }
    
}

- (void)openShppingCart {
    ShpingCartViewController *viewController = [[ShpingCartViewController alloc] initWithNibName:@"ShpingCartViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
