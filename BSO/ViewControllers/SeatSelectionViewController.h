//
//  SeatSelectionViewController.h
//  BSO
//
//  Created by MAC on 13/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeatSelectionViewController : UIViewController
@property (nonatomic, strong) AvailalbeSeatResponse *aAvailalbeSeat;
@property (nonatomic, strong) Subscription *aSubscription;
@property (nonatomic, strong) Event *aEvent;
@end
