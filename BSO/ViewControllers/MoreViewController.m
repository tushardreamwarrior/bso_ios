//
//  MoreViewController.m
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()
@property (nonatomic, strong) IBOutlet UIWebView *web;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnAbout;
@property (nonatomic, strong) IBOutlet BSOButton *btnVenueInfo;
@property (nonatomic, strong) IBOutlet BSOButton *btnGive;
@property (nonatomic, strong)  NSString *strAboutUS;
@property (nonatomic, strong)  NSString *strVenuInfo;
@property (nonatomic, strong)  NSString *strGive;
@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.web.opaque = NO;
    self.bsoManager = [BSOManager SharedInstance];
    self.bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackPatternBG"]];
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:gesture];
    
    gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.web addGestureRecognizer:gesture];
    [self btnAboutUSPressed];
    [self refreshContentForMoreViewScreen];
}

- (void)refreshContentForMoreViewScreen {
     self.strAboutUS = @"";
     self.strGive = @"";
     self.strVenuInfo = @"";
     [self.web loadHTMLString:@""  baseURL:nil];
    
    self.btnGive.backgroundColor = self.btnGive.selected ?[self.bsoManager getSelectedItemColor]: [self.bsoManager getSelectedItemBGColor];
    self.btnVenueInfo.backgroundColor = self.btnVenueInfo.selected ?[self.bsoManager getSelectedItemColor]: [self.bsoManager getSelectedItemBGColor];
    self.btnAbout.backgroundColor = self.btnAbout.selected ?[self.bsoManager getSelectedItemColor]: [self.bsoManager getSelectedItemBGColor];

    
    if(self.btnAbout.selected) {
        [self getAboutUsContent];
    }
}

- (void)getAboutUsContent {
    [Utility showLoader];
    [self.bsoManager getAboutUSContent:self.bsoManager.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSString class]]) {
            [self.web loadHTMLString:[NSString stringWithFormat:@"<font size='4' face='OpenSans' color='white'>%@</font>",responseObject] baseURL:nil];
            self.strAboutUS = responseObject;
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (IBAction)btnAboutUSPressed {
    self.btnGive.backgroundColor = [self.bsoManager getSelectedItemBGColor];
    self.btnVenueInfo.backgroundColor = [self.bsoManager getSelectedItemBGColor];
    self.btnAbout.backgroundColor = [self.bsoManager getSelectedItemColor];
    
    self.btnAbout.selected = TRUE;
    self.btnVenueInfo.selected = FALSE;
    self.btnGive.selected = FALSE;
    if(self.strAboutUS.length) {
         [self.web loadHTMLString:[NSString stringWithFormat:@"<font size='4' face='OpenSans' color='white'>%@</font>",self.strAboutUS] baseURL:nil];
    } else {
        [self getAboutUsContent];
    }
}

- (IBAction)btnvenueInfoPressed {
   
    self.btnGive.backgroundColor = [self.bsoManager getSelectedItemBGColor];
    self.btnVenueInfo.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.btnAbout.backgroundColor = [self.bsoManager getSelectedItemBGColor];
    
    self.btnAbout.selected = FALSE;
    self.btnVenueInfo.selected = TRUE;
    self.btnGive.selected = FALSE;
    [self.web loadHTMLString:[NSString stringWithFormat:@"<font size='4' face='OpenSans' color='white'>%@</font>",self.strVenuInfo] baseURL:nil];
}

- (IBAction)btnGiveAndJoinPressed {
    
    self.btnGive.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.btnVenueInfo.backgroundColor = [self.bsoManager getSelectedItemBGColor];
    self.btnAbout.backgroundColor = [self.bsoManager getSelectedItemBGColor];
  
    self.btnAbout.selected = FALSE;
    self.btnVenueInfo.selected = FALSE;
    self.btnGive.selected = TRUE;
     [self.web loadHTMLString:[NSString stringWithFormat:@"<font size='4' face='OpenSans' color='white'>%@</font>",self.strGive] baseURL:nil];
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)sender {
    if([self.delegate respondsToSelector:@selector(MoreViewBakButtonPressed)]) {
        [self.delegate MoreViewBakButtonPressed];
    }
}


@end
