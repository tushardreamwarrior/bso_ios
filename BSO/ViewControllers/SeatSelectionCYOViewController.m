//
//  SeatSelectionCYOViewController.m
//  BSO
//
//  Created by MAC on 06/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SeatSelectionCYOViewController.h"
#import "ShpingCartViewController.h"

@interface SeatSelectionCYOViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;
@property (nonatomic, strong) IBOutlet BSOTextField *txtSeat;
@property (nonatomic, strong) IBOutlet UIView *headerview;
@property (nonatomic, strong) NSMutableArray *selectedIndex;
@property (nonatomic, strong) NSMutableArray *selectedHeader;
@property (nonatomic, assign) NSInteger selectedHeaderValue;
@property (nonatomic, assign) BSOManager *bsoManager;
@end


@implementation SeatSelectionCYOViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    self.table.tableFooterView = [[UIView alloc] init];
    self.table.tableHeaderView = self.headerview;
    
    self.selectedHeaderValue = -1;
    
    self.selectedIndex = [[NSMutableArray alloc] init];
    self.selectedHeader = [[NSMutableArray alloc] init];
    
    for(int i=0;i<self.aCYO.arrPerformances.count;i++) {
        [self.selectedIndex addObject:@"-1"];
        [self.selectedHeader addObject:@"-1"];
        
    }
    self.aCYO.title = [self.aCYO.title uppercaseString];
    self.title = self.aCYO.title;
    
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.txtSeat.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    self.txtSeat.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtSeat.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    [self.bsoManager checkUserSession:GET_UD(USER_EMAIL) success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.aCYO.arrPerformances.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 80.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 80)];
    UIButton *btnTap = [UIButton buttonWithType:UIButtonTypeCustom];
    btnTap.tag = section;
    [btnTap addTarget:self action:@selector(btnHeaderPressed:) forControlEvents:UIControlEventTouchUpInside];
    btnTap.frame = CGRectMake(0, 0,  tableView.frame.size.width, 80);
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(25, 5, 80, 70)];
    lblTitle.numberOfLines = 0;
    lblTitle.font = [UIFont fontWithName:@"OpenSans-Semibold" size:12];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.textColor = [UIColor darkGrayColor];
    lblTitle.text = self.aCYO.title;
    [aView addSubview:lblTitle];
    
    AvailalbeSeatResponse *aAvailalbeSeat = [self.aCYO.arrPerformances objectAtIndex:section];
    
    UILabel *lblPerformance = [[UILabel alloc] initWithFrame:CGRectMake(160, 10, tableView.frame.size.width - 160, 40)];
    lblPerformance.numberOfLines = 0;
    lblPerformance.font = [UIFont fontWithName:@"OpenSans" size:14];
    lblPerformance.backgroundColor = [UIColor clearColor];
    lblPerformance.textColor = [UIColor blackColor];
    lblPerformance.text = aAvailalbeSeat.peroformanceTitle;
    [aView addSubview:lblPerformance];

    
    UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(160, 55, 500, 20)];
    lblDate.font = [UIFont fontWithName:@"OpenSans-Semibold" size:12];
    lblDate.backgroundColor = [UIColor clearColor];
    lblDate.textColor = [UIColor blackColor];
    lblDate.text = aAvailalbeSeat.peroformanceDate;
    [aView addSubview:lblDate];

    [aView addSubview:btnTap];
    
    UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 79, tableView.frame.size.width, 1)];
    lblLine.backgroundColor = [UIColor blackColor];
    lblLine.text = @"";
    lblLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [aView addSubview:lblLine];
    
    aView.backgroundColor = RGB(223, 223, 223);
    
    return aView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section == self.selectedHeaderValue) {
        return 50.0;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
   
    UIButton *btnPeformance = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPeformance.tag = section;

    if(section == self.selectedIndex.count-1) {
        BOOL flag = TRUE;
        for (int i=0;i<self.selectedIndex.count;i++) {
            int row = [[self.selectedIndex objectAtIndex:i] intValue];
            if(row == -1) {
                flag = FALSE;
                break;
            }
        }
        
        if(flag == FALSE) {
            btnPeformance.backgroundColor = [UIColor lightGrayColor];
            btnPeformance.enabled = FALSE;
        } else {
            btnPeformance.backgroundColor = [self.bsoManager getSelectedItemColor];
            btnPeformance.enabled = TRUE;
        }
        
        [btnPeformance setTitle:@"Add To Cart" forState:UIControlStateNormal];
    } else {
        [btnPeformance setTitle:@"Next Peformance" forState:UIControlStateNormal];
        
        int row = [[self.selectedIndex objectAtIndex:section] intValue];
        if(row == -1) {
            btnPeformance.backgroundColor = [UIColor lightGrayColor];
            btnPeformance.enabled = FALSE;
        } else {
            btnPeformance.backgroundColor = [self.bsoManager getSelectedItemColor];
            btnPeformance.enabled = TRUE;
        }
    }
    
    btnPeformance.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:14.0];
    [btnPeformance setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnPeformance.frame = CGRectMake(tableView.frame.size.width - 200, 10,  180, 30);
    [btnPeformance addTarget:self action:@selector(btnPeformancePressed:) forControlEvents:UIControlEventTouchUpInside];
    [aView addSubview:btnPeformance];
    aView.backgroundColor = [UIColor blackColor];
    return aView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == self.selectedHeaderValue) {
        AvailalbeSeatResponse *aAvailalbeSeat = [self.aCYO.arrPerformances objectAtIndex:section];
        return aAvailalbeSeat.arrSections.count;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int header = [[self.selectedHeader objectAtIndex:indexPath.section] intValue];
    if(header == indexPath.row) {
        AvailalbeSeatResponse *aAvailalbeSeat = [self.aCYO.arrPerformances objectAtIndex:indexPath.section];
        SeatSection *aSection = [aAvailalbeSeat.arrSections objectAtIndex:indexPath.row];
        return 35.0 + (aSection.arrSeats.count * 40.0);
    }
    return 35.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    //if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
    }
    
    AvailalbeSeatResponse *aAvailalbeSeat = [self.aCYO.arrPerformances objectAtIndex:indexPath.section];
    SeatSection *aSection = [aAvailalbeSeat.arrSections objectAtIndex:indexPath.row];
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
    UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 34, tableView.frame.size.width, 1)];
    lblLine.backgroundColor = [UIColor whiteColor];
    lblLine.text = @"";
    lblLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [cell.contentView addSubview:lblLine];


    UIButton *btnPrice = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPrice.frame = CGRectMake(20, 0, tableView.frame.size.width, 35);
    [btnPrice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnPrice setTitle:[NSString stringWithFormat:@"▶︎ $%@",aSection.sectionPrice] forState:UIControlStateNormal];
    btnPrice.titleLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
    [btnPrice addTarget:self action:@selector(btnPriceHeaderPressed:event:) forControlEvents:UIControlEventTouchUpInside];
    btnPrice.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    btnPrice.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [cell.contentView addSubview:btnPrice];
    
    cell.clipsToBounds = YES;
    cell.contentView.clipsToBounds = YES;
    
    int header = [[self.selectedHeader objectAtIndex:indexPath.section] intValue];
    if(header == indexPath.row) {
        float Y = 35;
        for(int i=0;i<aSection.arrSeats.count;i++) {
            Seat *aseat = [aSection.arrSeats objectAtIndex:i];
            UIButton *btnSeat = [UIButton buttonWithType:UIButtonTypeCustom];
            btnSeat.frame = CGRectMake(40, Y, tableView.frame.size.width-40, 40);
            [btnSeat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnSeat setTitle:[NSString stringWithFormat:@"%@",aseat.seatDescription] forState:UIControlStateNormal];
            btnSeat.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:14];
            [btnSeat addTarget:self action:@selector(btnSeatRowPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            btnSeat.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            btnSeat.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            
            btnSeat.tag = i;
            int row = [[self.selectedIndex objectAtIndex:indexPath.section] intValue];
            if(i == row) {
                UIImageView *imgTick = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tickMark"]];
                imgTick.frame = CGRectMake(tableView.frame.size.width-80, Y, 35, 33);
                [cell.contentView addSubview:imgTick];
            }
            
            UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, Y+39, tableView.frame.size.width, 1)];
            lblLine.backgroundColor = RGBA(255, 255, 255, .5);
            lblLine.text = @"";
            lblLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            
            [cell.contentView addSubview:lblLine];
            [cell.contentView addSubview:btnSeat];
            
            Y+= 40;
        }
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor blackColor];
    cell.contentView.backgroundColor = [UIColor blackColor];
    return cell;
}

- (void)btnHeaderPressed:(UIButton *)sender {
    if(sender.tag == self.selectedHeaderValue) {
      //  return;
    }
    [self exlpandCollapseHeader:sender.tag];
}

- (void)exlpandCollapseHeader:(NSInteger)section {
    [self.table beginUpdates];
    
    if(self.selectedHeaderValue != -1) {
        NSRange range = NSMakeRange(self.selectedHeaderValue, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.table reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if(section != self.selectedHeaderValue) {
        NSRange range = NSMakeRange(section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.table reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    if(section != self.selectedHeaderValue) {
        self.selectedHeaderValue = section;
    } else {
        self.selectedHeaderValue = -1;
    }
    
    [self.table endUpdates];
}

- (void)btnPeformancePressed:(UIButton *)sender {
    if(sender.tag == self.selectedIndex.count-1) {
        [self btnProceedPressed];
    } else {
        [self exlpandCollapseHeader:sender.tag+1];
    }
}

- (void)btnPriceHeaderPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint: currentTouchPosition];
    int header = [[self.selectedHeader objectAtIndex:indexPath.section] intValue];
    if(header == indexPath.row) {
        return;
    }
    
    if(indexPath) {
        [self.selectedHeader replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        [self.selectedIndex replaceObjectAtIndex:indexPath.section withObject:@"-1"];
        NSRange range = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.table beginUpdates];
        [self.table reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];
        [self.table endUpdates];
        
    }
}

- (void)btnSeatRowPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        [self.selectedIndex replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)sender.tag]];
        NSRange range = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.table reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationNone];

    }
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)btnProceedPressed {
    self.txtSeat.text = allTrim(self.txtSeat.text);
    if(self.txtSeat.text.length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter number of seats"];
        return;
    }
    if([self.txtSeat.text intValue] <= 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter valid number of seats"];
        return;
    }
    BOOL flag = TRUE;
    for (int i=0;i<self.selectedIndex.count;i++) {
        int row = [[self.selectedIndex objectAtIndex:i] intValue];
        if(row == -1) {
            flag = FALSE;
            break;
        }
    }

    if(flag == FALSE) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Select seating postion for all performances"];
        return;
    }
    
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([self.txtSeat.text rangeOfCharacterFromSet:notDigits].location != NSNotFound) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter valid number of seats"];
        return;
    }
    
    [self.view endEditing:YES];
    
    NSMutableDictionary *request = [[NSMutableDictionary alloc] init];
    [request setValue:self.aSubscription.ID forKey:@"subscription_id"];
    
    NSMutableArray *arrSeats = [[NSMutableArray alloc] init];
    for (int i=0;i<self.aCYO.arrPerformances.count;i++) {
        AvailalbeSeatResponse *aAvailalbeSeat = [self.aCYO.arrPerformances objectAtIndex:i];
        SeatSection *aSection = [aAvailalbeSeat.arrSections objectAtIndex:[[self.selectedHeader objectAtIndex:i] intValue]];
        Seat *aSeat = [aSection.arrSeats objectAtIndex:[[self.selectedIndex objectAtIndex:i] intValue]];
        
        NSDictionary *dictSeat = [[NSMutableDictionary alloc] init];
        [dictSeat setValue:self.txtSeat.text forKey:@"BAQuantity"];
        [dictSeat setValue:aAvailalbeSeat.peroformanceID forKey:@"PerformanceId"];
        [dictSeat setValue:aSeat.seatID forKey:@"ZoneId"];
        [dictSeat setValue:aSeat.seatPriceID forKey:@"PriceType_ExternalId"];
        [arrSeats addObject:dictSeat];
    }
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrSeats options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [request setValue:jsonString forKey:@"seat_data"];
    
    [Utility showLoader];
    [self.bsoManager SubscriptionCYOAddToCart:request success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
             [self performSelectorOnMainThread:@selector(openShppingCart) withObject:nil waitUntilDone:NO];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)openShppingCart {
    ShpingCartViewController *viewController = [[ShpingCartViewController alloc] initWithNibName:@"ShpingCartViewController" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        [self.table reloadData];
    } completion:^(id  _Nonnull context) {
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end