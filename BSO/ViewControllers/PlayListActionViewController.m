//
//  PlayListActionViewController.m
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PlayListActionViewController.h"
#import "MyMusicActionViewController.h"
#import "MyPlaylistActionViewController.h"

@interface PlayListActionViewController ()
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblArtist;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@end

@implementation PlayListActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    self.lblTitle.text = self.aItem.title;
    self.lblArtist.text = self.aItem.credit;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:self.aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
    
    [Utility setCornerToView:self.view corner:12];
    [Utility setCornerToView:self.bgView corner:12];
}

- (IBAction)btnSaveMyMusicPressed {
    MyMusicActionViewController *viewcontrolelr = [[MyMusicActionViewController alloc] initWithNibName:@"MyMusicActionViewController" bundle:nil];
    viewcontrolelr.delegate = self.delegate;
    [self.navigationController pushViewController:viewcontrolelr animated:YES];
}

- (IBAction)btnAddtoPlaylistPressed {
    MyPlaylistActionViewController *viewcontrolelr = [[MyPlaylistActionViewController alloc] initWithNibName:@"MyPlaylistActionViewController" bundle:nil];
    viewcontrolelr.delegate = self.delegate;
    [self.navigationController pushViewController:viewcontrolelr animated:YES];
}

- (IBAction)btnSharePressed {
    [self.delegate MyMusicActionSharePressed];
}

- (CGSize)preferredContentSize {
    return CGSizeMake(343, 425);
}


@end
