//
//  PlayListActionViewController.h
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PlayListActionDelegate <NSObject>
@optional
- (void)MyMusicActionSharePressed;
- (void)MyMusicActionShufflePressed;
- (void)MyMusicActionPlayPressed;
- (void)MyPlaylistShufflePressed;
- (void)MyPlaylistPlayPressed;
- (void)MyPlaylistDeletePressed;
- (void)MyPlaylistDeleteTrackPressed;
@end

@interface PlayListActionViewController : UIViewController
@property (nonatomic, assign) id <PlayListActionDelegate> delegate;
@property (nonatomic, strong) MediaItem *aItem;
@end
