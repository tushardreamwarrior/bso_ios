//m
//  MediaListViewController.m
//  BSO
//
//  Created by MAC on 13/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MediaListViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <QuickLook/QuickLook.h>
#import <MediaPlayer/MediaPlayer.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "PlayListActionViewController.h"
#import "MusicCell.h"
#import "PodcastCell.h"
#import "VideoCell.h"
#import "PhotoCell.h"

@interface MediaListViewController () <PlayListActionDelegate,UIPopoverControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,QLPreviewControllerDataSource,QLPreviewControllerDelegate,UIActionSheetDelegate,GPPSignInDelegate>

@property (nonatomic, strong) IBOutlet UICollectionView *collectionMusic;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionMyMusic;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionVideo;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionPhoto;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionVine;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIImageView *imgCover;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMusicTitle;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, strong) IBOutlet UIPopoverController *playlistPopOver;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) Media *aMedia;

// PlayerView
@property (nonatomic, strong) IBOutlet UIView *playerView;
@property (nonatomic, strong) IBOutlet UIView *playerInnerView;
@property (nonatomic, strong) IBOutlet UIView *brandColorBG;
@property (nonatomic, strong) IBOutlet BSOButton *btnMusicMore;
@property (nonatomic, strong) IBOutlet BSOButton *btnPlay;
@property (nonatomic, strong) IBOutlet BSOButton *btnRepeat;
@property (nonatomic, strong) IBOutlet BSOButton *btnShuffle;

@property (nonatomic, strong) AVPlayer *audioPlayer;
@property (nonatomic, strong) NSMutableArray *playerSongs;
@property (nonatomic, assign) NSInteger currentItem;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property (nonatomic, assign) BOOL isVideoPlaying;
@property (strong, nonatomic) MPMoviePlayerController *videoPlayer;
@end

@implementation MediaListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _playerSongs = [[NSMutableArray alloc] init];
    self.bsoManager = [BSOManager SharedInstance];
    self.collectionMusic.backgroundColor = [UIColor clearColor];
    [self.collectionMusic registerClass:[MusicCell class] forCellWithReuseIdentifier:@"MusicCell"];
    self.collectionMyMusic.backgroundColor = [UIColor clearColor];
    [self.collectionMyMusic registerClass:[MusicCell class] forCellWithReuseIdentifier:@"MusicCell"];
    self.collectionVideo.backgroundColor = [UIColor clearColor];
    [self.collectionVideo registerClass:[VideoCell class] forCellWithReuseIdentifier:@"VideoCell"];
  
    self.collectionPhoto.backgroundColor = [UIColor clearColor];
    [self.collectionPhoto registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"PhotoCell"];
    self.collectionVine.backgroundColor = [UIColor clearColor];
    [self.collectionVine registerClass:[PhotoCell class] forCellWithReuseIdentifier:@"PhotoCell"];
    
    self.playerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Player Background"]];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width, 20)];
    footerView.backgroundColor = self.tblView.backgroundColor;
    self.tblView.tableFooterView = footerView;
    
    [self.imgCover setImage:nil];
    self.lblMusicTitle.text = @"";
    
    [self getMediaAllMedia];
    [self hidePlayerView];
}

- (void)hidePlayerView {
    if(self.playerInnerView.userInteractionEnabled == FALSE) {
        return;
    }
    self.playerInnerView.userInteractionEnabled = FALSE;
    self.playerInnerView.alpha = 0.2;
    [self showHidePlayerView];

}
- (void)showPlayerView {
    if(self.playerInnerView.userInteractionEnabled == TRUE) {
        return;
    }
    self.playerInnerView.userInteractionEnabled = TRUE;
    self.playerInnerView.alpha = 1.0;
    [self showHidePlayerView];
}

- (void)showHidePlayerView {
    float yOffset;
    if(self.playerView.hidden) {
        yOffset = 2;
    } else {
        yOffset = 337;
    }

    for (int i=2001;i<=2005;i++) {
        UIView *aView = [self.srcView viewWithTag:i];
        CGRect frame = aView.frame;
        frame.origin.y = yOffset;
        aView.frame = frame;
        if(i == 2003) {
            if(self.aMedia.vineGallery.arrItmes.count) {
                yOffset = aView.frame.origin.y + aView.frame.size.height + 45;
            }
        }
        else if(i == 2004) {
            if(self.aMedia.photoGallery.arrItmes.count) {
                yOffset = aView.frame.origin.y + aView.frame.size.height + 45;
            }
        } else {
            yOffset = aView.frame.origin.y + aView.frame.size.height + 45;
        }
    }
    [self showMedia];
}

- (void)getMediaAllMedia {
    self.brandColorBG.backgroundColor = [self.bsoManager getSelectedItemColor];
    
     NSString *mediaFile = [NSString stringWithFormat:@"media_%d.plist",self.bsoManager.selectedType];
     NSDictionary *mediaDict = [NSDictionary dictionaryWithContentsOfFile:FILE_PATH(mediaFile)];
    if(mediaDict) {
        self.aMedia = [Media ParseMediaListResponse:mediaDict];
        [self.tblView reloadData];
        [self.collectionMusic reloadData];
        [self.collectionMyMusic reloadData];
        [self.collectionVideo reloadData];
        [self.collectionVine reloadData];
        [self.collectionPhoto reloadData];
        [self showHidePlayerView];
    } else {
        [Utility showLoader];
    }
    
    [self.bsoManager getAllMediaList:self.bsoManager.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[Media class]]) {
            self.aMedia = responseObject;
            [self.tblView reloadData];
            [self.collectionMusic reloadData];
            [self.collectionMyMusic reloadData];
            [self.collectionVideo reloadData];
            [self.collectionVine reloadData];
            [self.collectionPhoto reloadData];
            [self showHidePlayerView];
        } else  {
            
        }
        [Utility hideLoader];

    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)showMedia {
    CGRect frame = self.tblView.frame;
    frame.size.height = self.tblView.contentSize.height;
    self.tblView.frame = frame;
    UIView *myMusciView = [self.srcView viewWithTag:2006];
    
    if(self.aMedia.myMusicGallery.arrItmes.count) {
        
        CGRect mymusicFrame = myMusciView.frame;
        mymusicFrame.origin.y = frame.origin.y + frame.size.height + 45;
        myMusciView.frame = mymusicFrame;
        self.srcView.contentSize = CGSizeMake(self.srcView.frame.size.width, mymusicFrame.size.height + mymusicFrame.origin.y);
        myMusciView.hidden = FALSE;
    }
    else {
         self.srcView.contentSize = CGSizeMake(self.srcView.frame.size.width, frame.size.height + frame.origin.y);
        myMusciView.hidden = TRUE;;
    }
   
    
   
}

#pragma mark - Button Actions 

- (IBAction)btnShufflePressed {
     self.btnShuffle.selected = !self.btnShuffle.selected;
    if(self.btnShuffle.selected) {
        [self.btnShuffle setBackgroundColor:[UIColor lightGrayColor]];
    } else {
        [self.btnShuffle setBackgroundColor:[UIColor clearColor]];
    }
}

- (IBAction)btnForwardPressed {
    if(self.btnRepeat.selected) {
        [self playMusic];
    } else {
        if(self.btnShuffle.selected) {
            self.currentItem = arc4random()%self.playerSongs.count;
            [self playMusic];
        } else {
            if(self.currentItem < self.playerSongs.count-1) {
                self.currentItem ++;
                [self playMusic];
            }
        }
    }
}

- (IBAction)btnRewindPresed {
    if(self.btnRepeat.selected) {
        [self playMusic];
    } else {
        if(self.btnShuffle.selected) {
            self.currentItem = arc4random()%self.playerSongs.count;
            [self playMusic];
        } else {
            if(self.currentItem > 0) {
                self.currentItem --;
                [self playMusic];
            }
        }
    }
}

- (IBAction)btnPlayPressed:(UIButton *)sender {
    if(self.audioPlayer != nil && self.isVideoPlaying == FALSE) {
        if ((self.audioPlayer.rate != 0) && (self.audioPlayer.error == nil)) {
            self.btnPlay.selected = TRUE;
            [self.audioPlayer pause];
        } else {
            self.btnPlay.selected = FALSE;
            [self.audioPlayer play];
        }
    }
    
    if(self.moviePlayer != nil && self.isVideoPlaying == TRUE) {
        if(self.btnPlay.selected == TRUE) {
            self.btnPlay.selected = FALSE;
            [self.moviePlayer play];
        } else {
            self.btnPlay.selected = TRUE;
            [self.moviePlayer pause];

        }
    }
}

- (IBAction)btnRepeatPressed {
    self.btnRepeat.selected = !self.btnRepeat.selected;
    if(self.btnRepeat.selected) {
        [self.btnRepeat setBackgroundColor:[UIColor lightGrayColor]];
    } else {
        [self.btnRepeat setBackgroundColor:[UIColor clearColor]];
    }
}

- (IBAction)btnMoreInforPressed {
    PlayListActionViewController *viewControlelr = [[PlayListActionViewController alloc] initWithNibName:@"PlayListActionViewController" bundle:nil];
    viewControlelr.delegate = self;
    MediaItem *aItem = [self.playerSongs objectAtIndex:self.currentItem];
    viewControlelr.aItem = aItem;
    UINavigationController *navControlelr = [[UINavigationController alloc] initWithRootViewController:viewControlelr];
    navControlelr.navigationBarHidden = YES;
    self.playlistPopOver = [[UIPopoverController alloc] initWithContentViewController:navControlelr];
    CGRect frame = CGRectMake(self.view.frame.size.width, -15, 0, 0);
    
    
    [self.playlistPopOver presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    self.playlistPopOver.delegate = self;
    if ([self.playlistPopOver respondsToSelector:@selector(setBackgroundColor:)]) {
        [self.playlistPopOver setBackgroundColor:[UIColor blackColor]];
    }
    
}

#pragma mark - CollectionView DataSource

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == self.collectionMusic) {
        return self.aMedia.audioGallery.arrItmes.count;
    } else if(collectionView == self.collectionMyMusic) {
        return self.aMedia.myMusicGallery.arrItmes.count;
    } else if(collectionView == self.collectionVideo) {
        return self.aMedia.videoGallery.arrItmes.count;
    } else if(collectionView == self.collectionPhoto) {
        return self.aMedia.photoGallery.arrItmes.count;
    } else if(collectionView == self.collectionVine) {
        return self.aMedia.vineGallery.arrItmes.count;
    } else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.collectionVideo) {
        MediaItem *aItem = [self.aMedia.videoGallery.arrItmes objectAtIndex:indexPath.row];
        VideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoCell" forIndexPath:indexPath];
        cell.btnPlay.tag = indexPath.row;
        cell.lblVideoName.text = aItem.title;
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
        cell.btnMore.tag = indexPath.row;
         [cell.btnMore addTarget:self action:@selector(videoMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPlay addTarget:self action:@selector(videPlayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else if(collectionView == self.collectionPhoto) {
        MediaItem *aItem = [self.aMedia.photoGallery.arrItmes objectAtIndex:indexPath.row];
        PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
        cell.btnPlay.hidden = TRUE;
        cell.imgView.image = [UIImage imageWithContentsOfFile:aItem.URL];
        return cell;
    } else if(collectionView == self.collectionVine) {
        MediaItem *aItem = [self.aMedia.vineGallery.arrItmes objectAtIndex:indexPath.row];
        PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
        cell.btnPlay.hidden = FALSE;
        [cell.btnPlay addTarget:self action:@selector(btnYoutubeVideoPlayPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnPlay.tag = indexPath.row;
        cell.imgView.image = aItem.previewImage;
        return cell;
    } else if(collectionView == self.collectionMusic) {
        MediaItem *aItem = [self.aMedia.audioGallery.arrItmes objectAtIndex:indexPath.row];
        MusicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MusicCell" forIndexPath:indexPath];
        cell.btnPlay.tag = indexPath.row;
        cell.lblArtistName.text = aItem.title;
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
        cell.btnMore.tag = indexPath.row;
        [cell.btnMore addTarget:self action:@selector(musicMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
       [cell.btnPlay addTarget:self action:@selector(musicPlayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return cell;

    } else {
        MediaItem *aItem = [self.aMedia.myMusicGallery.arrItmes objectAtIndex:indexPath.row];
        MusicCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MusicCell" forIndexPath:indexPath];
        cell.btnPlay.tag = indexPath.row;
        cell.lblArtistName.text = aItem.title;
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
        cell.btnMore.tag = indexPath.row;
        [cell.btnMore addTarget:self action:@selector(myMusicMoreButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnPlay addTarget:self action:@selector(myMusicPlayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.collectionPhoto) {
        QLPreviewController *previewController=[[QLPreviewController alloc]init];
        previewController.delegate=self;
        previewController.dataSource=self;
        previewController.currentPreviewItemIndex = indexPath.row;
        [self presentViewController:previewController animated:YES completion:nil];
        [previewController.navigationItem setRightBarButtonItem:nil];
    }
}

- (void)btnYoutubeVideoPlayPressed:(BSOButton *)sender {
    MediaItem *aItem = [self.aMedia.vineGallery.arrItmes objectAtIndex:sender.tag];
    self.videoPlayer = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL fileURLWithPath:aItem.URL]];
    [self.videoPlayer setScalingMode:MPMovieScalingModeAspectFill];
    self.videoPlayer.view.tag = 1;
    [self.videoPlayer.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight];
    [[[[UIApplication sharedApplication] windows] objectAtIndex:1]addSubview:self.videoPlayer.view];
    self.videoPlayer.controlStyle = MPMovieControlStyleDefault;
    [self.videoPlayer setFullscreen:YES animated:YES];
    self.videoPlayer.shouldAutoplay = NO;
    [self.videoPlayer play];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.videoPlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDonePressed:) name:MPMoviePlayerDidExitFullscreenNotification object:self.videoPlayer];
}

- (void)musicMoreButtonPressed:(BSOButton *)sender {
    MediaItem *aItem = [self.aMedia.audioGallery.arrItmes objectAtIndex:sender.tag];
    [self.aMedia.myMusicGallery.arrItmes addObject:aItem];
    [MediaItem writeObjectsToDist:self.aMedia.myMusicGallery.arrItmes];
    [self showMedia];
    [self.collectionMyMusic reloadData];
}

- (void)myMusicMoreButtonPressed:(BSOButton *)sender {
    MediaItem *aItem = [self.aMedia.myMusicGallery.arrItmes objectAtIndex:sender.tag];
    [self.aMedia.myMusicGallery.arrItmes removeObject:aItem];
    [MediaItem writeObjectsToDist:self.aMedia.myMusicGallery.arrItmes];
    [self showMedia];
    [self.collectionMyMusic reloadData];
}

- (void)videoMoreButtonPressed:(BSOButton *)sender {
    MediaItem *aItem = [self.aMedia.videoGallery.arrItmes objectAtIndex:sender.tag];
    [self.aMedia.myMusicGallery.arrItmes addObject:aItem];
    [MediaItem writeObjectsToDist:self.aMedia.myMusicGallery.arrItmes];
    [self showMedia];
    [self.collectionMyMusic reloadData];
}

- (void)moviePlayBackDidFinish:(NSNotification*)notification {
    self.videoPlayer = [notification object];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoPlayer];
    
    if ([self.videoPlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
        [self.videoPlayer.view removeFromSuperview];
    }
}
- (void) moviePlayBackDonePressed:(NSNotification*)notification {
    [self.videoPlayer stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.videoPlayer];
    
    
    if ([self.videoPlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
        [self.videoPlayer.view removeFromSuperview];
    }
    self.videoPlayer=nil;
}


- (void)videPlayButtonPressed:(BSOButton *)sender {
    [self.playerSongs removeAllObjects];
    [self.playerSongs addObjectsFromArray:self.aMedia.videoGallery.arrItmes];
    self.currentItem = sender.tag;
    [self showPlayerView];
    [self playMusic];
}

- (void)musicPlayButtonPressed:(BSOButton *)sender {
    [self.playerSongs removeAllObjects];
    [self.playerSongs addObjectsFromArray:self.aMedia.audioGallery.arrItmes];
    self.currentItem = sender.tag;
    [self playMusic];
    [self showPlayerView];
}

- (void)myMusicPlayButtonPressed:(BSOButton *)sender {
    [self.playerSongs removeAllObjects];
    [self.playerSongs addObjectsFromArray:self.aMedia.myMusicGallery.arrItmes];
    self.currentItem = sender.tag;
    [self playMusic];
    [self showPlayerView];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.aMedia.podcastGallery.arrItmes.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    headerView.backgroundColor = RGB(26, 26, 26);
    BSOLabel *lbl = [[BSOLabel alloc] initWithFrame:CGRectMake(20, 0, 300, 40)];
    lbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:15];
    lbl.text = @"PODCASTS";
    lbl.textColor = [UIColor whiteColor];
    [headerView addSubview:lbl];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PodcastCell *cell = (PodcastCell *) [tableView dequeueReusableCellWithIdentifier:@"PodcastCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PodcastCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell.btnPlay addTarget:self action:@selector(podcastPlayButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMore addTarget:self action:@selector(podcastMoreButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    MediaItem *aItem = [self.aMedia.podcastGallery.arrItmes objectAtIndex:indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
    cell.lblTitle.text = aItem.title;
    cell.lblOther.text = aItem.credit;
    cell.lblDate.text = @"";
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)podcastPlayButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        [self.playerSongs removeAllObjects];
        [self.playerSongs addObjectsFromArray:self.aMedia.podcastGallery.arrItmes];
        self.currentItem = indexPath.row;
        [self playMusic];
        [self showPlayerView];
    }
}

- (void)podcastMoreButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        MediaItem *aItem = [self.aMedia.podcastGallery.arrItmes objectAtIndex:indexPath.row];
        [self.aMedia.myMusicGallery.arrItmes addObject:aItem];
        [MediaItem writeObjectsToDist:self.aMedia.myMusicGallery.arrItmes];
        [self showMedia];
        [self.collectionMyMusic reloadData];
    }
}

- (void)playMusic {
    if(self.playerSongs.count == 0) {
        return;
    }
    
    MediaItem *aItem = [self.playerSongs objectAtIndex:self.currentItem];

    self.isVideoPlaying = aItem.isVideo;
    NSLog(@"%@",aItem.URL);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(self.audioPlayer) {
        [self.audioPlayer pause];
        [self.audioPlayer setRate:0];
        self.audioPlayer = nil;
    }
    if(self.moviePlayer) {
        [self.moviePlayer stop];
        [self.moviePlayer.view removeFromSuperview];
        self.moviePlayer = nil;
    }
    
    if(self.isVideoPlaying) {
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:aItem.URL]];
        self.moviePlayer.controlStyle = MPMovieControlStyleNone;
        [self.moviePlayer setMovieSourceType:MPMovieSourceTypeStreaming];
        self.moviePlayer.view.frame = self.imgCover.frame;
        self.moviePlayer.view.tag = 1;
        [self.playerView addSubview:self.moviePlayer.view];
        
        [self.moviePlayer setUseApplicationAudioSession:NO];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoItemDidReachEnd) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoItemDidChangState:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoItemDidNowPlaying) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
        
        [self.moviePlayer play];
        
    } else {
        AVPlayerItem *aPlayerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:aItem.URL]];
        self.audioPlayer = [[AVPlayer alloc] initWithPlayerItem:aPlayerItem];
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(playerItemDidReachEnd:)
                                                     name: AVPlayerItemDidPlayToEndTimeNotification
                                                   object: [self.audioPlayer currentItem]];
        [self.audioPlayer play];
        
        [self.imgCover sd_setImageWithURL:[NSURL URLWithString:aItem.thumbURL] placeholderImage:[UIImage imageNamed:@"no_image.png"]];
    }
   
    self.lblMusicTitle.text = aItem.title;
    self.btnPlay.selected = FALSE;
}

- (void)playerItemDidReachEnd:(id)sender {
     [self btnForwardPressed];
}

- (void)videoItemDidChangState:(NSNotification*)notification {
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
        self.btnPlay.selected = FALSE;
    }
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused) {
        self.btnPlay.selected = TRUE;
    }
}

- (void)videoItemDidNowPlaying {
    
}

- (void)videoItemDidReachEnd {
    [self btnForwardPressed];
}

#pragma mark - QuickLook
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return self.aMedia.photoGallery.arrItmes.count;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    MediaItem *aItem = [self.aMedia.photoGallery.arrItmes objectAtIndex:index];
    return [NSURL fileURLWithPath:aItem.URL];
}

#pragma mark - PlayListActionDelegate

- (void)MyMusicActionShufflePressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyMusicActionPlayPressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyPlaylistShufflePressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyPlaylistPlayPressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyPlaylistDeletePressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyPlaylistDeleteTrackPressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
}

- (void)MyMusicActionSharePressed {
    [self.playlistPopOver dismissPopoverAnimated:YES];
    self.playlistPopOver = nil;
    
    UIActionSheet *actSeet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Twitter",@"Facebook",@"Google+",@"Instagram", nil];
    [actSeet showFromRect:self.btnMusicMore.frame inView:self.btnMusicMore.superview animated:YES];
}

- (void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing *)view {
    *rect = CGRectMake(self.view.frame.size.width, -15, 0, 0);
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    [actionSheet dismissWithClickedButtonIndex:0 animated:NO];
    
    MediaItem *aItem = [self.playerSongs objectAtIndex:self.currentItem];
    dispatch_async(dispatch_get_main_queue(), ^ {
        if(buttonIndex == 0) {
            [Utility shareOnTwiitter:self text:aItem.title image:self.imgCover.image];
        } else if(buttonIndex == 1) {
            [Utility shareOnFacebook:self text:aItem.title image:self.imgCover.image];
        } else if(buttonIndex == 2) {
            if (![GPPSignIn sharedInstance].authentication ||
                ![[GPPSignIn sharedInstance].scopes containsObject:
                  kGTLAuthScopePlusLogin]) {
                    [Utility signInGooglePlus:self];
                    
                } else {
                    [Utility shareOnGoolgePlus:self text:aItem.title image:self.imgCover.image];
                }
        }else if(buttonIndex == 3) {
            [Utility shareOnInstagram:self text:aItem.title image:self.imgCover.image frame:self.imgCover.frame];
        }

    });
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    
    MediaItem *aItem = [self.playerSongs objectAtIndex:self.currentItem];
    
    NSLog(@"Received error %@ and auth object %@", error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        [Utility shareOnGoolgePlus:self text:aItem.title image:self.imgCover.image];
    }
}

- (void)finishedSharingWithError:(NSError *)error {
    NSString *text;
    
    if (!error) {
        [Utility ShowAlertWithTitle:@"Success" Message:@"Successfully posted to Google +"];
    } else if (error.code == kGPPErrorShareboxCanceled) {
        text = @"Canceled";
    } else {
        text = [NSString stringWithFormat:@"Error (%@)", [error localizedDescription]];
    }
    
    NSLog(@"Status: %@", text);
}


@end
