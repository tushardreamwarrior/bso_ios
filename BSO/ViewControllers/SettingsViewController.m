//
//  SettingsViewController.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SettingsViewController.h"
#import "UserLoginViewController.h"
#import "UserProfileViewController.h"
#import "UserWalletViewController.h"
#import "CreatPinViewController.h"
#import "SMLoginViewController.h"
#import "SetBGViewController.h"
#import "NotificationListViewController.h"
#import "CardDetailViewController.h"

@implementation SettingsSection
- (instancetype)init {
    self = [super init];
    self.arrRows = [[NSArray alloc] init];
    return self;
}
@end

@interface SettingsViewController () <UITableViewDataSource,UITableViewDelegate,CreatPinViewControllerDelegate,UserLoginViewDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) NSMutableArray *arrItems;
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) CreatPinViewController *creatPinViewController;
@property (nonatomic, strong) NSString *loginAction;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
    SettingsSection *sectionOne = [[SettingsSection alloc] init];
    sectionOne.name = @"Preferences";
    sectionOne.arrRows = @[@"Enable Push",@"Auto Update",@"Enable Login",@"Enable 4-Digit Pin",@"Turn on Geolocation"];
    
    SettingsSection *sectionTwo = [[SettingsSection alloc] init];
    sectionTwo.name = @"Personalization";
    
    sectionTwo.arrRows = @[@"User Login",@"Profile",@"Social Media Logins",@"Set 4-Digit Pin",@"My Backgrounds"];
    
    SettingsSection *sectionThree = [[SettingsSection alloc] init];
    sectionThree.name = @"Wallet";
    
    SettingsSection *sectionFour = [[SettingsSection alloc] init];
    sectionFour.name = @"Notifications";
    
    SettingsSection *sectionFive = [[SettingsSection alloc] init];
    sectionFive.name = @"One Click Payment";
    
    _arrItems = [[NSMutableArray alloc] initWithObjects:sectionOne,sectionTwo,sectionThree,sectionFour,sectionFive,nil];
    self.tblView.tableFooterView = [[UIView alloc] init];
    
     [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
    self.title = @"Settings";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackPressed)];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tblView reloadData];
}

#pragma mark - TableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.arrItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 45)];
    SettingsSection *aSection = [self.arrItems objectAtIndex:section];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:aSection.name forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:BSO_TEXT_COLOR forState:UIControlStateSelected];
    btn.tag = section;
    btn.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:20.0];
    [btn addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(30, 0,  tableView.frame.size.width-30, 45);
    [headerView addSubview:btn];
    [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    btn.selected = aSection.isOpened;
    headerView.backgroundColor = [UIColor blackColor];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SettingsSection *aSection = [self.arrItems objectAtIndex:section];
    if(aSection.isOpened) {
     return   aSection.arrRows.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:18.0];
    }
    if(indexPath.section == 0) {
        UISwitch *sw = [[UISwitch alloc] init];
        sw.tag = indexPath.row;
        if(indexPath.row == 0) {
            sw.on = [GET_UD(ENABLE_PUSH) intValue];
            
        } else if(indexPath.row == 1) {
            sw.on = [GET_UD(AUTO_UPDATE) intValue];
        } else if(indexPath.row == 2) {
            sw.on = [GET_UD(ENABLE_TLOGIN) intValue];
        } else if(indexPath.row == 3) {
            sw.on = [GET_UD(ENABLE_4DIGITPIN) intValue];
        } else if(indexPath.row == 4) {
            sw.on = [GET_UD(ENABLE_GEOLOCATION) intValue];
        }
        [sw addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = sw;
    } else if(indexPath.section == 1) {
        if(indexPath.row == 0) {
            if(GET_UD(SESSION_ID)) {
                
            } else {
                
            }
        }
        UIImageView *imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightArrow"]];
        imgArrow.frame = CGRectMake(0, 0, 32.0, 32.0);
        cell.accessoryView = imgArrow;
    
    } else {
        cell.accessoryView = nil;
    }
   
    SettingsSection *aSection = [self.arrItems objectAtIndex:indexPath.section];
    
    cell.textLabel.text = [NSString stringWithFormat:@"            %@",aSection.arrRows[indexPath.row]];
    if(indexPath.section == 1 && indexPath.row == 0) {

        if(GET_UD(SESSION_ID)) {
            cell.textLabel.text = [NSString stringWithFormat:@"            Logout"];
        } else {
            cell.textLabel.text = [NSString stringWithFormat:@"            User Login"];
        }
        
    }
    cell.backgroundColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 0, 0,0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 65)];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section  == 1) {
        if(indexPath.row == 0) {
            if(GET_UD(SESSION_ID)) {
                [Utility showLoader];
                [self.bsoManager userLogout:GET_UD(USER_EMAIL) success:^(id responseObject) {
                    SET_UD(SESSION_ID, nil);
                    SET_UD(USER_EMAIL, nil);
                    SET_UD(USER_PASSWORD, nil);
                    
                    [self.tblView reloadData];
                    [Utility hideLoader];
                } failure:^(NSError *error) {
                    [Utility hideLoader];
                }];
               
            } else {
                UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
                [self.navigationController pushViewController:viewController animated:YES];
                
            }
        }
        else if(indexPath.row == 1) {
            UserProfileViewController *viewController = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 2) {
            SMLoginViewController *viewController = [[SMLoginViewController alloc] initWithNibName:@"SMLoginViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if(indexPath.row == 3) {
//            CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
//            CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:[tableView superview]];
            
            
            self.creatPinViewController = [[CreatPinViewController alloc] initWithNibName:@"CreatPinViewController" bundle:nil];
            self.creatPinViewController.preferredContentSize = CGSizeMake(748, 365);
            self.creatPinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
//            self.creatPinViewController.view.layer.borderColor = [UIColor whiteColor].CGColor;
//            self.creatPinViewController.view.layer.borderWidth = 1.0;
//            self.creatPinViewController.view.layer.cornerRadius = 5;
            self.creatPinViewController.delegate = self;
            [self presentViewController:self.creatPinViewController animated:YES completion:nil];
//         
//            CGRect frame = self.creatPinViewController.view.frame;
//            frame.size.width  = self.view.bounds.size.width;
//            frame.origin.y = rectInSuperview.origin.y;
//            [self addChildViewController:self.creatPinViewController];
//            [self.view addSubview:self.creatPinViewController.view];
//            self.creatPinViewController.view.frame = frame;
//            [self.creatPinViewController didMoveToParentViewController:self];
        }
        else if(indexPath.row == 4) {
            SetBGViewController *viewController = [[SetBGViewController alloc] initWithNibName:@"SetBGViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }

    }
}

- (void)headerButtonPressed:(UIButton *)sender {
    if(sender.tag == 2) {
        if(GET_UD(SESSION_ID)) {
            UserWalletViewController *viewController = [[UserWalletViewController alloc] initWithNibName:@"UserWalletViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
            
        } else {
            UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
            viewController.delegate = self;
            self.loginAction = @"Wallet";
            [self.navigationController pushViewController:viewController animated:YES];
        }
        
       
    } else if(sender.tag == 3) {
        NotificationListViewController *viewController = [[NotificationListViewController alloc] initWithNibName:@"NotificationListViewController" bundle:nil];
        [self.navigationController pushViewController:viewController animated:YES];
        
    } else if(sender.tag == 4) {
        if(GET_UD(SESSION_ID)) {
            CardDetailViewController *viewController = [[CardDetailViewController alloc] initWithNibName:@"CardDetailViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        } else {
            UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
            viewController.delegate = self;
            self.loginAction = @"Payment";
            [self.navigationController pushViewController:viewController animated:YES];
        }
      
    } else {
        SettingsSection *aSection = [self.arrItems objectAtIndex:sender.tag];
        aSection.isOpened = !aSection.isOpened;
        [self.tblView beginUpdates];
        if(aSection.isOpened ) {
            for (NSUInteger i = 0; i < aSection.arrRows.count; i++) {
                [self.tblView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:sender.tag]] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        } else {
            for (NSUInteger i = 0; i < aSection.arrRows.count; i++) {
                [self.tblView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:sender.tag]] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }
        
        [self.tblView endUpdates];
        sender.selected = aSection.isOpened;
    }
}

- (void)userLoginSuccessfully:(BOOL)success {
    if(success) {
        if([self.loginAction isEqualToString:@"Wallet"]) {
            UserWalletViewController *viewController = [[UserWalletViewController alloc] initWithNibName:@"UserWalletViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        } else  if([self.loginAction isEqualToString:@"Payment"]) {
            CardDetailViewController *viewController = [[CardDetailViewController alloc] initWithNibName:@"CardDetailViewController" bundle:nil];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        
    }
}

- (void)switchValueChanged:(UISwitch *)sender {
   
    NSString *value = [NSString stringWithFormat:@"%d",sender.on];
   
    if(sender.tag == 0) {
        SET_UD(ENABLE_PUSH, value);
        if(GET_UD(ENABLE_PUSH)) {
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
        }
    }
    else if(sender.tag == 1) {
        SET_UD(AUTO_UPDATE, value);
    }
    else if(sender.tag == 2) {
        SET_UD(ENABLE_TLOGIN, value);
    }
    else if(sender.tag == 3) {
        SET_UD(ENABLE_4DIGITPIN, value);
    }
    else if(sender.tag == 4) {
        SET_UD(ENABLE_GEOLOCATION, value);
    }
}

- (IBAction)btnLogoPressed {
    
}

- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)createPinViewCancelButtonPressed {
    [self.creatPinViewController dismissViewControllerAnimated:YES completion:nil];
//    [self.creatPinViewController willMoveToParentViewController:nil];
//    [self.creatPinViewController.view removeFromSuperview];
//    [self.creatPinViewController removeFromParentViewController];
//    
//    self.creatPinViewController = nil;
}

- (void)createPinViewSubmitButtonPressed:(NSString *)pin {
    [self createPinViewCancelButtonPressed];
}

@end