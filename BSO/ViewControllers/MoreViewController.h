//
//  MoreViewController.h
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MoreViewControllerDelegate <NSObject>
- (void)MoreViewBakButtonPressed;
@optional


@end


@interface MoreViewController : UIViewController
- (void)refreshContentForMoreViewScreen;
@property (nonatomic, assign) id<MoreViewControllerDelegate> delegate;

@end
