//
//  MyMusicActionViewController.m
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MyMusicActionViewController.h"

@interface MyMusicActionViewController ()
@property (nonatomic, strong) IBOutlet UITableView *table;
@end

@implementation MyMusicActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"anyCell"];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:20.0];
        cell.textLabel.textColor = [UIColor whiteColor];
        
        cell.detailTextLabel.font = [UIFont fontWithName:@"OpenSans" size:18];
        cell.detailTextLabel.textColor = RGB(137, 137, 137);
        
        UIButton *btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnPlay setImage:[UIImage imageNamed:@"myMusic_Play"] forState:UIControlStateNormal];
        btnPlay.frame = CGRectMake(0, 0, 25, 25);
        [btnPlay addTarget:self action:@selector(btnPlayPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = btnPlay;
    }
    cell.textLabel.text = @"Arstist Name";
    cell.detailTextLabel.text = @"Track title and more information";
    UIButton *btnPlay = (UIButton *)cell.accessoryView;
    btnPlay.tag = indexPath.row;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)btnPlayPressed:(UIButton *)sender {
    if([self.delegate respondsToSelector:@selector(MyMusicActionPlayPressed)]) {
        [self.delegate MyMusicActionPlayPressed];
    }
}

- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShufflePressed {
    if([self.delegate respondsToSelector:@selector(MyMusicActionShufflePressed)]) {
        [self.delegate MyMusicActionShufflePressed];
    }
}

- (CGSize)preferredContentSize {
    return CGSizeMake(465, 568);
}

@end