//
//  VineGalleryViewController.m
//  BSO
//
//  Created by Imobtree Solutions on 4/28/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "VineGalleryViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "MAB_GoogleOAuthTouchViewController.h"
#import "SDAVAssetExportSession.h"
#import "RestClient.h"
#import "MABYT3_APIRequest.h"

#define STEP 5

@interface VineGalleryViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIPopoverControllerDelegate,UIScrollViewDelegate>

@end

@implementation VineGalleryViewController {
    float width;
    float height;
    UIPopoverController *act;
    BOOL isBottomImageTap;
    NSTimer *timer;
    IBOutlet UIImageView *imgArrow;
 
    IBOutlet UIImageView *imgThumbnail;
    
    NSURL *PlayUrl;
    NSString *strPlayPath;
    UIButton *btnSelected;
    IBOutlet UITextField *txtAddtext;
    IBOutlet UIButton *btnNext;
    IBOutlet UILabel *lblAddText;
    IBOutlet UIButton *btnUpload;
    bool isFirstTime;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitleCaption.text = @"Choose a Video";
    self.btn1.selected = true;
    self.btn2.enabled = NO;
    self.btn3.enabled = NO;
    self.btn4.enabled = NO;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.scrView3.hidden = TRUE;
    self.scrView3.contentSize = CGSizeMake(self.scrView3.frame.size.width, self.view3.frame.size.height);

    self.view4.hidden = true;
    self.scrView4.hidden = TRUE;
    self.scrView4.contentSize = CGSizeMake(self.scrView4.frame.size.width, self.view4.frame.size.height);
    self.btnDone.hidden = true;
    btnNext.hidden = true;
    
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    self.imgMainImage.layer.cornerRadius = 10.0f;
    self.imgMainImage.layer.borderWidth = 2.0f;
    self.imgMainImage.layer.borderColor = [UIColor whiteColor].CGColor;
    imgThumbnail.layer.cornerRadius = 10.0f;
    imgThumbnail.layer.borderWidth = 2.0f;
    imgThumbnail.layer.borderColor = [UIColor whiteColor].CGColor;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeLoginSuccess) name:@"YoutubeLogin" object:nil];
    
    
      
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    
    btnSelected = self.btn1;
    [self adjustInnerViews:[UIScreen mainScreen].bounds.size];
    [self setArrorPosition];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCancelPressed:)];
    isFirstTime = TRUE;
}

- (void)viewDidAppear:(BOOL)animated {
    if(isFirstTime) {
        isFirstTime = FALSE;
        [self setArrorPosition];
    }
}


- (void) handleTap:(UITapGestureRecognizer *)tap {
    if([txtAddtext isFirstResponder]) {
        [txtAddtext resignFirstResponder];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    if (GET_UD(YOUTUBE_TOKEN)) {
        [btnUpload setTitle:@"Upload to YouTube" forState:UIControlStateNormal];
    } else {
        [btnUpload setTitle:@"Login to YouTube" forState:UIControlStateNormal];
    }

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)btnUploadToVinePressed:(UIButton *)sender {
    
    if (GET_UD(YOUTUBE_TOKEN)) {
            [Utility showLoader];
        [[RestClient sharedInstance] uploadYouTubeVideo:txtAddtext.text videoPath:PlayUrl apiKey:apiKey succes:^(NSURLSessionDataTask *task, id responseObject) {
            [Utility hideLoader];
            [Utility ShowAlertWithTitle:@"Success" Message:@"Video uploaded"];
       } failure:^(NSURLSessionDataTask *task, NSError *error) {
           [Utility hideLoader];
       }];
    } else {
        MAB_GoogleOAuthTouchViewController *signInVC = [[MAB_GoogleOAuthTouchViewController alloc] init];
        signInVC.scope = scope;
        signInVC.clientID = kMyClientID;
        signInVC.clientSecret = kMyClientSecret;
        [self presentViewController:signInVC animated:YES completion:^{
            
        }];
    }
}

- (void)youTubeLoginSuccess {
    [btnUpload setTitle:@"Upload to YouTube" forState:UIControlStateNormal];
    
    [[RestClient sharedInstance] uploadYouTubeVideo:txtAddtext.text videoPath:PlayUrl apiKey:apiKey succes:^(NSURLSessionDataTask *task, id responseObject) {
        [Utility hideLoader];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [Utility hideLoader];
    }];
}

- (IBAction)btnNextPressed:(id)sender {
    lblAddText.text = txtAddtext.text;
    self.btn3.enabled = YES;
    [self btn3Pressed:self.btn3];
    self.imgMainImage.image = imgThumbnail.image;
}

- (IBAction)btnCancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btnSelectPhoto:(UIButton *)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.videoMaximumDuration = 60;
    imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, nil];

    if(sender.tag == 101)
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Place image picker on the screen
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
    PlayUrl = videoUrl;
  
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
   
    [self getVideoThumbnail:videoUrl];
    self.btn2.enabled = YES;
    [self btn2Pressed:self.btn2];
}

- (void)getVideoThumbnail:(NSURL *)videoURL {
    MPMoviePlayerController *moviePlayer1 = [[MPMoviePlayerController alloc]  initWithContentURL:videoURL];
    imgThumbnail.image = [moviePlayer1 thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    [moviePlayer1 stop];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)playPause:(id)sender {
    NSURL *playURL = PlayUrl;
    self.videoPlayer = [[MPMoviePlayerController alloc]initWithContentURL:playURL];
    [self.videoPlayer setScalingMode:MPMovieScalingModeAspectFill];
    [self.videoPlayer.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight];
    [[[[UIApplication sharedApplication] windows] objectAtIndex:1]addSubview:self.videoPlayer.view];
    self.videoPlayer.controlStyle = MPMovieControlStyleDefault;
    [self.videoPlayer setFullscreen:YES animated:YES];
    self.videoPlayer.shouldAutoplay = NO;
    [self.videoPlayer play];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.videoPlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDonePressed:) name:MPMoviePlayerDidExitFullscreenNotification object:self.videoPlayer];
}

- (void)moviePlayBackDidFinish:(NSNotification*)notification {
    self.videoPlayer = [notification object];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoPlayer];
    
    if ([self.videoPlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
        [self.videoPlayer setFullscreen:NO animated:YES];
        [self.videoPlayer.view removeFromSuperview];
    }
}

- (void) moviePlayBackDonePressed:(NSNotification*)notification {
    [self.videoPlayer stop];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.videoPlayer];
    
    
    if ([self.videoPlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
        [self.videoPlayer setFullscreen:NO animated:YES];
        [self.videoPlayer.view removeFromSuperview];
    }
    self.videoPlayer=nil;
}

- (IBAction)btn1Pressed:(id)sender {
    self.btnDone.hidden = true;
    self.btn1.selected = true;
    btnNext.hidden = true;
    
    btnSelected = self.btn1;
    [self setArrorPosition];
    
    self.btn2.selected = false;
    self.btn3.selected = false;
    self.btn4.selected = false;
    self.lblTitleCaption.text = @"Choose a Video";
    self.view1.hidden = false;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = true;
    self.scrView4.hidden = true;
    self.scrView3.hidden = true;
    if([txtAddtext isFirstResponder]) {
        [txtAddtext resignFirstResponder];
    }
}

- (IBAction)btn2Pressed:(id)sender {
    btnNext.hidden = false;
    self.btnDone.hidden = true;
    self.btn1.selected = false;
    self.btn2.selected = true;
    self.btn3.selected = false;
    self.btn4.selected = false;
    
    btnSelected = self.btn2;
    [self setArrorPosition];
    
    
    self.lblTitleCaption.text = @"Add Text";
    self.view2.hidden = false;
    self.view1.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = true;
    self.scrView4.hidden = true;
    self.scrView3.hidden = true;
    if([txtAddtext isFirstResponder]) {
        [txtAddtext resignFirstResponder];
    }
}

- (IBAction)btn3Pressed:(id)sender {
    btnNext.hidden = true;
    self.btnDone.hidden = false;
    self.btn1.selected = false;
    self.btn2.selected = false;
    self.btn3.selected = true;
    self.btn4.selected = false;
    
    btnSelected = self.btn3;
    [self setArrorPosition];
    
    self.lblTitleCaption.text = @"Upload To Youtube";
    self.view1.hidden = true;
    self.view2.hidden = true;
    self.view3.hidden = false;
    self.view4.hidden = true;
    self.scrView4.hidden = true;
    self.scrView3.hidden = false;
    if([txtAddtext isFirstResponder]) {
        [txtAddtext resignFirstResponder];
    }
}

- (IBAction)btn4Pressed:(id)sender {
    btnNext.hidden = true;
    self.btnDone.hidden = true;
    self.btn1.selected = false;
    self.btn2.selected = false;
    self.btn3.selected = false;
    self.btn4.selected = true;
    
    btnSelected = self.btn4;
    [self setArrorPosition];
    
    self.lblTitleCaption.text = @"Share Your Friends";
    self.view1.hidden = true;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = false;
    self.scrView4.hidden = false;
    self.scrView3.hidden = true;
    if([txtAddtext isFirstResponder]) {
        [txtAddtext resignFirstResponder];
    }
}

- (IBAction)btnDonePressed:(id)sender {
    self.imgFinal.image = self.imgMainImage.image;
    self.imgFinal.layer.cornerRadius = 20.0f;
    self.imgFinal.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgFinal.layer.borderWidth = 2.0f;
    self.imgFinal.layer.masksToBounds = YES;
    
    self.btn4.enabled = YES;
    [self btn4Pressed:self.btn4];
    
    NSString *str = [NSString stringWithFormat:@"%@_%@",lblAddText.text,[Utility stringFromDate:[NSDate date] format:SERVER_DATE_TIME_FORMAT]];
    
    NSString *videoPath1 = FILE_PATH(@"Videos");
    NSString *fullPath = [videoPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",str]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:fullPath error:nil];
    }
    
    [[NSFileManager defaultManager] copyItemAtPath:PlayUrl.path toPath:fullPath error:nil];
}

- (IBAction)btnSavePressed:(id)sender {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UISaveVideoAtPathToSavedPhotosAlbum([PlayUrl relativePath], self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
    });
   
}

- (void) video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (!error){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Successfully Saved!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
         [alert show];

    } else {
        // Error saving
    }
}

- (IBAction)btnSharePressed:(id)sender {
  
    NSArray *activityItems = @[PlayUrl];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    [activityController setValue:@"BSO" forKey:@"subject"];
    act = [[UIPopoverController alloc] initWithContentViewController:activityController];
    [act presentPopoverFromRect:CGRectMake(self.btnShare.frame.origin.x+50, self.btnShare.frame.origin.y,self.btnShare.frame.size.width/2, self.btnShare.frame.size.height) inView:self.view4 permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    act.delegate = self;
    [self presentViewController:activityController animated:YES completion:nil];
}



- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // best call super just in case
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // will execute before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        [self setArrorPosition];
        [self adjustInnerViews:size];
        
    } completion:^(id  _Nonnull context) {
        
        // will execute after rotation
        
    }];
}

- (void)setArrorPosition {
    
    CGPoint center = imgArrow.center;
    center.x = btnSelected.center.x;
    imgArrow.center = center;
    
    self.imgSelectedBorder.frame = btnSelected.frame;
    
}

- (void)adjustInnerViews:(CGSize )size {
    if(size.width == 768) { // Portrait
        UIView *aView = [self.view2 viewWithTag:1];
        CGRect frame1 = aView.frame;
        frame1.origin.y = 50;
        frame1.origin.x = (size.width - frame1.size.width) / 2;
        aView.frame = frame1;
        
        aView = [self.view2 viewWithTag:2];
        CGRect frame2 = aView.frame;
        frame2.origin.y = frame1.origin.y + frame1.size.height + 60;
        frame2.origin.x = (size.width - frame2.size.width) / 2;
        aView.frame = frame2;
        
    } else { // LandScape
        UIView *aView = [self.view2 viewWithTag:1];
        CGRect frame1 = aView.frame;
        frame1.origin.x = 100;
        frame1.origin.y = 100;
        aView.frame = frame1;
        
        aView = [self.view2 viewWithTag:2];
        CGRect frame2 = aView.frame;
        frame2.origin.y = 100;
        frame2.origin.x = (size.width - frame2.size.width) - 100;
        aView.frame = frame2;
        
    }
}


@end
