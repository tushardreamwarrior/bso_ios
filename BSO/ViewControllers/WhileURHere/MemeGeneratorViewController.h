//
//  MemeGeneratorViewController.h
//  BSO
//
//  Created by MAC on 01/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemeLabel.h"

@interface MemeGeneratorViewController : UIViewController
@property (nonatomic, strong) IBOutlet MemeLabel *lblTopMeme;
@property (nonatomic, strong) IBOutlet MemeLabel *lblBottomMeme;
@property (nonatomic, strong) IBOutlet UITextField *txtToolBar;
@property (nonatomic, strong) IBOutlet UITextField *txtBottom;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewTop;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewBottom;

@property (nonatomic, strong) IBOutlet UIButton *btnCamera;
@property (nonatomic, strong) IBOutlet UIButton *btnAlbum;
@property (nonatomic, strong) IBOutlet UIButton *btn1;
@property (nonatomic, strong) IBOutlet UIButton *btn2;
@property (nonatomic, strong) IBOutlet UIButton *btn3;
@property (nonatomic, strong) IBOutlet UIButton *btn4;

@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;
@property (nonatomic, strong) IBOutlet UIView *view1;
@property (nonatomic, strong) IBOutlet UIView *view2;
@property (nonatomic, strong) IBOutlet UIView *view3;
@property (nonatomic, strong) IBOutlet UIView *view4;
@property (nonatomic, strong) IBOutlet UIScrollView *scrView4;
@property (nonatomic, strong) IBOutlet UIImageView *imgMainImage;
@property (nonatomic, strong) IBOutlet UIImageView *imgFinal;
@property (nonatomic, strong) IBOutlet UIButton *btnDone;
@property (nonatomic, strong) IBOutlet UIButton *btnShare;
@property (nonatomic, strong) IBOutlet UIButton *btnSave;
@property (nonatomic, retain) IBOutlet UIView *inputAccView;

@property (nonatomic, strong) IBOutlet UIView *viewScrenshot;
@property (nonatomic, strong) IBOutlet UIImageView *imgScreenShot;

@property (nonatomic, strong) IBOutlet UILabel *lblTopTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblBottomTitle;

@property (nonatomic, strong) IBOutlet UIButton *btnMeme;

@property (nonatomic ,strong) IBOutlet UIScrollView *scrlView;

@property (nonatomic, strong) IBOutlet UILabel *lblTitleCaption;

@end
