//
//  PhotoBoothViewController.h
//  BSO
//
//  Created by MAC on 01/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemeLabel.h"

@interface PhotoBoothViewController : UIViewController
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitleCaption;
@property (nonatomic, strong) IBOutlet BSOButton *btn1;
@property (nonatomic, strong) IBOutlet BSOButton *btn2;
@property (nonatomic, strong) IBOutlet BSOButton *btnShare;
@property (nonatomic, strong) IBOutlet UIView *view1;
@property (nonatomic, strong) IBOutlet UIView *view2;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet UIImageView *imgArrow;
@end
