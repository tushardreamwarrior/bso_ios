//
//  MemeGeneratorViewController.m
//  BSO
//
//  Created by MAC on 01/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MemeGeneratorViewController.h"
#define STEP 5

@interface MemeGeneratorViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UIPopoverControllerDelegate,UIScrollViewDelegate>

@property (nonatomic, assign) BOOL shouldOpen;
@end

@implementation MemeGeneratorViewController {
    float width;
    float height;
    UIPopoverController *act;
    BOOL isBottomImageTap;
    NSTimer *timer;
    IBOutlet UIImageView *imgArrow;
    
    IBOutlet UISlider *topSlider;
    IBOutlet UISlider *bottomSlider;
     UIButton *btnSelected;
    NSString *imageNameToSave;
    BOOL isFirstTime;
}

@synthesize inputAccView;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitleCaption.text = @"Choose a Picture";
    self.btn1.selected = true;
    self.btn2.enabled = NO;
    self.btn3.enabled = NO;
    self.btn4.enabled = NO;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = true;
    self.scrView4.hidden = TRUE;
    self.btnDone.hidden = true;
    self.scrlView.contentSize = CGSizeMake(self.scrlView.frame.size.width, self.view3.frame.size.height);
    self.scrView4.contentSize = CGSizeMake(self.scrView4.frame.size.width, self.view4.frame.size.height);
    
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    
    self.lblTopTitle.text = @"Tap to add a Caption";
    self.lblBottomTitle.text = @"Tap to add a Caption";
    
    [self.txtToolBar addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.txtBottom addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingChanged];

    [self.txtToolBar addTarget:self action:@selector(topTextFieldTap:) forControlEvents:UIControlEventEditingDidBegin];
    [self.txtBottom addTarget:self action:@selector(bottomTextFieldTap:) forControlEvents:UIControlEventEditingDidBegin];
    
    UITapGestureRecognizer *tapTopImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTopImageToEdit:)];
    tapTopImage.numberOfTapsRequired = 1;
    [self.imgViewTop addGestureRecognizer:tapTopImage];
    
    UITapGestureRecognizer *tapBottomImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBottomImageToEdit:)];
    tapBottomImage.numberOfTapsRequired = 1;
    [self.imgViewBottom addGestureRecognizer:tapBottomImage];

    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    
   btnSelected = self.btn1;
    [self adjustInnerViews:[UIScreen mainScreen].bounds.size];
    [self setArrorPosition];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCancelPressed:)];
    isFirstTime = TRUE;
}

- (void)viewWillAppear:(BOOL)animated {
    // OBSERVE KEYBOARD
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void)viewDidAppear:(BOOL)animated {
    if(isFirstTime) {
        isFirstTime = FALSE;
        [self setArrorPosition];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)topTextFieldTap:(UITextField *)textField {
    if (isBottomImageTap) {
        [self.scrlView setContentOffset:CGPointMake(0,150) animated:YES];
    } else {
        [self.scrlView setContentOffset:CGPointMake(0,20) animated:YES];
    }
    return YES;
}

- (BOOL)bottomTextFieldTap:(UITextField *)textField {
    if (isBottomImageTap) {
        [self.scrlView setContentOffset:CGPointMake(0,150) animated:YES];
    } else {
        [self.scrlView setContentOffset:CGPointMake(0,150) animated:YES];
    }

    return YES;
}


- (IBAction)btnCancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) handleTap:(UITapGestureRecognizer *)tap {
    [self.txtToolBar resignFirstResponder];
    [self.txtBottom resignFirstResponder];
}

- (void)tapTopImageToEdit:(UITapGestureRecognizer *)recogniser {
    isBottomImageTap = NO;
    [self imageSizeAfterAspectFit:self.imgViewTop];
    self.imgMainImage.image = self.imgViewTop.image;
    
    self.imgMainImage.layer.cornerRadius = 20.0f;
    self.imgMainImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgMainImage.layer.borderWidth = 2.0f;
    self.imgMainImage.layer.masksToBounds = YES;
    
    self.lblTopTitle.hidden = false;
    self.lblBottomTitle.hidden = false;
    
    [self imageSizeAfterAspectFit:self.imgScreenShot];
    self.viewScrenshot.frame = CGRectMake(0, 0, width, height);
    
    self.lblTopMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30,self.viewScrenshot.frame.origin.y+30, self.viewScrenshot.frame.size.width-60, 35);
    self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, 35);
    self.lblTopMeme.font = [UIFont systemFontOfSize:40];
    self.lblBottomMeme.font = [UIFont systemFontOfSize:40];
    //[self.view addSubview:self.viewScrenshot];
    self.btn3.enabled = YES;
    [self btn3Pressed:self.btn3];
}

- (void)tapBottomImageToEdit:(UITapGestureRecognizer *)recogniser {
    isBottomImageTap = YES;
    [self imageSizeAfterAspectFit:self.imgViewBottom];
    self.imgMainImage.image = self.imgViewBottom.image;
    
    self.imgMainImage.layer.cornerRadius = 20.0f;
    self.imgMainImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgMainImage.layer.borderWidth = 2.0f;
    self.imgMainImage.layer.masksToBounds = YES;

    self.lblTopTitle.hidden = true;
    
    [self imageSizeAfterAspectFit:self.imgScreenShot];
    self.viewScrenshot.frame = CGRectMake(0, 0, width, height);
    
    self.lblTopMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.lblBottomMeme.frame.origin.y-(self.lblTopMeme.frame.size.height+20), self.viewScrenshot.frame.size.width-60, 35);
    
    self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, 35);
    
    self.lblTopMeme.font = [UIFont systemFontOfSize:40];
    self.lblBottomMeme.font = [UIFont systemFontOfSize:25];
    
    self.btn3.enabled = YES;
    [self btn3Pressed:self.btn3];
}

- (IBAction)btnSelectPhoto:(UIButton *)sender {
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    if(sender.tag == 101)
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    else
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imgPicker.delegate = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Place image picker on the screen
        [self presentViewController:imgPicker animated:YES completion:nil];
    }];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    self.imgViewTop.image = info[UIImagePickerControllerOriginalImage];
    self.imgViewBottom.image = info[UIImagePickerControllerOriginalImage];
    self.imgScreenShot.image = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    imageNameToSave = [Utility stringFromDate:[NSDate date] format:SERVER_DATE_TIME_FORMAT];
    self.btn2.enabled = YES;
    [self btn2Pressed:self.btn2];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btn1Pressed:(id)sender {
    self.btnDone.hidden = true;
    self.btn1.selected = true;
    
   
    btnSelected = self.btn1;
    [self setArrorPosition];
    
    self.btn2.selected = false;
    self.btn3.selected = false;
    self.btn4.selected = false;
    self.lblTitleCaption.text = @"Choose a Picture";
    self.view1.hidden = false;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = true;
    self.scrView4.hidden = TRUE;
}

- (IBAction)btn2Pressed:(id)sender {
    self.btnDone.hidden = true;
    self.btn1.selected = false;
    self.btn2.selected = true;
    self.btn3.selected = false;
    self.btn4.selected = false;

    btnSelected = self.btn2;
    [self setArrorPosition];
  
    
    self.lblTitleCaption.text = @"Choose a Style";
    self.view2.hidden = false;
    self.view1.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = true;
    self.scrView4.hidden = TRUE;

}
- (IBAction)btn3Pressed:(id)sender {
    self.btnDone.hidden = false;
    self.btn1.selected = false;
    self.btn2.selected = false;
    self.btn3.selected = true;
    self.btn4.selected = false;

    btnSelected = self.btn3;
    [self setArrorPosition];
    
    self.lblTitleCaption.text = @"Add Caption";
    self.view1.hidden = true;
    self.view2.hidden = true;
    self.view3.hidden = false;
    self.view4.hidden = true;
    self.scrView4.hidden = TRUE;
}
- (IBAction)btn4Pressed:(id)sender {
    self.btnDone.hidden = true;
    self.btn1.selected = false;
    self.btn2.selected = false;
    self.btn3.selected = false;
    self.btn4.selected = true;
  
    btnSelected = self.btn4;
    [self setArrorPosition];
    
    self.lblTitleCaption.text = @"Share your Meme!";
    self.view1.hidden = true;
    self.view2.hidden = true;
    self.view3.hidden = true;
    self.view4.hidden = false;
    self.scrView4.hidden = FALSE;

    NSString *fileName = [NSString stringWithFormat:@"Photos/%@.png",imageNameToSave];
    if([[NSFileManager defaultManager] fileExistsAtPath:FILE_PATH(fileName)]) {
        [[NSFileManager defaultManager] removeItemAtPath:FILE_PATH(fileName) error:nil];
    }
    NSData *imgData = UIImagePNGRepresentation(self.imgFinal.image);
    [imgData writeToFile:FILE_PATH(fileName) atomically:YES];
}

- (IBAction)btnDonePressed:(id)sender {
    self.imgFinal.image = self.imgMainImage.image;
    self.imgFinal.layer.cornerRadius = 20.0f;
    self.imgFinal.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgFinal.layer.borderWidth = 2.0f;
    self.imgFinal.layer.masksToBounds = YES;

    self.btn4.enabled = YES;
    [self btn4Pressed:self.btn4];

}

- (IBAction)btnSavePressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Successfully Save!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImageWriteToSavedPhotosAlbum(self.imgFinal.image, nil, nil, nil);
    });
    [alert show];
}

- (IBAction)btnSharePressed:(id)sender {
    NSMutableArray *sharingItems = [NSMutableArray new];
    NSString *texttoshare = [NSString stringWithFormat:@"My #Mematic Meme"];
    [sharingItems addObject:texttoshare];
    [sharingItems addObject:self.imgFinal.image];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [activityController setValue:@"My Mematic Meme" forKey:@"subject"];
    act = [[UIPopoverController alloc] initWithContentViewController:activityController];
    [act presentPopoverFromRect:CGRectMake(self.btnShare.frame.origin.x+50, self.btnShare.frame.origin.y,self.btnShare.frame.size.width/2, self.btnShare.frame.size.height) inView:self.view4 permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    act.delegate = self;
    [self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)memeButtonPressed:(UIButton *)sender {
    [self.txtBottom becomeFirstResponder];
    [self.txtToolBar becomeFirstResponder];
}

- (void)textValueChanged:(UITextField *)txtField {
    
    self.lblTopTitle.hidden = true;
    self.lblBottomTitle.hidden = true;
    self.lblTopMeme.text = self.txtToolBar.text;
    self.lblBottomMeme.text = self.txtBottom.text;
    
    //FOR TOP LABEL LINE INCREMENT DEPENT ON TEXT LENGTH
    
    CGSize topLabelSize = CGSizeMake(708,600);
    float topLabelheightIs =
    [self.lblTopMeme.text
     boundingRectWithSize:topLabelSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblTopMeme.font }
     context:nil]
    .size.height;
    
    CGRect newFrame = self.lblTopMeme.frame;
    newFrame.size.height = topLabelheightIs;
    self.lblTopMeme.frame = newFrame;
    
    //FOR BOTTOM LABEL LINE INCREMENT DEPENT ON TEXT LENGTH
    
    CGSize bottomLabelSize = CGSizeMake(708,600);
    float heightIs =
    [self.lblBottomMeme.text
     boundingRectWithSize:bottomLabelSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblBottomMeme.font }
     context:nil]
    .size.height;
    CGRect newFrame1 = self.lblBottomMeme.frame;
    newFrame1.size.height = heightIs;
    //self.lblBottomMeme.frame = newFrame1;
    if (isBottomImageTap) {
        self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, heightIs);

        self.lblTopMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.lblBottomMeme.frame.origin.y-(self.lblTopMeme.frame.size.height+20), self.viewScrenshot.frame.size.width-60, topLabelheightIs);

    } else {
        self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, heightIs);
    }
    
    //TAKE SCREENSHOT OF ORIGINAL IMAGE
    
    [self imageSizeAfterAspectFit:self.imgScreenShot];
    self.viewScrenshot.frame = CGRectMake(0, 0, width, height);
    //[self.view3 addSubview:self.viewScrenshot];
    CGSize size1 = CGSizeMake(width, height);
    UIGraphicsBeginImageContextWithOptions(size1, NO, [UIScreen mainScreen].scale);
    [self.viewScrenshot.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.imgMainImage.image = image;
}

//FOR CHANGE FONT SIZE

- (IBAction)topSliderValueChanged:(UISlider *)sender {
    
    float size = self.lblTopMeme.font.pointSize;
    size = ceilf(sender.value);
    float  roundedValue = round(size / STEP) * STEP;
    if(roundedValue == sender.value) {
        return;
    }
    size = roundedValue;
    sender.value = roundedValue;
    NSLog(@"Slider Value %f",sender.value);
    CGSize topLabelSize = CGSizeMake(708,600);
    float topLabelheightIs =
    [self.lblTopMeme.text
     boundingRectWithSize:topLabelSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblTopMeme.font }
     context:nil]
    .size.height;
    CGRect newFrame = self.lblTopMeme.frame;
    newFrame.size.height = topLabelheightIs;
    
    if (isBottomImageTap) {
        self.lblTopMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.lblBottomMeme.frame.origin.y-(self.lblTopMeme.frame.size.height+20), self.viewScrenshot.frame.size.width-60, topLabelheightIs);
    } else {
        self.lblTopMeme.frame = newFrame;
    }
    self.lblTopMeme.font = [UIFont fontWithName:@"Arial" size:size];
    
    //SCREENSHOT AFTER FONT SIZE CHANGE
    [self imageSizeAfterAspectFit:self.imgScreenShot];
    self.viewScrenshot.frame = CGRectMake(0, 0, width, height);
    CGSize size2 = CGSizeMake(width, height);
    UIGraphicsBeginImageContextWithOptions(size2, NO, [UIScreen mainScreen].scale);
    [self.viewScrenshot.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.imgMainImage.image = image;


}

- (IBAction)bottomSliderValueChanged:(UISlider *)sender {
    //float size = self.lblTopMeme.font.pointSize;
    //size = sender.value;
    float size1 = self.lblBottomMeme.font.pointSize;
    size1 = ceilf(sender.value);
    float  roundedValue = round(sender.value / STEP) * STEP;
    if(roundedValue == sender.value) {
        return;
    }
    size1 = roundedValue;
    sender.value = roundedValue;
    CGSize topLabelSize = CGSizeMake(708,600);
    float topLabelheightIs =
    [self.lblTopMeme.text
     boundingRectWithSize:topLabelSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblTopMeme.font }
     context:nil]
    .size.height;
    CGRect newFrame = self.lblTopMeme.frame;
    newFrame.size.height = topLabelheightIs;
    
    
    CGSize bottomLabelSize = CGSizeMake(708,600);
    float heightIs =
    [self.lblBottomMeme.text
     boundingRectWithSize:bottomLabelSize
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblBottomMeme.font }
     context:nil]
    .size.height;
    CGRect newFrame1 = self.lblBottomMeme.frame;
    newFrame1.size.height = heightIs;
    if (isBottomImageTap) {
        self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, heightIs);
        self.lblTopMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.lblBottomMeme.frame.origin.y-(self.lblTopMeme.frame.size.height+20), self.viewScrenshot.frame.size.width-60, topLabelheightIs);
        
    } else {
        self.lblBottomMeme.frame = CGRectMake(self.viewScrenshot.frame.origin.x+30, self.viewScrenshot.frame.size.height-(self.lblBottomMeme.frame.size.height+30), self.viewScrenshot.frame.size.width-60, heightIs);
    }
    self.lblBottomMeme.font = [UIFont fontWithName:@"Arial" size:size1];
    
    //SCREENSHOT AFTER FONT SIZE CHANGE
    
    [self imageSizeAfterAspectFit:self.imgScreenShot];
    self.viewScrenshot.frame = CGRectMake(0, 0, width, height);
    CGSize size2 = CGSizeMake(width, height);
    UIGraphicsBeginImageContextWithOptions(size2, NO, [UIScreen mainScreen].scale);
    [self.viewScrenshot.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.imgMainImage.image = image;

}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.txtToolBar) {
        [self.txtBottom becomeFirstResponder];
        [self.scrlView setContentOffset:CGPointMake(0,150) animated:YES];
    } else {
        [self.txtToolBar becomeFirstResponder];
        [self.scrlView setContentOffset:CGPointMake(0,20) animated:YES];
        }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.shouldOpen = NO;
    [self.txtToolBar resignFirstResponder];
    [self.txtBottom resignFirstResponder];
}


-(CGSize)imageSizeAfterAspectFit:(UIImageView*)imgview{
    float newwidth;
    float newheight;
    
    UIImage *image=imgview.image;
    
    if (image.size.height>=image.size.width){
        newheight=imgview.frame.size.height;
        newwidth=(image.size.width/image.size.height)*newheight;
        
        if(newwidth>imgview.frame.size.width){
            float diff=imgview.frame.size.width-newwidth;
            newheight=newheight+diff/newheight*newheight;
            newwidth=imgview.frame.size.width;
        }
    }
    else{
        newwidth=imgview.frame.size.width;
        newheight=(image.size.height/image.size.width)*newwidth;
        
        if(newheight>imgview.frame.size.height){
            float diff=imgview.frame.size.height-newheight;
            newwidth=newwidth+diff/newwidth*newwidth;
            newheight=imgview.frame.size.height;
        }
    }
    
    width = newwidth;
    height = newheight;
    return CGSizeMake(newwidth, newheight);
    
}

- (void) keyboardWillShow:(NSNotification *)note {
    
    NSDictionary *keyboardAnimationDetail = [note userInfo];
    UIViewAnimationCurve animationCurve = [keyboardAnimationDetail[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat duration = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    NSValue* keyboardFrameBegin = [keyboardAnimationDetail valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    // working for hardware keyboard
    UIViewAnimationOptions options = (UIViewAnimationOptions)animationCurve;
    
    // working for virtual keyboard
    //UIViewAnimationOptions options = (animationCurve << 16);
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        self.inputAccView.frame = CGRectMake(0, self.view3.bounds.size.height - keyboardFrameBeginRect.size.height, self.view3.bounds.size.width, -100);
    } completion:nil];
    
}

- (void) keyboardWillHide:(NSNotification *)note {
    [self.scrlView setContentOffset:CGPointMake(0,0) animated:YES];
    NSDictionary *keyboardAnimationDetail = [note userInfo];
    UIViewAnimationCurve animationCurve = [keyboardAnimationDetail[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    CGFloat duration = [keyboardAnimationDetail[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    // hardware keyboard
    UIViewAnimationOptions options = (UIViewAnimationOptions)animationCurve;
    
    // virtual keyboard
    //UIViewAnimationOptions options = (animationCurve << 16);
    
    [UIView animateWithDuration:duration delay:0.0 options:options animations:^{
        self.inputAccView.frame = CGRectMake(0, self.view3.bounds.size.height, self.view3.bounds.size.width, -100);
    } completion:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // best call super just in case
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // will execute before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        [self setArrorPosition];
        [self adjustInnerViews:size];
        
    } completion:^(id  _Nonnull context) {
        
        // will execute after rotation
        
    }];
}

- (void)setArrorPosition {
   
    CGPoint center = imgArrow.center;
    center.x = btnSelected.center.x;
    imgArrow.center = center;
    
    self.imgSelectedBorder.frame = btnSelected.frame;
  
}

- (void)adjustInnerViews:(CGSize )size {
    if(size.width == 768) { // Portrait
        UIView *aView = [self.view2 viewWithTag:1];
        CGRect frame1 = aView.frame;
        frame1.origin.y = 50;
        frame1.origin.x = (size.width - frame1.size.width) / 2;
        aView.frame = frame1;
        
        aView = [self.view2 viewWithTag:2];
        CGRect frame2 = aView.frame;
        frame2.origin.y = frame1.origin.y + frame1.size.height + 60;
        frame2.origin.x = (size.width - frame2.size.width) / 2;
        aView.frame = frame2;
        
    } else { // LandScape
        UIView *aView = [self.view2 viewWithTag:1];
        CGRect frame1 = aView.frame;
        frame1.origin.x = 100;
        frame1.origin.y = 100;
        aView.frame = frame1;
        
        aView = [self.view2 viewWithTag:2];
        CGRect frame2 = aView.frame;
        frame2.origin.y = 100;
        frame2.origin.x = (size.width - frame2.size.width) - 100;
        aView.frame = frame2;
        
        

    }
}

@end
