//
//  VineGalleryViewController.h
//  BSO
//
//  Created by Imobtree Solutions on 4/28/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemeLabel.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VineGalleryViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *btnCamera;
@property (nonatomic, strong) IBOutlet UIButton *btnAlbum;
@property (nonatomic, strong) IBOutlet UIButton *btn1;
@property (nonatomic, strong) IBOutlet UIButton *btn2;
@property (nonatomic, strong) IBOutlet UIButton *btn3;
@property (nonatomic, strong) IBOutlet UIButton *btn4;
@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;
@property (nonatomic, strong) IBOutlet UIView *view1;
@property (nonatomic, strong) IBOutlet UIView *view2;
@property (nonatomic, strong) IBOutlet UIView *view3;
@property (nonatomic, strong) IBOutlet UIScrollView *scrView3;
@property (nonatomic, strong) IBOutlet UIView *view4;
@property (nonatomic, strong) IBOutlet UIScrollView *scrView4;
@property (nonatomic, strong) IBOutlet UIImageView *imgMainImage;
@property (nonatomic, strong) IBOutlet UIImageView *imgFinal;
@property (nonatomic, strong) IBOutlet UIButton *btnDone;
@property (nonatomic, strong) IBOutlet UIButton *btnShare;

@property (nonatomic, strong) IBOutlet UILabel *lblTitleCaption;

@property (strong, nonatomic) MPMoviePlayerController *videoPlayer;

@property (nonatomic ,strong) UIPopoverController *popOver;

@end
