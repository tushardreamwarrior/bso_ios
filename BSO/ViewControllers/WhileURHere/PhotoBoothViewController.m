//
//  PhotoBoothViewController.m
//  BSO
//
//  Created by MAC on 01/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PhotoBoothViewController.h"


@interface PhotoBoothViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate> {
    bool isFirstTime;
}

@property (nonatomic, assign) BOOL shouldOpen;

@end

@implementation PhotoBoothViewController {
    NSString *imageNameToSave;
    BSOButton *btnSelected;
    UIPopoverController *act;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitleCaption.text = @"Choose a Picture";
    self.btn1.selected = true;
    self.btn2.enabled = NO;
    btnSelected = self.btn1;
    self.view2.hidden = true;
    self.srcView.hidden = TRUE;
    self.srcView.contentSize = CGSizeMake( self.srcView.frame.size.width, self.view2.frame.size.height);
    
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnCancelPressed:)];
    isFirstTime = TRUE;
}

- (void)viewDidAppear:(BOOL)animated {
    if(isFirstTime) {
        isFirstTime = FALSE;
        [self setArrorPosition];
    }
}

- (IBAction)btnSelectPhoto:(UIButton *)sender {
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    if(sender.tag == 101)
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    else
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imgPicker.delegate = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Place image picker on the screen
        [self presentViewController:imgPicker animated:YES completion:nil];
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    self.imgView.image = info[UIImagePickerControllerOriginalImage];

    self.imgView.layer.cornerRadius = 20.0f;
    self.imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgView.layer.borderWidth = 2.0f;
    self.imgView.layer.masksToBounds = YES;

    
    [picker dismissViewControllerAnimated:YES completion:nil];
    imageNameToSave = [Utility stringFromDate:[NSDate date] format:SERVER_DATE_TIME_FORMAT];
    self.btn2.enabled = YES;
    [self btn2Pressed:self.btn2];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btn1Pressed:(id)sender {

    self.btn1.selected = true;
    btnSelected = self.btn1;
    [self setArrorPosition];
    
    self.btn2.selected = false;

    self.lblTitleCaption.text = @"Choose a Picture";
    self.view1.hidden = false;
    self.view2.hidden = true;
    self.srcView.hidden = TRUE;

}

- (IBAction)btn2Pressed:(id)sender {
    self.btn1.selected = false;
    self.btn2.selected = true;
   
    btnSelected = self.btn2;
    [self setArrorPosition];
  
    self.lblTitleCaption.text = @"Share your Photo";
    self.view2.hidden = false;
    self.view1.hidden = true;
    self.srcView.hidden = FALSE;
 
    NSString *fileName = [NSString stringWithFormat:@"Photos/%@.png",imageNameToSave];
    if([[NSFileManager defaultManager] fileExistsAtPath:FILE_PATH(fileName)]) {
        [[NSFileManager defaultManager] removeItemAtPath:FILE_PATH(fileName) error:nil];
    }
    NSData *imgData = UIImagePNGRepresentation(self.imgView.image);
    [imgData writeToFile:FILE_PATH(fileName) atomically:YES];


}


- (IBAction)btnSavePressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Successfully Save!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImageWriteToSavedPhotosAlbum(self.imgView.image, nil, nil, nil);
    });
    [alert show];
}

- (IBAction)btnSharePressed:(id)sender {
    NSMutableArray *sharingItems = [NSMutableArray new];
    NSString *texttoshare = [NSString stringWithFormat:@"My Photo"];
    [sharingItems addObject:texttoshare];
    [sharingItems addObject:self.imgView.image];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [activityController setValue:@"My Photo" forKey:@"subject"];
    act = [[UIPopoverController alloc] initWithContentViewController:activityController];
    [act presentPopoverFromRect:CGRectMake(self.btnShare.frame.origin.x+50, self.btnShare.frame.origin.y,self.btnShare.frame.size.width/2, self.btnShare.frame.size.height) inView:self.view2 permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    act.delegate = self;
    [self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)btnCancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // best call super just in case
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // will execute before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        [self setArrorPosition];
        
        
    } completion:^(id  _Nonnull context) {
        
        // will execute after rotation
        
    }];
}

- (void)setArrorPosition {
   CGPoint center = self.imgArrow.center;
    center.x = btnSelected.center.x;
    self.imgArrow.center = center;
    self.imgSelectedBorder.frame = btnSelected.frame;
  
}

@end