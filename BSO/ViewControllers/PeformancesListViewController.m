//
//  PeformancesListViewController.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PeformancesListViewController.h"
#import "PerformanceCell.h"
#import "SelectionView.h"

@interface PeformancesListViewController () <UITableViewDataSource,UITableViewDelegate,SelectionDelegate>
@property (nonatomic, strong) IBOutlet  BSOButton *btnToday;
@property (nonatomic, strong) IBOutlet  BSOButton *btnSelectDate;
@property (nonatomic, strong) IBOutlet  BSOButton *btnSelectCost;
@property (nonatomic, strong) IBOutlet  BSOTextField *txtSearch;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet  UIActivityIndicatorView *indicator;
@property (nonatomic, strong) NSMutableArray *arrPeformances;
@property (nonatomic, strong) NSMutableArray *arrAllItems;
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) NSString *selecteDate;
@property (nonatomic, strong) NSString *selecteCost;
@property (nonatomic, strong) NSArray *arrCost;
@end

@implementation PeformancesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selecteDate = nil;
    self.bsoManager = [BSOManager SharedInstance];
    self.arrCost = @[@"All Events",@"Paid Events",@"Free Events"];
    self.selecteCost = self.arrCost[0];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackPatternBG"]];
    self.tblView.tableHeaderView = self.headerView;
    self.headerView.backgroundColor = [self.bsoManager getSelectedItemColor];
    
    [self.txtSearch addTarget:self action:@selector(searchTextValueChaged) forControlEvents:UIControlEventEditingChanged];
    self.txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtSearch.enablesReturnKeyAutomatically = YES;
    if ([self.txtSearch respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtSearch.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
    [Utility setBorderColorWithView:self.btnSelectCost color:[UIColor lightGrayColor]];
    [Utility setBorderColorWithView:self.txtSearch color:[UIColor lightGrayColor]];
    [Utility setBorderColorWithView:self.btnSelectDate color:[UIColor lightGrayColor]];
    [Utility setBorderColorWithView:self.btnToday color:[UIColor lightGrayColor]];

    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:gesture];
    
    gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.tblView addGestureRecognizer:gesture];
    
    [self getPefrormancesList];
}

- (void)viewWillAppear:(BOOL)animated {
    CGRect frame = self.tblView.frame;
    frame.origin.y = 0;
    [self.tblView scrollRectToVisible:frame animated:FALSE];
}

- (void)getPefrormancesList {
    [self.indicator startAnimating];
    [self.bsoManager getShowList:self.bsoManager.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]]) {
            self.arrAllItems = [[NSMutableArray alloc] initWithArray:responseObject];
            self.arrPeformances =  [[NSMutableArray alloc] init];
            [self searchTextValueChaged];
            if(self.arrPeformances.count) {
                CGRect frame = self.headerView.frame;
                frame.size.height = 45;
                self.headerView.frame = frame;
                self.tblView.tableHeaderView = self.headerView;
            }
            self.headerView.backgroundColor = [self.bsoManager getSelectedItemColor];
        } else  {
            
        }
        
        [self.indicator stopAnimating];
    } failure:^(NSError *error) {
        [self.indicator stopAnimating];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.arrPeformances.count < indexPath.row ) {
        return 0;
    }
    
    Show *aShow = [self.arrPeformances objectAtIndex:indexPath.row];
    if(aShow.strDescription.length) {
        return 385 + aShow.height + 5;
    }
    else {
        return 385;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrPeformances.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PerformanceCell *cell = (PerformanceCell *) [tableView dequeueReusableCellWithIdentifier:@"PerformanceCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PerformanceCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
       // [cell.btnTicket addTarget:self action:@selector(ticketButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];

    }
    if(self.arrPeformances.count < indexPath.row ) {
        return cell;
    }
    Show *aShow = [self.arrPeformances objectAtIndex:indexPath.row];
    cell.lblName.text = [aShow.title stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    if(aShow.strDescription.length) {
        [cell.txtDesc setContentInset:UIEdgeInsetsZero];
        NSString *htmlString = [NSString stringWithFormat:@"<font size='5' face='OpenSans' color='white'>%@</font>",aShow.strDescription];
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                               documentAttributes:nil error:nil];
        
        
      
        cell.txtDesc.attributedText = attrString;
        
      
        CGRect frame = cell.txtDesc.frame;
        frame.size.height = aShow.height + 3;
        cell.txtDesc.frame = frame;
        
        frame = cell.bgView.frame;
        frame.size.height = cell.txtDesc.frame.origin.y + cell.txtDesc.frame.size.height + 2;
        cell.bgView.frame = frame;
        
    
    } else {
      //  cell.txtDesc.text = @"";
    }
   
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aShow.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
    if([aShow.startDate isEqualToString:aShow.endDate]) {
        cell.lblDate.text = [NSString stringWithFormat:@"%@",aShow.endDate];
    } else {
        cell.lblDate.text = [NSString stringWithFormat:@"%@ - %@",aShow.startDate,aShow.endDate];
    }
    
    cell.topColorView.backgroundColor = [self.bsoManager getSelectedItemColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath) {
        if([self.delegate respondsToSelector:@selector(performanceViewDetailButtonPressed:)]) {
            Show *aShow = [self.arrPeformances objectAtIndex:indexPath.row];
            [self.delegate performanceViewDetailButtonPressed:aShow];
        }
    }
}

- (void)ticketButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        if([self.delegate respondsToSelector:@selector(performanceViewTicketButtonPressed:)]) {
            Show *aShow = [self.arrPeformances objectAtIndex:indexPath.row];
            [self.delegate performanceViewTicketButtonPressed:aShow];
        }
    }
}

- (void)searchTextValueChaged {
    [self.arrPeformances removeAllObjects];
    NSMutableArray *arrQuery = [[NSMutableArray alloc] init];
    
    
    if(self.txtSearch.text.length) {
        [arrQuery addObject:[NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@",self.txtSearch.text]];
    }
    
    if(self.selecteDate) {
        [arrQuery addObject:[NSPredicate predicateWithFormat:@"startDate == %@",self.selecteDate]];
        
    }
    if([[self.btnSelectCost titleForState:UIControlStateNormal] isEqualToString:@"Paid Events"]) {
        [arrQuery addObject:[NSPredicate predicateWithFormat:@"isFreeEvent == %@",@"0"]];
        
    }
    if([[self.btnSelectCost titleForState:UIControlStateNormal] isEqualToString:@"Free Events"]) {
        [arrQuery addObject:[NSPredicate predicateWithFormat:@"isFreeEvent == %@",@"1"]];
        
    }
    if(arrQuery.count == 0) {
        [self.arrPeformances addObjectsFromArray:self.arrAllItems];
    }
    else {
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:arrQuery];
       [self.arrPeformances addObjectsFromArray:[self.arrAllItems filteredArrayUsingPredicate:predicate]];
    }
    [self.tblView reloadData];
}

- (IBAction)btnSelectDatePressed {
    SelectionView *selView = [[SelectionView alloc] initWithSelectionType:selectionTypeDatePicker];
    selView.delegate = self;
    if(self.selecteDate.length)
        selView.selecteDate = [Utility dateFromString:self.selecteDate format:RANGE_FORMAT];
     [selView showInView:self.btnSelectDate];
    self.btnToday.selected = FALSE;
}

- (IBAction)btnTodayPressed {
    self.btnToday.selected = !self.btnToday.selected;
    if(self.btnToday.selected) {
        self.selecteDate = [Utility stringFromDate:[NSDate date] format:RANGE_FORMAT];
        [self.btnSelectDate setTitle:[Utility stringFromDate:[NSDate date] format:@"MM/dd/yyyy"] forState:UIControlStateNormal];
    }
    else {
         self.selecteDate = nil;
         [self.btnSelectDate setTitle:@"SELECT DATE" forState:UIControlStateNormal];
        
    }
    [self searchTextValueChaged];
}

- (IBAction)btnSelectCostPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrCost];
    selView.delegate = self;
    selView.selectedIndex = [self.arrCost indexOfObject:self.selecteCost];
    [selView showInView:self.btnSelectCost];
}

#pragma mark - SelectionView

- (void)SelectionView:(SelectionView *)selectionView didSelectDate:(NSDate *)date SenderView:(UIView *)senderView {
    self.selecteDate = [Utility stringFromDate:date format:RANGE_FORMAT];
    [self.btnSelectDate setTitle:[Utility stringFromDate:date format:@"MM/dd/yyyy"] forState:UIControlStateNormal];
    [self searchTextValueChaged];
}

- (void)SelectionView:(SelectionView *)selectionView didSelectItem:(NSString *)item withIndex:(NSInteger)index SenderView:(UIView *)senderView {
    [self.btnSelectCost setTitle:item forState:UIControlStateNormal];
    [self searchTextValueChaged];
}

- (IBAction)btnBackPressed {
    if([self.delegate respondsToSelector:@selector(performanceViewBackButtonPressed)]) {
        [self.delegate performanceViewBackButtonPressed];
    }
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)sender {
    if([self.delegate respondsToSelector:@selector(performanceViewBackButtonPressed)]) {
        [self.delegate performanceViewBackButtonPressed];
    }
}

@end
