//
//  SeatSelectionCYOViewController.h
//  BSO
//
//  Created by MAC on 06/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeatSelectionCYOViewController : UIViewController
@property (nonatomic, strong) SubscriptionSeatCYO *aCYO;
@property (nonatomic, strong) Subscription *aSubscription;
@end
