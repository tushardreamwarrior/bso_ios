//
//  SocialViewController.m
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SocialViewController.h"
#import "HomeViewController.h"
#import "SocialCell.h"

@interface SocialViewController () <UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) NSMutableArray *arrSocial;
@property (nonatomic, strong) NSMutableArray *arrTempSocial;
@property (nonatomic, strong) IBOutlet UICollectionView *collection;
@property (nonatomic, assign) NSInteger postLoaded;
@end

@implementation SocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackPatternBG"]];
    self.bsoManager = [BSOManager SharedInstance];
    self.collection.backgroundColor = [UIColor clearColor];
    [self.collection registerClass:[SocialCell class] forCellWithReuseIdentifier:@"SocialCell"];
    self.arrSocial = [[NSMutableArray alloc] init];
    self.arrTempSocial = [[NSMutableArray alloc] init];
    [self getSocialPost];
}

- (void)getHashTagsPost {
    [Utility showLoader];
    [self.bsoManager getHashTagPostList:self.bsoManager.selectedType success:^(id responseObject) {
        [self.arrSocial removeAllObjects];
        [self.arrSocial addObjectsFromArray:responseObject];
        [self.collection reloadData];
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)getSocialPost {
    [self.arrSocial removeAllObjects];
    [self.arrTempSocial removeAllObjects];
    [self.collection reloadData];
    
    NSString *path = [NSString stringWithFormat:@"socialPost_%d.plist",self.bsoManager.selectedType];
    NSArray *cachedPost = [NSKeyedUnarchiver unarchiveObjectWithFile:FILE_PATH(path)];
    if(cachedPost) {
        [self.arrSocial addObjectsFromArray:cachedPost];
    } else {
        [Utility showLoader];
    }
    
    self.postLoaded = 0;
    [self.bsoManager getFBPostList:self.bsoManager.selectedType success:^(id responseObject) {
        [self.arrTempSocial addObjectsFromArray:responseObject];
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
    } failure:^(NSError *error) {
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
    }];
    
    [self.bsoManager getTwitterPostList:self.bsoManager.selectedType success:^(id responseObject) {
        [self.arrTempSocial addObjectsFromArray:responseObject];
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
    } failure:^(NSError *error) {
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
    }];
    
//    [self.bsoManager getInstagramPostList:self.bsoManager.selectedType success:^(id responseObject) {
//        [self.arrTempSocial addObjectsFromArray:responseObject];
//        self.postLoaded ++;
//        [self shuffleAndDisplayPost];
//    } failure:^(NSError *error) {
//        self.postLoaded ++;
//        [self shuffleAndDisplayPost];
//    }];
    
    [self.bsoManager getYouTubePostList:self.bsoManager.selectedType success:^(id responseObject) {
        [self.arrTempSocial addObjectsFromArray:responseObject];
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
        
    } failure:^(NSError *error) {
        self.postLoaded ++;
        [self shuffleAndDisplayPost];
    }];
}

- (void)shuffleAndDisplayPost {
    if(self.postLoaded == 3) {
        [Utility hideLoader];
        [self.arrSocial removeAllObjects];
        [self.arrSocial addObjectsFromArray:self.arrTempSocial];
        NSUInteger count = [self.arrSocial count];
        for (NSUInteger i = 0; i < count; ++i) {
            NSInteger remainingCount = count - i;
            NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
            [self.arrSocial exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
        }
        [self.collection reloadData];
        NSString *path = [NSString stringWithFormat:@"socialPost_%d.plist",self.bsoManager.selectedType];
        [NSKeyedArchiver archiveRootObject:self.arrSocial toFile:FILE_PATH(path)];
    }
}

#pragma mark - CollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.arrSocial.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SocialCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SocialCell" forIndexPath:indexPath];
    Social *aSocial = [self.arrSocial objectAtIndex:indexPath.row];
     [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aSocial.image] placeholderImage:nil options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
         //if(cacheType == SDImageCacheTypeNone || cacheType == SDImageCacheTypeDisk)
         {
//             float height = [Utility imageHeight:image scaledToWidth:cell.imgView.frame.size.width];
//             CGRect frame = cell.imgView.frame;
//             frame.size.height = height;
//             cell.imgView.frame = frame;
//             
//             frame = cell.txtDes.frame;
//             frame.origin.y = cell.imgView.frame.size.height + 5;
//             frame.size.height = cell.frame.size.height - (40 + height);
//             cell.txtDes.frame = frame;
            
        }
         
     }];
    cell.txtDes.text = aSocial.title;
    cell.btnLink.tag = indexPath.row;
    cell.imgSocial.image = [UIImage imageNamed:[NSString stringWithFormat:@"social_%d",aSocial.type]];
    [cell.btnLink addTarget:self action:@selector(btnLinkPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)btnLinkPressed:(BSOButton *)sender {
     Social *aSocial = [self.arrSocial objectAtIndex:sender.tag];
     [Utility openURLInInterBrowser:aSocial.postURL from:self];
}

@end
