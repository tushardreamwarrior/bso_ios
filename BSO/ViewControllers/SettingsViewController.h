//
//  SettingsViewController.h
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsSection : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *arrRows;
@property (nonatomic, assign) BOOL isOpened;
@end

@interface SettingsViewController : UIViewController

@end
