//
//  MenuViewController.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnBSO;
@property (nonatomic, strong) IBOutlet BSOButton *btnTangleWood;
@property (nonatomic, strong) IBOutlet BSOButton *btnBSPop;
@property (nonatomic, strong) NSMutableArray *arrItems;


@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.view.backgroundColor = [UIColor clearColor];
    self.menuView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackPatternBG"]];
    _arrItems = [[NSMutableArray alloc] initWithObjects:@"HOME",@"PERFORMANCES",@"PLAN YOUR VISIT",@"AUDIO & VIDEO",@"SHARE",@"WHILE YOU ARE HERE",@"MORE",@"SUBSCRIBE",@"SETTINGS", nil];
    self.btnBSO.selected = TRUE;
    self.selectedIndex = -1;
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:gesture];
    self.btnBSO.selected = FALSE;
    self.btnBSPop.selected = FALSE;
    self.btnTangleWood.selected = FALSE;
    if(self.bsoManager.selectedType == jBSO) {
        self.btnBSO.selected = TRUE;
    } else  if(self.bsoManager.selectedType == jBPop) {
        self.btnBSPop.selected = TRUE;
    } else if(self.bsoManager.selectedType == jTangleWood) {
        self.btnTangleWood.selected = TRUE;
    }
    
    gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.table addGestureRecognizer:gesture];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
        UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.height, 50)];
        aView.backgroundColor = [UIColor blackColor];
        cell.selectedBackgroundView = aView;
        
    }
    if(indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3) {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.arrItems[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"menuItem_%ld",(long)indexPath.row+1]];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.selectedIndex  == indexPath.row) {
        self.selectedIndex = -1;
        [tableView reloadData];
    } else {
        self.selectedIndex = indexPath.row;
    }
    if([self.delegate respondsToSelector:@selector(MenuViewItemPressed:)]) {
        [self.delegate MenuViewItemPressed:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}

- (IBAction)btnOrginizationPressed:(UIButton *)sender {
    self.bsoManager.selectedType = (OrganizationType)sender.tag - 1 ;
    self.btnBSO.selected = FALSE;
    self.btnBSPop.selected = FALSE;
    self.btnTangleWood.selected = FALSE;
    sender.selected = TRUE;
    if([self.delegate respondsToSelector:@selector(MenuViewOrginizationButtonPressed:)]) {
        [self.delegate MenuViewOrginizationButtonPressed:sender.tag];
    }
}

- (IBAction)btnBackPressed {
    if([self.delegate respondsToSelector:@selector(MenuViewBackPressed)]) {
        [self.delegate MenuViewBackPressed];
    }
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)sender {
    if([self.delegate respondsToSelector:@selector(MenuViewBackPressed)]) {
        [self.delegate MenuViewBackPressed];
    }
}
@end
