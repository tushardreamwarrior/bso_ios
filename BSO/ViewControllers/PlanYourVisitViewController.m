//
//  PlanYourVisitViewController.m
//  BSO
//
//  Created by MAC on 13/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PlanYourVisitViewController.h"
#import "DinigCell.h"
#import "ParkingCell.h"
#import "StepCell.h"
#import "Annotation.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface PlanYourVisitViewController () <UITableViewDelegate,UITableViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
@property (nonatomic, strong) IBOutlet UIView *hearView;
@property (nonatomic, strong) IBOutlet UIView *dateView;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblEventInfo;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMonth;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;

@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) GMSMapView *mapView;
@property (nonatomic, strong) IBOutlet UIView *mapContainer;
@property (nonatomic, assign) BSOManager *bsoManager;

@property (nonatomic, strong) IBOutlet BSOButton *btnTraffic;
@property (nonatomic, strong) IBOutlet BSOButton *btnDining;
@property (nonatomic, strong) IBOutlet BSOButton *btnLodging;
@property (nonatomic, strong) IBOutlet BSOButton *btnParking;
@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;

@property (nonatomic, strong) IBOutlet UIView *btnToogleBGView;
@property (nonatomic, strong) IBOutlet BSOButton *btnMap;
@property (nonatomic, strong) IBOutlet BSOButton *btnList;

@property (nonatomic, strong)  BSOButton *btnSelected;

@property (nonatomic, strong) NSDate *currentDate;
@property (nonatomic, strong) PlaceDetails *aPlace;
@property (nonatomic, strong) RouteDetails *aRoute;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong)  GMSMarker *userAnnotation;
@property (nonatomic, strong) GMSMarker *venueAnnomation;
@property (nonatomic, strong)  GMSPolyline *polyline;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSMutableArray *arrAllEvents;
@property (nonatomic, assign) OrganizationType selectedType;

@end

@implementation PlanYourVisitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.hearView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"planVisitBG"]];
    
    self.currentDate = [NSDate date];
    
    [self btnMapTogglePressed];
    
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    
    [self showDate];
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.delegate = self;
   
    
    self.coordinate = CLLocationCoordinate2DMake(42.3429552, -71.0856199);
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.coordinate.latitude
                                                            longitude:self.coordinate.longitude
                                                                 zoom:6];
    self.mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapContainer.frame.size.width, self.mapContainer.frame.size.height) camera:camera];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    self.btnSelected = self.btnTraffic;
    [self getPlanVisitDetails:self.bsoManager.selectedType];
    
    self.aPlace = [[PlaceDetails alloc] init];

    GMSCameraUpdate *updateCamera = [GMSCameraUpdate setTarget:self.coordinate zoom:6];
    [self.mapView animateWithCameraUpdate:updateCamera];
   
    self.mapView.myLocationEnabled = NO;
    [self.mapContainer addSubview:self.mapView];
}

- (void)gesShowList {
    self.imgView.image = nil;
    self.lblEventInfo.text = @"";
    
    [self.bsoManager getAllEventsList:self.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]]) {
             self.arrAllEvents = [[NSMutableArray alloc] initWithArray:responseObject];
            [self showDate];
         } else  {
             self.arrAllEvents = [[NSMutableArray alloc] init];
             [self showDate];
        }
    } failure:^(NSError *error) {
       
    }];
}

- (void)getNearByPlacesByFromWS {
    [self.bsoManager getNearByLocatios:self.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSMutableDictionary class]]) {
            NSArray *arrDining = [responseObject valueForKey:@"Dining"];
            NSArray *arrLodging = [responseObject valueForKey:@"Lodging"];
            NSArray *arrParking = [responseObject valueForKey:@"Parking"];
            
            [self.aPlace.arrDining addObjectsFromArray:arrDining];
            [self.aPlace.arrLodging addObjectsFromArray:arrLodging];
            [self.aPlace.arrParking addObjectsFromArray:arrParking];
            
            [self.tblView reloadData];
            [self addMarkersToMapView];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)getPlanVisitDetails:(OrganizationType)type {
    self.selectedType = type;
    self.dateView.backgroundColor = [self.bsoManager getSelectedItemColor:self.selectedType];
    self.btnToogleBGView.backgroundColor = [self.bsoManager getSelectedItemColor:self.selectedType];
    
    if(self.selectedType == jBSO) {
        self.coordinate = CLLocationCoordinate2DMake(42.3429552, -71.0856199);
    }
    else if(self.selectedType == jTangleWood) {
        self.coordinate = CLLocationCoordinate2DMake(42.349216, -73.3099504);
    } else {
        self.coordinate = CLLocationCoordinate2DMake(42.3428873, -71.0857128);
    }
    
    self.venueAnnomation.map = nil;
    self.venueAnnomation = [[GMSMarker alloc] init];
    self.venueAnnomation.position = self.coordinate;
    self.venueAnnomation.title = @"";
    self.venueAnnomation.snippet = @"";
    self.venueAnnomation.map = self.mapView;
    
    self.aPlace = [[PlaceDetails alloc] init];
    self.aRoute = nil;
    if(self.polyline) {
        self.polyline.map = nil;
        self.polyline = nil;
    }
    
    [self.aPlace.arrDining removeAllObjects];
    [self.aPlace.arrLodging removeAllObjects];
    [self.aPlace.arrParking removeAllObjects];
    
    [self topButtonPressed:self.btnSelected];
    [self.locationManager startUpdatingLocation];
    
    [self gesShowList];
    [self getNearByPlacesByFromWS];
}

- (void)drawRouteBetweenTwoPoints {
    if(self.userAnnotation && self.venueAnnomation) {
        NSString *urlString = [NSString stringWithFormat:
                               @"%@?origin=%f,%f&destination=%f,%f&key=%@&sensor=true",
                               @"https://maps.googleapis.com/maps/api/directions/json",
                               self.userAnnotation.position.latitude,
                               self.userAnnotation.position.longitude,
                               self.venueAnnomation.position.latitude,
                               self.venueAnnomation.position.longitude,
                               GOOGLE_API_KEY];
        [Utility showLoader];
        [self.bsoManager getRouteBetweenLocations:urlString success:^(id responseObject) {
            NSDictionary *json = responseObject;
            NSArray *routes = [json valueForKey:@"routes"];
            if(routes.count) {
                GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                if(self.polyline) {
                    self.polyline.map = nil;
                    self.polyline = nil;
                }
                self.polyline  = [GMSPolyline polylineWithPath:path];
                self.polyline.strokeWidth = 7;
                self.polyline.strokeColor = [UIColor blueColor];
                self.polyline.map = self.mapView;
                
                self.aRoute = [[RouteDetails alloc] init];
                
                NSDictionary *route = json[@"routes"][0];
                NSDictionary *leg = [route valueForKey:@"legs"][0];
                self.aRoute.distance = [[leg valueForKey:@"distance"] valueForKey:@"text"];
                self.aRoute.estimatedTime = [[leg valueForKey:@"duration"] valueForKey:@"text"];
                self.aRoute.startAddress = [leg valueForKey:@"start_address"];
                self.aRoute.endAddress = [leg valueForKey:@"end_address"];
                self.aRoute.startCoordinate = CLLocationCoordinate2DMake([[[leg valueForKey:@"start_location"] valueForKey:@"lat"] doubleValue], [[[leg valueForKey:@"start_location"] valueForKey:@"lat"] doubleValue]);
                self.aRoute.endCoordinate = CLLocationCoordinate2DMake([[[leg valueForKey:@"end_location"] valueForKey:@"lat"] doubleValue], [[[leg valueForKey:@"end_location"] valueForKey:@"lat"] doubleValue]);
                
                NSArray *steps = [leg valueForKey:@"steps"];
                
                for(NSDictionary *step in steps) {
                    Step *aStep = [[Step alloc] init];
                    aStep.distance = [[step valueForKey:@"distance"] valueForKey:@"text"];
                    aStep.instruection = [step valueForKey:@"html_instructions"];
                    aStep.maneuver = [step valueForKey:@"maneuver"];
                    [self.aRoute.arrSteps addObject:aStep];
                }
                [self.tblView reloadData];
                [Utility hideLoader];
            }
            
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];
    }
}

#pragma mark - Button Actions
- (IBAction)topButtonPressed:(BSOButton *)sender {
    
    self.btnSelected.selected = FALSE;
    sender.selected = TRUE;
    self.btnSelected = sender;
    
    CGRect frame = self.imgSelectedBorder.frame;
    frame.origin.x = sender.frame.origin.x;
    self.imgSelectedBorder.frame = frame;
    if(sender == self.btnTraffic) {
        if(self.aRoute == nil) {
            [self drawRouteBetweenTwoPoints];
        }
    }
    else if(sender == self.btnDining) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isGoogleRecord = 1"];
        NSArray *records = [self.aPlace.arrDining filteredArrayUsingPredicate:predicate];
        
        if(records.count == 0) {
            [Utility showLoader];
            [self.bsoManager getNearByPlaces:self.venueAnnomation.position radius:8046 keyword:@"Dining" success:^(id responseObject) {
                [self.aPlace.arrDining addObjectsFromArray:responseObject];
                [Utility hideLoader];
                [self.tblView reloadData];
                [self addMarkersToMapView];
            } failure:^(NSError *error) {
                [Utility hideLoader];
            }];
        }
    }
    else if(sender == self.btnLodging) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isGoogleRecord = 1"];
        NSArray *records = [self.aPlace.arrLodging filteredArrayUsingPredicate:predicate];
        
        if(records.count == 0) {
            [Utility showLoader];
            [self.bsoManager getNearByPlaces:self.venueAnnomation.position radius:8046 keyword:@"Lodging" success:^(id responseObject) {
                [self.aPlace.arrLodging addObjectsFromArray:responseObject];
                [Utility hideLoader];
                [self.tblView reloadData];
                [self addMarkersToMapView];
            } failure:^(NSError *error) {
                [Utility hideLoader];
            }];
        }
    }
    else if(sender == self.btnParking) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isGoogleRecord = 1"];
        NSArray *records = [self.aPlace.arrParking filteredArrayUsingPredicate:predicate];
        
        if(records.count == 0) {
            [Utility showLoader];
            [self.bsoManager getNearByPlaces:self.venueAnnomation.position radius:8046 keyword:@"Parking" success:^(id responseObject) {
                [self.aPlace.arrParking addObjectsFromArray:responseObject];
                [Utility hideLoader];
                [self.tblView reloadData];
                [self addMarkersToMapView];
            } failure:^(NSError *error) {
                [Utility hideLoader];
            }];
        }
    }
    [self.tblView reloadData];
    [self addMarkersToMapView];
}

- (void)addMarkersToMapView {
    [self.mapView clear];
    NSMutableArray *markers = [[NSMutableArray alloc] init];
    if(self.venueAnnomation) {
        self.venueAnnomation.map = self.mapView;
        [markers addObject:self.venueAnnomation];
    }
    if(self.btnSelected == self.btnTraffic) {
        if(self.polyline) {
            self.polyline.map = self.mapView;
        }
        if(self.userAnnotation) {
            self.userAnnotation.map = self.mapView;
            [markers addObject:self.userAnnotation];
        }
    }
    else if(self.btnSelected == self.btnDining) {
        for(Place *aPlace in self.aPlace.arrDining) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = aPlace.coordinate;
            marker.title = aPlace.name;
            marker.snippet = @"";
            marker.icon = [GMSMarker markerImageWithColor:[self.bsoManager getSelectedItemColor:self.selectedType]];
            marker.map = self.mapView;
            [markers addObject:marker];
        }
    }
    else if(self.btnSelected == self.btnLodging) {
        for(Place *aPlace in self.aPlace.arrLodging) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = aPlace.coordinate;
            marker.title = aPlace.name;
            marker.snippet = @"";
            marker.icon = [GMSMarker markerImageWithColor:[self.bsoManager getSelectedItemColor:self.selectedType]];
            marker.map = self.mapView;
            [markers addObject:marker];
        }
    }
    else if(self.btnSelected == self.btnParking) {
        for(Place *aPlace in self.aPlace.arrParking) {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = aPlace.coordinate;
            marker.title = aPlace.name;
            marker.snippet = @"";
            marker.icon = [GMSMarker markerImageWithColor:[self.bsoManager getSelectedItemColor:self.selectedType]];
            marker.map = self.mapView;
            [markers addObject:marker];
        }
    }
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];;
    
    for (GMSMarker *marker in markers) {
        bounds = [bounds includingCoordinate:marker.position];
    }
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [self.mapView moveCamera:update];
    
}

- (IBAction)btnNextMonthPressed {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self.currentDate];
    [components setMonth:components.month+1];
    self.currentDate  = [gregorianCalendar dateFromComponents:components];
    
    [self showDate];
}

- (IBAction)btnPreviousMonthPressed {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self.currentDate];
    [components setMonth:components.month-1];
    self.currentDate  = [gregorianCalendar dateFromComponents:components];
    
    [self showDate];
}

- (IBAction)btnNextDatePressed {
    self.currentDate = [self.currentDate dateByAddingTimeInterval:86400];
    [self showDate];
}

- (IBAction)btnPreviousDatePressed {
    self.currentDate = [self.currentDate dateByAddingTimeInterval:-86400];
    [self showDate];
}

- (IBAction)btnMapTogglePressed {
    if(self.btnMap.selected) {
        return;
    }
    self.btnMap.selected = TRUE;
    self.btnList.selected = FALSE;
    self.mapView.hidden = FALSE;
    self.tblView.hidden = TRUE;
}

- (IBAction)btnListTogglePressed {
    if(self.btnList.selected) {
        return;
    }
    self.btnMap.selected = FALSE;
    self.btnList.selected = TRUE;
    self.mapView.hidden = TRUE;
    self.tblView.hidden = FALSE;
}

- (void)showDate {
    self.lblMonth.text = [Utility stringFromDate:self.currentDate format:@"MMMM"];
    self.lblDate.text = [Utility stringFromDate:self.currentDate format:@"dd"];
    NSString *selecteDate = [Utility stringFromDate:self.currentDate format:CALERNDAR_FORMAT];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date  = %@",selecteDate];
    NSArray *performances = [self.arrAllEvents filteredArrayUsingPredicate:predicate];
    if(performances.count) {
        Event *aEvent = performances[0];
        NSArray *placeHolderImg = @[@"BSO TAB",@"TW TAB",@"POPS TAB"];
//        [self.imgView sd_setImageWithURL:[NSURL URLWithString:aEvent.imageURL] placeholderImage:[UIImage imageNamed:[placeHolderImg objectAtIndex:self.selectedType]] options:SDWebImageRefreshCached];
        self.imgView.image = [UIImage imageNamed:[placeHolderImg objectAtIndex:self.selectedType]];
        self.lblEventInfo.text = [NSString stringWithFormat:@"%@\n%@",aEvent.title,aEvent.time];
    } else {
        self.imgView.image = nil;
        self.lblEventInfo.text = @"";
    }
}

- (IBAction)btnOrganisationPressed:(BSOButton *)sender {
    [self getPlanVisitDetails:sender.tag-1];
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.btnSelected == self.btnParking) {
        Place *aParking = [self.aPlace.arrParking objectAtIndex:indexPath.row];
        float infoTextHeight = [Utility getTextHeightOfText:aParking.address font:[UIFont fontWithName:@"OpenSans" size:15] width:tableView.frame.size.width-40];
        return MAX(infoTextHeight, 20) + 45;
        
    }if(self.btnSelected == self.btnTraffic) {
        Step *aStep = [self.aRoute.arrSteps objectAtIndex:indexPath.row];
        float infoTextHeight = [Utility getTextHeightOfText:aStep.instruection font:[UIFont fontWithName:@"OpenSans" size:14] width:tableView.frame.size.width-60];
        return MAX(infoTextHeight, 40);
        
    } else {
        return 80;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.btnSelected == self.btnTraffic) {
        return self.aRoute.arrSteps.count;
    }
    if(self.btnSelected == self.btnDining) {
        return self.aPlace.arrDining.count;
    } else if(self.btnSelected == self.btnLodging) {
        return self.aPlace.arrLodging.count;
    } else {
       return self.aPlace.arrParking.count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    headerView.backgroundColor = RGB(26, 26, 26);
    BSOLabel *lbl = [[BSOLabel alloc] initWithFrame:CGRectMake(20, 0, 700, 40)];
    lbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
    if(self.btnTraffic == self.btnSelected) {
        if(self.aRoute.distance) {
            lbl.text = [NSString stringWithFormat:@"Total Distance: %@   Estimated Duration: %@",self.aRoute.distance,self.aRoute.estimatedTime];
        } else {
            lbl.text = @"";
        }
        
    } else {
        lbl.text = [self.btnSelected titleForState:UIControlStateNormal];
    }
    
    lbl.textColor = [UIColor whiteColor];
    [headerView addSubview:lbl];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.btnSelected == self.btnParking) {
        ParkingCell *cell = (ParkingCell *) [tableView dequeueReusableCellWithIdentifier:@"ParkingCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ParkingCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
           
        }

        Place *aParking = [self.aPlace.arrParking objectAtIndex:indexPath.row];
        cell.lblInfo.text = aParking.address;
    
        float infoTextHeight = [Utility getTextHeightOfText:aParking.address font:[UIFont fontWithName:@"OpenSans" size:15] width:tableView.frame.size.width-40];

        CGRect frame = cell.lblInfo.frame;
        frame.size.height = MAX(infoTextHeight, 20) + 5;
        
        cell.lblInfo.frame = frame;
        
        cell.lblName.text = aParking.name;
        
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if(self.btnSelected == self.btnTraffic) {
        StepCell *cell = (StepCell *) [tableView dequeueReusableCellWithIdentifier:@"StepCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StepCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            
        }
        [cell.txtInstruction setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        Step *aStep = [self.aRoute.arrSteps objectAtIndex:indexPath.row];
        NSString *htmlString =  [NSString stringWithFormat:@"<font size='3' face='OpenSans' color='white'>%@</font>",aStep.instruection];
      
        cell.txtInstruction.attributedText =  [[NSAttributedString alloc] initWithData: [htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                           options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                     NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                documentAttributes:nil error:nil];

        
        
        
        float distanceWidth = [Utility getTextWidthOfText:aStep.distance font:[UIFont fontWithName:@"OpenSans" size:14] height:20];
        CGRect frame = cell.lblDistance.frame;
        frame.size.width = distanceWidth + 5;
        cell.lblDistance.frame = frame;
        
        frame = cell.txtInstruction.frame;
        frame.origin.x = cell.lblDistance.frame.size.width + cell.lblDistance.frame.origin.x + 5;
        frame.size.width = tableView.frame.size.width - (frame.origin.x + 10);
        cell.txtInstruction.frame = frame;
        cell.txtInstruction.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        cell.lblDistance.text = aStep.distance;
        
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else {
        DinigCell *cell = (DinigCell *) [tableView dequeueReusableCellWithIdentifier:@"DinigCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DinigCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            [cell.btnLink addTarget:self action:@selector(linkButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        Place *aHotel;
        if(self.btnSelected == self.btnDining) {
            aHotel = self.aPlace.arrDining[indexPath.row];
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aHotel.icon] placeholderImage:[UIImage imageNamed:@"Dining"]];
        } else {
            aHotel = self.aPlace.arrLodging[indexPath.row];
              [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aHotel.icon] placeholderImage:[UIImage imageNamed:@"Hotel"]];
        }
      
        cell.lblName.text = [NSString stringWithFormat:@"%@",aHotel.name];
        cell.lblAddress.text = aHotel.address;
        cell.lblPhone.text = @"";
        if(aHotel.isGoogleRecord) {
            cell.btnLink.hidden = FALSE;
            cell.imgArrow.hidden = FALSE;
        } else {
            if(aHotel.website.length) {
                cell.btnLink.hidden = FALSE;
                cell.imgArrow.hidden = FALSE;
            } else {
                cell.btnLink.hidden = TRUE;
                cell.imgArrow.hidden = TRUE;
            }
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
    
- (void)linkButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        Place *aHotel;
        if(self.btnSelected == self.btnDining) {
            aHotel = self.aPlace.arrDining[indexPath.row];
        } else {
            aHotel = self.aPlace.arrLodging[indexPath.row];
        }
        [Utility showLoader];
        
        [self.bsoManager getPlaceDetails:aHotel.ID success:^(id responseObject) {
            [Utility hideLoader];
            if([responseObject isKindOfClass:[Place class]]) {
              Place  *aHotel =  (Place *)responseObject;
                [Utility openURLInInterBrowser:aHotel.website from:self];
            }
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];
        
    }
}
    
    
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - LocationManager 

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = locations[0];
    self.userAnnotation.map = nil;
    self.userAnnotation = [[GMSMarker alloc] init];
    self.userAnnotation.position = location.coordinate;
    self.userAnnotation.title = @"Current Location";
    self.userAnnotation.snippet = @"";
    self.userAnnotation.map = self.mapView;
    if(self.btnSelected == self.btnTraffic)
        [self drawRouteBetweenTwoPoints];
    [manager stopUpdatingLocation];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
}

- (void)zoomToFitMapAnnotations:(MKMapView*)aMapView {
    if([aMapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(Annotation *annotation in aMapView.annotations) {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    
    region = [aMapView regionThatFits:region];
    [aMapView setRegion:region animated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
    } completion:^(id  _Nonnull context) {
        
        self.imgSelectedBorder.frame = self.btnSelected.frame;
        
    }];
}

@end