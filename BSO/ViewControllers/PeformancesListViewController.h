//
//  PeformancesListViewController.h
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PerformanceViewControllerDelegate <NSObject>
@optional
- (void)performanceViewBackButtonPressed;
- (void)performanceViewTicketButtonPressed:(Show *)aShow;
- (void)performanceViewDetailButtonPressed:(Show *)aShow;
@end

@interface PeformancesListViewController : UIViewController
@property (nonatomic, assign) id<PerformanceViewControllerDelegate> delegate;
- (void)getPefrormancesList;
@end
