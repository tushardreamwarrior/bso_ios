//
//  ShowDetailViewController.m
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "ShowDetailViewController.h"
#import "UserLoginViewController.h"
#import "ShowDetailViewController.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <AVFoundation/AVFoundation.h>
#import "SeatSelectionViewController.h"
#import "ArtistCell.h"
#import "ProgramCell.h"

@interface ShowDetailViewController () <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UIDocumentInteractionControllerDelegate,GPPSignInDelegate,UserLoginViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) AVPlayer *audioPlayer;
@property (nonatomic, assign) NSInteger currentSong;
@property (nonatomic, strong) IBOutlet UICollectionView *collection;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCollectionLine;

@property (nonatomic, strong) IBOutlet  BSOButton *btnDatesDropDown;
@property (nonatomic, strong) IBOutlet  BSOButton *btnShareEvent;
@property (nonatomic, strong) IBOutlet  UIImageView *imgCover;
@property (nonatomic, strong) IBOutlet  UIImageView *imgAudioCover;
@property (nonatomic, strong) IBOutlet  UIImageView *imgPhotoCover;
@property (nonatomic, strong) IBOutlet  UIView *photoView;
@property (nonatomic, strong) IBOutlet  UIView *audioView;

@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, strong) IBOutlet UIView *bottomView;
@property (nonatomic, strong) IBOutlet UITableView *tblPeformance;
@property (nonatomic, strong) IBOutlet UITableView *tblProgram;
@property (nonatomic, strong) IBOutlet UIView *programHeaderView;
@property (nonatomic, strong) IBOutlet UIView *shareView;

@property (nonatomic, strong) IBOutlet BSOLabel *lblShowTitle;
@property (nonatomic, strong) IBOutlet BSOTextView *txtDesc;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDescLine;
@property (nonatomic, strong) IBOutlet BSOLabel *lblOrganisation;
@property (nonatomic, strong) IBOutlet BSOLabel *lblVenue;
@property (nonatomic, strong) IBOutlet UIView *middleView;

@property (nonatomic, strong) IBOutlet BSOLabel *lblRunningTime;
@property (nonatomic, strong) IBOutlet BSOLabel *lblRunningTimeLine;

@property (nonatomic, strong) IBOutlet BSOLabel *lblMoreInfo;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMoreInfoLine;
@property (nonatomic, strong) IBOutlet UIImageView *imgMoreInfoPlus;

@property (nonatomic, strong) IBOutlet BSOTextView *txtRecipe;
@property (nonatomic, strong) IBOutlet UIImageView *imgRecipe;
@property (nonatomic, strong) IBOutlet BSOLabel *lblHashTag;
@property (nonatomic, assign) BOOL userLoggedIn;

@property (nonatomic, strong) Event *selectedEvent;
@end

@implementation ShowDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentSong = -1;
    self.bsoManager = [BSOManager SharedInstance];
    self.collection.backgroundColor = [UIColor clearColor];
    [self.collection registerClass:[ArtistCell class] forCellWithReuseIdentifier:@"ArtistCell"];
    [Utility setBorderColorWithView:self.btnShareEvent color:[UIColor lightGrayColor]];
    [self getShowDetails];
    
    [self.txtDesc setTextContainerInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    self.srcView.contentSize = CGSizeMake(self.srcView.frame.size.width, self.bottomView.frame.size.height + self.bottomView.frame.origin.y);
    
    CGRect frame = self.tblPeformance.frame;
    frame.size.height = 0;
    self.tblPeformance.frame = frame;
    
   frame = self.shareView.frame;
    frame.size.height = 0;
    self.shareView.frame = frame;
    
    [Utility setBorderColorWithView:self.tblPeformance color:[UIColor lightGrayColor]];
    [Utility setCornerToView:self.tblPeformance corner:2];
    
    if(self.aShow.title.length) {
        [self.imgCover sd_setImageWithURL:[NSURL URLWithString:self.aShow.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
        self.lblShowTitle.text = [self.aShow.title stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    }
    
    NSArray *orginisation = @[@"Boston Symphony Orchestra",@"TangleWood",@"Pops"];
    
    self.lblOrganisation.text = orginisation[self.bsoManager.selectedType];
    self.photoView.hidden = TRUE;
    self.audioView.hidden = TRUE;

    if(GET_UD(SESSION_ID) == nil) {
        self.userLoggedIn = FALSE;
    } else {
        self.userLoggedIn = TRUE;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    if(self.userLoggedIn == FALSE) {
        if(GET_UD(SESSION_ID)) {
            self.userLoggedIn = TRUE;
            [self.bsoManager getShowDetails:self.aShow.ID success:^(id responseObject) {
                Show *aShow = responseObject;
                [self.aShow.arrEvents removeAllObjects];
                [self.aShow.arrEvents addObjectsFromArray:aShow.arrEvents];
                [self.tblPeformance reloadData];
                
            } failure:^(NSError *error) {
                
            }];
        }
    }
    
}

- (void)getShowDetails {
    [Utility showLoader];
    [self.bsoManager getShowDetails:self.aShow.ID success:^(id responseObject) {
        if([responseObject isKindOfClass:[Show class]]) {
            self.aShow = responseObject;
            
            [self.imgCover sd_setImageWithURL:[NSURL URLWithString:self.aShow.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
            if(self.aShow.aMedia.audioGallery.arrItmes.count) {
                  [self.imgAudioCover sd_setImageWithURL:[NSURL URLWithString:self.aShow.aMedia.audioGallery.imageURL] placeholderImage:[UIImage imageNamed:@"Audio Image"] options:SDWebImageRefreshCached];
                self.audioView.hidden = FALSE;
            }
          
            if(self.aShow.aMedia.photoGallery.arrItmes.count) {
                [self.imgPhotoCover sd_setImageWithURL:[NSURL URLWithString:self.aShow.aMedia.photoGallery.imageURL] placeholderImage:[UIImage imageNamed:@"Photo Gallery Image"] options:SDWebImageRefreshCached];
                self.photoView.hidden = FALSE;
            }
            if([self.aShow.startDate isEqualToString:self.aShow.endDate]) {
                self.lblVenue.text = [NSString stringWithFormat:@"%@",[Utility converDateFormat:self.aShow.startDate from:DATE_FORMAT to:@"MMMM dd"]];
            }
            else {
                self.lblVenue.text = [NSString stringWithFormat:@"%@ - %@",[Utility converDateFormat:self.aShow.startDate from:DATE_FORMAT to:@"MMMM dd"],[Utility converDateFormat:self.aShow.endDate from:DATE_FORMAT to:@"MMMM dd"]];
            }
            //JORDAN HALL BOSTON - MARCH 24 - APRIL 24
            if(self.aShow.strDescription == nil) {
                self.aShow.strDescription = @"";
            }
            self.aShow.strDescription = allTrim(self.aShow.strDescription);
          
            if(self.aShow.strDescription.length == 0) {
                CGRect frame = self.txtDesc.frame;
                frame.size.height = 0;
                self.txtDesc.frame = frame;
                self.lblDescLine.hidden = TRUE;
                self.txtDesc.attributedText = [[NSAttributedString alloc] initWithString:@""];

            } else {
                self.aShow.strDescription = [NSString stringWithFormat:@"<font size='5' face='OpenSans'>%@</font>",self.aShow.strDescription];
                
                NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[self.aShow.strDescription dataUsingEncoding:NSUTF8StringEncoding]
                                                                                  options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                            NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                       documentAttributes:nil error:nil];
                self.txtDesc.attributedText = attrString;
                
                CGRect paragraphRect = [attrString boundingRectWithSize:CGSizeMake(self.txtDesc.frame.size.width, 999) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
                CGRect frame = self.txtDesc.frame;
                frame.size.height = paragraphRect.size.height;
                self.txtDesc.frame = frame;

            }
            
            
            
          CGRect  frame = self.middleView.frame;
            [self.collection reloadData];
            
            if(self.aShow.arrCast.count == 0) {
                frame.origin.y = self.collection.frame.origin.y - 36;
                self.collection.hidden = TRUE;
                self.lblCollectionLine.hidden = TRUE;
            }
            else {
                CGRect collectionFrame = self.collection.frame;
                collectionFrame.size.height =  self.collection.collectionViewLayout.collectionViewContentSize.height;
                self.collection.frame = collectionFrame;
                frame.origin.y = collectionFrame.origin.y + collectionFrame.size.height + 36;
                
            }
            
            if(self.aShow.aMedia.photoGallery.arrItmes.count == 0 && self.aShow.aMedia.audioGallery.arrItmes.count == 0) {
                frame.size.height = 0;
            } else {
                if(self.aShow.aMedia.photoGallery.arrItmes.count != 0 && self.aShow.aMedia.audioGallery.arrItmes.count !=0 ) { // No Change in frame
                    
                } else {
                      if(self.aShow.aMedia.audioGallery.arrItmes.count != 0 && self.aShow.aMedia.photoGallery.arrItmes.count == 0) {
                          frame.size.height = 346;
                      }
                     if(self.aShow.aMedia.audioGallery.arrItmes.count == 0 && self.aShow.aMedia.photoGallery.arrItmes.count != 0) {
                        CGRect photoRect = self.photoView.frame;
                        photoRect.origin.y = 44;
                        self.photoView.frame = photoRect;
                        frame.size.height = 346;
                    }
                }
            }
            
            self.middleView.frame = frame;
            
            frame = self.txtDesc.frame;
            if( self.middleView.frame.size.height) {
                frame.origin.y = self.middleView.frame.origin.y + self.middleView.frame.size.height;
            } else {
                frame.origin.y = self.middleView.frame.origin.y + self.middleView.frame.size.height + 40;
            }
            
            self.txtDesc.frame = frame;
            
            frame = self.lblDescLine.frame;
            frame.origin.y = self.txtDesc.frame.origin.y - 20;
            self.lblDescLine.frame = frame;
            
            [self.tblProgram reloadData];
            
            float y = self.txtDesc.frame.origin.y + self.txtDesc.frame.size.height;
            if(self.aShow.arrProgram.count == 0) {
                self.tblProgram.hidden = TRUE;
            } else {
                self.programHeaderView.backgroundColor = [self.bsoManager getSelectedItemColor];
                self.tblProgram.tableHeaderView = self.programHeaderView;
                CGRect frame = self.tblProgram.frame;
                frame.origin.y = y;
                frame.size.height = self.tblProgram.contentSize.height;
                self.tblProgram.frame = frame;
                y = frame.size.height + frame.origin.y + 10;
            }
            
            frame = self.bottomView.frame;
            frame.origin.y = y;
            self.bottomView.frame = frame;
            
            y = 1;
          //  self.aShow.runningTime = @"time";
            if(self.aShow.runningTime) {
                self.lblRunningTime.hidden = FALSE;
                self.lblRunningTimeLine.hidden = FALSE;
                y = self.lblRunningTimeLine.frame.origin.y + 1;
            } else {
                self.lblRunningTime.hidden = TRUE;
                self.lblRunningTimeLine.hidden = TRUE;
            }
          //  self.aShow.moreInfo = @"more info";
            if(self.aShow.moreInfo) {
                self.lblMoreInfo.hidden = FALSE;
                self.lblMoreInfoLine.hidden = FALSE;
                self.imgMoreInfoPlus.hidden = FALSE;
                
                CGRect frame = self.lblMoreInfo.frame;
                frame.origin.y = y + 16;
                self.lblMoreInfo.frame = frame;
                
                frame = self.imgMoreInfoPlus.frame;
                frame.origin.y = y + 21;
                self.imgMoreInfoPlus.frame = frame;
                
                frame = self.lblMoreInfoLine.frame;
                frame.origin.y = self.lblMoreInfo.frame.origin.y + self.lblMoreInfo.frame.size.height + 16;
                self.lblMoreInfoLine.frame = frame;
                
                y = frame.origin.y + 1;
                
            } else {
                self.lblMoreInfo.hidden = TRUE;
                self.lblMoreInfoLine.hidden = TRUE;
                self.imgMoreInfoPlus.hidden = TRUE;
            }
            
             frame = self.txtRecipe.frame;
            frame.origin.y = y + 20;
            self.txtRecipe.frame = frame;
            
            frame = self.imgRecipe.frame;
            frame.origin.y = y + 20;
            self.imgRecipe.frame = frame;
            
            frame = self.bottomView.frame;;
            frame.size.height = self.txtRecipe.frame.origin.y + self.txtRecipe.frame.size.height + 20;
            self.bottomView.frame = frame;
            
            [self.tblPeformance reloadData];

            
            if(self.aShow.arrEvents.count <= 10) {
                self.tblPeformance.scrollEnabled = FALSE;
            }
            
            //[NSString stringWithFormat:@"%@ - %@",aShow.startDate,aShow.endDate];
            
            self.lblHashTag.text = self.aShow.hashTag;
            self.srcView.contentSize = CGSizeMake(self.srcView.frame.size.width, self.bottomView.frame.size.height + self.bottomView.frame.origin.y);
            
            self.lblShowTitle.text = [self.aShow.title stringByReplacingOccurrencesOfString:@"\n" withString:@" "];

          
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

#pragma mark - CollectionView DataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width/2,100);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.aShow.arrCast.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ArtistCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ArtistCell" forIndexPath:indexPath];
    Cast *aCast = [self.aShow.arrCast objectAtIndex:indexPath.row];
    cell.lblName.text = aCast.name;
    cell.lblOther.text = aCast.role;
    cell.btnBio.tag = indexPath.row;
    [cell.btnBio addTarget:self action:@selector(casetFullBioPressed:) forControlEvents:UIControlEventTouchUpInside];
     [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aCast.imageURL] placeholderImage:[UIImage imageNamed:@"no_image.png"] options:SDWebImageRefreshCached];
    return cell;
}

- (void)casetFullBioPressed:(UIButton *)sender {
     Cast *aCast = [self.aShow.arrCast objectAtIndex:sender.tag];
    NSString *castDescription = [NSString stringWithFormat:@"<font size='5' face='OpenSans'>%@</font>",aCast.strDescription];
    
    [Utility openContentInInterBrowser:castDescription from:self];
}

#pragma mark - UITableView 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tblPeformance) {
        return self.aShow.arrEvents.count;
    } else {
        return self.aShow.arrProgram.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.tblPeformance) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
            cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:16.0];
        }
        BSOButton *btnTicket = [BSOButton buttonWithType:UIButtonTypeCustom];
        [btnTicket setTitle:@"BUY TICKETS" forState:UIControlStateNormal];
        btnTicket.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:16.0];
        [btnTicket setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        btnTicket.tag = indexPath.row;
        [btnTicket addTarget:self action:@selector(btnBuyTicketPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btnTicket setFrame:CGRectMake(0, 0, 120, 40)];
        
        Event *aEvent = [self.aShow.arrEvents objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@" %@ at %@",[aEvent.date uppercaseString],aEvent.time];
        cell.accessoryView = btnTicket;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        btnTicket.hidden = !aEvent.isOnSale;
        return cell;

    } else {
        ProgramCell *cell = (ProgramCell *) [tableView dequeueReusableCellWithIdentifier:@"ProgramCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProgramCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            [cell.btnPlay addTarget:self action:@selector(programPlayButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
          
        }
        Program *aProgram = [self.aShow.arrProgram objectAtIndex:indexPath.row];
        NSString *desc = [NSString stringWithFormat:@"<font size='4' face='OpenSans-Bold'>%@</font>",aProgram.title];
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[desc dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                               documentAttributes:nil error:nil];
        cell.txtDesc.attributedText = attrString;
        
        cell.btnPlay.hidden = !aProgram.mediarURL.length;
        if(indexPath.row == self.currentSong) {
            cell.btnPlay.selected = TRUE;
        } else {
            cell.btnPlay.selected = FALSE;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)btnBuyTicketPressed:(UIButton *)sender {
    Event *aEvent = [self.aShow.arrEvents objectAtIndex:sender.tag];
    self.selectedEvent = aEvent;
    if(GET_UD(SESSION_ID)) {
        [self callBestAvailabeSeatWS];

    } else {
        UserLoginViewController *viewController = [[UserLoginViewController alloc] initWithNibName:@"UserLoginViewController" bundle:nil];
        viewController.delegate = self;
        [self.navigationController pushViewController:viewController animated:YES];
       
    }
}

- (void)programPlayButtonPressed:(BSOButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblProgram];
    NSIndexPath *indexPath = [self.tblProgram indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        Program *aProgram = [self.aShow.arrProgram objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
       
        if(self.audioPlayer) {
            [self.audioPlayer pause];
            [self.audioPlayer setRate:0];
            self.audioPlayer = nil;
        }
        aProgram.isPlaying = !aProgram.isPlaying;
        if(self.currentSong == indexPath.row) {
            self.currentSong = -1;
        } else {
            self.currentSong = indexPath.row;
            AVPlayerItem *aPlayerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:aProgram.mediarURL]];
            self.audioPlayer = [[AVPlayer alloc] initWithPlayerItem:aPlayerItem];
            [[NSNotificationCenter defaultCenter] addObserver: self
                                                     selector: @selector(playerItemDidReachEnd:)
                                                         name: AVPlayerItemDidPlayToEndTimeNotification
                                                       object: [self.audioPlayer currentItem]];
            [self.audioPlayer play];
   
        }
       
        [self.tblProgram reloadData];
        

    }
}

- (void)playerItemDidReachEnd:(id)sender {
    self.currentSong = -1;
    [self.tblProgram reloadData];
}

- (void)callBestAvailabeSeatWS {
    [Utility showLoader];
    [self.bsoManager getBestAvailableSeats:self.selectedEvent.pefrormanceID success:^(id responseObject) {
        if([responseObject isKindOfClass:[AvailalbeSeatResponse class]]) {
            SeatSelectionViewController *viewController = [[SeatSelectionViewController alloc] initWithNibName:@"SeatSelectionViewController" bundle:nil];
            viewController.aAvailalbeSeat = responseObject;
            self.selectedEvent.title= self.aShow.title;
            viewController.aEvent = self.selectedEvent;
            [self.navigationController pushViewController:viewController animated:YES];
        } else {
            [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
            
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)userLoginSuccessfully:(BOOL)success {
    if(success) {
        [self callBestAvailabeSeatWS];
    }
}

#pragma mark - Button Actions

- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDatesDropDownPressed {
    if(self.aShow.arrEvents.count == 0) {
        return;
    }
    CGRect frame = self.tblPeformance.frame;
    if(frame.size.height == 0) {
        frame.size.height = MIN(self.aShow.arrEvents.count * 40, 400);
    } else {
        frame.size.height = 0;
    }
    
    [UIView animateWithDuration:.5 animations:^{
        self.tblPeformance.frame = frame;
    }];
}

- (IBAction)btnShareEventPressed {
    CGRect frame = self.shareView.frame;
    if(frame.size.height == 0) {
        frame.size.height = 140;
    } else {
        frame.size.height = 0;
    }
    
    [UIView animateWithDuration:.5 animations:^{
        self.shareView.frame = frame;
    }];
}

- (IBAction)btnShareItemPressed:(UIButton *)sender {
    [self btnShareEventPressed];
    if(sender.tag == 1) {
        [Utility shareOnTwiitter:self text:self.aShow.title image:self.imgCover.image];
    } else if(sender.tag == 2) {
        [Utility shareOnFacebook:self text:self.aShow.title image:self.imgCover.image];
    } else if(sender.tag == 3) {
        if (![GPPSignIn sharedInstance].authentication ||
            ![[GPPSignIn sharedInstance].scopes containsObject:
              kGTLAuthScopePlusLogin]) {
                [Utility signInGooglePlus:self];
                
            } else {
                [Utility shareOnGoolgePlus:self text:self.aShow.title image:self.imgCover.image];
            }
    }else if(sender.tag == 4) {
        [Utility shareOnInstagram:self text:self.aShow.title image:self.imgCover.image frame:self.shareView.frame];
    }
}


- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    NSLog(@"Received error %@ and auth object %@", error, auth);
    if (error) {
        // Do some error handling here.
    } else {
        [Utility shareOnGoolgePlus:self text:self.aShow.title image:self.imgCover.image];
      }
}

- (void)finishedSharingWithError:(NSError *)error {
    NSString *text;
    
    if (!error) {
        [Utility ShowAlertWithTitle:@"Success" Message:@"Successfully posted to Google +"];
    } else if (error.code == kGPPErrorShareboxCanceled) {
        text = @"Canceled";
    } else {
        text = [NSString stringWithFormat:@"Error (%@)", [error localizedDescription]];
    }
    
    NSLog(@"Status: %@", text);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        CGRect paragraphRect = [self.txtDesc.attributedText boundingRectWithSize:CGSizeMake(self.txtDesc.frame.size.width, 999) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
        
        CGRect frame = self.txtDesc.frame;
        frame.size.height = paragraphRect.size.height;
        self.txtDesc.frame = frame;
        frame = self.bottomView.frame;
        float y = self.txtDesc.frame.origin.y + self.txtDesc.frame.size.height;
        if(self.aShow.arrProgram.count) {
            CGRect frame = self.tblProgram.frame;
            y = frame.size.height + frame.origin.y + 10;

        }
        frame.origin.y = y;
        self.bottomView.frame = frame;

    } completion:^(id  _Nonnull context) {
        self.srcView.contentSize = CGSizeMake(self.srcView.frame.size.width, self.bottomView.frame.size.height + self.bottomView.frame.origin.y);
    }];
}

- (void)stopMusicPlayer {
    self.currentSong = -1;
    [self.tblProgram reloadData];
    if(self.audioPlayer) {
        [self.audioPlayer pause];
        [self.audioPlayer setRate:0];
        self.audioPlayer = nil;
    }
}
@end