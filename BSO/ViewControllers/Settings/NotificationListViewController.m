//
//  NotificationListViewController.m
//  BSO
//
//  Created by MAC on 14/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "NotificationListViewController.h"

@interface NotificationListViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSMutableArray *arrNotifications;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation NotificationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.title = @"Notifications";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackPressed)];
    self.table.tableFooterView = [[UIView alloc] init];
    [self getNotificationList];
}

- (void)getNotificationList {
    [Utility showLoader];
    [self.bsoManager getListOfNotifications:^(id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]]) {
            self.arrNotifications = [[NSMutableArray alloc] initWithArray:responseObject];
            [self.table reloadData];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableView 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrNotifications.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
        UIImageView *imgArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightArrow"]];
        imgArrow.frame = CGRectMake(0, 0, 32, 32);
        cell.accessoryView = imgArrow;
    }
    
    Notification *aNotification = [self.arrNotifications objectAtIndex:indexPath.row];
    
    cell.textLabel.text = aNotification.title;
    cell.backgroundColor = [UIColor clearColor];

    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   Notification *aNotification = [self.arrNotifications objectAtIndex:indexPath.row];
    NSString *castDescription = [NSString stringWithFormat:@"<font size='5' face='OpenSans'>%@</font>",aNotification.desc];
    [Utility openContentInInterBrowser:castDescription from:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end