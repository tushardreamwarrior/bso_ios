//
//  UserLoginViewController.h
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserLoginViewDelegate <NSObject>

- (void)userLoginSuccessfully:(BOOL)success;

@end

@interface UserLoginViewController : UIViewController
@property (nonatomic, assign) id <UserLoginViewDelegate> delegate;
@end
