//
//  CreatPinViewController.m
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "CreatPinViewController.h"

@interface CreatPinViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet BSOTextField *txtPin;
@property (nonatomic, strong) IBOutlet BSOTextField *txtConfrimPin;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMessage;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnBack;

@end

@implementation CreatPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name: UITextFieldTextDidChangeNotification object:nil];
    
    self.txtPin.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    [Utility setLeftViewWithTextField:self.txtPin];
    
    self.txtConfrimPin.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    [Utility setLeftViewWithTextField:self.txtConfrimPin];
    
     UIColor *color = [UIColor lightGrayColor];
    self.txtPin.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPin.placeholder attributes:@{NSForegroundColorAttributeName: color}];
     self.txtConfrimPin.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtConfrimPin.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    self.txtPin.text = GET_UD(DIGIT_PIN);
    self.txtConfrimPin.text = GET_UD(DIGIT_PIN);
    
    self.navigationController.navigationBarHidden = YES;
    
    if(self.bsoManager.wizardStep == 2) {
        CGRect frame = self.txtConfrimPin.frame;
        frame.origin.y = 15;
        frame.size.height = 65;
        self.lblMessage.frame = frame;
        
    } else {
        self.btnBack.hidden = TRUE;
    }
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.txtPin) {
        [self.txtConfrimPin becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (IBAction)backButtonPressed {
    [self cancelButtonPressed];
}

- (void)textDidChange:(NSNotification*)notification {
    UITextField *textField = (UITextField *)[notification object];
    if(textField.text.length > 4) {
        textField.text = [textField.text substringToIndex:4];
    }
}

- (IBAction)cancelButtonPressed {
    if([self.delegate respondsToSelector:@selector(createPinViewCancelButtonPressed)]) {
        [self.delegate createPinViewCancelButtonPressed];
    }
    if(self.bsoManager.wizardStep == 2) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)submitButtonPressed {
    if(self.txtPin.text.length < 4) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Pin must be 4 digit long"];
        return;
    }
    if(![self.txtConfrimPin.text isEqualToString:self.txtPin.text]) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Pin and Confirm Pin must be same"];
        return;
    }
    SET_UD(DIGIT_PIN, self.txtPin.text);
    if(self.bsoManager.wizardStep == 2) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if([self.delegate respondsToSelector:@selector(createPinViewSubmitButtonPressed:)]) {
        [self.delegate createPinViewSubmitButtonPressed:self.txtPin.text];
    }
  
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end