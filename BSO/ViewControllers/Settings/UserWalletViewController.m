//
//  UserWalletViewController.m
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "UserWalletViewController.h"
#import "PurchaeHistoryViewController.h"

@interface UserWalletViewController ()
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet UIView *buttonView;
@property (nonatomic, strong) IBOutlet BSOButton *btnMyTicket;
@property (nonatomic, strong) IBOutlet BSOButton *btnMySettings;
@property (nonatomic, strong) IBOutlet BSOButton *btnMyPromotions;
@property (nonatomic, strong) IBOutlet BSOButton *btnMyPurchases;
@property (nonatomic, strong)  BSOButton *btnSelected;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (nonatomic, strong) PurchaeHistoryViewController *purchaeHistoryViewController;
@end

@implementation UserWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
   
    for(BSOButton *btn in self.buttonView.subviews) {
        [btn setTitleColor:BSO_COLOR  forState:UIControlStateSelected];
    }
    
    self.btnSelected = self.btnMyTicket;
    
    self.title = @"My Wallet";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    if(self.isMyPurchase) {
        [self topButtonPressed:self.btnMyPurchases];
        [self.btnMyPurchases layoutIfNeeded];
        [self.btnMyPurchases setNeedsLayout];

    } else {
        [self.bsoManager checkUserSession:GET_UD(USER_EMAIL) success:^(id responseObject) {
            
        } failure:^(NSError *error) {
            
        }];
    }
}

- (IBAction)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)removeOtherViewController {
    if(self.purchaeHistoryViewController) {
        [self.purchaeHistoryViewController willMoveToParentViewController:nil];
        [self.purchaeHistoryViewController.view removeFromSuperview];
        [self.purchaeHistoryViewController removeFromParentViewController];
        
        self.purchaeHistoryViewController = nil;
    }
}

- (IBAction)topButtonPressed:(BSOButton *)sender {
    if(sender.selected) {
        return;
    }
    
    sender.selected = TRUE;
    self.btnSelected.selected = FALSE;
    self.btnSelected = sender;
    
    [self removeOtherViewController];
    
    if(sender == self.btnMyPurchases) {
        [self openMyPurchaseViewController];
    }
}

- (void)openMyPurchaseViewController {
   self.purchaeHistoryViewController = [[PurchaeHistoryViewController alloc] initWithNibName:@"PurchaeHistoryViewController" bundle:nil];
    CGRect frame = self.purchaeHistoryViewController.view.frame;
    frame.size.height  =  [UIScreen mainScreen].bounds.size.height -  self.containerView.frame.origin.y;
    
    [self addChildViewController:self.purchaeHistoryViewController];
    [self.containerView addSubview:self.purchaeHistoryViewController.view];
    self.purchaeHistoryViewController.view.frame = frame;
    [self.purchaeHistoryViewController didMoveToParentViewController:self];
}

@end