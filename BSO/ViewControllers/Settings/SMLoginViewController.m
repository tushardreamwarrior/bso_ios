//
//  SMLoginViewController.m
//  BSO
//
//  Created by MAC on 18/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SMLoginViewController.h"

@interface SMLoginViewController ()
@property (nonatomic, strong) IBOutlet BSOTextField *txtName;
@property (nonatomic, strong) IBOutlet BSOTextField *txtPassword;
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation SMLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
    
    self.txtName.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    [Utility setLeftViewWithTextField:self.txtName];
    self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtName.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.txtPassword.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    [Utility setLeftViewWithTextField:self.txtPassword];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPassword.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    self.title = @"Sign In With Social Network";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.txtName == textField) {
        [self.txtPassword becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signInButtonPressed  {
    
}

- (IBAction)twitterButtonPressed {
    
}

- (IBAction)instagramButtonPressed {
    
}

- (IBAction)fbButtonPressed {
    
}

@end
