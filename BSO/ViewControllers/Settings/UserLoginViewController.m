//
//  UserLoginViewController.m
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "UserLoginViewController.h"

@interface UserLoginViewController () <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet BSOTextField *txtName;
@property (nonatomic, strong) IBOutlet BSOTextField *txtPassword;
@property (nonatomic, strong) IBOutlet UIView *viewName;
@property (nonatomic, strong) IBOutlet UIView *viewPassword;
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, strong) IBOutlet BSOButton *btnBack;
@property (nonatomic, strong) IBOutlet BSOButton *btnForgotPass;
@property (nonatomic, strong) IBOutlet BSOButton *btnAboutUS;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation UserLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
    
    self.viewName.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    self.viewPassword.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
    if ([self.txtName respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtName.placeholder attributes:@{NSForegroundColorAttributeName: color}];

        self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPassword.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
    if(GET_UD(USER_EMAIL)) {
        self.txtName.text = GET_UD(USER_EMAIL);
        self.txtPassword.text = GET_UD(USER_PASSWORD);
    }
    
    self.title = @"Log In";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    if(self.bsoManager.wizardStep == 1) {
        self.navigationController.navigationBarHidden = TRUE;
        self.btnAboutUS.hidden = TRUE;
        self.btnForgotPass.hidden = TRUE;
        CGRect frame = self.srcView.frame;
        frame.origin.y += 25;
        frame.size.height -= 25;
        self.srcView.frame = frame;
        
    } else {
        self.btnBack.hidden = TRUE;
        self.lblTitle.hidden = TRUE;
    }
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.txtName == textField) {
        [self.txtPassword becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (IBAction)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logInButtonPresse {
    if(self.txtName.text.length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Username can not be blank"];
        return;
    }
    if(self.txtPassword.text.length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Password can not be blank"];
       return;
    }
    
    [Utility showLoader];
    [self.bsoManager bsoLogin:self.txtName.text password:self.txtPassword.text success:^(id responseObject) {
        SET_UD(USER_EMAIL, self.txtName.text);
        SET_UD(USER_PASSWORD, self.txtPassword.text);
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            [self backButtonPressed];
            [Utility ShowAlertWithTitle:@"Success" Message:@"Logged in succesfully"];
            
            BOOL isLogin = [[[responseObject valueForKey:DATA] valueForKey:@"is_login"] boolValue];
            [[NSUserDefaults standardUserDefaults] setBool:isLogin forKey:@"isUserLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if([self.delegate respondsToSelector:@selector(userLoginSuccessfully:)]) {
                [self.delegate userLoginSuccessfully:YES];
            }
            
        } else {
             [Utility ShowAlertWithTitle:@"Failed" Message:@"Username or password not valid"];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
         [Utility hideLoader];
    }];
}

- (IBAction)signUpButtonPressed {
    
}

- (IBAction)forgotPasswordButtonPressed {
    
}

- (IBAction)aboutBSOButtonPressed {
    
}

@end
