//
//  EntePinNumberViewController.m
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "EnterPinNumberViewController.h"

@interface EnterPinNumberViewController () <UITextFieldDelegate>

@end

@implementation EnterPinNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    for(int i=1;i<=4;i++) {
        UITextField *txtField = (UITextField *)[self.view viewWithTag:i];
        txtField.text = @"";
    }
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name: UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    UITextField *txtField = (UITextField *)[self.view viewWithTag:1];
    [txtField becomeFirstResponder];
}

- (IBAction)nextButtonPressed {
    
    NSString *pinNumber = @"";
    for(int i=1;i<=4;i++) {
        UITextField *txtField = (UITextField *)[self.view viewWithTag:i];
        if(txtField.text.length)
            pinNumber = [pinNumber stringByAppendingString:[txtField.text substringFromIndex:1]];
    }
    if(pinNumber.length < 4) {
     //   self.pinNumberCaptionLbl.text = @"Enter 4 digit Pin Number";
        //self.pinNumberCaptionLbl.textColor = [UIColor redColor];
        return;
    } else {
        self.pinNumberCaptionLbl.text = @"PIN Number";
        self.pinNumberCaptionLbl.textColor = [UIColor whiteColor];
    }
    
    NSString *passcode = GET_UD(DIGIT_PIN);
    
    if([passcode isEqualToString:pinNumber]) {
        [self dismissViewControllerAnimated:YES completion:^{
           
        }];
    } else {
        //self.resetPasswordBtn.hidden = FALSE;
        self.pinNumberCaptionLbl.text = @"Invalid PIN Number";
        self.pinNumberCaptionLbl.textColor = [UIColor redColor];
        for(int i=1;i<=7;i++) {
            UITextField *txtField = (UITextField *)[self.view viewWithTag:i];
            txtField.text = @"";
            [txtField setBackground:[UIImage imageNamed:@"pinNumber.png"]];
        }
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textField.layer.borderWidth = 10.0;
    textField.layer.cornerRadius = 10.0f;
    [textField setText:@"P"];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if (isBackSpace == -8) {
        if(textField.tag != 1) {
            UITextField *txtField = (UITextField *)[self.view viewWithTag:textField.tag - 1];
            [txtField setBackground:[UIImage imageNamed:@"pinNumber.png"]];
            [self performSelector:@selector(updateTextFiled:) withObject:txtField afterDelay:.03];
        } else {
            return NO;
        }
    }
    return YES;
}

- (void)updateTextFiled:(UITextField *)txtField {
    [txtField becomeFirstResponder];
}

- (void)textDidChange:(NSNotification*)notification {
    UITextField *textField = (UITextField *)[notification object];
    if(textField.text.length) {
        [textField setBackground:[UIImage imageNamed:@"pinNumberSelected.png"]];
        if(![textField.text hasPrefix:@"P"]) {
            textField.text = [NSString stringWithFormat:@"P%@",textField.text];
        }
        if(textField.tag != 4) {
             UITextField *txtField = (UITextField *)[self.view viewWithTag:textField.tag + 1];
            [txtField becomeFirstResponder];
        } else {
            [textField resignFirstResponder];
            [self nextButtonPressed];
        }
        
    } else {
        [textField setBackground:[UIImage imageNamed:@"pinNumber.png"]];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)resetPasswordButtonPressed {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)preferredContentSize {
    return CGSizeMake(470.0, 200.0);
}



@end