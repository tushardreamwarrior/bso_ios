//
//  CardDetailViewController.m
//  BSO
//
//  Created by MAC on 29/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "CardDetailViewController.h"
#import "SelectionView.h"

@interface CardDetailViewController ()  <SelectionDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) NSMutableArray *arrMonths;
@property (nonatomic, strong) NSMutableArray *arrYear;
@property (nonatomic, strong) NSMutableArray *arrCards;

@property (nonatomic, strong) IBOutlet BSOTextField *txtCarNumber;
@property (nonatomic, strong) IBOutlet BSOTextField *txtNameonCard;
@property (nonatomic, strong) IBOutlet BSOTextField *txtCVV;
@property (nonatomic, strong) IBOutlet BSOButton *btnCardType;
@property (nonatomic, strong) IBOutlet BSOButton *btnExpireMonth;
@property (nonatomic, strong) IBOutlet BSOButton *btnExpireYear;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;

@property (nonatomic, strong) IBOutlet BSOLabel *lblCardNumber;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardNameOnCard;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardExpire;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardCVV;
@property (nonatomic, strong) IBOutlet UIScrollView *srcCarDetails;
@end

@implementation CardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.title = @"Crediat Card Information";
    
    self.arrMonths = [[NSMutableArray alloc] init];
    [self.btnExpireMonth setTitle:@"01" forState:UIControlStateNormal];
    [self.btnExpireYear setTitle:[Utility stringFromDate:[NSDate date] format:@"yyyy"] forState:UIControlStateNormal];
    
    self.arrMonths = [[NSMutableArray alloc] init];
    self.arrYear = [[NSMutableArray alloc] init];
    self.arrCards = [[NSMutableArray alloc] init];
    
    for (CreditCard *aCard in self.bsoManager.arrCreditCards) {
        [self.arrCards addObject:aCard.cardName];
    }
    if(self.arrCards.count)
        [self.btnCardType setTitle:self.arrCards[0] forState:UIControlStateNormal];
    
    for(int i=1;i<=12;i++) {
        [self.arrMonths addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    int currentYear = [[Utility stringFromDate:[NSDate date] format:@"yyyy"] intValue];
    for(int i=0;i<5;i++) {
        [self.arrYear addObject:[NSString stringWithFormat:@"%d",currentYear+i]];
    }
    
    for(UIView *aView in self.srcView.subviews) {
        if([aView isKindOfClass:[BSOTextField class]]) {
            BSOTextField *txtField = (BSOTextField *)aView;
            [Utility setBorderColorWithView:txtField color:RGB(135, 135, 135)];
            [Utility setLeftViewWithTextField:txtField];
            [Utility setCornerToView:txtField corner:10];
        }
    }
  
    if(GET_UD(PAYMENT_CARD_NUMBER) == nil) {
        self.srcCarDetails.hidden = TRUE;
        self.srcView.hidden = FALSE;
    } else {
        self.srcCarDetails.hidden = FALSE;
        self.srcView.hidden = TRUE;
        [self getUserCardDetail];
    }
    
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUserCardDetail {
    [Utility showLoader];
    [self.bsoManager getCardInfo:^(id responseObject) {
       if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
           NSDictionary *data = [responseObject valueForKey:DATA];
           if(![[data valueForKey:@"card_number"] isKindOfClass:[NSNull class]]) {
               NSString *cardNumber = [data valueForKey:@"card_number"];
               NSString *cardMonth = [data valueForKey:@"card_month"];
               NSString *cardYear = [data valueForKey:@"car_year"];
               if(cardMonth.length == 1) {
                   cardMonth = [@"0" stringByAppendingString:cardMonth];
               }
               
               NSString *key = GET_UD(KEY);
               NSString *cardType = [Utility decryptString:GET_UD(PAYMENT_CARD_TYPE) withKey:key];
               NSString *cardCVV = [Utility decryptString:GET_UD(PAYMENT_CARD_CVV) withKey:key];
               NSString *cardName = [Utility decryptString:GET_UD(PAYMENT_CARD_NAME) withKey:key];
            
               self.lblCardNumber.text = [NSString stringWithFormat:@"%@ ending in %@",cardType,cardNumber];
               self.lblCardNameOnCard.text = cardName;
               self.lblCardExpire.text = [NSString stringWithFormat:@"%@ %@",cardMonth,cardYear];
               
               NSString *starCVV = @"";
               for (int i=1;i<=cardCVV.length;i++) {
                   starCVV = [starCVV stringByAppendingString:@"*"];
               }
               self.lblCardCVV.text = starCVV;
               
           } else {
               self.srcCarDetails.hidden = TRUE;
               self.srcView.hidden = FALSE;
           }
       } else {
           [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
       }
      [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (IBAction)btnCardTypePressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrCards];
    selView.delegate = self;
    selView.selectedIndex = [self.arrCards indexOfObject:[self.btnCardType titleForState:UIControlStateNormal]];
    [selView showInView:self.btnCardType];
}

- (IBAction)btnExpireMonthPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrMonths];
    selView.delegate = self;
    [selView showInView:self.btnExpireMonth];
}

- (IBAction)btnExpireYearPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrYear];
    selView.delegate = self;
    [selView showInView:self.btnExpireYear];
}

- (void)SelectionView:(SelectionView *)selectionView didSelectItem:(NSString *)item withIndex:(NSInteger)index SenderView:(UIView *)senderView {
    BSOButton *btn = (BSOButton *)senderView;
    [btn setTitle:item forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnSubmitPressed {
    if(allTrim(self.txtCarNumber.text).length < 8) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Valid Card number"];
        return;
    }
    if(allTrim(self.txtNameonCard.text).length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Name of card"];
        return;
    }
    if(allTrim(self.txtCVV.text).length == 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter CVV"];
        return;
    }
    
    CreditCard *aCard = [[CreditCard alloc] init];
    aCard.cardNumber = self.txtCarNumber.text;
    aCard.cardName = self.txtNameonCard.text;
    aCard.cardCVV = self.txtCVV.text;
    aCard.cardExpireMonth = [self.btnExpireMonth titleForState:UIControlStateNormal];
    aCard.cardExpireYear = [self.btnExpireYear titleForState:UIControlStateNormal];
    aCard.cardType = [self.btnCardType titleForState:UIControlStateNormal];
    
    [Utility showLoader];
    BSOManager *bsoManager = [BSOManager SharedInstance];
    [bsoManager validateCardInfo:aCard success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            
            
            NSString *key = GET_UD(KEY);
            NSString *encyptedName = [Utility encryptString:self.txtNameonCard.text withKey:key];
            NSString *encyptedType = [Utility encryptString:[self.btnCardType titleForState:UIControlStateNormal] withKey:key];
            NSString *encyptedNumer = [Utility encryptString:[self.txtCarNumber.text substringToIndex:self.txtCarNumber.text.length - 4] withKey:key];
            NSString *encyptedCVV = [Utility encryptString:self.txtCVV.text withKey:key];
            
            
            SET_UD(PAYMENT_CARD_NAME,encyptedName);
            SET_UD(PAYMENT_CARD_TYPE,encyptedType);
            SET_UD(PAYMENT_CARD_NUMBER,encyptedNumer);
            SET_UD(PAYMENT_CARD_CVV,encyptedCVV);
            
            [self backButtonPressed];
            
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (IBAction)btnEditCardPressed {
    self.srcCarDetails.hidden = TRUE;
    self.srcView.hidden = FALSE;
}

@end
