//
//  CreatPinViewController.h
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol CreatPinViewControllerDelegate <NSObject>
@optional
- (void)createPinViewCancelButtonPressed;
- (void)createPinViewSubmitButtonPressed:(NSString *)pin;
@end

@interface CreatPinViewController : UIViewController
@property (nonatomic, assign) id <CreatPinViewControllerDelegate> delegate;
@end
