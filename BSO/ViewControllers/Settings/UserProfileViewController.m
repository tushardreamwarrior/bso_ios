//
//  UserProfileViewController.m
//  BSO
//
//  Created by MAC on 06/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "UserProfileViewController.h"

@interface UserProfileViewController () <UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet BSOTextField *txtName;
@property (nonatomic, strong) IBOutlet BSOTextField *txtEmail;
@property (nonatomic, strong) IBOutlet BSOTextField *txtPassword;
@property (nonatomic, strong) IBOutlet BSOTextField *txtMobile;
@property (nonatomic, strong) IBOutlet BSOTextField *txtAddress;
@property (nonatomic, strong) IBOutlet UIScrollView *srcView;
@property (nonatomic, strong) IBOutlet BSOButton *btnLogo;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self.btnLogo setImage:[UIImage imageNamed:[NSString stringWithFormat:@"logo_%d",self.bsoManager.selectedType]] forState:UIControlStateNormal];
    
    for(UITextField *txtField in self.srcView.subviews) {
        if([txtField isKindOfClass:[BSOTextField class]]) {
            txtField.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternBG"]];
            [Utility setLeftViewWithTextField:txtField];
            txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        }
    }
    
    self.title = @"My Profile";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.txtName == textField) {
        [self.txtEmail becomeFirstResponder];
    }
    else if(self.txtEmail == textField) {
        [self.txtPassword becomeFirstResponder];
    }
    else if(self.txtPassword == textField) {
        [self.txtMobile becomeFirstResponder];
    }
    else if(self.txtMobile == textField) {
        [self.txtAddress becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }

    return YES;
}

- (IBAction)createaAccountButtonPressed {
    
}

- (IBAction)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
