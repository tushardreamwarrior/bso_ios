//
//  EntePinNumberViewController.h
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterPinNumberViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *pinNumberCaptionLbl;
@property (nonatomic, strong) IBOutlet UIButton *resetPasswordBtn;


@end
