//
//  PurchaeHistoryViewController.m
//  BSO
//
//  Created by MAC on 29/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PurchaeHistoryViewController.h"
#import "OrderItemCell.h"

@interface PurchaeHistoryViewController () <UIScrollViewDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) SCart *aSCart;
@property (nonatomic, strong) IBOutlet BSOLabel *lblNoData;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIImageView *imgBarCodeView;
@property (nonatomic, strong) UIViewController *barCodecontroller;
@end

@implementation PurchaeHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self getOrderHistory];
}

- (void)getOrderHistory {
    [Utility showLoader];
    [self.bsoManager getOrderHistory:^(id responseObject) {
        if([responseObject isKindOfClass:[SCart class]]) {
            self.aSCart = responseObject;
            if(self.aSCart.arrCartSection.count) {
                self.lblNoData.hidden = TRUE;
            } else {
                self.lblNoData.hidden = FALSE;
            }
            [self.tblView reloadData];
        }
        else {
            self.lblNoData.hidden = FALSE;
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

#pragma mark - TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.aSCart.arrCartSection.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CartSection  *aSection = [self.aSCart.arrCartSection objectAtIndex:indexPath.section];
    CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
    if(aItem.isSeatOpened) {
        return [Utility getTextHeightOfText:aItem.seatDesc font:[UIFont fontWithName:@"OpenSans-Regular" size:12] width:440] + 95;
    }
    return 85;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    CartSection  *aSection = [self.aSCart.arrCartSection objectAtIndex:section];
    return aSection.arrCartItems.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderItemCell *cell = (OrderItemCell *) [tableView dequeueReusableCellWithIdentifier:@"OrderItemCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"OrderItemCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell.btnSeatDetails addTarget:self action:@selector(seatDetailButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnBarCode addTarget:self action:@selector(getBarCodeButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];

        cell.lblDate.text = @"";
    }
    
    CartSection  *aSection = [self.aSCart.arrCartSection objectAtIndex:indexPath.section];
    CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
    
    NSString *description = [NSString stringWithFormat:@"<font size='4' face='OpenSans' color='white'>%@</font>",aItem.title];
    
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[description dataUsingEncoding:NSUTF8StringEncoding]
                                                                      options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding),NSForegroundColorAttributeName: [UIColor whiteColor]}
                                                           documentAttributes:nil error:nil];
    cell.txtTitle.attributedText = attrString;
    cell.txtTitle.textContainerInset = UIEdgeInsetsMake(0,0,0,0);
    
    cell.btnBarCode.hidden = !aSection.showETickets;
    
    cell.lblDate.text = aItem.date;
    cell.lblQty.text = [NSString stringWithFormat:@"QUANTITY: %@",aItem.quantity];
    cell.lblTotal.text = [NSString stringWithFormat:@"$%@",aItem.totalCost];
    
    if(aItem.type == jTicket) {
        cell.btnSeatDetails.hidden = FALSE;
    } else if(aItem.type == jSubscription) {
        cell.btnSeatDetails.hidden = FALSE;
    }
    else {
        cell.btnSeatDetails.hidden = TRUE;
    }
    
    if(aItem.isSeatOpened) {
        cell.lblSeatDetais.text = aItem.seatDesc;
        float height = [Utility getTextHeightOfText:aItem.seatDesc font:[UIFont fontWithName:@"OpenSans-Regular" size:12] width:440];
        CGRect frame = cell.lblSeatDetais.frame;
        frame.size.height = height + 13;
        cell.lblSeatDetais.frame = frame;
        cell.lblSeatDetais.hidden = FALSE;
        cell.btnSeatDetails.selected = TRUE;
        
    }
    else {
        cell.lblSeatDetais.hidden = FALSE;
        cell.btnSeatDetails.selected = FALSE;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)seatDetailButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        CartSection  *aSection = [self.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        aItem.isSeatOpened = !aItem.isSeatOpened;
        [self.tblView reloadData];
    }
}

- (void)getBarCodeButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        CartSection  *aSection = [self.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        [Utility showLoader];
        [self.bsoManager getTicketBarCode:aItem.orderNumber number:aItem.subLineItemNumber success:^(id responseObject) {
            if([responseObject isKindOfClass:[UIImage class]]) {
                self.barCodecontroller = [[UIViewController alloc] init];
                self.barCodecontroller.modalPresentationStyle = UIModalPresentationFormSheet;
                
                UIImage *imgBarCode = (UIImage *)responseObject;
                CGSize size = imgBarCode.size;
                size = CGSizeMake(MIN(768, size.width), MIN(768, size.height+50));
                
                self.barCodecontroller.preferredContentSize = size;
                
                UIScrollView *srcView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, size.width, size.height-50)];
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, 50)];
                
                UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
                [btnDone setTitle:@"Done" forState:UIControlStateNormal];
                btnDone.frame = CGRectMake(0, 4, 100, 40);
                [btnDone addTarget:self action:@selector(btnBarCodeDonePressed) forControlEvents:UIControlEventTouchUpInside];
                [headerView addSubview:btnDone];
                
                self.imgBarCodeView = [[UIImageView alloc] initWithImage:imgBarCode];
                self.imgBarCodeView.frame = CGRectMake(0, 0,size.width, size.height-50);
                [srcView  addSubview:self.imgBarCodeView];
                srcView.delegate = self;
                srcView.minimumZoomScale = 1.0;
                srcView.maximumZoomScale = 2.0;
                srcView.backgroundColor = [UIColor whiteColor];
                
                UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
                aView.backgroundColor = [UIColor blackColor];
                [aView addSubview:headerView];
                [aView addSubview:srcView];
                
                
                self.barCodecontroller.view = aView;

                [self presentViewController:self.barCodecontroller animated:YES completion:nil];
            } else {
                [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
            }
            
            [Utility hideLoader];
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];
    }
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imgBarCodeView;
}

- (void)btnBarCodeDonePressed {
    [self.barCodecontroller dismissViewControllerAnimated:YES completion:nil];
}

@end
