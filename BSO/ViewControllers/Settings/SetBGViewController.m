//
//  SetBGViewController.m
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SetBGViewController.h"

@interface SetBGViewController () <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) IBOutlet BSOButton *btnBSO;
@property (nonatomic, strong) IBOutlet BSOButton *btnPop;
@property (nonatomic, strong) IBOutlet BSOButton *btnTangleWood;
@property (nonatomic, strong) IBOutlet BSOButton *btnChange;
@property (nonatomic, strong) BSOButton *btnSelected;
@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, assign) BSOManager *bsoManager;
@end

@implementation SetBGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bsoManager = [BSOManager SharedInstance];
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    
    [self btnOrganisationPressed:self.btnBSO];
    
    self.title = @"Change Background";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackPressed)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Change" style:UIBarButtonItemStylePlain target:self action:@selector(btnChangePhotoPressed)];
}

- (void)displayImageFor:(NSInteger )index {
    NSString *fileName = [NSString stringWithFormat:@"bg_%ld.png",(long)index];
    NSData *imgData = [NSData dataWithContentsOfFile:FILE_PATH(fileName)];
    if(imgData) {
        self.imgView.image = [UIImage imageWithData:imgData];
    } else {
        if(index == 0) {
            self.imgView.image = [UIImage imageNamed:@"BSO_BG"];
        }
        else if(index == 2) {
            self.imgView.image = [UIImage imageNamed:@"POPS_BG"];
        } else {
            self.imgView.image = [UIImage imageNamed:@"TANGLEWOOD_BG"];
        }
    }
}

- (void)saveImageFor:(NSInteger )index image:(UIImage *)img {
    NSString *fileName = [NSString stringWithFormat:@"bg_%ld.png",(long)index];
    NSData *imgData = UIImagePNGRepresentation(img);
    [imgData writeToFile:FILE_PATH(fileName) atomically:YES];
}

- (IBAction)btnOrganisationPressed:(BSOButton *)sender {
    if(sender == self.btnSelected) {
        return;
    }
    self.btnSelected = sender;
    CGRect frame = sender.superview.frame;
    self.imgSelectedBorder.frame = frame;
    self.imgSelectedBorder.autoresizingMask = sender.superview.autoresizingMask;
    [self displayImageFor:sender.tag];
}

- (IBAction)btnChangePhotoPressed {
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:@"Select" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose Photo", nil];
    [actSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}

- (IBAction)btnBackPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    if(buttonIndex == 1)
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    else
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imgPicker.delegate = self;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // Place image picker on the screen
        [self presentViewController:imgPicker animated:YES completion:nil];
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    self.imgView.image = info[UIImagePickerControllerOriginalImage];
    [self saveImageFor:self.btnSelected.tag image:self.imgView.image];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end