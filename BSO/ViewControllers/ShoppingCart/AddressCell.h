//
//  AddressCell.h
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblAddress;
@property (nonatomic, strong) IBOutlet BSOButton *btnSelect;
@end
