//
//  ShpingCartViewController.m
//  BSO
//
//  Created by MAC on 26/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "ShpingCartViewController.h"
#import "CartItemsViewController.h"
#import "ShippingViewController.h"
#import "PaymentViewController.h"
#import "FinalizeViewController.h"

@interface ShpingCartViewController ()

@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnCart;
@property (nonatomic, strong) IBOutlet BSOButton *btnShipping;
@property (nonatomic, strong) IBOutlet BSOButton *btnPayment;
@property (nonatomic, strong) IBOutlet BSOButton *btnFinilize;
@property (nonatomic, strong) IBOutlet  UIImageView *imgArrow;
@property (nonatomic, strong) BSOButton *btnSelected;

@property (nonatomic, strong) IBOutlet UIImageView *imgSelectedBorder;

@property (nonatomic, strong) CartItemsViewController *cartItemsViewController;
@property (nonatomic, strong) ShippingViewController *shippingViewController;
@property (nonatomic, strong) PaymentViewController *paymentViewController;
@property (nonatomic, strong) FinalizeViewController *finalizeViewController;
@end

@implementation ShpingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.imgSelectedBorder.layer.borderColor = RGB(63, 92, 125).CGColor;
    self.imgSelectedBorder.layer.borderWidth = 1;
    self.bsoManager.aSCart = [[SCart alloc] init];
    self.lblCaption.text = @"";
    [self btnCartPRessed];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    [self.bsoManager retriveCrediCars];
}


- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCartPRessed {
    if(self.btnCart == self.btnSelected) {
        return;
    }
    self.btnCart.selected = TRUE;
    self.btnSelected.selected = FALSE;
    self.btnSelected = self.btnCart;
    self.imgSelectedBorder.frame = self.btnCart.frame;
    CGPoint center = self.imgArrow.center;
    center.x = self.btnCart.center.x;
    self.imgArrow.center = center;
    
    if(self.cartItemsViewController == nil) {
        self.cartItemsViewController = [[CartItemsViewController alloc] initWithNibName:@"CartItemsViewController" bundle:nil];
        CGRect frame = self.view.frame;
        [self addChildViewController:self.cartItemsViewController];
        [self.view addSubview:self.cartItemsViewController.view];
        frame.origin.y = 195;
        frame.size.height -= frame.origin.y;
        self.cartItemsViewController.view.frame = frame;
        [self.cartItemsViewController didMoveToParentViewController:self];

    } else {
        [self.view bringSubviewToFront:self.cartItemsViewController.view];
    }
}

- (IBAction)btnShippingPressed {
    if(self.btnShipping == self.btnSelected) {
        return;
    }
    self.lblCaption.text = @"HOW DO YOU WANT YOUR ITEMS DELIVERED";
    self.btnCart.enabled = FALSE;
    self.btnShipping.selected = TRUE;
    self.btnSelected.selected = FALSE;
    self.btnSelected = self.btnShipping;
    self.imgSelectedBorder.frame = self.btnShipping.frame;
    self.btnShipping.enabled = TRUE;
    
    CGPoint center = self.imgArrow.center;
    center.x = self.btnShipping.center.x;
    self.imgArrow.center = center;
    
    if(self.shippingViewController == nil) {
        self.shippingViewController = [[ShippingViewController alloc] initWithNibName:@"ShippingViewController" bundle:nil];
        CGRect frame = self.view.frame;
        [self addChildViewController:self.shippingViewController];
        [self.view addSubview:self.shippingViewController.view];
        frame.origin.y = 195;
        frame.size.height -= frame.origin.y;
        self.shippingViewController.view.frame = frame;
        [self.shippingViewController didMoveToParentViewController:self];
        
    } else {
        [self.view bringSubviewToFront:self.shippingViewController.view];
    }
}

- (IBAction)btnPaymentPressed {
    if(self.btnPayment == self.btnSelected) {
        return;
    }
    self.lblCaption.text = @"HOW DO YOU WISH TO PAY";
    self.btnShipping.enabled = FALSE;
    self.btnFinilize.enabled = FALSE;
    self.btnPayment.selected = TRUE;
    self.btnSelected.selected = FALSE;
    self.btnSelected = self.btnPayment;
    self.imgSelectedBorder.frame = self.btnPayment.frame;
    self.btnPayment.enabled = TRUE;
    
    CGPoint center = self.imgArrow.center;
    center.x = self.btnPayment.center.x;
    self.imgArrow.center = center;

    if(self.paymentViewController == nil) {
        self.paymentViewController = [[PaymentViewController alloc] initWithNibName:@"PaymentViewController" bundle:nil];
        CGRect frame = self.view.frame;
        [self addChildViewController:self.paymentViewController];
        [self.view addSubview:self.paymentViewController.view];
        frame.origin.y = 195;
        frame.size.height -= frame.origin.y;
        self.paymentViewController.view.frame = frame;
        [self.paymentViewController didMoveToParentViewController:self];
        
    } else {
        [self.view bringSubviewToFront:self.paymentViewController.view];
    }
}

- (IBAction)btnFinilizePressed {
    if(self.btnFinilize == self.btnSelected) {
        return;
    }
    self.lblCaption.text = @"PLEASE CONFIRM YOUR ORDER";
    self.btnPayment.enabled = FALSE;
    self.btnFinilize.selected = TRUE;
    self.btnSelected.selected = FALSE;
    self.btnSelected = self.btnFinilize;
    self.imgSelectedBorder.frame = self.btnFinilize.frame;
    self.btnFinilize.enabled = TRUE;
    
    CGPoint center = self.imgArrow.center;
    center.x = self.btnFinilize.center.x;
    self.imgArrow.center = center;

    if(self.finalizeViewController == nil) {
        self.finalizeViewController = [[FinalizeViewController alloc] initWithNibName:@"FinalizeViewController" bundle:nil];
        CGRect frame = self.view.frame;
        [self addChildViewController:self.paymentViewController];
        [self.view addSubview:self.finalizeViewController.view];
        frame.origin.y = 195;
        frame.size.height -= frame.origin.y;
        self.finalizeViewController.view.frame = frame;
        self.finalizeViewController.parentController = self;
        [self.finalizeViewController didMoveToParentViewController:self];
        
    } else {
        [self.finalizeViewController setFinizeItems];
        [self.view bringSubviewToFront:self.finalizeViewController.view];
    }
}

- (void)btnProccedPressed {
    if(self.btnCart == self.btnSelected) {
        [self btnShippingPressed];
    }
    else if(self.btnShipping == self.btnSelected) {
        [self btnPaymentPressed];
    }
    else if(self.btnPayment == self.btnSelected) {
        [self btnFinilizePressed];
    }
    else {
        
    }
}

- (void)finalizeProcesseFailed {
    [self btnPaymentPressed];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        self.imgSelectedBorder.frame = self.btnSelected.frame;
        CGPoint center = self.imgArrow.center;
        center.x = self.btnSelected.center.x;
        self.imgArrow.center = center;
        
        } completion:^(id  _Nonnull context) {
        
        self.imgSelectedBorder.frame = self.btnSelected.frame;
        CGPoint center = self.imgArrow.center;
        center.x = self.btnSelected.center.x;
        self.imgArrow.center = center;
        
    }];
}

@end
