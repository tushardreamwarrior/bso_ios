//
//  FinalizeViewController.m
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "FinalizeViewController.h"
#import "FinalizeItemCell.h"
#import "ShpingCartViewController.h"
#import "OrderConfirmViewController.h"

@interface FinalizeViewController ()
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblSubTotal;
@property (nonatomic, strong) IBOutlet BSOLabel *lblFees;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTotal;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPaymentMethod;
@property (nonatomic, strong) IBOutlet BSOLabel *lblBillingAddress;
@property (nonatomic, strong) IBOutlet BSOLabel *lblShippingAddress;
@property (nonatomic, strong) IBOutlet BSOLabel *lblShippingMethod;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;
@end

@implementation FinalizeViewController

- (void)viewDidLoad {
        [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    [self setFinizeItems];
}

- (void)setFinizeItems {
    
    self.tblView.tableHeaderView = self.headerView;
    self.tblView.tableFooterView = self.footerView;
    
    self.lblSubTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.subTotal];
    self.lblFees.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.tessituraFees];
    self.lblTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.totalCost];

    self.lblPaymentMethod.text = [NSString stringWithFormat:@"%@ ending in %@",self.bsoManager.aSCart.aCard.cardType,[self.bsoManager.aSCart.aCard.cardNumber substringFromIndex:self.bsoManager.aSCart.aCard.cardNumber.length - 4]];
    for (Address *aAddress in self.bsoManager.aSCart.arrShippingAddress) {
        if(aAddress.isSelected) {
            self.lblBillingAddress.text = [aAddress.fullAddress stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
            break;
        }
    }
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.lblShippingAddress.text = @"";
    self.lblShippingMethod.text = @"";
    
//    for(int i=1;i<=5;i++) {
//        BSOLabel *lbl = (BSOLabel *)[self.headerView viewWithTag:i];
//        lbl.textColor = [self.bsoManager getSelectedItemTextColor];
//    }
}

#pragma mark - TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.bsoManager.aSCart.arrCartSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:section];
    return aSection.arrCartItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
    UILabel *lbl  = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 300, 25)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor whiteColor];
    CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:section];
    lbl.text = aSection.headerString;
    lbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
    [headerView addSubview:lbl];
    headerView.backgroundColor = RGB(26, 26, 26);
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FinalizeItemCell *cell = (FinalizeItemCell *) [tableView dequeueReusableCellWithIdentifier:@"FinalizeItemCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FinalizeItemCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    
    CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:indexPath.section];
    CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];

   
    cell.lblTitle.text = aItem.title;
    cell.lblQty.text = [NSString stringWithFormat:@"%@",aItem.quantity];
    cell.lblTotal.text = [NSString stringWithFormat:@"$%@",aItem.totalCost];
    
    if(aSection.type == jTicket) {
        SeatItem *aSeat = [aItem.arrReats objectAtIndex:0];
        cell.lblPrice.text = [NSString stringWithFormat:@"$%@",aSeat.cost];
    } else {
        cell.lblPrice.text = [NSString stringWithFormat:@"$%@",aItem.orginalPrice];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (IBAction)btnPlaceOrderPressed {
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
    if(self.bsoManager.aSCart.printAtHome) {
        [dict setValue:@"0" forKey:@"IsHoldAtBoxOffice"];
        [dict setValue:@"1" forKey:@"IsPrintAtHome"];

    } else {
        [dict setValue:@"1" forKey:@"IsHoldAtBoxOffice"];
        [dict setValue:@"0" forKey:@"IsPrintAtHome"];

    }
    
    [Utility showLoader];
    [self.bsoManager checkoutFinalize:dict success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            OrderConfirmViewController *viewController = [[OrderConfirmViewController alloc] initWithNibName:@"OrderConfirmViewController" bundle:nil];
            viewController.orderNo = [[responseObject valueForKey:DATA] valueForKey:@"OrderNumber"];
            [self.parentController.navigationController pushViewController:viewController animated:YES];
        }
        else {
            ShpingCartViewController *controller = (ShpingCartViewController *)self.parentController;
            [controller finalizeProcesseFailed];
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
        ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
        [controller btnProccedPressed];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}
@end
