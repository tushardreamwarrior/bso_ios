//
//  ShippingViewController.m
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "ShippingViewController.h"
#import "ShpingCartViewController.h"
#import "AddressCell.h"

@interface ShippingViewController () <UITextFieldDelegate,UITableViewDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOButton *btnPrintAtHome;
@property (nonatomic, strong) IBOutlet BSOButton *btnHoldAtOffice;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;
@end

@implementation ShippingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.tblView.tableFooterView = [[UIView alloc] init];
    [Utility setLeftViewWithTextField:self.txtName];
    self.txtName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtName.placeholder attributes:@{NSForegroundColorAttributeName: RGB(135, 135, 135)}];
    [Utility setBorderColorWithView:self.txtName color:RGB(135, 135, 135)];
    [Utility setCornerToView:self.txtName corner:10];
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];
    
    [self getShippingAddress];
}

- (void)getShippingAddress {
    [Utility showLoader];
    [self.bsoManager checkOutShipping:^(id responseObject) {
        self.tblView.tableHeaderView = self.headerView;
        self.tblView.tableFooterView = self.footerView;
        
        self.lblSubTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.subTotal];
        self.lblFees.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.tessituraFees];
        self.lblTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.totalCost];

        [self.tblView reloadData];
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

#pragma mark - TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
    return  [Utility getTextHeightOfText:aAddress.fullAddressWithPhone font:[UIFont fontWithName:@"OpenSans" size:14] width:tableView.frame.size.width-70]  + 20;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bsoManager.aSCart.arrShippingAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = (AddressCell *) [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AddressCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell.btnSelect addTarget:self action:@selector(btnSelectPressed:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
    
    CGRect frame = cell.lblAddress.frame;
    frame.size.height = [Utility getTextHeightOfText:aAddress.fullAddressWithPhone font:[UIFont fontWithName:@"OpenSans" size:14] width:tableView.frame.size.width-70] ;
    cell.lblAddress.frame = frame;

    cell.lblAddress.text = aAddress.fullAddressWithPhone;
    cell.btnSelect.selected = aAddress.isSelected;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)btnSelectPressed:(UIButton *)sender event:(id)event {
    if(sender.selected) {
        return;
    }
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        for(Address *aAddress in self.bsoManager.aSCart.arrShippingAddress) {
            aAddress.isSelected = FALSE;
        }
        
        Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
        aAddress.isSelected = TRUE;
        [self.tblView reloadData];
    }
}

- (IBAction)btnHoldAtBoxOfficePressed {
    self.btnHoldAtOffice.selected = TRUE;
    self.btnPrintAtHome.selected = FALSE;
}

- (IBAction)btnPrintAtHomePressed {
    self.btnHoldAtOffice.selected = FALSE;
    self.btnPrintAtHome.selected = TRUE;
}

- (IBAction)btnProccedPressed {
    if(self.txtName.text.length) {
        self.bsoManager.aSCart.shippingName = self.txtName.text;
    }
    [self.view endEditing:YES];
    self.bsoManager.aSCart.printAtHome = self.btnPrintAtHome.selected;
    ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
    [controller btnProccedPressed];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
