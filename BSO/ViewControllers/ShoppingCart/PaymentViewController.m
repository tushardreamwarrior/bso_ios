//
//  PaymentViewController.m
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PaymentViewController.h"
#import "ShpingCartViewController.h"
#import "AddressCell.h"
#import "SelectionView.h"

@interface PaymentViewController () <SelectionDelegate>
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) NSMutableArray *arrMonths;
@property (nonatomic, strong) NSMutableArray *arrYear;
@property (nonatomic, strong) NSMutableArray *arrCards;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;

@property (nonatomic, strong) IBOutlet UIView *enterPaymentView;

@property (nonatomic, strong) IBOutlet UIView *viewPaymentView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardNumber;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardNameOnCard;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardExpire;
@property (nonatomic, strong) IBOutlet BSOLabel *lblCardCVV;

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
    self.arrMonths = [[NSMutableArray alloc] init];
    [self.btnExpireMonth setTitle:@"1" forState:UIControlStateNormal];
    [self.btnExpireYear setTitle:[Utility stringFromDate:[NSDate date] format:@"yyyy"] forState:UIControlStateNormal];
    self.arrMonths = [[NSMutableArray alloc] init];
    self.arrYear = [[NSMutableArray alloc] init];
    self.arrCards = [[NSMutableArray alloc] init];
    for (CreditCard *aCard in self.bsoManager.arrCreditCards) {
        [self.arrCards addObject:aCard.cardName];
    }
    if(self.arrCards.count)
       [self.btnCardType setTitle:self.arrCards[0] forState:UIControlStateNormal];
    
    for(int i=1;i<=12;i++) {
        [self.arrMonths addObject:[NSString stringWithFormat:@"%d",i]];
    }
    int currentYear = [[Utility stringFromDate:[NSDate date] format:@"yyyy"] intValue];
    for(int i=0;i<5;i++) {
        [self.arrYear addObject:[NSString stringWithFormat:@"%d",currentYear+i]];
    }

    self.tblView.tableHeaderView = self.headerView;
    self.tblView.tableFooterView = self.footerView;
    
    self.lblSubTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.subTotal];
    self.lblFees.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.tessituraFees];
    self.lblTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.totalCost];
    
    for(UIView *aView in self.headerView.subviews) {
        if([aView isKindOfClass:[BSOTextField class]]) {
            BSOTextField *txtField = (BSOTextField *)aView;
            [Utility setBorderColorWithView:txtField color:RGB(135, 135, 135)];
            [Utility setLeftViewWithTextField:txtField];
            [Utility setCornerToView:txtField corner:10];
        }
    }
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];
    
    if(GET_UD(PAYMENT_CARD_NUMBER)) {
        
        NSString *cardNumber = [self.bsoManager.aSCart.aCard.cardNumber substringFromIndex:self.bsoManager.aSCart.aCard.cardNumber.length - 4];
        NSString *cardMonth = self.bsoManager.aSCart.aCard.cardExpireMonth;
        NSString *cardYear = self.bsoManager.aSCart.aCard.cardExpireYear;
        if(cardMonth.length == 1) {
            cardMonth = [@"0" stringByAppendingString:cardMonth];
        }
        
        NSString *cardType = self.bsoManager.aSCart.aCard.cardType;
        NSString *cardCVV = self.bsoManager.aSCart.aCard.cardCVV;
        NSString *cardName = self.bsoManager.aSCart.aCard.cardName;
        
        self.lblCardNumber.text = [NSString stringWithFormat:@"%@ ending in %@",cardType,cardNumber];
        self.lblCardNameOnCard.text = cardName;
        self.lblCardExpire.text = [NSString stringWithFormat:@"%@ %@",cardMonth,cardYear];
        
        NSString *starCVV = @"";
        for (int i=1;i<=cardCVV.length;i++) {
            starCVV = [starCVV stringByAppendingString:@"*"];
        }
        self.lblCardCVV.text = starCVV;

        self.enterPaymentView.hidden = TRUE;
    } else {
        self.viewPaymentView.hidden = TRUE;
    }
}

#pragma mark - TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
    return  [Utility getTextHeightOfText:aAddress.fullAddress font:[UIFont fontWithName:@"OpenSans" size:14] width:tableView.frame.size.width-70]  + 20;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bsoManager.aSCart.arrShippingAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = (AddressCell *) [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AddressCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        [cell.btnSelect addTarget:self action:@selector(btnSelectPressed:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
    
    CGRect frame = cell.lblAddress.frame;
    frame.size.height = [Utility getTextHeightOfText:aAddress.fullAddress font:[UIFont fontWithName:@"OpenSans" size:14] width:tableView.frame.size.width-70] ;
    cell.lblAddress.frame = frame;
    
    cell.lblAddress.text = aAddress.fullAddress;
    cell.btnSelect.selected = aAddress.isSelected;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)btnSelectPressed:(UIButton *)sender event:(id)event {
    if(sender.selected) {
        return;
    }
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        for(Address *aAddress in self.bsoManager.aSCart.arrShippingAddress) {
            aAddress.isSelected = FALSE;
        }
        
        Address *aAddress = [self.bsoManager.aSCart.arrShippingAddress objectAtIndex:indexPath.row];
        aAddress.isSelected = TRUE;
        [self.tblView reloadData];
    }
}

- (IBAction)btnProccedPressed {
    if(self.enterPaymentView.hidden == FALSE) {
        if(allTrim(self.txtCarNumber.text).length < 8) {
            [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Valid Card number"];
            return;
        }
        if(allTrim(self.txtNameonCard.text).length == 0) {
            [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter Name of card"];
            return;
        }
        if(allTrim(self.txtCVV.text).length == 0) {
            [Utility ShowAlertWithTitle:@"Alert" Message:@"Please Enter CVV"];
            return;
        }
        
        self.bsoManager.aSCart.aCard.cardCVV = self.txtCVV.text;
        self.bsoManager.aSCart.aCard.cardName = self.txtNameonCard.text;
        self.bsoManager.aSCart.aCard.cardNumber = self.txtCarNumber.text;
        self.bsoManager.aSCart.aCard.cardType = [self.btnCardType titleForState:UIControlStateNormal];
        self.bsoManager.aSCart.aCard.cardExpireMonth = [self.btnExpireMonth titleForState:UIControlStateNormal];
        self.bsoManager.aSCart.aCard.cardExpireYear = [self.btnExpireYear titleForState:UIControlStateNormal];

    }
    
    [self.view endEditing:YES];
    
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
    [dict setValue:@"" forKey:@"SelectedSavedPaymentMethod"];
    [dict setValue:self.bsoManager.aSCart.aCard.cardNumber forKey:@"CardNumber"];
    [dict setValue:[self getCardNumber:self.bsoManager.aSCart.aCard.cardType] forKey:@"CardType"];
    [dict setValue:self.bsoManager.aSCart.aCard.cardName forKey:@"CardOwnerName"];
    [dict setValue:self.bsoManager.aSCart.aCard.cardExpireMonth forKey:@"CardExpirationMonth"];
    [dict setValue:self.bsoManager.aSCart.aCard.cardExpireYear forKey:@"CardExpirationYear"];
    [dict setValue:self.bsoManager.aSCart.aCard.cardCVV forKey:@"CvvCode"];
    for (Address *aAddress in self.bsoManager.aSCart.arrShippingAddress) {
        if(aAddress.isSelected) {
            [dict setValue:aAddress.ID forKey:@"SelectedSavedBillingAddress"];
            break;
        }
    }
    
    if(self.bsoManager.aSCart.printAtHome) {
        [dict setValue:@"0" forKey:@"IsHoldAtBoxOffice"];
        [dict setValue:@"1" forKey:@"IsPrintAtHome"];
        
    } else {
        [dict setValue:@"1" forKey:@"IsHoldAtBoxOffice"];
        [dict setValue:@"0" forKey:@"IsPrintAtHome"];
        
    }
    
    [Utility showLoader];
    [self.bsoManager checkOutPayment:dict success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
            [controller btnProccedPressed];
        } else {
            [Utility ShowAlertWithTitle:@"Alert" Message:[responseObject valueForKey:MESSAGE]];
        }
        
        [Utility hideLoader];
        ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
        [controller btnProccedPressed];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
    
}

- (IBAction)btnCardTypePressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrCards];
    selView.delegate = self;
    selView.selectedIndex = [self.arrCards indexOfObject:[self.btnCardType titleForState:UIControlStateNormal]];
    [selView showInView:self.btnCardType];
}

- (IBAction)btnExpireMonthPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrMonths];
    selView.delegate = self;
    [selView showInView:self.btnExpireMonth];
}

- (IBAction)btnExpireYearPressed {
    SelectionView *selView = [[SelectionView alloc] initWithItemArray:self.arrYear];
    selView.delegate = self;
    [selView showInView:self.btnExpireYear];
}

- (void)SelectionView:(SelectionView *)selectionView didSelectItem:(NSString *)item withIndex:(NSInteger)index SenderView:(UIView *)senderView {
    BSOButton *btn = (BSOButton *)senderView;
    [btn setTitle:item forState:UIControlStateNormal];
}

#pragma mark -

- (IBAction)btnEditCardDetailsPressed {
    self.enterPaymentView.hidden = FALSE;
    self.viewPaymentView.hidden = TRUE;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (NSString *)getCardNumber:(NSString *)name {
    NSString *number = @"";
    for (CreditCard *aCard in self.bsoManager.arrCreditCards) {
        if([aCard.cardName isEqualToString:name]) {
            number = aCard.cardType;
            break;
        }
    }
    return number;
}

@end
