//
//  FinalizeViewController.h
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinalizeViewController : UIViewController
@property (nonatomic, strong) UIViewController *parentController;
- (void)setFinizeItems;
@end
