//
//  OrderConfirmViewController.m
//  BSO
//
//  Created by MAC on 29/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "OrderConfirmViewController.h"
#import "UserWalletViewController.h"


@interface OrderConfirmViewController ()
@property (nonatomic, strong) IBOutlet BSOLabel *lblOrderNo;
@property (nonatomic, strong) IBOutlet BSOButton *btnViewHistory;
@property (nonatomic, assign) BSOManager *bsoManager;

@end

@implementation OrderConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.btnViewHistory.backgroundColor = [self.bsoManager getSelectedItemColor];
    self.lblOrderNo.text = [NSString stringWithFormat:@"Your order number is: %@",self.orderNo];
    
    self.title = @"CHECKOUT";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"leftArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(btnBackPressed)];
}

- (IBAction)btnBackPressed {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)btnViewOrderHistoryPressed {
    UserWalletViewController *viewController = [[UserWalletViewController alloc] initWithNibName:@"UserWalletViewController" bundle:nil];
    viewController.isMyPurchase = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


@end
