//
//  CartItemsViewController.m
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "CartItemsViewController.h"
#import "ShpingCartViewController.h"
#import "CartItemCell.h"

@interface CartItemsViewController () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) IBOutlet BSOTextField *txtDonationAmount;
@property (nonatomic, strong) IBOutlet BSOTextField *txtPromotionalCode;
@property (nonatomic, strong) IBOutlet BSOButton *btnAddToCart;
@property (nonatomic, strong) IBOutlet BSOButton *btnProceed;
@property (nonatomic, strong) IBOutlet BSOButton *btnApply;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPromotionCodeError;

@property (nonatomic, strong) BSOButton *btnSelected;
@property (nonatomic, strong) IBOutlet UIView *donationView;
@property (nonatomic, strong) IBOutlet UIView *totalView;
@end

@implementation CartItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    
    [Utility showLoader];
    [self.bsoManager checkUserSession:GET_UD(USER_EMAIL) success:^(id responseObject) {
        [self getShoppingCartDetails];
    } failure:^(NSError *error) {
        
    }];
    
   
    self.txtDonationAmount.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtDonationAmount.placeholder attributes:@{NSForegroundColorAttributeName: RGB(135, 135, 135)}];
    [Utility setBorderColorWithView:self.txtDonationAmount color:RGB(135, 135, 135)];
    
    self.txtPromotionalCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtPromotionalCode.placeholder attributes:@{NSForegroundColorAttributeName: RGB(135, 135, 135)}];
    [Utility setBorderColorWithView:self.txtPromotionalCode color:RGB(135, 135, 135)];
    
    [Utility setCornerToView:self.txtDonationAmount corner:10];
    self.btnAddToCart.backgroundColor = [self.bsoManager getSelectedItemColor];
    [self.btnApply setTitleColor:[self.bsoManager getSelectedItemTextColor] forState:UIControlStateNormal];
    self.btnProceed.backgroundColor = [self.bsoManager getSelectedItemColor];

    self.tblView.tableFooterView = [[UIView alloc] init];
}

- (void)getShoppingCartDetails {
    
    [self.bsoManager getShoppingCartItems:^(id responseObject) {
        if([responseObject isKindOfClass:[SCart class]]) {
            ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
            self.bsoManager.aSCart = responseObject;
            if(self.bsoManager.aSCart.arrCartSection.count) {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %d",jCondtribution];
                NSArray *arrContributions = [self.bsoManager.aSCart.arrCartSection filteredArrayUsingPredicate:predicate];
                if(arrContributions.count) {
                    //[self.donationView removeFromSuperview];
                    self.donationView.hidden = TRUE;
                    CGRect frame = self.footerView.frame;
                    frame.size.height = 130;
                    self.footerView.frame = frame;
                } else {
                    self.donationView.hidden = FALSE;
                    CGRect frame = self.footerView.frame;
                    frame.size.height = 281;
                    self.txtDonationAmount.text = @"";
                    self.footerView.frame = frame;
                    
                    BSOButton *btn = (BSOButton *)[self.donationView viewWithTag:self.bsoManager.selectedType-1];
                    btn.selected = TRUE;
                    self.btnSelected = btn;
                }
                
                controller.lblCaption.text = @"ITEMS ALREADY ADDED TO YOUR CART";
                self.tblView.tableHeaderView = self.headerView;
                self.tblView.tableFooterView = self.footerView;
                [self.btnProceed setHidden:FALSE];
                self.lblSubTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.subTotal];
                self.lblFees.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.tessituraFees];
                self.lblTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.totalCost];
                
            } else {
                [self.btnProceed setHidden:YES];
                self.lblSubTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.subTotal];
                self.lblFees.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.tessituraFees];
                self.lblTotal.text = [NSString stringWithFormat:@"$%@",self.bsoManager.aSCart.totalCost];

                controller.lblCaption.text = @"THERE ARE NO ITEMS IN YOUR CART";
                self.tblView.tableHeaderView = nil;
                self.tblView.tableFooterView = self.footerView;
                
                self.donationView.hidden = FALSE;
                CGRect frame = self.footerView.frame;
                frame.size.height = 281;
                self.txtDonationAmount.text = @"";
                self.footerView.frame = frame;
                
                BSOButton *btn = (BSOButton *)[self.donationView viewWithTag:self.bsoManager.selectedType+1];
                btn.selected = TRUE;
                self.btnSelected = btn;
                
            }
            [self.tblView reloadData];
            
            if(GET_UD(PAYMENT_CARD_NUMBER)) {
                [self getUserCardDetail];
            }
            
        } else {
            [Utility ShowAlertWithTitle:@"" Message:[responseObject valueForKey:MESSAGE]];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

#pragma mark - TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(tableView == self.tblView) {
        return self.bsoManager.aSCart.arrCartSection.count;
    } else {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.tblView) {
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        if(aItem.isSeatOpened) {
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 450, 0)];
            lbl.font = [UIFont fontWithName:@"OpenSans" size:12];
            lbl.text = aItem.seatDesc;
            lbl.numberOfLines = 0;
            [lbl sizeToFit];
            
            return lbl.frame.size.height + 90;
        }
        return 85;
    } else {
        return 21;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tblView) {
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:section];
        return aSection.arrCartItems.count;
    } else {
      return self.bsoManager.aSCart.arrDiscoutCodes.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(tableView == self.tblView) {
        return 25.0;
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(tableView == self.tblView) {
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:section];
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 25)];
        UILabel *lbl  = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 300, 25)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor whiteColor];
        lbl.text = aSection.headerString;
        lbl.font = [UIFont fontWithName:@"OpenSans-Semibold" size:16];
        [headerView addSubview:lbl];
        headerView.backgroundColor =  RGB(26, 26, 26);
        return headerView;
    } else {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
        return headerView;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == self.tblView) {
        CartItemCell *cell = (CartItemCell *) [tableView dequeueReusableCellWithIdentifier:@"CartItemCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CartItemCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            [cell.btnSeatDetails addTarget:self action:@selector(seatDetailButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
            cell.lblDate.text = @"";
        }
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aItem.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
        
        cell.lblTitle.text = aItem.title;
        cell.lblDate.text = aItem.date;
        cell.lblQty.text = [NSString stringWithFormat:@"%@",aItem.quantity];
        cell.lblTotal.text = [NSString stringWithFormat:@"$%@",aItem.totalCost];
        
        if(aSection.type == jTicket) {
            SeatItem *aSeat = [aItem.arrReats objectAtIndex:0];
            cell.lblPrice.text = [NSString stringWithFormat:@"$%@",aSeat.cost];
            cell.btnSeatDetails.hidden = FALSE;
        } else if(aSection.type == jSubscription) {
            cell.btnSeatDetails.hidden = FALSE;
            cell.lblPrice.text = [NSString stringWithFormat:@"$%d",[aItem.totalCost intValue] /[aItem.quantity intValue]];
        } else {
            cell.btnSeatDetails.hidden = TRUE;
            cell.lblPrice.text = [NSString stringWithFormat:@"$%@",aItem.orginalPrice];
        }
      
        if(aItem.isSeatOpened) {
            cell.lblSeatDetais.text = aItem.seatDesc;
            cell.lblSeatDetais.numberOfLines = 0;
            [cell.lblSeatDetais sizeToFit];
            cell.lblSeatDetais.hidden = FALSE;
            cell.btnSeatDetails.selected = TRUE;
        }
        else {
            cell.lblSeatDetais.hidden = FALSE;
            cell.btnSeatDetails.selected = FALSE;
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
     
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"anyCell"];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"anyCell"];
            cell.textLabel.font = [UIFont fontWithName:@"OpenSans" size:12];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
        cell.textLabel.text =  [self.bsoManager.aSCart.arrDiscoutCodes objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Remove"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        [Utility showLoader];
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        
        [self.bsoManager removeItemFromCart:aItem.lineNumber success:^(id responseObject) {
            
            [self getShoppingCartDetails];
        } failure:^(NSError *error) {
            [Utility hideLoader];
        }];
    
    }];
    return @[deleteAction];
}

- (void)seatDetailButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        CartSection  *aSection = [self.bsoManager.aSCart.arrCartSection objectAtIndex:indexPath.section];
        CartItem *aItem = [aSection.arrCartItems objectAtIndex:indexPath.row];
        aItem.isSeatOpened = !aItem.isSeatOpened;
        [self.tblView reloadData];
    }
}

- (IBAction)btnProccedPressed {
    [self.view endEditing:YES];
    ShpingCartViewController *controller = (ShpingCartViewController *)self.parentViewController;
    [controller btnProccedPressed];
}

#pragma makr - Donation 

- (IBAction)btnOrganizationPressed:(BSOButton *)sender {
    self.btnSelected.selected = FALSE;
    sender.selected = TRUE;
    self.btnSelected = sender;
}

- (IBAction)btnMakeDonationPressed {
    if([self.txtDonationAmount.text intValue] <= 0) {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Enter Donation Amount"];
        return;
    }
    [Utility showLoader];
    [self.bsoManager makeImpulseDonation:(OrganizationType)self.btnSelected.tag-1 amount:self.txtDonationAmount.text success:^(id responseObject) {
//        [self.donationView removeFromSuperview];
//        CGRect frame = self.footerView.frame;
//        frame.size.height = 130;
//        self.footerView.frame = frame;
//        [self.btnProceed setHidden:FALSE];
        [self getShoppingCartDetails];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
    
}

- (IBAction)btnApplyPromotionalCodePressed {
    [Utility showLoader];
    [self.txtPromotionalCode resignFirstResponder];
    [self.bsoManager applyDiscoutCode:self.txtPromotionalCode.text success:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            self.lblPromotionCodeError.hidden = TRUE;
            self.txtPromotionalCode.text = @"";
            NSDictionary *data = [responseObject valueForKey:DATA];
            self.bsoManager.aSCart.arrDiscoutCodes = [[NSMutableArray alloc] initWithArray:[data valueForKey:@"AppliedDiscountCodes"]];
            [self.codetblView reloadData];
        } else {
            self.lblPromotionCodeError.hidden = FALSE;
            self.lblPromotionCodeError.text = [responseObject valueForKey:MESSAGE];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)getUserCardDetail {
    [self.bsoManager getCardInfo:^(id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSDictionary *data = [responseObject valueForKey:DATA];
            if(![[data valueForKey:@"card_number"] isKindOfClass:[NSNull class]]) {
                NSString *cardNumber = [data valueForKey:@"card_number"];
                NSString *cardMonth = [data valueForKey:@"card_month"];
                NSString *cardYear = [data valueForKey:@"car_year"];
                if(cardMonth.length == 1) {
                    cardMonth = [@"0" stringByAppendingString:cardMonth];
                }
                
                NSString *key = GET_UD(KEY);
                NSString *cardType = [Utility decryptString:GET_UD(PAYMENT_CARD_TYPE) withKey:key];
                NSString *cardCVV = [Utility decryptString:GET_UD(PAYMENT_CARD_CVV) withKey:key];
                NSString *cardName = [Utility decryptString:GET_UD(PAYMENT_CARD_NAME) withKey:key];
                NSString *storedCardNumber = [Utility decryptString:GET_UD(PAYMENT_CARD_NUMBER) withKey:key];
                
                cardNumber = [storedCardNumber stringByAppendingString:cardNumber];
                
                self.bsoManager.aSCart.aCard.cardCVV = cardCVV;
                self.bsoManager.aSCart.aCard.cardName = cardName;
                self.bsoManager.aSCart.aCard.cardNumber = cardNumber;
                self.bsoManager.aSCart.aCard.cardType = cardType;
                self.bsoManager.aSCart.aCard.cardExpireMonth = cardMonth;
                self.bsoManager.aSCart.aCard.cardExpireYear = cardYear;
            }
        } else {
            
        }
        
    } failure:^(NSError *error) {
        
    }];
}


@end
