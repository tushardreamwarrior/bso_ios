//
//  ShpingCartViewController.h
//  BSO
//
//  Created by MAC on 26/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShpingCartViewController : UIViewController
- (void)btnProccedPressed;
- (void)finalizeProcesseFailed;
@property (nonatomic, strong) IBOutlet  BSOLabel *lblCaption;
@end
