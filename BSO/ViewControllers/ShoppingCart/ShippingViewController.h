//
//  ShippingViewController.h
//  BSO
//
//  Created by MAC on 10/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShippingViewController : UIViewController
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblNoData;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *footerView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblSubTotal;
@property (nonatomic, strong) IBOutlet BSOLabel *lblFees;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTotal;
@property (nonatomic, strong) IBOutlet BSOTextField *txtName;
@end
