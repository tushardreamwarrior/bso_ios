//
//  SocialViewController.h
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialViewController : UIViewController
- (void)getSocialPost;
- (void)getHashTagsPost;
@end
