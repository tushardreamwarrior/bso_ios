//
//  MenuViewController.h
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewControllerDelegate <NSObject>
@optional
- (void)MenuViewOrginizationButtonPressed:(NSInteger)organization;
- (void)MenuViewItemPressed:(NSInteger)index;
- (void)MenuViewBackPressed;

@end

@interface MenuViewController : UIViewController
@property (nonatomic, assign) id <MenuViewControllerDelegate> delegate;
@property (nonatomic, strong) IBOutlet UIView *menuView;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, assign) NSInteger selectedIndex;
@end
