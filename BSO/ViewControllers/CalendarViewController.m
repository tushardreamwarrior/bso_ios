//
//  CalendarViewController.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "CalendarViewController.h"
#import "SelectionView.h"
#import "CalendarCell.h"

@interface CalendarViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,SelectionDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet  BSOTextField *txtSearch;
@property (nonatomic, strong) IBOutlet  BSOButton *btnToday;
@property (nonatomic, strong) IBOutlet  BSOButton *btnSelectDate;
@property (nonatomic, strong) IBOutlet  UIActivityIndicatorView *indicator;
@property (nonatomic, strong) NSMutableArray *arrItems;
@property (nonatomic, strong) NSMutableArray *arrAllItems;
@property (nonatomic, strong) BSOManager *bsoManager;
@property (nonatomic, strong) NSString *selecteDate;
@end

@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bsoManager = [BSOManager SharedInstance];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blackPatternBG"]];
    [self.txtSearch addTarget:self action:@selector(searchTextValueChaged) forControlEvents:UIControlEventEditingChanged];
    self.txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.txtSearch.enablesReturnKeyAutomatically = YES;
    if ([self.txtSearch respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor lightGrayColor];
        self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.txtSearch.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    }
    self.selecteDate = @"";
    [Utility setBorderColorWithView:self.btnToday color:[UIColor lightGrayColor]];
    [Utility setBorderColorWithView:self.txtSearch color:[UIColor lightGrayColor]];
    [Utility setBorderColorWithView:self.btnSelectDate color:[UIColor lightGrayColor]];
  
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:gesture];
    
    gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    gesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self.tblView addGestureRecognizer:gesture];

    [self getCalendarEvents];
    
}
- (void)getCalendarEvents {
    [self.indicator startAnimating];
    [self.bsoManager getAllEventsList:self.bsoManager.selectedType success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSArray class]]) {
            self.arrItems = [[NSMutableArray alloc] initWithArray:responseObject];
            self.arrAllItems = [[NSMutableArray alloc] initWithArray:responseObject];
            [self.tblView reloadData];
        } else  {
            
        }
        [self.indicator stopAnimating];
    } failure:^(NSError *error) {
        [self.indicator stopAnimating];
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
    float height = [Utility getTextHeightOfText:aEvent.title font:[UIFont fontWithName:@"OpenSans" size:16] width:tableView.frame.size.width-20];
    return 250 + height; // Label Y 225 + Bottom space 25
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CalendarCell *cell = (CalendarCell *) [tableView dequeueReusableCellWithIdentifier:@"CalendarCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CalendarCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        
        [cell.btnInfo addTarget:self action:@selector(infoButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnTicket addTarget:self action:@selector(ticketButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
         [cell.btnFB addTarget:self action:@selector(facebookButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
         [cell.btnTW addTarget:self action:@selector(twitterButtonPressed:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
    cell.lblName.text = aEvent.title;
     [cell.imgView sd_setImageWithURL:[NSURL URLWithString:aEvent.imageURL] placeholderImage:[UIImage imageNamed:@"noimage"] options:SDWebImageRefreshCached];
    cell.lblDate.text = aEvent.date;
    cell.lblTime.text = aEvent.time;

    cell.lblPriceDescription.text =  aEvent.priceDescription;
    cell.btnTicket.hidden = !aEvent.isOnSale;
    
    float height = [Utility getTextHeightOfText:aEvent.title font:[UIFont fontWithName:@"OpenSans" size:16] width:tableView.frame.size.width-20];
    CGRect frame = cell.lblName.frame;
    frame.size.height = height + 2;
    cell.lblName.frame = frame;
    
    cell.topColorView.backgroundColor = [self.bsoManager getSelectedItemColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)ticketButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        
        if([self.delegate respondsToSelector:@selector(CalendarViewTicketButtonPressed:)]) {
            Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
            [self.delegate CalendarViewTicketButtonPressed:aEvent];
        }
    }
}

- (void)infoButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        if([self.delegate respondsToSelector:@selector(CalendarViewInfoButtonPressed:)]) {
            Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
            [self.delegate CalendarViewInfoButtonPressed:aEvent];
        }
    }
}

- (void)facebookButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
        // Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
        CalendarCell *cell = (CalendarCell *)[self.tblView cellForRowAtIndexPath:indexPath];
        [Utility shareOnFacebook:self.parentViewController text:cell.lblName.text image:cell.imgView.image];
    }
}

- (void)twitterButtonPressed:(UIButton *)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tblView];
    NSIndexPath *indexPath = [self.tblView indexPathForRowAtPoint: currentTouchPosition];
    
    if(indexPath) {
       //Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
        CalendarCell *cell = (CalendarCell *)[self.tblView cellForRowAtIndexPath:indexPath];
        [Utility shareOnTwiitter:self.parentViewController text:cell.lblName.text image:cell.imgView.image];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Event *aEvent = [self.arrItems objectAtIndex:indexPath.row];
    [self.delegate CalendarViewInfoButtonPressed:aEvent];
}

- (IBAction)btnTodayPressed {
    self.btnToday.selected = !self.btnToday.selected;
    [self.btnSelectDate setTitle:@"SELECT DATE" forState:UIControlStateNormal];
    if(self.btnToday.selected) {
        self.selecteDate = [Utility stringFromDate:[NSDate date] format:CALERNDAR_FORMAT];
    } else {
        self.selecteDate = @"";
    }
    
    [self searchTextValueChaged];
}

- (IBAction)btnSelectDatePressed {
    SelectionView *selView = [[SelectionView alloc] initWithSelectionType:selectionTypeDatePicker];
    selView.delegate = self;
    if(self.selecteDate.length)
        selView.selecteDate = [Utility dateFromString:self.selecteDate format:CALERNDAR_FORMAT];
    [selView showInView:self.btnSelectDate];
}

- (void)searchTextValueChaged {
    [self.arrItems removeAllObjects];
    if(self.txtSearch.text.length == 0) {
        if(self.selecteDate.length != 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date  = %@", self.selecteDate];
            [self.arrItems addObjectsFromArray:[self.arrAllItems filteredArrayUsingPredicate:predicate]];

        } else {
            [self.arrItems addObjectsFromArray:self.arrAllItems];
        }
        
    } else {
        if(self.selecteDate.length != 0) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title  CONTAINS[cd] %@ AND date = %@", self.txtSearch.text,self.selecteDate];
            [self.arrItems addObjectsFromArray:[self.arrAllItems filteredArrayUsingPredicate:predicate]];
        } else {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", self.txtSearch.text];
            [self.arrItems addObjectsFromArray:[self.arrAllItems filteredArrayUsingPredicate:predicate]];
        }
    }
    [self.tblView reloadData];
}

- (IBAction)backButtonPressed {
    if([self.delegate respondsToSelector:@selector(CalendarViewBakButtonPressed)]) {
        [self.delegate CalendarViewBakButtonPressed];
    }
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)sender {
    if([self.delegate respondsToSelector:@selector(CalendarViewBakButtonPressed)]) {
        [self.delegate CalendarViewBakButtonPressed];
    }
}

#pragma mark - Date Selection

- (void)SelectionView:(SelectionView *)selectionView didSelectDate:(NSDate *)date SenderView:(UIView *)senderView {
    self.selecteDate = [Utility stringFromDate:date format:CALERNDAR_FORMAT];
    [self.btnSelectDate setTitle:[Utility stringFromDate:date format:@"dd-MM-yyy"] forState:UIControlStateNormal];
    [self searchTextValueChaged];
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end
