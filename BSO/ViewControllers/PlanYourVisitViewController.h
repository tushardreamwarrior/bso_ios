//
//  PlanYourVisitViewController.h
//  BSO
//
//  Created by MAC on 13/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlanYourVisitViewController : UIViewController
- (void)getPlanVisitDetails:(OrganizationType)type;
@end
