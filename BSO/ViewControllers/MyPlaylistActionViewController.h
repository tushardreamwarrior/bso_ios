//
//  MyPlaylistActionViewController.h
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListActionViewController.h"

@interface MyPlaylistActionViewController : UIViewController
@property (nonatomic, assign) id <PlayListActionDelegate> delegate;
@end
