//
//  AppDelegate.m
//  BSO
//
//  Created by MAC on 29/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import <GooglePlus/GooglePlus.h>
#import "HomeViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <Gimbal/Gimbal.h>
#include "EnterPinNumberViewController.h"


@implementation UINavigationBar (customNav)
- (CGSize)sizeThatFits:(CGSize)size {
    CGRect rec = self.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    rec.size.width = screenRect.size.width;
    if([BSOManager SharedInstance].compactNav)
        rec.size.height = 44;
    else
        rec.size.height = 70;
    return rec.size;
}
@end

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[GimbalManager SharedInstance] initializeCommunication];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName: [UIColor whiteColor],
                                                           NSFontAttributeName: [UIFont fontWithName:@"OpenSans-Semibold" size:20.0],
                                                           }];

    
    [GPPSignIn sharedInstance].clientID = GOOGLE_PLUS_KEY;
    [GMSServices provideAPIKey:GOOGLE_MAP_KEY];
    
    BOOL isDir = YES;
    if(![[NSFileManager defaultManager] fileExistsAtPath:FILE_PATH(@"Photos") isDirectory:&isDir]) {

        [[NSFileManager defaultManager] createDirectoryAtPath:FILE_PATH(@"Photos") withIntermediateDirectories:NO attributes:nil error:nil];
        [[NSFileManager defaultManager] createDirectoryAtPath:FILE_PATH(@"Videos") withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    if(GET_UD(ENABLE_PUSH) == nil) {
        SET_UD(ENABLE_PUSH, @"1");
        SET_UD(AUTO_UPDATE, @"0");
        SET_UD(ENABLE_4DIGITPIN, @"0");
        SET_UD(ENABLE_GEOLOCATION, @"0");
        SET_UD(ENABLE_TLOGIN, @"0");
        SET_UD(DIGIT_PIN, @"");
    }
    
    if(GET_UD(KEY) == nil) {
      NSString *key = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        SET_UD(KEY, key);
    }
    
    [self registerForRemoteNotifications];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
    
    [self validatePinNumber];
    
    UILocalNotification *pushNotification =
    [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (pushNotification) {
        NSString *push = [pushNotification.userInfo objectForKey:@"aps"];
        NSString *notification_id = [push valueForKey:@"notification_id"];
        if(notification_id) {
            [self callNotificationDetailWS:notification_id];
        }

    }
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [GPPURLHandler handleURL:url
                  sourceApplication:sourceApplication
                         annotation:annotation];
}

- (void)registerForRemoteNotifications {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        if([GET_UD(ENABLE_PUSH) intValue])
            [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
  
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge categories:nil]];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    self.deviceToken = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
   // [Utility ShowAlertWithTitle:@"" Message:token];
    [Gimbal setPushDeviceToken:deviceToken];
    BSOManager *bsoManager = [BSOManager SharedInstance];
    [bsoManager registerDeviceToken:self.deviceToken success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    GMBLCommunication *communication = [GMBLCommunicationManager communicationForLocalNotification:notification];
    [[GimbalManager SharedInstance] didRecieveCommunicationData:communication];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSDictionary *push = [userInfo objectForKey:@"aps"];
    NSString *notification_id = [push valueForKey:@"notification_id"];
    if(notification_id) {
        [self callNotificationDetailWS:notification_id];
    } else {
        GMBLCommunication *communication = [GMBLCommunicationManager communicationForRemoteNotification:userInfo];
        [[GimbalManager SharedInstance] didRecieveCommunicationData:communication];
    }
}

- (void)callNotificationDetailWS:(NSString *)notification_id {
    
    BSOManager *manager = [BSOManager SharedInstance];
    [Utility showLoader];
    [manager getNotificationDetail:notification_id success:^(id responseObject) {
        if([responseObject isKindOfClass:[Notification class]]) {
            Notification *aNotification = (Notification *)responseObject;
            NSString *castDescription = [NSString stringWithFormat:@"<font size='5' face='OpenSans'>%@</font>",aNotification.desc];
            UINavigationController *nav  = (UINavigationController *)self.window.rootViewController;
            [Utility openContentInInterBrowser:castDescription from:nav.visibleViewController];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (void)validatePinNumber {

   NSString *passCode = GET_UD(DIGIT_PIN);
    if([GET_UD(ENABLE_4DIGITPIN) intValue] && passCode.length == 4) {
        EnterPinNumberViewController *entePinNumberViewController = [[EnterPinNumberViewController alloc] initWithNibName:@"EnterPinNumberViewController" bundle:nil];
        entePinNumberViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        //  UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        [self.window.rootViewController presentViewController:entePinNumberViewController animated:YES completion:nil];
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self validatePinNumber];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
