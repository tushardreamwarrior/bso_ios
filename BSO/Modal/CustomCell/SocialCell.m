//
//  SocialCell.m
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SocialCell.h"

@implementation SocialCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SocialCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    [Utility setBorderColorWithView:self color:[UIColor whiteColor]];
    
    return self;
}



@end
