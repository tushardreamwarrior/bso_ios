//
//  ArtistCell.h
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblOther;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet UIButton *btnBio;

@end
