//
//  FinalizeItemCell.h
//  BSO
//
//  Created by MAC on 14/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinalizeItemCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPrice;
@property (nonatomic, strong) IBOutlet BSOLabel *lblQty;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTotal;
@end
