//
//  CollectionCell.m
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CollectionCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    return self;
}
@end
