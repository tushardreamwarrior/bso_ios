//
//  ParkingCell.h
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkingCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblInfo;
@end
