//
//  VideoCell.h
//  BSO
//
//  Created by MAC on 15/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblVideoName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMusic;
@property (nonatomic, strong) IBOutlet BSOButton *btnPlay;
@property (nonatomic, strong) IBOutlet BSOButton *btnMore;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@end
