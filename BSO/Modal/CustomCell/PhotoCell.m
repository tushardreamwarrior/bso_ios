//
//  PhotoCell.m
//  BSO
//
//  Created by MAC on 28/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PhotoCell.h"

@implementation PhotoCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PhotoCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    return self;
}

@end
