//
//  DinigCell.h
//  BSO
//
//  Created by MAC on 13/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DinigCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPhone;
@property (nonatomic, strong) IBOutlet BSOLabel *lblAddress;
@property (nonatomic, strong) IBOutlet BSOButton *btnLink;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet UIImageView *imgArrow;
@end
