//
//  OrderItemCell.h
//  BSO
//
//  Created by MAC on 29/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderItemCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet BSOTextView *txtTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblQty;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTotal;
@property (nonatomic, strong) IBOutlet BSOButton *btnSeatDetails;
@property (nonatomic, strong) IBOutlet BSOButton *btnBarCode;
@property (nonatomic, strong) IBOutlet BSOLabel *lblSeatDetais;
@end
