//
//  ArtistCell.m
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "ArtistCell.h"

@implementation ArtistCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"ArtistCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    [Utility setBorderColorWithView:self.btnBio];
    [Utility setCornerToView:self.btnBio corner:3];
    return self;
}

@end
