//
//  CollectionCell.h
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet BSOButton *btnCheck;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@end
