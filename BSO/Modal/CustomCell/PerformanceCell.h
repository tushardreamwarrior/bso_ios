//
//  PerformanceCell.h
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerformanceCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIView *topColorView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet UITextView *txtDesc;
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet UIButton *btnTicket;
@end
