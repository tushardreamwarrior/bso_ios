//
//  PodcastCell.h
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PodcastCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet BSOLabel *lblOther;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOButton *btnPlay;
@property (nonatomic, strong) IBOutlet BSOButton *btnMore;
@end
