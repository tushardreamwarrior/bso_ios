//
//  MusicCell.m
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "MusicCell.h"

@implementation MusicCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MusicCell" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    
    return self;
}

@end
