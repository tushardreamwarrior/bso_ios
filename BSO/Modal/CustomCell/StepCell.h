//
//  StepCell.h
//  BSO
//
//  Created by MAC on 30/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StepCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOTextView *txtInstruction;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDistance;

@end
