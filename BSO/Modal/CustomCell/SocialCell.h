//
//  SocialCell.h
//  BSO
//
//  Created by MAC on 27/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet BSOTextView *txtDes;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOButton *btnLink;
@property (nonatomic, strong) IBOutlet UIImageView *imgSocial;
@end
