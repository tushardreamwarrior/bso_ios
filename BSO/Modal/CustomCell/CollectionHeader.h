//
//  CollectionHeader.h
//  DreamWarrior
//
//  Created by MAC on 12/08/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeader : UICollectionReusableView {
    
}
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPeromanceDesc;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDesc;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPrice;
//@property (nonatomic, strong) IBOutlet BSOLabel *lblSubType;
@property (nonatomic, strong) IBOutlet BSOButton *btnSeats;
@property (nonatomic, strong) IBOutlet BSOButton *btnClick;
@property (nonatomic, strong) IBOutlet UIView *innerView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblLine;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPeformanceRange;
@end
