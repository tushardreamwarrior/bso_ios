//
//  ProgramCell.h
//  BSO
//
//  Created by MAC on 27/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramCell : UITableViewCell
@property (nonatomic, strong) IBOutlet BSOTextView *txtDesc;
@property (nonatomic, strong) IBOutlet BSOButton *btnPlay;
@property (nonatomic, strong) IBOutlet BSOButton *btnNotes;
@end
