//
//  CalendarCell.h
//  BSO
//
//  Created by MAC on 30/03/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIView *topColorView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTime;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPriceDescription;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOButton *btnTicket;
@property (nonatomic, strong) IBOutlet BSOButton *btnInfo;
@property (nonatomic, strong) IBOutlet BSOButton *btnFB;
@property (nonatomic, strong) IBOutlet BSOButton *btnTW;
@end
