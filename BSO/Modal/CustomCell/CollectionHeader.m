//
//  CollectionHeader.m
//  DreamWarrior
//
//  Created by MAC on 12/08/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import "CollectionHeader.h"

@implementation CollectionHeader

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CollectionHeader" owner:self options:nil];
        self = [arrayOfViews objectAtIndex:0];
        
    }
    return self;
}


@end
