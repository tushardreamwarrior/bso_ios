//
//  CartItemCell.h
//  BSO
//
//  Created by MAC on 27/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartItemCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) IBOutlet BSOLabel *lblDate;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTitle;
@property (nonatomic, strong) IBOutlet BSOLabel *lblPrice;
@property (nonatomic, strong) IBOutlet BSOLabel *lblQty;
@property (nonatomic, strong) IBOutlet BSOLabel *lblTotal;
@property (nonatomic, strong) IBOutlet BSOButton *btnSeatDetails;
@property (nonatomic, strong) IBOutlet BSOLabel *lblSeatDetais;
@end
