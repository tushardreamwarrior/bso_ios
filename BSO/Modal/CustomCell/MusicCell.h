//
//  MusicCell.h
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet BSOLabel *lblArtistName;
@property (nonatomic, strong) IBOutlet BSOLabel *lblMusic;
@property (nonatomic, strong) IBOutlet BSOButton *btnPlay;
@property (nonatomic, strong) IBOutlet BSOButton *btnMore;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@end
