//
//  Constant.h
//  FLIXnCHILL
//
//  Created by Imobtree solutions on 08/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define DEVICE_WIDTH    [[UIScreen mainScreen] bounds].size.width
#define DEVICE_HEIGHT	[[UIScreen mainScreen] bounds].size.height
#define DEVICE_FRAME    [[UIScreen mainScreen] bounds]

#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]
#define GET_UD(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]
#define SET_UD(key, object) \
[[NSUserDefaults standardUserDefaults] setObject:object forKey:key]; \
[[NSUserDefaults standardUserDefaults] synchronize];

#define RGB(r,g,b)    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define OS_VER [[[UIDevice currentDevice] systemVersion] floatValue]

#define TANGELWOOD_COLOR RGB(25,106,65)
#define BSO_COLOR RGB(138,18,40)
#define BPOP_COLOR RGB(5,28,67)
#define BSO_TEXT_COLOR RGB(128,0,40)
#define BPOP_TEXT_COLOR RGB(21,92,167)

#define TANGELWOOD_BG_COLOR RGBA(25,106,65,.5)
#define BSO_BG_COLOR RGBA(138,18,40,.5)
#define BPOP_BG_COLOR RGBA(5,28,67,.5)


#define DOC_DIR [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define FILE_PATH(path) [DOC_DIR stringByAppendingPathComponent:path]

#define DATE_FORMAT @"yyyy-MM-dd"
#define TIME_FORMAT @"hh:mm:ss a"
#define SERVER_DATE_TIME_FORMAT @"yyy-MM-dd HH:mm:ss"

#define DATE_TIME_FORMAT @"dd-MM-yyyy hh:mm a"
#define CALERNDAR_FORMAT @"EEEE, MMMM dd, yyyy"
#define RANGE_FORMAT @"MMMM dd, yyyy"

#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REMEMBER_ACTIVE @"remember"
#define REMEMBER_EMAIL @"email"
#define PASSCODE @"passcode"

#define APP_URL @"http://bso.li"
#define BASE_URL @"http://bso.li/"
#define API_FULL_URL(API_URL)  [BASE_URL stringByAppendingString:API_URL]

#define SERVER_ADDRESS      @"http://bso.li/api/"

#define CODE @"code"
#define DATA @"data"
#define MESSAGE @"message"

#define SELECTED_TYPE @"SELECTEDTYPE"
#define ENABLE_PUSH @"ENABLEPUSH"
#define AUTO_UPDATE @"AUTOUPDATE"
#define ENABLE_TLOGIN @"ENABLETLOGIN"
#define ENABLE_4DIGITPIN @"ENABLE4DIGITPIN"
#define ENABLE_GEOLOCATION @"ENABLEGEOLOCATION"
#define DIGIT_PIN @"DIGITPIN"
#define USER_EMAIL @"USEREMAIL"
#define USER_PASSWORD @"USERPASSWORD"

#define EXTERNAL_ID @"external_id"
#define SESSION_ID @"asp_session_id"
#define SESSION_TIME @"SESSIONTIME"
#define FIRST_TIME @"FIRSTTIME"

#define PAYMENT_CARD_NUMBER @"cardNumber"
#define PAYMENT_CARD_NAME @"cardName"
#define PAYMENT_CARD_TYPE @"cardType"
#define PAYMENT_CARD_MONTH @"cardMonth"
#define PAYMENT_CARD_YEAR @"cardYear"
#define PAYMENT_CARD_CVV @"cardCVV"
#define KEY @"key"

#define DAY_LENGTH 86400

#define YOUTUBE_THUMB(ID) [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",ID]

static NSString * const GOOGLE_PLUS_KEY = @"901953066766-u4d8ke47dkvlrom2ec3ujhohi6v32g7c.apps.googleusercontent.com";
static NSString *const GOOGLE_MAP_KEY = @"AIzaSyBB90RO1ZgdHpruCf-iNgaUVPHsq2NKIig";
static NSString *const YOUTUBE_TOKEN = @"YOUTUBETOKEN";
static NSString *const GOOGLE_API_KEY = @"AIzaSyDvfsMq0t-9aP6WgfH9-DmBEyG9M8SyaDg";

static NSString *const GIMBLE_KEY = @"b5aaf524-c9f3-4868-a009-435acaad35f4";

typedef enum {
    jSuccess = 0,
    jServerError ,
    jFalseResponse,
    jInvalidResponse
}ErrorCode;


typedef enum {
    jTangleWood = 1,
    jBSO = 0,
    jBPop = 2,
} OrganizationType;

//#define FONT_NAME @""

#endif /* Constant_h */
