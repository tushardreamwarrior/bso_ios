//
//  RestClient.h
//  BSO
//
//  Created by MAC on 17/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFNetworking.h"
typedef void(^successCallback)(NSURLSessionDataTask *task, id responseObject);
typedef void(^failureCallback)(NSURLSessionDataTask *task, NSError *error);


@interface RestClient : AFHTTPSessionManager
+ (id)sharedInstance;
- (void)getShowList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getAllEventsList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)bsoLogin:(NSString *)email password:(NSString *)password success:(successCallback)success failure:(failureCallback)failure;
- (void)getShowDetails:(NSString * )show_id success:(successCallback)success failure:(failureCallback)failure;
- (void)getAllMediaList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getFBPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getTwitterPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getGooglePlus:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getYouTubePostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getInstagramPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getHashTagPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getAboutUSContent:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)uploadYouTubeVideo:(NSString *)title videoPath:(NSURL *)filePath apiKey:(NSString *)key succes:(successCallback)success failure:(failureCallback)failure;
- (void)getBeaconContent:(NSString *)beaconID success:(successCallback)success failure:(failureCallback)failure;
- (void)getRouteBetweenLocations:(NSString *)url success:(successCallback)success failure:(failureCallback)failure;
- (void)getNearByPlaces:(CLLocationCoordinate2D)coordinate radius:(int)radius keyword:(NSString *)keyword success:(successCallback)success failure:(failureCallback)failure;
- (void)getPlaceDetails:(NSString *)placeID success:(successCallback)success failure:(failureCallback)failure;
- (void)getBestAvailableSeats:(NSString *)perfrormance success:(successCallback)success failure:(failureCallback)failure;
- (void)chekAvailableSeats:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)addToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)checkUserSession:(NSString *)email success:(successCallback)success failure:(failureCallback)failure;
- (void)getShoppingCartItems:(successCallback)success failure:(failureCallback)failure;
- (void)checkOutShipping:(successCallback)success failure:(failureCallback)failure;
- (void)getDevicePreferences:(successCallback)success failure:(failureCallback)failure;
- (void)checkOutPayment:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)checkoutFinalize:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)makeImpulseDonation:(OrganizationType )type amount:(NSString *)amount success:(successCallback)success failure:(failureCallback)failure;
- (void)getNearByLocatios:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)getSubscriptionListing:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure;
- (void)applyDiscoutCode:(NSString *)code success:(successCallback)success failure:(failureCallback)failure;
- (void)getOrderHistory:(successCallback)success failure:(failureCallback)failure;
- (void)subscriptionBestAvailableCYO:(NSString *)subscription_ID list:(NSMutableArray *)arrList success:(successCallback)success failure:(failureCallback)failure;
- (void)SubscriptionCYOAddToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)subscriptionBestAvailableFixed:(NSString *)subscription_ID success:(successCallback)success failure:(failureCallback)failure;
- (void)SubscriptionFixedAddToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure;
- (void)registerDeviceToken:(NSString *)deviceToken success:(successCallback)success failure:(failureCallback)failure;
- (void)getListOfNotifications:(successCallback)success failure:(failureCallback)failure;
- (void)getNotificationDetail:(NSString *)notification_id success:(successCallback)success failure:(failureCallback)failure;
- (void)userLogout:(NSString *)email success:(successCallback)success failure:(failureCallback)failure;
- (void)getTicketBarCode:(NSString *)orderId number:(NSString *)subLineItemNumber success:(successCallback)success failure:(failureCallback)failure;
- (void)removeItemFromCart:(NSString *)lineNum success:(successCallback)success failure:(failureCallback)failure;
- (void)validateCardInfo:(CreditCard *)card success:(successCallback)success failure:(failureCallback)failure;
- (void)getPaymentCards:(successCallback)success failure:(failureCallback)failure;
- (void)getCardInfo:(successCallback)success failure:(failureCallback)failure;
@end
