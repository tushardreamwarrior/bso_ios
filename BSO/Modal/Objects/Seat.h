//
//  Seat.h
//  BSO
//
//  Created by MAC on 13/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AvailalbeSeatResponse : NSObject
@property (nonatomic, strong) NSMutableArray *arrSections; //  Seat
@property (nonatomic, strong) NSMutableDictionary *dictOtherInfo;
+ (AvailalbeSeatResponse *)parseAvailableSeat:(NSDictionary *)dict;
@property (nonatomic, strong) NSString *peroformanceTitle;
@property (nonatomic, strong) NSString *peroformanceDate;
@property (nonatomic, strong) NSString *peroformanceID;
@end

@interface SeatSection : NSObject
@property (nonatomic, strong) NSString *sectionPrice;
@property (nonatomic, strong) NSMutableArray *arrSeats;
@end

@interface Seat : NSObject
@property (nonatomic, strong) NSString *seatID;
@property (nonatomic, strong) NSString *seatsAvailable;
@property (nonatomic, strong) NSString *seatDescription;

@property (nonatomic, strong) NSString *seatPriceID;


@end
