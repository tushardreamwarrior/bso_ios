//
//  SCart.h
//  BSO
//
//  Created by MAC on 27/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    jCondtribution = 0,
    jTicket ,
    jSubscription ,
}ItemType;

@interface CreditCard : NSObject
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *cardName;
@property (nonatomic, strong) NSString *cardType;
@property (nonatomic, strong) NSString *cardCVV;
@property (nonatomic, strong) NSString *cardExpireMonth;
@property (nonatomic, strong) NSString *cardExpireYear;
+(NSMutableArray *)parserCardJsonResponse:(NSArray *)array;
@end

@interface Address : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *streetPrimary;
@property (nonatomic, strong) NSString *streetSeconday;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *contry;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *fullAddress;
@property (nonatomic, strong) NSString *fullAddressWithPhone;
@property (nonatomic, assign) BOOL isSelected;
@end

@interface CartSection : NSObject
@property (nonatomic, strong) NSMutableArray *arrCartItems;// CartItem
@property (nonatomic, strong) NSString *headerString;
@property (nonatomic, assign) ItemType type;
@property (nonatomic, assign) BOOL showETickets;
@end

@interface SeatItem : NSObject
@property (nonatomic, strong) NSString *cost;
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSString *row;
@property (nonatomic, strong) NSString *number;
@end

@interface CartItem : NSObject
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *totalCost;
@property (nonatomic, strong) NSString *performanceDate;
@property (nonatomic, strong) NSMutableArray *arrReats;
@property (nonatomic, strong) NSString *seatDesc;
@property (nonatomic, assign) BOOL isSeatOpened;
@property (nonatomic, assign) ItemType type;
@property (nonatomic, strong) NSString *orginalPrice;
@property (nonatomic, strong) NSString *subLineItemNumber;
@property (nonatomic, strong) NSString *orderNumber;
@property (nonatomic, strong) NSString *lineNumber;
@end


@interface SCart : NSObject
@property (nonatomic, strong) NSMutableArray *arrCartSection; //CartSection
@property (nonatomic, strong) NSMutableArray *arrDiscoutCodes;
@property (nonatomic, strong) NSMutableArray *arrShippingAddress;
@property (nonatomic, strong) NSString *subTotal;
@property (nonatomic, strong) NSString *tessituraFees;
@property (nonatomic, strong) NSString *totalCost;
@property (nonatomic, strong) NSString *shippingName;
@property (nonatomic, assign) BOOL printAtHome; // Shiping;
@property (nonatomic, strong) CreditCard *aCard;


+(SCart *)parseShoppingCartResponse:(NSDictionary *)dict;
+ (SCart *)parseShoppingCartShippingResponse:(NSDictionary *)dict;
+ (SCart *)parseOrderHistoryResponse:(NSDictionary *)dict;
@end
