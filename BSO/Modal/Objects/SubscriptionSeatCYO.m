//
//  SubscriptionSeatCYO.m
//  BSO
//
//  Created by MAC on 05/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SubscriptionSeatCYO.h"

@implementation SubscriptionSeatCYO
+ (SubscriptionSeatCYO *)parseSubscriptionAvailableSeat:(NSDictionary *)dict {
    
    SubscriptionSeatCYO *CYO = [[SubscriptionSeatCYO alloc] init];
    CYO.title = [dict valueForKey:@"Title"];
    CYO.arrPerformances = [[NSMutableArray alloc] init];
                                
    NSArray *performances = [dict valueForKey:@"IndividualPerformanceViewModel"];
    for(NSDictionary *performance in performances) {
        
        NSArray *object = [[performance valueForKey:@"Data"] valueForKey:@"AvailableZones"];
        
        NSArray *uniqueprices = [[NSSet setWithArray:[object valueForKey:@"MinimumPrice"]] allObjects];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: NO];
        uniqueprices = [uniqueprices sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
        
        AvailalbeSeatResponse *responseObject = [[AvailalbeSeatResponse alloc] init];
        responseObject.peroformanceTitle = [[performance valueForKey:@"Data"] valueForKey:@"Title"];
        responseObject.peroformanceTitle = [responseObject.peroformanceTitle stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        responseObject.peroformanceDate = [Utility converDateFormat:[[performance valueForKey:@"Data"] valueForKey:@"PerformanceDate"] from:@"EEE, dd MMM yyyy HH:mm:ss" to:@"MM/dd/YYYY hh a"];
        responseObject.peroformanceID = [[performance valueForKey:@"Data"] valueForKey:@"ExternalId"];
        
        responseObject.arrSections = [[NSMutableArray alloc] init];

        
        for(NSString *price in uniqueprices) {
            SeatSection *aSection = [[SeatSection alloc] init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"MinimumPrice = %@",price];
            NSArray *seats = [object filteredArrayUsingPredicate:predicate];
            for(NSDictionary *dict in seats) {
                Seat *aSeat = [[Seat alloc] init];
                aSeat.seatsAvailable = [NSString stringWithFormat:@"%@",[dict valueForKey:@"SeatsAvailable"]];
                aSeat.seatDescription = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Description"]];
                aSection.sectionPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"MinimumPrice"]];
                aSeat.seatID = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
                aSeat.seatPriceID = [NSString stringWithFormat:@"%@",[[[dict valueForKey:@"PriceTypes"]  objectAtIndex:0] valueForKey:@"Id"]];
                [aSection.arrSeats addObject:aSeat];
            }
            [responseObject.arrSections addObject:aSection];
        }
        [CYO.arrPerformances addObject:responseObject];
    }
    
    return CYO;
}
@end
