//
//  Subscription.m
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "Subscription.h"

@implementation Performance
+ (NSMutableArray *)parsePerformanceList:(NSArray *)array {
    NSMutableArray *arrPerformance = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in array) {
        Performance *aPerformance = [[Performance alloc] init];
        aPerformance.ID = [dict valueForKey:@"PerformanceId"];
        aPerformance.listID = [dict valueForKey:@"ListId"];
        aPerformance.title = [[dict valueForKey:@"Title"] unescapeUnicodeString];
        aPerformance.title = [aPerformance.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        aPerformance.date = [Utility converDateFormat:[dict valueForKey:@"TimeStart"] from:@"MM-dd-yyyy hh a" to:@"MM/dd/YYYY , hh a"];
        aPerformance.weekDay = [[Utility converDateFormat:[dict valueForKey:@"TimeStart"] from:@"MM-dd-yyyy hh a" to:@"EEE"] uppercaseString];

        aPerformance.imageURL = [dict valueForKey:@"Image"];
        [arrPerformance addObject:aPerformance];
    }
    return arrPerformance;
}
@end

@implementation Subscription
- (instancetype)init {
    self = [super init];
    _arrPerformance = [[NSMutableArray alloc] init];
    return self;
}

+ (NSMutableArray *)parseSubscriptionList:(NSArray *)array {
    NSMutableArray *arrSubscription = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in array) {
        Subscription *aSubscription = [[Subscription alloc] init];
        aSubscription.ID = [dict valueForKey:@"subscriptionId"];
        aSubscription.title = [[dict valueForKey:@"Title"] unescapeUnicodeString];
        aSubscription.strDescription = [[dict valueForKey:@"Description"]  unescapeUnicodeString];
        aSubscription.imageURL = [dict valueForKey:@"thumbnail"];
        aSubscription.subType = [dict valueForKey:@"SubscriptionTypeName"] ;
        aSubscription.subCode = [[dict valueForKey:@"SubscriptionType"] intValue];
        aSubscription.strPeformanceDescription = [[dict valueForKey:@"PerformancesShortDescription"] unescapeUnicodeString];
        aSubscription.priceRange = [dict valueForKey:@"priceRange"];
        aSubscription.isOnSale = [[dict valueForKey:@"IsOnSale"] intValue];
        aSubscription.minPerformance = [[dict valueForKey:@"MinimumNumberOfPerformances"] intValue];
        aSubscription.maxPerformance = [[dict valueForKey:@"MaximumNumberOfPerformances"] intValue];
      
        [aSubscription.arrPerformance addObjectsFromArray:[Performance parsePerformanceList:[dict valueForKey:@"performaces"]]];
        [arrSubscription addObject:aSubscription];
    }
    return arrSubscription;
}
@end
