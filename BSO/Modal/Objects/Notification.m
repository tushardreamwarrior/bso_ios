//
//  Notification.m
//  BSO
//
//  Created by MAC on 14/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "Notification.h"

@implementation Notification
+ (NSMutableArray *)parseNotificationListRespose:(NSArray *)array {
    NSMutableArray *arrNotifications = [[NSMutableArray alloc] init];
    
    for(NSDictionary *dict in array) {
        Notification *aNotification = [[Notification alloc] init];
        aNotification.ID = [dict valueForKey:@"id"];
        aNotification.title = [dict valueForKey:@"title"];
        aNotification.desc = [dict valueForKey:@"description"];
        aNotification.startDate = [dict valueForKey:@"start_date"];
        aNotification.endDate = [dict valueForKey:@"end_date"];
        [arrNotifications addObject:aNotification];
    }
    
    return arrNotifications;
}
@end
