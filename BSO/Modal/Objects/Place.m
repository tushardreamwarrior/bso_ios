//
//  Place.m
//  BSO
//
//  Created by MAC on 05/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "Place.h"

@implementation Place

+ (NSMutableArray *)parsePlacesListContent:(NSArray *)results {
    NSMutableArray *arrPlaces = [[NSMutableArray alloc] init];
    if(results == nil) {
        return arrPlaces;
    }
    for(NSDictionary *dict in results) {
        Place *aPlace = [[Place alloc] init];
        aPlace.coordinate = CLLocationCoordinate2DMake([[[[dict valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"] doubleValue], [[[[dict valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"] doubleValue]);
        aPlace.icon = [dict valueForKey:@"icon"];
        aPlace.ID = [dict valueForKey:@"place_id"];
        aPlace.name = [[dict valueForKey:@"name"] unescapeUnicodeString];
        aPlace.address = [dict valueForKey:@"vicinity"];
        aPlace.isGoogleRecord = TRUE;
        
        [arrPlaces addObject:aPlace];
    }
    return arrPlaces;
}

+ (NSMutableDictionary *)parseNearByLocationWSResponse:(NSDictionary *)dictionary {
    NSMutableDictionary *dictResult = [[NSMutableDictionary alloc] init];
    NSArray *dining = [dictionary valueForKey:@"dining"];
    NSArray *parking = [dictionary valueForKey:@"parking"];
    NSArray *lodging = [dictionary valueForKey:@"lodging"];
    
    NSMutableArray *arrDining = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in dining) {
        Place *aPlace = [[Place alloc] init];
        aPlace.name = [dict valueForKey:@"name"] ;
        aPlace.address = [dict valueForKey:@"address"];
        aPlace.icon = [dict valueForKey:@"picture"];
        aPlace.website = [dictionary valueForKey:@"link"];
        aPlace.coordinate = CLLocationCoordinate2DMake([[dict  valueForKey:@"latitude"] doubleValue], [[dict  valueForKey:@"longitude"] doubleValue]);
        aPlace.isGoogleRecord = FALSE;
        [arrDining addObject:aPlace];
    }
   
    NSMutableArray *arrLodging = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in lodging) {
        Place *aPlace = [[Place alloc] init];
        aPlace.name = [dict valueForKey:@"name"] ;
        aPlace.address = [dict valueForKey:@"address"];
        aPlace.icon = [dict valueForKey:@"picture"];
        aPlace.website = [dictionary valueForKey:@"link"];
        aPlace.coordinate = CLLocationCoordinate2DMake([[dict  valueForKey:@"latitude"] doubleValue], [[dict  valueForKey:@"longitude"] doubleValue]);
        aPlace.isGoogleRecord = FALSE;
        [arrLodging addObject:aPlace];
    }

    NSMutableArray *arrParking = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in parking) {
        Place *aPlace = [[Place alloc] init];
        aPlace.name = [dict valueForKey:@"name"] ;
        aPlace.address = [dict valueForKey:@"address"];
        aPlace.icon = [dict valueForKey:@"picture"];
        aPlace.website = [dictionary valueForKey:@"link"];
        aPlace.coordinate = CLLocationCoordinate2DMake([[dict  valueForKey:@"latitude"] doubleValue], [[dict  valueForKey:@"longitude"] doubleValue]);
        aPlace.isGoogleRecord = FALSE;
        [arrParking addObject:aPlace];
    }
    
    [dictResult setValue:arrDining forKey:@"Dining"];
    [dictResult setValue:arrLodging forKey:@"Lodging"];
    [dictResult setValue:arrParking forKey:@"Parking"];
    
    return dictResult;
}

+ (Place *)parsePlaceDetailContent:(NSDictionary *)results {
    if(results == nil) {
        return nil;
    }
    
    Place *aPlace = [[Place alloc] init];
    aPlace.coordinate = CLLocationCoordinate2DMake([[[[results valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"] doubleValue], [[[[results valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"] doubleValue]);
    aPlace.icon = [results valueForKey:@"icon"];
    aPlace.ID = [results valueForKey:@"place_id"];
    aPlace.name = [[results valueForKey:@"name"] unescapeUnicodeString];
    aPlace.address = [results valueForKey:@"vicinity"];
    aPlace.website = [results valueForKey:@"website"];
    aPlace.isGoogleRecord = TRUE;
    return aPlace;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
  //  [encoder encodeObject:_title forKey:@"title"];
}

- (id)initWithCoder:(NSCoder *)decoder {
  //  _title = [decoder decodeObjectForKey:@"title"];
    return self;
}



@end
