//
//  SubscriptionSeatCYO.h
//  BSO
//
//  Created by MAC on 05/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubscriptionSeatCYO : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableArray *arrPerformances;
+ (SubscriptionSeatCYO *)parseSubscriptionAvailableSeat:(NSDictionary *)dict;
@end
