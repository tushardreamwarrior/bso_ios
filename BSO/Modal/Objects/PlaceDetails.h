//
//  PlaceDetails.h
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@interface Step : NSObject
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *instruection;
@property (nonatomic, strong) NSString *maneuver;
@end

@interface RouteDetails : NSObject
@property (nonatomic, assign) CLLocationCoordinate2D startCoordinate;
@property (nonatomic, strong) NSString *startAddress;
@property (nonatomic, assign) CLLocationCoordinate2D endCoordinate;
@property (nonatomic, strong) NSString *endAddress;
@property (nonatomic, strong) NSMutableArray *arrSteps;
@property (nonatomic, strong) NSString *estimatedTime;
@property (nonatomic, strong) NSString *distance;
@end

@interface PlaceDetails : NSObject
@property (nonatomic, strong) Event *aEvent;
@property (nonatomic, strong) NSMutableArray *arrDining;
@property (nonatomic, strong) NSMutableArray *arrLodging;
@property (nonatomic, strong) NSMutableArray *arrParking;
@end
