//
//  Notification.h
//  BSO
//
//  Created by MAC on 14/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;

+ (NSMutableArray *)parseNotificationListRespose:(NSArray *)array;
@end
