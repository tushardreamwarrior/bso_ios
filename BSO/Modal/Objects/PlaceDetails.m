//
//  PlaceDetails.m
//  BSO
//
//  Created by MAC on 14/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "PlaceDetails.h"

@implementation Step

- (instancetype)init {
    self = [super init];
    return self;
}

@end


@implementation RouteDetails

- (instancetype)init {
    self = [super init];
    self.arrSteps = [[NSMutableArray alloc] init];
    return self;
}

@end


@implementation PlaceDetails
- (instancetype)init {
    self = [super init];
    self.arrDining = [[NSMutableArray alloc] init];
    self.arrLodging = [[NSMutableArray alloc] init];
    self.arrParking = [[NSMutableArray alloc] init];
    
    return self;
}

@end

