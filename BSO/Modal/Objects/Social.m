//
//  Social.m
//  BSO
//
//  Created by MAC on 25/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "Social.h"

@implementation Social

+ (NSArray *)parseFacebookPostResponse:(NSDictionary *)dict {
    NSArray *posts = [[[dict valueForKey:@"responseData"] valueForKey:@"feed"] valueForKey:@"entries"];
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    if(posts == nil) {
        return arrPost;
    }
    for(NSDictionary *dict in posts) {
        Social *aSocial = [[Social alloc] init];
        aSocial.postURL = [dict valueForKey:@"link"];
        aSocial.date = [dict valueForKey:@"publishedDate"];
        aSocial.title = [[dict valueForKey:@"content"] unescapeUnicodeString];
        if([[dict valueForKey:@"thumb"] isKindOfClass:[NSNull class]]) {
            aSocial.image = @"";
        } else {
            aSocial.image = [dict valueForKey:@"thumb"];
        }
        
        aSocial.type = jFacebook;
        [arrPost addObject:aSocial];
    }
    return arrPost;
    
}

+ (NSArray *)parseTwitterPostResponse:(NSArray *)posts {
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    if(posts == nil || ![posts isKindOfClass:[NSArray class]]) {
        return arrPost;
    }
    for(NSDictionary *dict in posts) {
        Social *aSocial = [[Social alloc] init];
        aSocial.date = [dict valueForKey:@"created_at"];
        aSocial.title = [[dict valueForKey:@"text"] unescapeUnicodeString];
        aSocial.ID = [dict valueForKey:@"id_str"];
        
        if([[dict valueForKey:@"entities"] valueForKey:@"media"] != nil) {
            NSDictionary *media = [[[dict valueForKey:@"entities"] valueForKey:@"media"] objectAtIndex:0];
            aSocial.image = [media valueForKey:@"media_url"];
            aSocial.postURL = [media valueForKey:@"url"];
        } else {
            NSDictionary *screenName = [[dict valueForKey:@"user"] valueForKey:@"screen_name"];
            
            aSocial.postURL = [NSString stringWithFormat:@"https://twitter.com/%@/status/%@",screenName,aSocial.ID];
            aSocial.image = [[dict valueForKey:@"user"] valueForKey:@"profile_image_url"];
        }
       
        aSocial.type = jTwitter;
        [arrPost addObject:aSocial];
    }
    return arrPost;
    
}

+ (NSArray *)parseGooglePlusPostResponse:(NSArray *)posts {
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    if(posts == nil) {
        return arrPost;
    }
    for(NSDictionary *dict in posts) {
        Social *aSocial = [[Social alloc] init];
        aSocial.title = [[dict valueForKey:@"title"] unescapeUnicodeString];
      //  aSocial.title = [dict valueForKey:@"content"];
        aSocial.image = [dict valueForKey:@"image"];
        aSocial.postURL = [dict valueForKey:@"post_url"];;
        aSocial.type = jGoogle;
        [arrPost addObject:aSocial];
    }
    return arrPost;
    
}

+ (NSArray *)parseInstagramPostResponse:(NSArray *)posts {
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    if(posts == nil) {
        return arrPost;
    }
    for(NSDictionary *dict in posts) {
        Social *aSocial = [[Social alloc] init];
        aSocial.title = [[dict valueForKey:@"title"] unescapeUnicodeString];
        // aSocial.title = [dict valueForKey:@"content"];
        aSocial.image = [dict valueForKey:@"image_url"];
        aSocial.postURL = [dict valueForKey:@"url"];
        aSocial.ID = [dict valueForKey:@"id"];
        aSocial.type = jInstagram;
        [arrPost addObject:aSocial];
    }
    return arrPost;
    
}

+ (NSArray *)parseYouTubePostResponse:(NSArray *)posts {
    NSMutableArray *arrPost = [[NSMutableArray alloc] init];
    if(posts == nil) {
        return arrPost;
    }
    for(NSDictionary *dict in posts) {
        Social *aSocial = [[Social alloc] init];
        aSocial.title = [[dict valueForKey:@"title"] unescapeUnicodeString];
       // aSocial.title = [dict valueForKey:@"content"];
        aSocial.image = [dict valueForKey:@"image_url"];
        aSocial.postURL = [dict valueForKey:@"youtube_url"];
        aSocial.ID = [dict valueForKey:@"video_id"];
        aSocial.type = jYouTube;
        [arrPost addObject:aSocial];
    }
    return arrPost;
    
}


- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_title forKey:@"title"];
    [encoder encodeObject:_image forKey:@"image"];
    [encoder encodeObject:_postURL forKey:@"url"];
    [encoder encodeInteger:_type forKey:@"type"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    _title = [decoder decodeObjectForKey:@"title"];
    _image = [decoder decodeObjectForKey:@"image"];
    _postURL = [decoder decodeObjectForKey:@"url"];
    _type = (socialType)[decoder decodeIntegerForKey:@"type"];
    return self;
}


@end
