//
//  Show.h
//  ArtDynamix
//
//  Created by MAC on 26/06/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Media;

@interface Program : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *mediarURL;
@property (nonatomic, strong) NSString *noteURL;
@property (nonatomic, assign) BOOL isPlaying;
@end

@interface Show : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *thumbURL;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *strDescription;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *hashTag;
@property (nonatomic, strong) NSString *accessibility;
@property (nonatomic, strong) NSMutableArray *arrEvents;
@property (nonatomic, strong) NSMutableArray *arrCast;
@property (nonatomic, strong) NSMutableArray *arrProgram;
@property (nonatomic, strong) NSString *ticketURL;
@property (nonatomic, strong) NSString *runningTime;
@property (nonatomic, strong) NSString *moreInfo;
@property (nonatomic, strong) Media *aMedia;
@property (nonatomic, assign) BOOL isFreeEvent;
@property (nonatomic, assign) float height;

+ (NSMutableArray *)parseServerResponse:(NSArray *)records;
+ (Show *)ParserShowDetailsServerResponse:(NSDictionary *)object;
@end
