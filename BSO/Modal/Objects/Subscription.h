//
//  Subscription.h
//  BSO
//
//  Created by MAC on 17/06/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Performance : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *listID;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *weekDay;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, assign) BOOL isSelected;
@end

@interface Subscription : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *strDescription;
@property (nonatomic, strong) NSString *strPeformanceDescription;
@property (nonatomic, strong) NSString *minCost;
@property (nonatomic, strong) NSString *maxCost;
@property (nonatomic, strong) NSString *priceRange;
@property (nonatomic, strong) NSString *subType;
@property (nonatomic, assign) int subCode;
@property (nonatomic, assign) int minPerformance;
@property (nonatomic, assign) BOOL isOnSale;
@property (nonatomic, assign) int maxPerformance;
@property (nonatomic, strong) NSMutableArray *arrPerformance; // Peformance
@property (nonatomic, assign) BOOL isOpened;
+ (NSMutableArray *)parseSubscriptionList:(NSArray *)array;
@end
