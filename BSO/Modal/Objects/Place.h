//
//  Place.h
//  BSO
//
//  Created by MAC on 05/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Place : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *website;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *photoReference;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, assign) BOOL isGoogleRecord;

+ (NSMutableArray *)parsePlacesListContent:(NSArray *)results;
+ (NSMutableDictionary *)parseNearByLocationWSResponse:(NSDictionary *)dictionary;
+ (Place *)parsePlaceDetailContent:(NSDictionary *)results;
@end
