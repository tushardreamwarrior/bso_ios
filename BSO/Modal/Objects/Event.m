//
//  Event.m
//  ArtDynamix
//
//  Created by MAC on 21/05/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import "Event.h"

@implementation Event
+ (NSMutableArray *)parseServerResponse:(NSArray *)records {
    NSMutableArray *arrEvents = [[NSMutableArray alloc] init];
    for (NSDictionary *object in records) {
        Event *aEvent = [[Event alloc] init];
//        NSLog(@"%@ = %@",[object valueForKey:@"show_title"],[object valueForKey:@"performance_id"]);
        aEvent.ID = [object valueForKey:@"event_id"];
        aEvent.pefrormanceID = [object valueForKey:@"performance_id"];
        aEvent.date = [Utility converDateFormat:[object valueForKey:@"event_date"] from:DATE_FORMAT to:CALERNDAR_FORMAT];
        aEvent.time = [object valueForKey:@"event_time"];
        aEvent.dateTime = [object valueForKey:@"event_full_date"];
        aEvent.startDate = [object valueForKey:@"show_start_date"];
        aEvent.endDate = [object valueForKey:@"show_end_date"];
        aEvent.showID = [object valueForKey:@"show_id"];
        aEvent.title = [[object valueForKey:@"show_title"] unescapeUnicodeString];
        aEvent.title = [aEvent.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        aEvent.imageURL = [object valueForKey:@"image"];
        if( [[object valueForKey:@"is_on_sale"] isKindOfClass:[NSNull class]]) {
            aEvent.isOnSale = YES;
        } else {
            aEvent.isOnSale = [[object valueForKey:@"is_on_sale"] intValue];
        }
        
        if( [[object valueForKey:@"price_description"] isKindOfClass:[NSNull class]]) {
            aEvent.priceDescription = @"";
        } else {
            aEvent.priceDescription = [object valueForKey:@"price_description"];
        }
        
//        if(![[object valueForKey:@"cost_minimum"] isEqualToString:@"$0.00"]) {
//            if([[object valueForKey:@"cost_minimum"] isEqualToString:[object valueForKey:@"cost_minimum"]]) {
//                aEvent.priceDescription = [aEvent.priceDescription stringByAppendingFormat:@"\n %@",[object valueForKey:@"cost_minimum"]];
//            } else {
//                aEvent.priceDescription = [aEvent.priceDescription stringByAppendingFormat:@"\n %@-%@",[object valueForKey:@"cost_minimum"],[object valueForKey:@"cost_maximum"]];
//            }
//            
//        }
//        
        if( [[object valueForKey:@"ticket_link"] isKindOfClass:[NSNull class]]) {
            aEvent.ticketURL = @"";
        } else {
            aEvent.ticketURL = [object valueForKey:@"ticket_link"];
        }
        aEvent.venue = [object valueForKey:@"show_venue"];
        
        [arrEvents addObject:aEvent];
    }
    return arrEvents;
}

@end
