//
//  Seat.m
//  BSO
//
//  Created by MAC on 13/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "Seat.h"

@implementation SeatSection

- (instancetype)init {
    self = [super init];
    self.arrSeats = [[NSMutableArray alloc] init];
    return self;
}

@end

@implementation AvailalbeSeatResponse
+ (AvailalbeSeatResponse *)parseAvailableSeat:(NSDictionary *)dict {
    
    NSMutableArray *object = [[NSMutableArray alloc] initWithArray:[dict valueForKey:@"AvailableZones"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"LowAvailability = 1"];
    NSArray *lowAvailable = [object filteredArrayUsingPredicate:predicate];
    [object removeObjectsInArray:lowAvailable];
    
    NSArray *uniqueprices = [[NSSet setWithArray:[object valueForKey:@"price"]] allObjects];
    
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: NO];
    uniqueprices = [uniqueprices sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    
    AvailalbeSeatResponse *responseObject = [[AvailalbeSeatResponse alloc] init];
    responseObject.arrSections = [[NSMutableArray alloc] init];
    
    responseObject.dictOtherInfo = [[NSMutableDictionary alloc] initWithDictionary:dict];
    [responseObject.dictOtherInfo  removeObjectForKey:@"AvailableZones"];
    
    for(NSString *price in uniqueprices) {
        SeatSection *aSection = [[SeatSection alloc] init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"price = %@",price];
        NSArray *seats = [object filteredArrayUsingPredicate:predicate];
        
        for(NSDictionary *dict in seats) {
            Seat *aSeat = [[Seat alloc] init];
            aSeat.seatsAvailable = [NSString stringWithFormat:@"%@",[dict valueForKey:@"SeatsAvailable"]];
            aSeat.seatDescription = [NSString stringWithFormat:@"%@",[dict valueForKey:@"description"]];
            aSection.sectionPrice = [NSString stringWithFormat:@"%@",[dict valueForKey:@"price"]];
            aSeat.seatID = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
            aSeat.seatPriceID = [NSString stringWithFormat:@"%@",[[[dict valueForKey:@"PriceTypes"]  objectAtIndex:0] valueForKey:@"Id"]];
            [aSection.arrSeats addObject:aSeat];
        }
        [responseObject.arrSections addObject:aSection];
    }
    
    
    return responseObject;
}

@end

@implementation Seat


@end
