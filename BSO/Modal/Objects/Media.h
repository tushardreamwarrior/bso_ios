//
//  Media.h
//  ArtDynamix
//
//  Created by MAC on 29/06/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaItem : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *URL;
@property (nonatomic, strong) UIImage *previewImage;
@property (nonatomic, strong) NSString *thumbURL;
@property (nonatomic, strong) NSString *credit;
@property (nonatomic, assign) BOOL isVideo;
+ (NSMutableArray *)readObjectsFromDisk;
+ (void)writeObjectsToDist:(NSMutableArray *)items;
@end

@interface Gallery : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSMutableArray *arrItmes; //MediaItem

@end

@interface Media : NSObject

@property (nonatomic, strong) Gallery *photoGallery;
@property (nonatomic, strong) Gallery *videoGallery;
@property (nonatomic, strong) Gallery *audioGallery;
@property (nonatomic, strong) Gallery *myMusicGallery;
@property (nonatomic, strong) Gallery *podcastGallery;
@property (nonatomic, strong) Gallery *vineGallery;
+ (id)ParseMediaResponse:(NSDictionary *)dict;
+ (id)ParseMediaListResponse:(NSDictionary *)dict;

@end
