//
//  Social.h
//  BSO
//
//  Created by MAC on 25/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    jFacebook = 0,
    jTwitter ,
    jGoogle,
    jYouTube,
    jInstagram
}socialType;


@interface Social : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *postURL;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, assign) socialType type;

+ (NSArray *)parseFacebookPostResponse:(NSDictionary *)dict;
+ (NSArray *)parseTwitterPostResponse:(NSArray *)posts;
+ (NSArray *)parseYouTubePostResponse:(NSArray *)posts;
+ (NSArray *)parseGooglePlusPostResponse:(NSArray *)posts;
+ (NSArray *)parseInstagramPostResponse:(NSArray *)posts;
@end
