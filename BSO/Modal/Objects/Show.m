//
//  Show.m
//  ArtDynamix
//
//  Created by MAC on 26/06/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import "Show.h"

@implementation Program


@end

@implementation Show

+ (NSMutableArray *)parseServerResponse:(NSArray *)records {
    NSMutableArray *arrShows = [[NSMutableArray alloc] init];
    for (NSDictionary *object in records) {
        Show *aShow = [[Show alloc] init];
        aShow.ID = [object valueForKey:@"show_id"];
        aShow.startDate = [Utility converDateFormat:[object valueForKey:@"show_start_date"] from:DATE_FORMAT to:RANGE_FORMAT];
        aShow.endDate =  [Utility converDateFormat:[object valueForKey:@"show_end_date"] from:DATE_FORMAT to:RANGE_FORMAT];
        aShow.title = [[object valueForKey:@"show_title"] unescapeUnicodeString];
        aShow.title = [aShow.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        aShow.title = allTrim( aShow.title);
        aShow.imageURL = [object valueForKey:@"image"];
        aShow.thumbURL = [object valueForKey:@"thumb"];
        aShow.strDescription = [[Utility decodeFromBase64:[object valueForKey:@"description"]] unescapeUnicodeString];
        if( [[object valueForKey:@"single_ticket"] isKindOfClass:[NSNull class]]) {
            aShow.ticketURL = @"";
        } else {
            aShow.ticketURL = [object valueForKey:@"single_ticket"];
        }
        NSString *htmlString = [NSString stringWithFormat:@"<font size='5' face='OpenSans' color='white'>%@</font>",aShow.strDescription];
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]
                                                                          options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                               documentAttributes:nil error:nil];
        
        
        CGRect paragraphRect = [attrString boundingRectWithSize:CGSizeMake(720, 999) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
        aShow.height = MIN(paragraphRect.size.height,100);
        
        aShow.price = @"0";
        [arrShows addObject:aShow];
        
    }
    return arrShows;
}

+ (Show *)ParserShowDetailsServerResponse:(NSDictionary *)object {
    Show *aShow = [[Show alloc] init];
    aShow.ID = [object valueForKey:@"show_id"];
    aShow.startDate = [object valueForKey:@"show_start_date"];
    aShow.endDate = [object valueForKey:@"show_end_date"];
    aShow.title = [[object valueForKey:@"show_title"] unescapeUnicodeString];
    aShow.title = [aShow.title stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
     aShow.title = allTrim( aShow.title);
    NSString *image_HighRes = [object valueForKey:@"image_HighRes"];
    if(image_HighRes.length) {
        aShow.imageURL = [object valueForKey:@"image_HighRes"];
    } else {
        aShow.imageURL = [object valueForKey:@"image"];
    }
    
    aShow.hashTag = [object valueForKey:@"hashTag"];
    aShow.venue = [object valueForKey:@"show_venue"];
    aShow.ticketURL = [object valueForKey:@"single_ticket"];
    aShow.accessibility = [object valueForKey:@"accessibility"];
    aShow.strDescription = [[Utility decodeFromBase64:[object valueForKey:@"description"]] unescapeUnicodeString];
    aShow.arrEvents = [[NSMutableArray alloc] init];
    
    NSArray *arr = [object valueForKey:@"show_events"];
    for (NSDictionary *object in arr) {
        Event *aEvent = [[Event alloc] init];
        aEvent.ID = [object valueForKey:@"event_id"];
        if( [[object valueForKey:@"is_on_sale"] isKindOfClass:[NSNull class]]) {
            aEvent.isOnSale = YES;
        } else {
            aEvent.isOnSale = [[object valueForKey:@"is_on_sale"] intValue];
        }
        aEvent.priceDescription = [object valueForKey:@"price_description"];
        aEvent.pefrormanceID = [object valueForKey:@"performance_id"];
        aEvent.date = [Utility converDateFormat:[object valueForKey:@"event_date"] from:DATE_FORMAT to:CALERNDAR_FORMAT];
        aEvent.time = [object valueForKey:@"event_time"];
        aEvent.dateTime = [object valueForKey:@"event_full_date"];
        if( [[object valueForKey:@"ticket_link"] isKindOfClass:[NSNull class]]) {
            aEvent.ticketURL = @"";
        } else {
            aEvent.ticketURL = [object valueForKey:@"ticket_link"];
        }
        
        [aShow.arrEvents  addObject:aEvent];
    }
    
    NSArray  *arrCasts = [object valueForKey:@"show_casts"];
    aShow.arrCast = [[NSMutableArray alloc] init];
    
    for (NSDictionary *cast in arrCasts) {
        Cast *aCast = [[Cast alloc] init];
        aCast.ID = [cast valueForKey:@"cast_id"];
        aCast.imageURL = [cast valueForKey:@"cast_image"];
        aCast.name = [cast valueForKey:@"cast_name"];
        aCast.role = [cast valueForKey:@"cast_role"];
        aCast.strDescription = [Utility decodeFromBase64:[cast valueForKey:@"description"]] ;
        [aShow.arrCast addObject:aCast];
    }
    
    NSArray  *arrProgram = [object valueForKey:@"program_notes"];
    aShow.arrProgram = [[NSMutableArray alloc] init];
    
    for (NSDictionary *program in arrProgram) {
        if([program  isKindOfClass:[NSDictionary class]]) {
            Program *aProgram = [[Program alloc] init];
            aProgram.title = [program valueForKey:@"program"];
            aProgram.mediarURL = [program valueForKey:@"media_URL"];
            aProgram.noteURL = [program valueForKey:@"program_Brief"];
            [aShow.arrProgram addObject:aProgram];
        }
    }
    

    NSDictionary *mediaDict = [object valueForKey:@"show_media"];
    aShow.aMedia = [Media ParseMediaResponse:mediaDict];
    
    NSDictionary *dictImages = [object valueForKey:@"gallary_images"];
    aShow.aMedia.photoGallery.imageURL = [[dictImages valueForKey:@"mobile_photo_file"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    aShow.aMedia.audioGallery.imageURL = [[dictImages valueForKey:@"mobile_audio_file"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return aShow;
}

@end