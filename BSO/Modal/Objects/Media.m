//
//  Media.m
//  ArtDynamix
//
//  Created by MAC on 29/06/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import "Media.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation MediaItem
+ (NSMutableArray *)readObjectsFromDisk {
    NSMutableArray *arrItmes = [[NSMutableArray alloc] init];
    NSArray *items = [[NSArray alloc] initWithContentsOfFile:FILE_PATH(@"myMusic.plist")];
    if(items) {
        for(NSDictionary *item in items) {
            MediaItem *aItem = [[MediaItem alloc] init];
            aItem.ID = [item valueForKey:@"ID"];
            aItem.title = [item valueForKey:@"title"];
            aItem.URL = [item valueForKey:@"URL"];
            aItem.thumbURL = [item valueForKey:@"thumbURL"];
            aItem.isVideo = [[item valueForKey:@"isVideo"] intValue];
            [arrItmes addObject:aItem];
        }
    }
    return arrItmes;
}

+ (void)writeObjectsToDist:(NSMutableArray *)items {
    NSMutableArray *arrItmes = [[NSMutableArray alloc] init];
    for(MediaItem *aItem in items) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        [item setValue:aItem.ID forKey:@"ID"];
        [item setValue:aItem.title forKey:@"title"];
        [item setValue:aItem.URL forKey:@"URL"];
        [item setValue:aItem.thumbURL forKey:@"thumbURL"];
        [item setValue:[NSString stringWithFormat:@"%d",aItem.isVideo] forKey:@"isVideo"];
        
        [arrItmes addObject:item];
    }
    [arrItmes writeToFile:FILE_PATH(@"myMusic.plist") atomically:YES];
}

@end

@implementation Gallery
- (instancetype)init {
    self.arrItmes = [[NSMutableArray alloc] init];
    return [super init];
}

@end

@implementation Media
- (instancetype)init {
    self.photoGallery = [[Gallery alloc] init];
    self.videoGallery = [[Gallery alloc] init];
    self.audioGallery = [[Gallery alloc] init];
    self.myMusicGallery  = [[Gallery alloc] init];
    self.podcastGallery = [[Gallery alloc] init];
    self.vineGallery = [[Gallery alloc] init];
    return [super init];
}

+ (id)ParseMediaResponse:(NSDictionary *)dict {
    Media *aMedia = [[Media alloc] init];
    
    NSArray *video_galleries = [dict valueForKey:@"video_galleries"];
    if(video_galleries.count) {
        
        for (NSDictionary *video in video_galleries) {
            MediaItem *aVideo = [[MediaItem alloc] init];
            aVideo.ID = [video valueForKey:@"id"];
            aVideo.title = [[video valueForKey:@"title"] unescapeUnicodeString];
            aVideo.URL = [video valueForKey:@"audio_file"];
            aVideo.thumbURL = [video valueForKey:@"img_file"];
            
            [aMedia.videoGallery.arrItmes addObject:aVideo];
        }
    }
    
    NSArray *audio_galleries = [dict valueForKey:@"audio_galleries"];
    if(audio_galleries.count) {
        
    for (NSDictionary *audio in audio_galleries) {
            MediaItem *aAudio = [[MediaItem alloc] init];
            aAudio.ID = [audio valueForKey:@"id"];
            aAudio.title = [[audio valueForKey:@"title"] unescapeUnicodeString];
            aAudio.URL = [audio valueForKey:@"audio_file"];
            aAudio.thumbURL = [audio valueForKey:@"img_file"];
        
            [aMedia.audioGallery.arrItmes addObject:aAudio];
        }
    }

    
    NSArray *photo_galleries = [dict valueForKey:@"photo_galleries"];
    if(photo_galleries.count) {
        
        for (NSDictionary *audio in photo_galleries) {
            MediaItem *aAudio = [[MediaItem alloc] init];
            aAudio.ID = [audio valueForKey:@"id"];
            aAudio.title = [[audio valueForKey:@"title"] unescapeUnicodeString];
            aAudio.URL = [audio valueForKey:@"audio_file"];
            aAudio.thumbURL = [audio valueForKey:@"img_file"];
            
            [aMedia.photoGallery.arrItmes addObject:aAudio];
        }
    }
    
    return aMedia;
}

+ (id)ParseMediaListResponse:(NSDictionary *)dict {
    Media *aMedia = [[Media alloc] init];
    NSArray *audio = [dict valueForKey:@"audio"];
    for(NSDictionary *dic in audio) {
        MediaItem *aItem = [[MediaItem alloc] init];
        aItem.ID = [dic valueForKey:@"id"];
        aItem.title = [[dic valueForKey:@"title"] unescapeUnicodeString];
        aItem.URL = [dic valueForKey:@"audio_file"];
        aItem.thumbURL = [dic valueForKey:@"img_file"];
        aItem.isVideo = NO;
        [aMedia.audioGallery.arrItmes addObject:aItem];
    }
    
    NSArray *video = [dict valueForKey:@"video"];
    
    for(NSDictionary *dic in video) {
        MediaItem *aItem = [[MediaItem alloc] init];
        aItem.ID = [dic valueForKey:@"id"];
        aItem.title = [[dic valueForKey:@"title"] unescapeUnicodeString];
        aItem.URL = [dic valueForKey:@"video_source"];
        aItem.thumbURL = [dic valueForKey:@"thumb_file"];
        aItem.isVideo = YES;
        [aMedia.videoGallery.arrItmes addObject:aItem];
    }
    
    NSArray *podcast = [dict valueForKey:@"podcast"];
    for(NSDictionary *dic in podcast) {
        MediaItem *aItem = [[MediaItem alloc] init];
        aItem.ID = [dic valueForKey:@"id"];
        aItem.title = [[dic valueForKey:@"title"] unescapeUnicodeString];
        aItem.URL = [dic valueForKey:@"audio_file"];
        aItem.thumbURL = [dic valueForKey:@"img_file"];
        aItem.credit = [dic valueForKey:@"desc"];
        aItem.isVideo = NO;
        [aMedia.podcastGallery.arrItmes addObject:aItem];
    }

    NSArray *phothos = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:FILE_PATH(@"Photos") error:nil];
    for(NSString *path in phothos) {
        MediaItem *aItem = [[MediaItem alloc] init];
        aItem.URL = [FILE_PATH(@"Photos") stringByAppendingPathComponent:path];
        [aMedia.photoGallery.arrItmes addObject:aItem];
    }
    NSArray *vidoes = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:FILE_PATH(@"Videos") error:nil];
    for(NSString *path in vidoes) {
        MediaItem *aItem = [[MediaItem alloc] init];
        aItem.isVideo = YES;
        aItem.URL = [FILE_PATH(@"Videos") stringByAppendingPathComponent:path];
        MPMoviePlayerController *moviePlayer = [[MPMoviePlayerController alloc]  initWithContentURL:[NSURL fileURLWithPath:aItem.URL]];
        aItem.previewImage = [moviePlayer thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
        [moviePlayer stop];
        moviePlayer = nil;
        [aMedia.vineGallery.arrItmes addObject:aItem];
    }
    
    [aMedia.myMusicGallery.arrItmes addObjectsFromArray:[MediaItem readObjectsFromDisk]];
    
    return aMedia;
}

@end
