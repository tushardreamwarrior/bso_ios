//
//  SCart.m
//  BSO
//
//  Created by MAC on 27/05/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "SCart.h"

@implementation CreditCard
+(NSMutableArray *)parserCardJsonResponse:(NSArray *)array {
    NSMutableArray *arrCards = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in array) {
        CreditCard *aCard = [[CreditCard alloc] init];
        aCard.cardType = [dict valueForKey:@"card_id"];
        aCard.cardName = [dict valueForKey:@"name"];
        [arrCards addObject:aCard];
        
    }
    return arrCards;
}
@end

@implementation Address

+ (NSMutableArray *)parseShippingAddress:(NSArray *)array {
    NSMutableArray *arrAddress = [[NSMutableArray alloc] init];
    for(NSDictionary *dict in array) {
        Address *aAddress = [[Address alloc] init];
        aAddress.ID = [dict valueForKey:@"ExternalAddressId"];
        aAddress.streetPrimary = [dict valueForKey:@"StreetPrimary"];
        aAddress.streetSeconday = [dict valueForKey:@"StreetSecondary"];
        aAddress.city = [dict valueForKey:@"City"];
        aAddress.state = [dict valueForKey:@"Region"];
        aAddress.postalCode = [dict valueForKey:@"PostalCode"];
        aAddress.contry = [dict valueForKey:@"Country"];
        aAddress.phone = [dict valueForKey:@"Phone"];
        if(![aAddress.streetSeconday isKindOfClass:[NSNull class]]) {
            aAddress.fullAddress = [NSString stringWithFormat:@"%@\n%@\n%@, %@ %@\n%@",aAddress.streetPrimary,aAddress.streetSeconday,aAddress.city,aAddress.state,aAddress.postalCode,aAddress.contry];
            aAddress.fullAddressWithPhone = [NSString stringWithFormat:@"%@\n%@\n%@, %@ %@\n%@\n\n☏ %@",aAddress.streetPrimary,aAddress.streetSeconday,aAddress.city,aAddress.state,aAddress.postalCode,aAddress.contry,aAddress.phone];
        } else {
            aAddress.fullAddress = [NSString stringWithFormat:@"%@\n%@, %@ %@\n%@",aAddress.streetPrimary,aAddress.city,aAddress.state,aAddress.postalCode,aAddress.contry];
            aAddress.fullAddressWithPhone = [NSString stringWithFormat:@"%@\n%@, %@ %@\n%@\n\n☏ %@",aAddress.streetPrimary,aAddress.city,aAddress.state,aAddress.postalCode,aAddress.contry,aAddress.phone];

        }
        aAddress.isSelected = [[dict valueForKey:@"Primary"] intValue];
        [arrAddress addObject:aAddress];
    }
    return arrAddress;
}

@end

@implementation SeatItem

+ (NSMutableArray *)parseSeatItems:(NSArray *)array with:(CartItem *)cartItem {
     cartItem.seatDesc = @"";
    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in array) {
        SeatItem *aItem = [[SeatItem alloc] init];
        aItem.cost = [dict valueForKey:@"Cost"];
        aItem.section = [dict valueForKey:@"Section"];
        aItem.row = [dict valueForKey:@"Row"];
        aItem.number = [dict valueForKey:@"Number"];
        cartItem.seatDesc = [cartItem.seatDesc  stringByAppendingString:[NSString stringWithFormat:@"Section:%@; Row %@; Seat:%@; Price: %@\n",aItem.section,aItem.row,aItem.number,aItem.cost]];
        [arrItems addObject:aItem];
    }
    return arrItems;
}

+ (NSMutableArray *)parseSubscriptionSeatItems:(NSArray *)array with:(CartItem *)cartItem {
    cartItem.seatDesc = @"";
    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
    for(NSDictionary *item in array) {
        cartItem.orginalPrice = [item valueForKey:@"TotalCost"];
        
        cartItem.seatDesc = [cartItem.seatDesc stringByAppendingFormat:@"Performance: %@\n",[item valueForKey:@"Title"]];
        NSArray *seats = [item valueForKey:@"Seats"];
        for (NSDictionary *dict in seats) {
            SeatItem *aItem = [[SeatItem alloc] init];
            aItem.cost = [dict valueForKey:@"Cost"];
           
            cartItem.seatDesc = [cartItem.seatDesc  stringByAppendingFormat:@"  ● Zone:%@\n",[dict valueForKey:@"Zone"]];
            [arrItems addObject:aItem];
        }
    }
    
    
    return arrItems;
}

@end


@implementation CartItem
- (instancetype)init {
    self = [super init];
    _arrReats = [[NSMutableArray alloc] init];
    return self;
}

+ (NSMutableArray *)parseCartItems:(NSArray *)array {
    NSMutableArray *arrItems = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in array) {
         CartItem *aItem = [[CartItem alloc] init];
        if([[dict valueForKey:@"IsTicket"] intValue]) {
            aItem.type = jTicket;
        }
        if([[dict valueForKey:@"IsContribution"] intValue]) {
            aItem.type = jCondtribution;
        }
        if([[dict valueForKey:@"IsSubscription"] intValue]) {
            aItem.type = jSubscription;
        }
        
         aItem.imageURL = @"";
         aItem.title = [[dict valueForKey:@"Title"] unescapeUnicodeString];
        if([aItem.title isKindOfClass:[NSNull class]] || aItem.title == nil) {
            aItem.title = [dict valueForKey:@"Title"];
        }
        
        aItem.quantity = [dict valueForKey:@"Quantity"];
        aItem.totalCost = [dict valueForKey:@"TotalCost"];
        aItem.orginalPrice = [dict valueForKey:@"OriginalPrice"];
        aItem.performanceDate = [dict valueForKey:@"PerformanceDate"];
        NSString *performanceDate = [dict valueForKey:@"PerformanceDate"];
        NSRange range = [performanceDate rangeOfString:@" -"];
        if(range.length) {
            performanceDate = [performanceDate substringToIndex:range.location];
            aItem.date = [Utility converDateFormat:performanceDate from:@"EEE, dd MMM yyyy HH:mm:ss" to:@"M/dd/yyyy hh:mm:ss a"];
        } else {
            aItem.date = @"";
        }
        
        if( aItem.type == jSubscription) {
            if(![[dict valueForKey:@"SubCartItems"] isKindOfClass:[NSNull class]]) {
                NSArray *subCartItems = [dict valueForKey:@"SubCartItems"];
                [aItem.arrReats addObjectsFromArray:[SeatItem parseSubscriptionSeatItems:subCartItems with:aItem]];
                aItem.seatDesc = allTrim(aItem.seatDesc);

            }
        } else {
            if(![[dict valueForKey:@"Seats"] isKindOfClass:[NSNull class]]) {
                [aItem.arrReats addObjectsFromArray:[SeatItem parseSeatItems:[dict valueForKey:@"Seats"] with:aItem]];
                aItem.seatDesc = allTrim(aItem.seatDesc);
            }
        }
        
        if(![[dict valueForKey:@"LineNum"] isKindOfClass:[NSNull class]]) {
            aItem.lineNumber = [dict valueForKey:@"LineNum"];
        }
        
        if([[dict valueForKey:@"TicketInformation"] isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dictTicket = [dict valueForKey:@"TicketInformation"];
            aItem.subLineItemNumber = [dictTicket valueForKey:@"SubLineItemNumber"];
            aItem.orderNumber = [dictTicket valueForKey:@"OrderNumber"];
        }
        [arrItems addObject:aItem];
    }
    
    return arrItems;
}

@end

@implementation CartSection

- (instancetype)init {
    self = [super init];
    _arrCartItems = [[NSMutableArray alloc] init];
    return self;
}

+ (NSMutableArray *)parseCartSections:(NSArray *)array {
    NSMutableArray *arrSections = [[NSMutableArray alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"IsTicket = 1"];
    NSArray *tickets = [array filteredArrayUsingPredicate:predicate];
    if(tickets.count) {
        CartSection *aSection = [[CartSection alloc] init];
        aSection.type = jTicket;
        [aSection.arrCartItems addObjectsFromArray:[CartItem parseCartItems:tickets]];
        NSDictionary *itemDict = tickets[0];
        aSection.headerString = [itemDict valueForKey:@"HeaderDisplayValue"];
        [arrSections addObject:aSection];
    }
    predicate = [NSPredicate predicateWithFormat:@"IsContribution = 1"];
    NSArray *condtributions = [array filteredArrayUsingPredicate:predicate];
    if(condtributions.count) {
        CartSection *aSection = [[CartSection alloc] init];
        aSection.type = jCondtribution;
        [aSection.arrCartItems addObjectsFromArray:[CartItem parseCartItems:condtributions]];
        NSDictionary *itemDict = condtributions[0];
        aSection.headerString = [itemDict valueForKey:@"HeaderDisplayValue"];
        [arrSections addObject:aSection];
    }
    
    predicate = [NSPredicate predicateWithFormat:@"IsSubscription = 1"];
    NSArray *subscriptions = [array filteredArrayUsingPredicate:predicate];
    if(subscriptions.count) {
        CartSection *aSection = [[CartSection alloc] init];
        aSection.type = jSubscription;
        [aSection.arrCartItems addObjectsFromArray:[CartItem parseCartItems:subscriptions]];
        NSDictionary *itemDict = subscriptions[0];
        aSection.headerString = [itemDict valueForKey:@"HeaderDisplayValue"];
        [arrSections addObject:aSection];
    }
    
    return arrSections;
}
@end


@implementation SCart
- (instancetype)init {
    self = [super init];
    _arrCartSection = [[NSMutableArray alloc] init];
    _arrShippingAddress = [[NSMutableArray alloc] init];
    _arrDiscoutCodes = [[NSMutableArray alloc] init];
    _aCard = [[CreditCard alloc] init];
    return self;
}

+ (SCart *)parseShoppingCartResponse:(NSDictionary *)dict {
    SCart *aCart = [[SCart alloc] init];
    aCart.subTotal = [dict valueForKey:@"CartSubTotal"];
    aCart.tessituraFees = [dict valueForKey:@"TessituraFees"];
    aCart.totalCost = [dict valueForKey:@"TotalCost"];
    [aCart.arrCartSection addObjectsFromArray:[CartSection parseCartSections:[dict valueForKey:@"CartItems"]]];
    if(![[dict valueForKey:@"AppliedDiscountCodes"] isKindOfClass:[NSNull class]]) {
        [aCart.arrDiscoutCodes addObjectsFromArray:[dict valueForKey:@"AppliedDiscountCodes"]];
    }
    return aCart;
}

+ (SCart *)parseOrderHistoryResponse:(NSDictionary *)dict {
    SCart *aCart = [[SCart alloc] init];
    for(NSDictionary *order in [dict valueForKey:@"Orders"]) {
        CartSection *aSection = [[CartSection alloc] init];
        aSection.type = jTicket;
        if(![[order valueForKey:@"ShowETickets"] isKindOfClass:[NSNull class]]) {
            aSection.showETickets = [[order valueForKey:@"ShowETickets"] intValue];
        }
        [aSection.arrCartItems addObjectsFromArray:[CartItem parseCartItems:[order valueForKey:@"CartItems"]]];
        aSection.headerString = @"";
        [aCart.arrCartSection addObject:aSection];

    }
    
    return aCart;
}

+ (SCart *)parseShoppingCartShippingResponse:(NSDictionary *)dict {
    SCart *aCart = [BSOManager SharedInstance].aSCart;
    [aCart.arrShippingAddress addObjectsFromArray:[Address parseShippingAddress:[dict valueForKey:@"Shipping"]]];
    return aCart;
}

@end