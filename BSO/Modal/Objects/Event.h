//
//  Event.h
//  ArtDynamix
//
//  Created by MAC on 21/05/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *pefrormanceID;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *dateTime;
@property (nonatomic, strong) NSString *showID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSString *ticketURL;
@property (nonatomic, strong) NSString *strDescription;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *minCost;
@property (nonatomic, strong) NSString *maxCost;
@property (nonatomic, assign) BOOL isOnSale;
@property (nonatomic, strong) NSString *priceDescription;
+ (NSMutableArray *)parseServerResponse:(NSArray *)records;
@end
