//
//  Cast.h
//  ArtDynamix
//
//  Created by MAC on 29/06/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cast : NSObject
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *strDescription;
@end
