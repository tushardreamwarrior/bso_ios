//
//  GimbalManager.h
//  BSO
//
//  Created by MAC on 28/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GMBLCommunication;

@interface GimbalManager : NSObject
+ (GimbalManager *) SharedInstance;
- (void)initializeCommunication;
- (void)didRecieveCommunicationData:(GMBLCommunication *)communication;
- (void)startBeaconMonitoring;
- (void)stopBeaconMonitoring;
@end
