//
//  Utility.h
//  BSO
//
//  Created by Imobtree solutions on 08/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#include <netinet/in.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface Utility : NSObject

+ (BOOL)validateEmail:(NSString *)candidate;
+ (BOOL)connectedToNetwork;
+ (NSString *)decodeFromBase64:(NSString *)encoded;
+ (void)ShowAlertWithTitle:(NSString *)title Message:(NSString *)message;
+ (void)setBorderColorWithView:(UIView *)aView;
+ (void)setBorderColorWithView:(UIView *)aView color:(UIColor *)color;
+ (void)setLeftViewWithTextField:(UITextField *)textField andImage:(UIImage *)image;
+ (void)setLeftViewWithTextField:(UITextField*)textField;
+ (void)setProfileImageCornerWithImageView:(UIImageView *)imageView ;
+ (void)setCornerToView:(UIView *)aView corner:(float)corner;

+ (void)showLoader;
+ (void)showLoaderWith:(MBProgressHUDMode)mode message:(NSString *)text;
+ (void)hideLoader;

+ (NSDate *)dateFromString:(NSString *)strDate format:(NSString *)fommat;
+ (NSString *)stringFromDate:(NSDate *)date format:(NSString *)format;
+ (NSString *)converDateFormat:(NSString *)strDate from:(NSString *)from to:(NSString *)to;

+ (float)getTextHeightOfText:(NSString *)string font:(UIFont *)aFont width:(float)width;
+ (float)getTextWidthOfText:(NSString *)string font:(UIFont *)aFont height:(float)height;
+ (float)getExactTextHeightOfText:(NSString *)string font:(UIFont *)aFont width:(float)width;

+ (void)openURLInInterBrowser:(NSString *)strURL from:(UIViewController *)controller;
+ (void)openContentInInterBrowser:(NSString *)htmlString from:(UIViewController *)controller;
+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+ (float)imageHeight: (UIImage*) sourceImage scaledToWidth: (float) i_width;

+ (void)shareOnTwiitter:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image;
+ (void)shareOnFacebook:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image;
+ (void)shareOnInstagram:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image frame:(CGRect )frame;
+ (void)shareOnGoolgePlus:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image;
+ (void)signInGooglePlus:(UIViewController *)controller;
+ (NSString *) encryptString:(NSString*)plaintext withKey:(NSString*)key;
+ (NSString *) decryptString:(NSString *)ciphertext withKey:(NSString*)key;
@end
