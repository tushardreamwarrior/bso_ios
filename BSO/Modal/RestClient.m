//
//  RestClient.m
//  BSO
//
//  Created by MAC on 17/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import "RestClient.h"
#import "AFHTTPSessionManager.h"

static NSString* const showsList = @"shows";
static NSString* const eventList = @"events";
static NSString* const showsDetails = @"show";
static NSString* const mediaList = @"medias";
static NSString* const fbPost = @"social/facebook.php";
static NSString* const twPost = @"social/twitter.php";
static NSString* const googlePost = @"social/google.php";
static NSString* const tubePost = @"social/youtube.php";
static NSString* const instagramPost = @"social/instagram.php";
static NSString* const hashTagsPost = @"social/instagram.php";
static NSString* const aboutUS = @"page";
static NSString* const BSOLogin = @"BSOLogin";
static NSString* const getContetForBeacon = @"location/ws_location_push_content.php";
static NSString* const getBestAvailableSeats = @"SeatBestAvailable";
static NSString* const checkAvailableSeats = @"CheckSeat";
static NSString* const AddToCart = @"AddToCart";
static NSString* const checkSession = @"CheckUserSession";
static NSString* const shoppingCartItems = @"CheckoutIndex";
static NSString* const checkOutShipping = @"CheckoutShipping";
static NSString* const checkOutPayment = @"CheckoutPayment";
static NSString* const checkoutFinalize = @"CheckoutFinalize";
static NSString* const getDevicePreference = @"AppSetting";
static NSString* const makeImpulseDonation = @"ImpulseDonation";
static NSString* const getNearByLocations = @"DiningParkingLodginData";
static NSString* const getSubscriptionListing = @"SubscriptionListing";
static NSString* const applyDiscount = @"ApplyDiscountTablet";
static NSString* const orderHistory = @"OrderHistory";
static NSString* const subscriptionBestAvailableCYO = @"SubscriptionBestAvailableCYO";
static NSString* const subscriptionCYOAddToCart = @"SubscriptionCYOAddToCart";
static NSString* const subscriptionBestAvailableFixed = @"SubscriptionBestAvailableFixed";
static NSString* const subscriptionFixedAddToCart = @"SubscriptionFixedAddToCart";
static NSString* const registeredDeviceToken = @"RegisteredDeviceToken";
static NSString* const notificationsList = @"NotificationList";
static NSString* const notificationsDetail = @"Notification";
static NSString* const userLogout = @"BsoLogout";
static NSString* const getTicketBarCode = @"GetTicketBarCode";
static NSString* const removeItemFromCart = @"RemoveItemCart";
static NSString* const validateCCInfo = @"validateCCInfo";
static NSString* const getPaymentCards = @"PaymentCards";
static NSString* const getCCInfo = @"CCInfo";

@implementation RestClient
+ (RestClient *)sharedInstance {
    static RestClient *_sharedServerManagerHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedServerManagerHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:SERVER_ADDRESS]];
    });
    
    return _sharedServerManagerHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url{
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

- (void)getShowList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,showsList];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getAllEventsList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,eventList];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)bsoLogin:(NSString *)email password:(NSString *)password success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,BSOLogin];
        [dictionary setValue:email forKey:@"email"];
        [dictionary setValue:password forKey:@"password"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getShowDetails:(NSString * )show_id success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,showsDetails];
        [dictionary setValue:show_id forKey:@"id"];
       // BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:@"isUserLogin"];
        BOOL isLogin;
        if(GET_UD(SESSION_ID)) {
            isLogin = TRUE;
        } else {
            isLogin = FALSE;
        }
        [dictionary setValue:[NSString stringWithFormat:@"%d",isLogin] forKey:@"is_login"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getAllMediaList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,mediaList];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
       // [self internetNotAvailable];
    }
}

- (void)getFBPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,fbPost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getTwitterPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,twPost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [dictionary setValue:@"timeline" forKey:@"feed_type"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getGooglePlus:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,googlePost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getYouTubePostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,tubePost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getInstagramPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,instagramPost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getHashTagPostList:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,twPost];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [dictionary setValue:@"hash_tag" forKey:@"feed_type"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}


- (void)getAboutUSContent:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        int pageID;
        if(type == jBSO) {
            pageID = 165;
        } else if(type == jBPop) {
            pageID = 166;
        } else {
            pageID = 167;
        }
        NSString *urlString =[NSString stringWithFormat:@"%@%@/%d",SERVER_ADDRESS,aboutUS,pageID];
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        // [self internetNotAvailable];
    }
}

- (void)getBeaconContent:(NSString *)beaconID success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getContetForBeacon];
        [dictionary setObject:@"getLocationPushContent" forKey:@"action"];
        [dictionary setObject:beaconID forKey:@"location_equipment_id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)uploadYouTubeVideo:(NSString *)title videoPath:(NSURL *)filePath apiKey:(NSString *)key succes:(successCallback)success failure:(failureCallback)failure  {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *token = [NSString stringWithFormat:@"Bearer %@",GET_UD(YOUTUBE_TOKEN)];
    [manager.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    NSDictionary *parameters = @{@"snippet" : @{@"title" : @"",
                                                @"description" : @"",@"categoryId":@"22",@"tags":@[]},@"status":@{@"privacyStatus":@"public"}};
    [manager POST:@"https://www.googleapis.com/upload/youtube/v3/videos?part=snippet,status" parameters:parameters  constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"video" fileName:@"video.mov" mimeType:@"video/*" error:NULL];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        
       NSDictionary *parameters = @{@"id":[responseObject valueForKey:@"id"],@"snippet" : @{@"title" : title,
                                      @"description" : @"",@"categoryId":@"22"}};



        [manager PUT:[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=id,snippet&key=%@",key] parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            success(nil,responseObject);
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
            failure(nil,error);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(nil,error);
    }];
}

- (void)getRouteBetweenLocations:(NSString *)url success:(successCallback)success failure:(failureCallback)failure  {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        [self POST:url parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
       // [self internetNotAvailable];
    }
}

- (void)getNearByPlaces:(CLLocationCoordinate2D)coordinate radius:(int)radius keyword:(NSString *)keyword success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=%@&location=%f,%f&keyword=%@&rankby=distance",GOOGLE_API_KEY,coordinate.latitude,coordinate.longitude,keyword];

        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getPlaceDetails:(NSString *)placeID success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=%@&placeid=%@",GOOGLE_API_KEY,placeID];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getBestAvailableSeats:(NSString *)perfrormance success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getBestAvailableSeats];
        [dictionary setValue:perfrormance forKey:@"performaceId"];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"user_session_id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)chekAvailableSeats:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,checkAvailableSeats];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"user_session_id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)addToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,AddToCart];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)checkUserSession:(NSString *)email success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,checkSession];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        [dictionary setValue:email forKey:@"email"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)getShoppingCartItems:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,shoppingCartItems];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
       [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)checkOutShipping:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,checkOutShipping];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}



- (void)getDevicePreferences:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getDevicePreference];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)checkOutPayment:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,checkOutPayment];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)checkoutFinalize:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,checkoutFinalize];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)makeImpulseDonation:(OrganizationType )type amount:(NSString *)amount success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,makeImpulseDonation];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        [dictionary setValue:amount forKey:@"ContributionCost"];
        [dictionary setValue:[self getBrandCodeDonationFor:type] forKey:@"FundType"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getNearByLocatios:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getNearByLocations];
        
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getSubscriptionListing:(OrganizationType )type success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getSubscriptionListing];
        
        [dictionary setValue:[NSString stringWithFormat:@"%d",[self getBrandCodeFor:type]] forKey:@"brand_id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)applyDiscoutCode:(NSString *)code success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,applyDiscount];
         [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        [dictionary setValue:code forKey:@"promotionalCode"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getOrderHistory:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,orderHistory];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)subscriptionBestAvailableCYO:(NSString *)subscription_ID list:(NSMutableArray *)arrList success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,subscriptionBestAvailableCYO];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        [dictionary setValue:subscription_ID forKey:@"subscriptionId"];
        [dictionary setValue:arrList forKey:@"list"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)SubscriptionCYOAddToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,subscriptionCYOAddToCart];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
 
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)subscriptionBestAvailableFixed:(NSString *)subscription_ID success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,subscriptionBestAvailableFixed];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        [dictionary setValue:subscription_ID forKey:@"subscriptionId"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)SubscriptionFixedAddToCart:(NSMutableDictionary *)dictionary success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,subscriptionFixedAddToCart];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)registerDeviceToken:(NSString *)deviceToken success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,registeredDeviceToken];
        [dictionary setValue:deviceToken forKey:@"token"];
        [dictionary setValue:@"ios" forKey:@"type"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getListOfNotifications:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,notificationsList];
        NSMutableDictionary* dictionary =[[NSMutableDictionary alloc] init];
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getNotificationDetail:(NSString *)notification_id success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,notificationsDetail];
        [dictionary setValue:notification_id forKey:@"id"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}


- (void)userLogout:(NSString *)email success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,userLogout];
        [dictionary setValue:email forKey:@"email"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getTicketBarCode:(NSString *)orderId number:(NSString *)subLineItemNumber success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getTicketBarCode];
        [dictionary setValue:orderId forKey:@"orderId"];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
        [dictionary setValue:subLineItemNumber forKey:@"subLineItemNumber"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)removeItemFromCart:(NSString *)lineNum success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,removeItemFromCart];
        [dictionary setValue:lineNum forKey:@"lineNum"];
        [dictionary setValue:GET_UD(SESSION_ID) forKey:@"ASP_SESSIONID"];
    
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)validateCardInfo:(CreditCard *)card success:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,validateCCInfo];
        [dictionary setValue:card.cardNumber forKey:@"number"];
        [dictionary setValue:card.cardType forKey:@"type"];
        [dictionary setValue:card.cardExpireMonth forKey:@"month"];
        [dictionary setValue:card.cardExpireYear forKey:@"year"];
        [dictionary setValue:card.cardName forKey:@"name"];
        [dictionary setValue:card.cardCVV forKey:@"cvv"];
        [dictionary setValue:GET_UD(USER_EMAIL) forKey:@"user_email"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getPaymentCards:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getPaymentCards];
       [self GET:urlString parameters:nil success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (void)getCardInfo:(successCallback)success failure:(failureCallback)failure {
    if([Utility connectedToNetwork]) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        NSString *urlString =[NSString stringWithFormat:@"%@%@",SERVER_ADDRESS,getCCInfo];
        [dictionary setValue:GET_UD(USER_EMAIL) forKey:@"user_email"];
        
        [self POST:urlString parameters:dictionary success:success failure:failure];
    } else{
        NSLog(@"No Internet Connection");
        [self internetNotAvailable];
    }
}

- (NSString *)getBrandCodeDonationFor:(OrganizationType )type {
    if(type == jBSO) {
        return @"81";
    } else if(type == jBPop) {
        return @"82";
    } else {
        return @"83";
    }
}

- (int)getBrandCodeFor:(OrganizationType )type {
    if(type == jBSO) {
        return 1182;
    } else if(type == jBPop) {
        return 6425;
    } else {
        return 6427;
    }
}



- (void)internetNotAvailable {
    NSString *message = NSLocalizedString (@"No Internet Connection",
                                           @"Please Connect Your Device To Internet");
    [Utility ShowAlertWithTitle:nil Message:message];
    [Utility hideLoader];
}

@end