//
//  BSOManager.h
//  BSO
//
//  Created by MAC on 17/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>
@class SCart;
@class CreditCard;

typedef void(^successCall)(id responseObject);
typedef void(^failureCall)(NSError *error);

@interface BSOManager : NSObject

+ (BSOManager *) SharedInstance;
@property (nonatomic, assign) BOOL compactNav;
@property (nonatomic, strong) SCart *aSCart;
@property (nonatomic, assign) OrganizationType selectedType;
@property (nonatomic, assign) int wizardStep;
@property (nonatomic, strong) NSMutableArray *arrCreditCards;

- (void)retriveCrediCars;
- (UIColor *)getSelectedItemColor;
- (UIColor *)getSelectedItemTextColor;
- (UIColor *)getSelectedItemBGColor;
- (UIColor *)getSelectedItemColor:(OrganizationType)type;

- (void)getShowList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getAllEventsList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)bsoLogin:(NSString *)email password:(NSString *)password success:(successCall)success failure:(failureCall)failure;
- (void)getShowDetails:(NSString * )show_id success:(successCall)success failure:(failureCall)failure;
- (void)getAllMediaList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getFBPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getTwitterPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getGooglePlus:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getYouTubePostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getInstagramPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getHashTagPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getAboutUSContent:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getBeaconContent:(NSString *)beaconID success:(successCall)success failure:(failureCall)failure;
- (void)getRouteBetweenLocations:(NSString *)url success:(successCall)success failure:(failureCall)failure;
- (void)getNearByPlaces:(CLLocationCoordinate2D)coordinate radius:(int)radius keyword:(NSString *)keyword success:(successCall)success failure:(failureCall)failure;
- (void)getPlaceDetails:(NSString *)placeID success:(successCall)success failure:(failureCall)failure;
- (void)getBestAvailableSeats:(NSString *)perfrormance success:(successCall)success failure:(failureCall)failure;
- (void)chekAvailableSeats:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)addToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)checkUserSession:(NSString *)email success:(successCall)success failure:(failureCall)failure;
- (void)getShoppingCartItems:(successCall)success failure:(failureCall)failure;
- (void)checkOutShipping:(successCall)success failure:(failureCall)failure;
- (void)checkOutPayment:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)checkoutFinalize:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)makeImpulseDonation:(OrganizationType )type amount:(NSString *)amount success:(successCall)success failure:(failureCall)failure;
- (void)getNearByLocatios:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)getSubscriptionListing:(OrganizationType )type success:(successCall)success failure:(failureCall)failure;
- (void)applyDiscoutCode:(NSString *)code success:(successCall)success failure:(failureCall)failure;
- (void)getOrderHistory:(successCall)success failure:(failureCall)failure;
- (void)subscriptionBestAvailableCYO:(NSString *)subscription_ID list:(NSMutableArray *)arrList success:(successCall)success failure:(failureCall)failure;
- (void)SubscriptionCYOAddToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)subscriptionBestAvailableFixed:(NSString *)subscription_ID success:(successCall)success failure:(failureCall)failure;
- (void)SubscriptionFixedAddToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure;
- (void)registerDeviceToken:(NSString *)deviceToken success:(successCall)success failure:(failureCall)failure;
- (void)getListOfNotifications:(successCall)success failure:(failureCall)failure;
- (void)getNotificationDetail:(NSString *)notification_id success:(successCall)success failure:(failureCall)failure;
- (void)userLogout:(NSString *)email success:(successCall)success failure:(failureCall)failure;
- (void)getTicketBarCode:(NSString *)orderId number:(NSString *)subLineItemNumber success:(successCall)success failure:(failureCall)failure;
- (void)removeItemFromCart:(NSString *)lineNum success:(successCall)success failure:(failureCall)failure;
- (void)validateCardInfo:(CreditCard *)card success:(successCall)success failure:(failureCall)failure;
- (void)getPaymentCards:(successCall)success failure:(failureCall)failure;
- (void)getCardInfo:(successCall)success failure:(failureCall)failure;
@end
