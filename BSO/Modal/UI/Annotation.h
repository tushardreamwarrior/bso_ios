//
//  Annotation.h
//  ArtDynamix
//
//  Created by MAC on 23/07/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface Annotation : NSObject <MKAnnotation> {
    
}

@property(nonatomic, assign)  CLLocationCoordinate2D coordinate;
@property(nonatomic, copy)  NSString *title;
@property(nonatomic, copy)  NSString *subtitle;
- (id)initWith:(CLLocationCoordinate2D)coords andTitle:(NSString *)title;

@end
