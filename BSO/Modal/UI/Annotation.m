//
//  Annotation.m
//  ArtDynamix
//
//  Created by MAC on 23/07/15.
//  Copyright (c) 2015 iMobtree Solutions. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation

- (id)initWith:(CLLocationCoordinate2D)coords andTitle:(NSString *)title {
    _coordinate = coords;
    title = title;
    self = [super init];
    return self;
}

@end
