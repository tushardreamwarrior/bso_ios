//
//  FlixNChillManager.m
//  FLIXnCHILL
//
//  Created by MAC on 17/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import "BSOManager.h"
#import "RestClient.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <MobileCoreServices/MobileCoreServices.h>

@implementation BSOManager

+ (BSOManager *) SharedInstance {
    static id sharedManager = nil;
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
    }
    return sharedManager;
}

- (id)init {

    if(self=[super init]) {
        if(GET_UD(SELECTED_TYPE) == nil) {
            [self getDevicePreferences:^(id responseObject) {
                
            } failure:^(NSError *error) {
                
            }];
        }
        self.selectedType = [GET_UD(SELECTED_TYPE) intValue];
        
    }
    return self;
}

- (UIColor *)getSelectedItemColor {
    if(self.selectedType == jTangleWood) {
        return TANGELWOOD_COLOR;
    }
    else if(self.selectedType == jBSO) {
        return BSO_COLOR;
    } else {
        return BPOP_COLOR;
    }
}

- (UIColor *)getSelectedItemTextColor {
    if(self.selectedType == jTangleWood) {
        return TANGELWOOD_COLOR;
    }
    else if(self.selectedType == jBSO) {
        return BSO_TEXT_COLOR;
    } else {
        return BPOP_TEXT_COLOR;
    }
}

- (UIColor *)getSelectedItemColor:(OrganizationType)type {
    if(type == jTangleWood) {
        return TANGELWOOD_COLOR;
    }
    else if(type == jBSO) {
        return BSO_COLOR;
    } else {
        return BPOP_COLOR;
    }
}


- (UIColor *)getSelectedItemBGColor {
    if(self.selectedType == jTangleWood) {
        return TANGELWOOD_BG_COLOR;
    }
    else if(self.selectedType == jBSO) {
        return BSO_BG_COLOR;
    } else {
        return BPOP_BG_COLOR;
    }
}

- (void)retriveCrediCars {
    if(self.arrCreditCards.count == 0 || self.arrCreditCards == nil) {
        [self getPaymentCards:^(id responseObject) {
            if([responseObject isKindOfClass:[NSMutableArray class]]) {
                self.arrCreditCards = [[NSMutableArray alloc] initWithArray:responseObject];
            }
        } failure:^(NSError *error) {
            
        }];
    }
}

- (void)getDevicePreferences:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getDevicePreferences:^(NSURLSessionDataTask *task, id responseObject) {
       if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
           NSDictionary *dict = [responseObject valueForKey:DATA];

           if([[dict valueForKey:@"first_load_feed_brand_id"] intValue] == 1182) {
               self.selectedType = jBSO;
           } else if([[dict valueForKey:@"first_load_feed_brand_id"] intValue] == 6425) {
               self.selectedType = jBPop;
           } else if([[dict valueForKey:@"first_load_feed_brand_id"] intValue] == 6427) {
               self.selectedType = jTangleWood;
           }
           NSString *strType = [NSString stringWithFormat:@"%u",self.selectedType];
           SET_UD(SELECTED_TYPE, strType);

           success(responseObject);
       }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getShowList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getShowList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableArray *arrShows = [Show parseServerResponse:[responseObject valueForKey:DATA]];
            success(arrShows);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getAllEventsList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getAllEventsList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableArray *arrShows = [Event parseServerResponse:[responseObject valueForKey:DATA]];
            success(arrShows);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)bsoLogin:(NSString *)email password:(NSString *)password success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client bsoLogin:email password:password success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSDictionary *dict = [responseObject valueForKey:DATA];
            SET_UD(EXTERNAL_ID, [dict valueForKey:EXTERNAL_ID]);
            SET_UD(SESSION_ID, [dict valueForKey:SESSION_ID]);
            NSString *interval = [NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]];
            SET_UD(SESSION_TIME, interval);
        }
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getShowDetails:(NSString * )show_id success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getShowDetails:show_id success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            Show *aShow = [Show ParserShowDetailsServerResponse:[responseObject valueForKey:DATA]];
            success(aShow);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getAllMediaList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getAllMediaList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSDictionary *mediaDict = [responseObject valueForKey:DATA];
            Media *aMedia = [Media ParseMediaListResponse:mediaDict];
            NSString *mediaFile = [NSString stringWithFormat:@"media_%d.plist",type];
            [mediaDict writeToFile:FILE_PATH(mediaFile) atomically:YES];
            success(aMedia);
         } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getFBPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getFBPostList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        success([Social parseFacebookPostResponse:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getTwitterPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getTwitterPostList:type success:^(NSURLSessionDataTask *task, id responseObject) {
       success([Social parseTwitterPostResponse:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getGooglePlus:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getGooglePlus:type success:^(NSURLSessionDataTask *task, id responseObject) {
        success([Social parseGooglePlusPostResponse:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getYouTubePostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getYouTubePostList:type success:^(NSURLSessionDataTask *task, id responseObject) {
       success([Social parseYouTubePostResponse:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getInstagramPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getInstagramPostList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        success([Social parseInstagramPostResponse:responseObject]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getHashTagPostList:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getHashTagPostList:type success:^(NSURLSessionDataTask *task, id responseObject) {
        success([Social parseTwitterPostResponse:[responseObject valueForKey:@"statuses"]]);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getAboutUSContent:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getAboutUSContent:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSDictionary *dict = [responseObject valueForKey:DATA];
            NSString *content = [dict valueForKey:@"content"];
            success([Utility decodeFromBase64:content]);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getBeaconContent:(NSString *)beaconID success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getBeaconContent:beaconID success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSDictionary *dict = [responseObject valueForKey:DATA];
            NSString *content = [dict valueForKey:@"p_content"];
            success(content);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}


- (void)getRouteBetweenLocations:(NSString *)url success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getRouteBetweenLocations:url success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getNearByPlaces:(NSString *)url success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getRouteBetweenLocations:url success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getNearByPlaces:(CLLocationCoordinate2D)coordinate radius:(int)radius keyword:(NSString *)keyword success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getNearByPlaces:coordinate radius:radius keyword:keyword success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSMutableArray *arrPlaces = [Place parsePlacesListContent:[responseObject valueForKey:@"results"]];
        success(arrPlaces);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getPlaceDetails:(NSString *)placeID success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getPlaceDetails:placeID success:^(NSURLSessionDataTask *task, id responseObject) {
        Place *aPlace = [Place parsePlaceDetailContent:[responseObject valueForKey:@"result"]];
        success(aPlace);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getBestAvailableSeats:(NSString *)perfrormance success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getBestAvailableSeats:perfrormance success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success([AvailalbeSeatResponse parseAvailableSeat:[responseObject valueForKey:DATA]]);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)chekAvailableSeats:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
     RestClient *client = [RestClient sharedInstance];
    [client chekAvailableSeats:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
          success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)addToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client addToCart:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)checkUserSession:(NSString *)email success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client checkUserSession:email success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
             NSDictionary *dict = [responseObject valueForKey:DATA];
             SET_UD(SESSION_ID, [dict valueForKey:@"asp_session_id"]);
            success(responseObject);
        } else {
            NSDictionary *dict = [responseObject valueForKey:DATA];
            SET_UD(SESSION_ID, [dict valueForKey:@"asp_session_id"]);
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getShoppingCartItems:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getShoppingCartItems:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            SCart *aCart = [SCart parseShoppingCartResponse:[responseObject valueForKey:DATA]];
            success(aCart);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)checkOutShipping:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client checkOutShipping:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            SCart *aCart = [SCart parseShoppingCartShippingResponse:[responseObject valueForKey:DATA]];
            success(aCart);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)checkOutPayment:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client checkOutPayment:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
        }
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)checkoutFinalize:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client checkoutFinalize:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
        }
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];

}

- (void)makeImpulseDonation:(OrganizationType )type amount:(NSString *)amount success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client makeImpulseDonation:type amount:amount success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getNearByLocatios:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
     RestClient *client = [RestClient sharedInstance];
    [client getNearByLocatios:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableDictionary *result = [Place parseNearByLocationWSResponse:[responseObject valueForKey:DATA]];
            success(result);
        }
        else {
            success(nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getSubscriptionListing:(OrganizationType )type success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getSubscriptionListing:type success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableArray *arrResult = [Subscription parseSubscriptionList:[responseObject valueForKey:DATA]];
            success(arrResult);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)applyDiscoutCode:(NSString *)code success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client applyDiscoutCode:code success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
           success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getOrderHistory:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getOrderHistory:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess && ![[responseObject valueForKey:DATA] isKindOfClass:[NSNull class]]) {
            SCart *aCart = [SCart parseOrderHistoryResponse:[responseObject valueForKey:DATA]];
            success(aCart);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)subscriptionBestAvailableCYO:(NSString *)subscription_ID list:(NSMutableArray *)arrList success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client subscriptionBestAvailableCYO:subscription_ID list:arrList success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            SubscriptionSeatCYO *CYO = [SubscriptionSeatCYO parseSubscriptionAvailableSeat:[responseObject valueForKey:DATA]];
            success(CYO);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)SubscriptionCYOAddToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
    
    RestClient *client = [RestClient sharedInstance];
    [client SubscriptionCYOAddToCart:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)subscriptionBestAvailableFixed:(NSString *)subscription_ID success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client subscriptionBestAvailableFixed:subscription_ID success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success([AvailalbeSeatResponse parseAvailableSeat:[responseObject valueForKey:DATA]]);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];

}

- (void)SubscriptionFixedAddToCart:(NSMutableDictionary *)dictionary success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client SubscriptionFixedAddToCart:dictionary success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)registerDeviceToken:(NSString *)deviceToken success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client registerDeviceToken:deviceToken success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success(responseObject);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];

}

- (void)getListOfNotifications:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getListOfNotifications:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableArray *arrNotification = [Notification parseNotificationListRespose:[responseObject valueForKey:DATA]];
            success(arrNotification);
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getNotificationDetail:(NSString *)notification_id success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getNotificationDetail:notification_id success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSMutableArray *arrNotification = [Notification parseNotificationListRespose:[responseObject valueForKey:DATA]];
            if(arrNotification.count) {
                Notification *aNotification = [arrNotification objectAtIndex:0];
                success(aNotification);
            } else {
                 success(responseObject);
            }
           
        } else {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)userLogout:(NSString *)email success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client userLogout:email success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getTicketBarCode:(NSString *)orderId number:(NSString *)subLineItemNumber success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getTicketBarCode:orderId number:subLineItemNumber success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            NSString *string64Bit = [responseObject valueForKey:DATA];
             NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:string64Bit options:0];
            UIImage *imgBarCode = [UIImage imageWithData:decodedData];
            success(imgBarCode);
            
        } else {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)removeItemFromCart:(NSString *)lineNum success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client removeItemFromCart:lineNum success:^(NSURLSessionDataTask *task, id responseObject) {
       success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)validateCardInfo:(CreditCard *)card success:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client validateCardInfo:card success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getPaymentCards:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getPaymentCards:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success([CreditCard parserCardJsonResponse:[responseObject valueForKey:DATA]]);
        } else {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

- (void)getCardInfo:(successCall)success failure:(failureCall)failure {
    RestClient *client = [RestClient sharedInstance];
    [client getCardInfo:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject valueForKey:CODE] integerValue] == jSuccess) {
            success(responseObject);
        } else {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

@end