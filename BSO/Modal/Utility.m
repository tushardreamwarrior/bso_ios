//
//  Utility.m
//  BSO
//
//  Created by Imobtree solutions on 08/12/15.
//  Copyright © 2015 ImobtreeSolutions. All rights reserved.
//

#import "Utility.h"
#import "EGYWebViewController.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <Social/Social.h>
#import "NSData+Encryption.h"

@implementation Utility

static MBProgressHUD *hud = nil;
static  UIDocumentInteractionController *documentController;

#pragma mark Internet Connection
+ (BOOL)connectedToNetwork {
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags){
        printf("Error. Could not recover network reachability flags\n");
        return 0;
    }
    
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    
    BOOL isconnected = (isReachable && !needsConnection) ? YES : NO;
    if (!isconnected) {
        // [ApplicationData ShowAlertWithTitle:@"Alert" Message:@"Please check internet connection in this device!"];
    }
    return isconnected;
}

+ (NSString *)decodeFromBase64:(NSString *)encoded {
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:encoded options:0];
    return [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
}

#pragma mark Alert
+ (void)ShowAlertWithTitle:(NSString *)title Message:(NSString *)message {
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark Validation Methods

+ (BOOL)validateEmail:(NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark Textfield
//Border Color
+ (void)setBorderColorWithView:(UIView *)aView {
    aView.layer.borderWidth = 1.0;
    aView.layer.borderColor = [UIColor blackColor].CGColor;
}

+ (void)setBorderColorWithView:(UIView *)aView color:(UIColor *)color {
    aView.layer.borderWidth = 1.0;
    aView.layer.borderColor = color.CGColor;
}

+ (void)setCornerToView:(UIView *)aView corner:(float)corner {
    aView.layer.cornerRadius = corner;
    aView.layer.masksToBounds = YES;
}

+ (void)setProfileImageCornerWithImageView:(UIImageView *)imageView {
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.layer.masksToBounds = YES;
}

//LeftView
+ (void)setLeftViewWithTextField:(UITextField *)textField andImage:(UIImage *)image {
    UIView *viewBorder = [[UIView alloc]initWithFrame:CGRectMake(0, 0, textField.frame.size.height, textField.frame.size.height)];
    viewBorder.layer.borderWidth = 1.0;
    viewBorder.layer.borderColor = [UIColor whiteColor].CGColor;
    UIImageView *leftImageView = [[UIImageView alloc] initWithImage:image];
    leftImageView.frame = CGRectMake(5, 5, 32, 32);
    leftImageView.contentMode = UIViewContentModeScaleAspectFit;
       leftImageView.frame = CGRectInset(leftImageView.frame, -5, 0);
    [viewBorder addSubview:leftImageView];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = viewBorder;
}

+ (void)setLeftViewWithTextField:(UITextField*)textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 30)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

+ (void)showLoader {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    if(!hud) {
        hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    }
    else {
        [[UIApplication sharedApplication].keyWindow addSubview:hud];
    }
    hud.detailsLabelText = @"";
    hud.mode = MBProgressHUDModeIndeterminate;
}

+ (void)showLoaderWith:(MBProgressHUDMode)mode message:(NSString *)text {
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    if(!hud) {
        hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    }
    else {
        [[UIApplication sharedApplication].keyWindow addSubview:hud];
    }
    hud.progress = 0;
    hud.detailsLabelText = text;
    hud.mode = mode;
}

+ (void)hideLoader {
    if([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    [hud removeFromSuperview];
}

+ (NSDate *)dateFromString:(NSString *)strDate format:(NSString *)fommat {
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:fommat];
    return [dtFormatter dateFromString:strDate];
}

+ (NSString *)stringFromDate:(NSDate *)date format:(NSString *)format {
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:format];
    return [dtFormatter stringFromDate:date];
}

+ (NSString *)converDateFormat:(NSString *)strDate from:(NSString *)from to:(NSString *)to {
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:from];
    NSDate *aDate = [dtFormatter dateFromString:strDate];
    NSDateFormatter *dtFormatter1 = [[NSDateFormatter alloc] init];
    [dtFormatter1 setDateFormat:to];
    return [dtFormatter1 stringFromDate:aDate];
    
}

+ (float)getTextHeightOfText:(NSString *)string font:(UIFont *)aFont width:(float)width {
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentLeft];
    style.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           aFont, NSFontAttributeName,
                           [UIColor blackColor],NSForegroundColorAttributeName,
                           style, NSParagraphStyleAttributeName, nil];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){ width, 10000.0 }
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    return rect.size.height;
}

+ (float)getExactTextHeightOfText:(NSString *)string font:(UIFont *)aFont width:(float)width {
    CGSize  textSize = { width, 10000.0 };
    CGSize size = [string sizeWithFont:aFont
                     constrainedToSize:textSize
                         lineBreakMode:NSLineBreakByWordWrapping];
    return size.height;
}

+ (float)getTextWidthOfText:(NSString *)string font:(UIFont *)aFont height:(float)height {
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentLeft];
    style.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                           aFont, NSFontAttributeName,
                           [UIColor blackColor],NSForegroundColorAttributeName,
                           style, NSParagraphStyleAttributeName, nil];
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attrs];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){ 1000.0, height }
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    return rect.size.width;
}

+ (void)openURLInInterBrowser:(NSString *)strURL from:(UIViewController *)controller {
    NSURL *URL = [NSURL URLWithString:strURL];
#ifdef __IPHONE_8_0
    EGYWebViewController *webViewController = [[EGYWebViewController alloc] initWithURL:URL usingWebkit:NO];
#else
    EGYWebViewController *webViewController = [[EGYWebViewController alloc] initWithURL:URL];
#endif
    [controller.navigationController pushViewController:webViewController animated:YES];
}

+ (void)openContentInInterBrowser:(NSString *)htmlString from:(UIViewController *)controller {
    EGYWebViewController *webViewController = [[EGYWebViewController alloc] initWithContent:htmlString usingWebkit:NO];
    [controller.navigationController pushViewController:webViewController animated:YES];
}

+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width {
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (float)imageHeight: (UIImage*) sourceImage scaledToWidth: (float) i_width {
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    return newHeight;
}

+ (void)shareOnTwiitter:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image {

        SLComposeViewController *fbController =[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // if([SLComposeViewController isAvailableForServiceType:service])
        {
            SLComposeViewControllerCompletionHandler __block completionHandler=
            ^(SLComposeViewControllerResult result){
                
                [fbController dismissViewControllerAnimated:YES completion:nil];
                
                switch(result){
                    case SLComposeViewControllerResultCancelled:
                    default: {
                        
                    }
                        break;
                    case SLComposeViewControllerResultDone: {
                        [Utility ShowAlertWithTitle:@"Success" Message:@"Successfully posted to Twitter"];
                    }
                        break;
                }};
            
            [fbController addImage:image];
            [fbController setInitialText:text];
            //   [fbController addURL:[NSURL URLWithString:SCHOOL_REGISTER_URL]];
            [fbController setCompletionHandler:completionHandler];
            [viewController presentViewController:fbController animated:YES completion:nil];
        }
        //    else {
        //        [ApplicationData ShowAlertWithTitle:@"Alert" Message:@"Please Login From Settings"];
        //    }
    
}

+ (void)shareOnFacebook:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image {
    
    SLComposeViewController *fbController =[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    // if([SLComposeViewController isAvailableForServiceType:service])
    {
        SLComposeViewControllerCompletionHandler __block completionHandler=
        ^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default: {
                    
                }
                    break;
                case SLComposeViewControllerResultDone: {
                    [Utility ShowAlertWithTitle:@"Success" Message:@"Successfully posted to Facebook"];
                }
                    break;
            }};
        
        [fbController addImage:image];
        [fbController setInitialText:text];
        //   [fbController addURL:[NSURL URLWithString:SCHOOL_REGISTER_URL]];
        [fbController setCompletionHandler:completionHandler];
        [viewController presentViewController:fbController animated:YES completion:nil];
    }
    //    else {
    //        [ApplicationData ShowAlertWithTitle:@"Alert" Message:@"Please Login From Settings"];
    //    }
    
}

+ (void)shareOnInstagram:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image frame:(CGRect )frame {

    NSLog(@"Instagram");
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSLog(@"yes Installed");
    }
    else{
        [Utility ShowAlertWithTitle:@"Alert" Message:@"Instagram not installed in this device!\nTo share image please install instagram."];
        return;
    }
    NSData *imgData = UIImageJPEGRepresentation(image, 1.0);
    NSFileManager *fileMangar = [NSFileManager defaultManager];
    NSString *imgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/image.igo"];
    if([fileMangar fileExistsAtPath:imgPath]) {
        [fileMangar removeItemAtPath:imgPath error:nil];
    }
    [imgData writeToFile:imgPath atomically:YES];
    
     NSURL *imgAttchFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", imgPath]];
    
    documentController =  [UIDocumentInteractionController interactionControllerWithURL:imgAttchFile];
    documentController.delegate = viewController;
    documentController.UTI = @"com.instagram.exclusivegram";
    documentController.annotation = [NSDictionary dictionaryWithObject:text forKey:@"InstagramCaption"];
    [documentController presentOpenInMenuFromRect:frame inView:viewController.view animated:YES];
    
}

+ (void)shareOnGoolgePlus:(UIViewController *)viewController text:(NSString *)text image:(UIImage *)image {
    id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
    [GPPShare sharedInstance].delegate = viewController;
    [shareBuilder setPrefillText:text];
    [shareBuilder attachImage:image];
    [shareBuilder open];
}

+ (void)signInGooglePlus:(UIViewController *)controller {
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = NO;
    signIn.shouldFetchGoogleUserID = NO;
    
    // You previously set kClientId in the "Initialize the Google+ client" step
    signIn.clientID = GOOGLE_PLUS_KEY;
    
    // Uncomment one of these two statements for the scope you chose in the previous step
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
    //signIn.scopes = @[ @"profile" ];            // "profile" scope
    
    // Optional: declare signIn.actions, see "app activities"
    signIn.delegate = controller;
    
    [[GPPSignIn sharedInstance] authenticate];
     
}

+ (NSString *) encryptString:(NSString*)plaintext withKey:(NSString*)key {
   
    NSData *data = [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
    return [data base64EncodedStringWithOptions:kNilOptions];
}

+ (NSString *) decryptString:(NSString *)ciphertext withKey:(NSString*)key {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:ciphertext options:kNilOptions];
    return [[NSString alloc] initWithData:[data AES256DecryptWithKey:key] encoding:NSUTF8StringEncoding];
}

@end