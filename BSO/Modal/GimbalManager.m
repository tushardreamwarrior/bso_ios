//
//  GimbalManager.m
//  BSO
//
//  Created by MAC on 28/04/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import "GimbalManager.h"
#import <Gimbal/Gimbal.h>

@interface GimbalManager () <GMBLCommunicationManagerDelegate,GMBLBeaconManagerDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) GMBLCommunicationManager *communicationManager;
@property (nonatomic, assign) BSOManager *bsoManager;
@property (nonatomic, strong) GMBLBeaconManager *beaconManager;
@property (nonatomic, strong) GMBLCommunication*communication;
@property (nonatomic, strong) NSTimer *beaconTimer;
@end

@implementation GimbalManager

+ (GimbalManager *) SharedInstance {
    static id sharedManager = nil;
    if (sharedManager == nil) {
        [Gimbal setAPIKey:GIMBLE_KEY options:nil];
        sharedManager = [[self alloc] init];
    }
    return sharedManager;
}

- (void)initializeCommunication {
    self.bsoManager = [BSOManager SharedInstance];
    self.communicationManager = [GMBLCommunicationManager new];
    self.communicationManager.delegate = self;
    [GMBLCommunicationManager startReceivingCommunications];
    self.beaconManager = [GMBLBeaconManager new];
}

- (void)startBeaconMonitoring {
    self.beaconManager.delegate = self;
    [self.beaconManager startListening];
    if(self.beaconTimer) {
        [self.beaconTimer invalidate];
        self.beaconTimer = nil;
    }
    [Utility showLoader];
    self.beaconTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(beaconFindTimer) userInfo:nil repeats:NO];
}
                                                                                                    
- (void)beaconFindTimer {
    [Utility hideLoader];
    [self stopBeaconMonitoring];
    [Utility ShowAlertWithTitle:@"Alert" Message:@"No Beacon found near to you"];
}

- (void)stopBeaconMonitoring {
    self.beaconManager.delegate = nil;
    [self.beaconManager stopListening];
    if(self.beaconTimer) {
        [self.beaconTimer invalidate];
        self.beaconTimer = nil;
    }
}

- (void)beaconManager:(GMBLBeaconManager *)manager didReceiveBeaconSighting:(GMBLBeaconSighting *)sighting {
    if(sighting.RSSI >= -90) {
        [self stopBeaconMonitoring];
        [self showBeaconContent:sighting.beacon.identifier];
    } else {
        [Utility ShowAlertWithTitle:@"Alert" Message:@"No Beacon found near to you"];
        [self stopBeaconMonitoring];
    }
}

- (void)didRecieveCommunicationData:(GMBLCommunication *)communication {
    self.communication = communication;
    if([[UIApplication sharedApplication] applicationState ] == UIApplicationStateActive) {
        UIAlertView *alrView = [[UIAlertView alloc] initWithTitle:communication.title message:@"Do you want check details?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No",nil];
        [alrView show];
    } else {
        [self showBeaconContent:self.communication.URL];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        [self showBeaconContent:self.communication.URL];
    }
}

- (void)showBeaconContent:(NSString *)beaconID {
    [Utility showLoader];
    [self.bsoManager getBeaconContent:self.communication.URL success:^(id responseObject) {
        if([responseObject isKindOfClass:[NSString class]]) {
            UINavigationController *navController = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
            [Utility openContentInInterBrowser:responseObject from:navController.visibleViewController];
        }
        [Utility hideLoader];
    } failure:^(NSError *error) {
        [Utility hideLoader];
    }];
}

- (NSArray *)communicationManager:(GMBLCommunicationManager *)manager presentLocalNotificationsForCommunications:(NSArray *)communications forVisit:(GMBLVisit *)visit {
    // This will be invoked when a user has a communication for the place that was entered or exited.
    // Return an array of communications you would like presented as local notifications.
    
    return communications;
}

- (UILocalNotification *)communicationManager:(GMBLCommunicationManager *)manager prepareNotificationForDisplay: (UILocalNotification *)notification forCommunication:(GMBLCommunication *)communication {
    // This method will be invoked when a communicate is received for a place event and after the communications
    // have been returned from communicationManager:presentLocalNotificationsForCommunications:forVisit:
    // if implimented. The caller is given a chance to customize this notification.
    // Caller should return a customized UILocalNotification for the SDK to use for the notification.
    // SDK will use the default notification if null is returned from this method.
    
    return notification;
}

@end
