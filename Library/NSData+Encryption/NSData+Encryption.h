//
//  NSData+Encryption.h
//  BSO
//
//  Created by MAC on 29/07/16.
//  Copyright © 2016 iMobtree Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Encryption)
- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
@end