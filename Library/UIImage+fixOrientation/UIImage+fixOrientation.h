//
//  UIImage+fixOrientation.h
//  momentz
//
//  Created by MAC on 02/09/15.
//  Copyright (c) 2015 Kanav Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end