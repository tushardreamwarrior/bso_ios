//
//  MAB_GoogleUserCredentials.h
//  YTAPI3Demo
//
//  Created by Muhammad Bassio on 4/30/14.
//  Copyright (c) 2014 Muhammad Bassio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAB_GoogleAccessToken.h"
#import "MABYT3_Channel.h"
#import "MABYT3_Subscription.h"


#define IsPad      (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)

#define UIColorFromRGBA(rgbValue, alphaValue) [UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0 green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0 blue:((CGFloat)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma message "You have to register your app at https://console.developers.google.com/ to get ClientID, Client secret & apiKey"

static NSString *kMyClientID = @"901953066766-at4lbjlqecfp9egtj7bbfb60227n48qd.apps.googleusercontent.com";
static NSString *kMyClientSecret = @"JQsT-aDWRu8qvA9sJMlYYXwL";
static NSString *apiKey = @"AIzaSyBB90RO1ZgdHpruCf-iNgaUVPHsq2NKIig";

//static NSString *kMyClientID = @"260538518527-j10tlv26u3d6mppr86r3ppje173qafv3.apps.googleusercontent.com";
//static NSString *kMyClientSecret = @"fba0ngV1JSf9iILa15lMym36";
//static NSString *apiKey = @"AIzaSyAqU9oqjr0Cgdsh3kcbHy5X_G9OZeYifIc";

static NSString *scope = @"https://www.googleapis.com/auth/youtube  https://www.googleapis.com/auth/youtubepartner https://www.googleapis.com/auth/youtubepartner-channel-audit https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube.force-ssl";



@protocol MAB_GoogleUserCredentialsDelegate <NSObject>

- (void) CredUserNoChannel;
- (void) CredUserUpdated;
- (void) CredUserPrepare;
- (void) CredUserSubsComplete;

@optional
- (void) ChooseChannel:(NSMutableArray *)channels;

@end


@interface MAB_GoogleUserCredentials : NSObject
{
    BOOL userFetched;
    NSString *nxtSubURL;
}

@property (nonatomic) BOOL signedin;
@property (strong, nonatomic) MAB_GoogleAccessToken *token;
@property (strong, nonatomic) MABYT3_Channel *user;
@property (strong, nonatomic) NSMutableArray *ytSubs;
@property (weak, nonatomic) id<MAB_GoogleUserCredentialsDelegate> delegate;


+ (MAB_GoogleUserCredentials *) sharedInstance;
- (void) saveToken;
- (void) initUser;
- (void) SignOut;

@end
