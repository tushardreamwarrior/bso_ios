//
//  NSString+Util.h
//  FeedIt
//
//  Created by MAC on 30/04/15.
//  Copyright (c) 2015 DreamWarrior. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UnicodeUtilities)

- (NSString*) unescapeUnicodeString;
- (NSString*) escapeUnicodeString;

@end