//
//  SelectionView.m
//  PatientRounds
//
//  Created by MAC on 03/10/15.
//  Copyright © 2015 Daniel Moran. All rights reserved.
//

#import "SelectionView.h"

@implementation SelectionView

- (id)initWithSelectionType:(SelectionType)type {
    self.selectionType = type;
    self = [self initWithItemArray:nil];
    return self;
}

- (id)initWithItemArray:(NSArray *)_arrItem {
    return [self initWithItemArray:_arrItem size:CGSizeMake(320, 256)];
}

- (id)initWithItemArray:(NSArray *)_arrItem size:(CGSize )size {
    self.size = size;
    self = [super initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
    if (self) {
        
        self.arrItems = [[NSMutableArray alloc] initWithArray:_arrItem];
        
        UIViewController *VC = [[UIViewController alloc] init];
        [VC setPreferredContentSize:CGSizeMake(size.width, size.height)];
        [VC.view addSubview:self];
        [self setBackgroundColor:[UIColor whiteColor]];
        [VC.view setFrame:CGRectMake(0, 0, size.width, size.height)];
        
        UIToolbar *toolBar=[[UIToolbar alloc] init];
        toolBar.frame=CGRectMake(0, 0, size.width, 44);
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            toolBar.tintColor = RGB(0, 122, 255);
            
        }
        
        toolBar.barTintColor = [UIColor whiteColor];
        toolBar.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        
        UIBarButtonItem *btnDone=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donebuttonPressed)];
        btnDone.tintColor = [UIColor blueColor];
        
        UIBarButtonItem *btnCancel=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
        btnCancel.tintColor = [UIColor blueColor];
        self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 22)] ;
        self.lblTitle.backgroundColor = [UIColor clearColor];
//        self.lblTitle.textColor = RGB(0, 122, 255);
        self.lblTitle.textColor = [UIColor darkGrayColor];
        self.lblTitle.textAlignment = NSTextAlignmentCenter;
        self.lblTitle.font = [UIFont systemFontOfSize:19.0];
        UIBarButtonItem *btnTitle = [[UIBarButtonItem alloc ] initWithCustomView:self.lblTitle];
        
        UIBarButtonItem *flexibal=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        
        [toolBar setItems:[NSArray arrayWithObjects:btnCancel,flexibal,btnTitle,flexibal,btnDone, nil]];
        
        [self addSubview:toolBar];
        switch (self.selectionType) {
            case selectionTypeTimePicker:
                datePicker = [[UIDatePicker alloc] init];
                [datePicker setFrame:CGRectMake(0, 40, size.width, size.height-40)];
                [datePicker setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
                [datePicker setDatePickerMode:UIDatePickerModeTime];
                [self addSubview:datePicker];
                self.selecteDate=[NSDate date];
                break;
            case selectionTypeDatePicker:
                datePicker = [[UIDatePicker alloc] init];
                [datePicker setFrame:CGRectMake(0, 40, size.width, size.height-40)];
                [datePicker setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
                [datePicker setDatePickerMode:UIDatePickerModeDate];
                self.selecteDate = [NSDate date];
                [self addSubview:datePicker];
                break;
            case selectionTypeDateTimePicker:
                datePicker = [[UIDatePicker alloc] init];
                [datePicker setFrame:CGRectMake(0, 40, size.width, size.height-40)];
                [datePicker setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
                [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
                self.selecteDate = [NSDate date];
                [self addSubview:datePicker];
                break;
            default:
                itemPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, size.width, size.height-40)];
                [itemPicker setDataSource:self];
                [itemPicker setDelegate:self];
                [itemPicker setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
                [itemPicker setShowsSelectionIndicator:TRUE];
                [self addSubview:itemPicker];
                break;
        }
        
        pickerPopover = [[UIPopoverController alloc]initWithContentViewController:VC];
        
    }
    return self;
}

- (void)reloadSelectionViewWithArray:(NSArray *)_arrItem {
    [self.arrItems removeAllObjects];
    [self.arrItems addObjectsFromArray:_arrItem];
    [itemPicker reloadAllComponents];
}

- (void)showInView:(UIView *)sender {
    senderView = sender;
    [pickerPopover presentPopoverFromRect:sender.frame inView:sender.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
    
    if((self.selectionType == selectionTypeDatePicker || self.selectionType == selectionTypeTimePicker || self.selectionType == selectionTypeDateTimePicker) && self.selecteDate != nil) {
        [datePicker setDate:self.selecteDate];
    }
    if((self.selectionType == selectionTypeDatePicker || self.selectionType == selectionTypeTimePicker || self.selectionType == selectionTypeDateTimePicker) && self.minDate != nil) {
        [datePicker setMinimumDate:self.minDate];
    }
}

- (void)donebuttonPressed {
    switch (self.selectionType) {
        case selectionTypeDatePicker:
        case selectionTypeTimePicker:
        case selectionTypeDateTimePicker:
            if ([self.delegate respondsToSelector:@selector(SelectionView:didSelectDate:SenderView:)]) {
                [self.delegate SelectionView:self didSelectDate:datePicker.date SenderView:senderView];
            }
            
            break;
        default:
            self.selectedIndex = [itemPicker selectedRowInComponent:0];
           if ([self.delegate respondsToSelector:@selector(SelectionView:didSelectItem:withIndex:SenderView:)]) {
               if(self.arrItems.count)
                   [self.delegate SelectionView:self didSelectItem:[self.arrItems objectAtIndex:self.selectedIndex] withIndex:self.selectedIndex SenderView:senderView];
            }
            break;
    }
    
    [self cancelButtonPressed];
}

- (void)cancelButtonPressed {
    if ([self.delegate respondsToSelector:@selector(SelectionViewDidDismiss:)]) {
        [self.delegate SelectionViewDidDismiss:self];
    }
    [pickerPopover dismissPopoverAnimated:TRUE];
}

#pragma mark PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.arrItems count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.size.width-20, 35)];
    label.text = [self.arrItems objectAtIndex:row];
    label.font=[UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}

- (void)setSelectionTitle:(NSString *)selectionTitle {
    _lblTitle.text = selectionTitle;
    _selectionTitle = selectionTitle;
}

#pragma mark draw rect
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (self.selectedIndex<self.arrItems.count) {
        [itemPicker selectRow:self.selectedIndex inComponent:0 animated:FALSE];
    }
}

@end