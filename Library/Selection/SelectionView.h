//
//  SelectionView.h
//  PatientRounds
//
//  Created by MAC on 03/10/15.
//  Copyright © 2015 Daniel Moran. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SelectionDelegate;
typedef enum SelectionType {
    selectionTypeDatePicker = 1,
    selectionTypeTimePicker = 2,
    selectionTypeDateTimePicker = 3
}SelectionType;


@interface SelectionView : UIView <UIPickerViewDelegate,UIPickerViewDataSource> {
    UIPopoverController* pickerPopover;
    UIPickerView *itemPicker;
    UIDatePicker *datePicker;
    UIView *senderView;
}
@property (nonatomic, assign) CGSize size;
@property (nonatomic, retain) id <SelectionDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *arrItems;
@property (nonatomic, assign) NSInteger selectedIndex;
@property(nonatomic)SelectionType selectionType;
@property(nonatomic,retain) NSDate *selecteDate;
@property(nonatomic,retain) NSDate *minDate;
@property (nonatomic, retain) NSString *selectionTitle;
@property (nonatomic, retain) UILabel *lblTitle;

- (void)showInView:(UIView *)sender;
- (id)initWithSelectionType:(SelectionType)type;
- (id)initWithItemArray:(NSArray *)_arrItem;
- (id)initWithItemArray:(NSArray *)_arrItem size:(CGSize )size;

- (void)reloadSelectionViewWithArray:(NSArray *)_arrItem;
@end

@protocol SelectionDelegate <NSObject>
@optional
- (void)SelectionView:(SelectionView *)selectionView didSelectItem:(NSString *)item withIndex:(NSInteger)index SenderView:(UIView*)senderView;

- (void)SelectionView:(SelectionView *)selectionView didSelectDate:(NSDate*)date SenderView:(UIView*)senderView;
- (void)SelectionViewDidDismiss:(SelectionView *)selectionVie;

@end